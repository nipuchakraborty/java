-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2017 at 09:16 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `resturantmanagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `addfooddinner`
--

CREATE TABLE `addfooddinner` (
  `id` int(11) NOT NULL,
  `Food Name` varchar(222) DEFAULT NULL,
  `Price` int(222) DEFAULT NULL,
  `Category` varchar(222) DEFAULT NULL,
  `Status` varchar(222) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `addfoodeveningsnacks`
--

CREATE TABLE `addfoodeveningsnacks` (
  `id` int(11) NOT NULL,
  `foodname` varchar(222) DEFAULT NULL,
  `Price` int(222) DEFAULT NULL,
  `Category` varchar(222) DEFAULT NULL,
  `Status` varchar(222) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `addfoodlunch`
--

CREATE TABLE `addfoodlunch` (
  `id` int(11) NOT NULL,
  `FoodName` varchar(222) DEFAULT NULL,
  `Price` int(222) DEFAULT NULL,
  `Category` varchar(222) DEFAULT NULL,
  `Status` varchar(222) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addfoodlunch`
--

INSERT INTO `addfoodlunch` (`id`, `FoodName`, `Price`, `Category`, `Status`) VALUES
(1, 'gray', 500, 'G', 'a'),
(2, 'lunch', 44, 'Lunch', NULL),
(3, 'Black Tea', 66, 'Lunch', 'Available'),
(4, 'Black Tea', 66, 'Lunch', 'Available'),
(5, 'Black Tea', 66, 'Lunch', 'Available');

-- --------------------------------------------------------

--
-- Table structure for table `add_food_breakfast`
--

CREATE TABLE `add_food_breakfast` (
  `Id` int(11) NOT NULL,
  `Name` varchar(22) DEFAULT NULL,
  `Price` varchar(22) DEFAULT NULL,
  `Category` varchar(22) DEFAULT NULL,
  `Status` varchar(22) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `add_food_breakfast`
--

INSERT INTO `add_food_breakfast` (`Id`, `Name`, `Price`, `Category`, `Status`) VALUES
(1, 'Tea', '10', 'Brakfast', NULL),
(2, 'Tea', '10', 'Brakfast', NULL),
(3, 'Tea', '10', 'Brakfast', NULL),
(4, 'Tea', '10', 'Brakfast', NULL),
(5, 'Tea', '10', 'Brakfast', NULL),
(6, 'Tea', '10', 'Brakfast', NULL),
(7, 'Tea', '10', 'Brakfast', NULL),
(8, 'Eagg', '15', 'Brakfast', 'Available'),
(9, 'Omlet', '15', 'Brakfast', 'Available'),
(10, 'Black Tea', '44', 'Brakfast', 'Available'),
(11, 'Black Tea', '44', 'Brakfast', 'Available'),
(12, 'Black Tea', '44', 'Brakfast', 'Available'),
(13, 'Black Tea', '44', 'Brakfast', 'Available'),
(14, 'Black Tea', '44', 'Brakfast', 'Available'),
(15, 'Green Tea', '4', 'Brakfast', 'Available'),
(16, 'Green Tea', '55', 'Brakfast', 'Available'),
(17, 'Black Tea', '44', 'Brakfast', 'Available'),
(18, 'Black Tea', '4', 'Brakfast', 'Available'),
(19, 'Black Tea', '66', 'Brakfast', 'Select One'),
(20, 'Green Tea', '7', 'Brakfast', 'Available'),
(21, 'Black Tea', '3', 'Brakfast', 'Available');

-- --------------------------------------------------------

--
-- Stand-in structure for view `breakfast_view`
-- (See below for the actual view)
--
CREATE TABLE `breakfast_view` (
`Id` int(11)
,`Name` varchar(22)
,`Price` varchar(22)
,`Category` varchar(22)
,`Status` varchar(22)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `dinner_view`
-- (See below for the actual view)
--
CREATE TABLE `dinner_view` (
`id` int(11)
,`Food Name` varchar(222)
,`Price` int(222)
,`Category` varchar(222)
,`Status` varchar(222)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `evening`
-- (See below for the actual view)
--
CREATE TABLE `evening` (
`id` int(11)
,`foodname` varchar(222)
,`Price` int(222)
,`Category` varchar(222)
,`Status` varchar(222)
);

-- --------------------------------------------------------

--
-- Table structure for table `loing`
--

CREATE TABLE `loing` (
  `id` int(11) NOT NULL,
  `fullname` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `confirmpassword` varchar(200) DEFAULT NULL,
  `picture` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `lunch`
-- (See below for the actual view)
--
CREATE TABLE `lunch` (
`id` int(11)
,`FoodName` varchar(222)
,`Price` int(222)
,`Category` varchar(222)
,`Status` varchar(222)
);

-- --------------------------------------------------------

--
-- Table structure for table `new_account`
--

CREATE TABLE `new_account` (
  `id` int(11) NOT NULL,
  `fullname` varchar(25) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `confirmPassword` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `new_account`
--

INSERT INTO `new_account` (`id`, `fullname`, `userName`, `password`, `confirmPassword`, `position`, `picture`) VALUES
(1, 'sdss', 'sdss', '2', '2', 'Manager', 'C:\\Users\\Nk chakraborty\\OneDrive\\IMG_20160817_211440.jpg'),
(2, '', '', '', '', 'Select One', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `presentlist`
--

CREATE TABLE `presentlist` (
  `id` int(11) NOT NULL,
  `Name` varchar(222) NOT NULL,
  `Gender` varchar(222) NOT NULL,
  `PresentList` varchar(222) NOT NULL,
  `Post` varchar(44) NOT NULL,
  `Date` varchar(222) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `presentlist`
--

INSERT INTO `presentlist` (`id`, `Name`, `Gender`, `PresentList`, `Post`, `Date`) VALUES
(2, 'Amin', 'Male', 'Present', 'Mon Aug 14 00:00:00 BDT 2017', 'Manager'),
(3, 'Panna', 'Male', 'Present', 'Manager', 'Mon Aug 14 00:00:00 BDT 2017'),
(4, 'Raju', 'Male', 'Present', 'Select', 'Mon Aug 07 00:00:00 BDT 2017'),
(5, 'Rakib', 'Male', 'Present', 'Manager', 'Mon Aug 14 00:00:00 BDT 2017');

-- --------------------------------------------------------

--
-- Table structure for table `pricetab`
--

CREATE TABLE `pricetab` (
  `id` int(225) NOT NULL,
  `Food Name` varchar(255) DEFAULT NULL,
  `Price` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pricetab`
--

INSERT INTO `pricetab` (`id`, `Food Name`, `Price`) VALUES
(1, 'Balack_Tea', '100'),
(2, 'GreenTea', '100'),
(3, 'Lemon Tea', '100'),
(4, 'Lemon Tea', '30'),
(5, 'Milk Tea', '50'),
(6, 'Black Coffee', '20'),
(7, 'Cold Coffee', '35'),
(8, 'Sanwich', '35'),
(9, 'Omlet', '45'),
(10, 'Pizza', '80'),
(11, 'Cheaken Pizza', '50'),
(12, 'Cheaken Roll', '70'),
(13, 'Cheaken Bargar', '90'),
(14, 'Bargar', '95'),
(15, 'Bargar', '78');

-- --------------------------------------------------------

--
-- Table structure for table `takeorder`
--

CREATE TABLE `takeorder` (
  `id` int(11) NOT NULL,
  `customerName` varchar(255) DEFAULT NULL,
  `FoodName` varchar(255) DEFAULT NULL,
  `Foodprice` int(255) DEFAULT NULL,
  `Category` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_order`
--

CREATE TABLE `temp_order` (
  `Id` int(11) NOT NULL,
  `TableNO` varchar(222) DEFAULT NULL,
  `Customer_Name` varchar(222) DEFAULT NULL,
  `Category` varchar(225) DEFAULT NULL,
  `FoodName` varchar(225) DEFAULT NULL,
  `Quntity` int(222) DEFAULT NULL,
  `Total` int(222) DEFAULT NULL,
  `Discount` int(222) DEFAULT NULL,
  `date` varchar(222) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `breakfast_view`
--
DROP TABLE IF EXISTS `breakfast_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `breakfast_view`  AS  select `add_food_breakfast`.`Id` AS `Id`,`add_food_breakfast`.`Name` AS `Name`,`add_food_breakfast`.`Price` AS `Price`,`add_food_breakfast`.`Category` AS `Category`,`add_food_breakfast`.`Status` AS `Status` from `add_food_breakfast` ;

-- --------------------------------------------------------

--
-- Structure for view `dinner_view`
--
DROP TABLE IF EXISTS `dinner_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dinner_view`  AS  select `addfooddinner`.`id` AS `id`,`addfooddinner`.`Food Name` AS `Food Name`,`addfooddinner`.`Price` AS `Price`,`addfooddinner`.`Category` AS `Category`,`addfooddinner`.`Status` AS `Status` from `addfooddinner` ;

-- --------------------------------------------------------

--
-- Structure for view `evening`
--
DROP TABLE IF EXISTS `evening`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `evening`  AS  select `addfoodeveningsnacks`.`id` AS `id`,`addfoodeveningsnacks`.`foodname` AS `foodname`,`addfoodeveningsnacks`.`Price` AS `Price`,`addfoodeveningsnacks`.`Category` AS `Category`,`addfoodeveningsnacks`.`Status` AS `Status` from `addfoodeveningsnacks` ;

-- --------------------------------------------------------

--
-- Structure for view `lunch`
--
DROP TABLE IF EXISTS `lunch`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lunch`  AS  select `addfoodlunch`.`id` AS `id`,`addfoodlunch`.`FoodName` AS `FoodName`,`addfoodlunch`.`Price` AS `Price`,`addfoodlunch`.`Category` AS `Category`,`addfoodlunch`.`Status` AS `Status` from `addfoodlunch` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addfooddinner`
--
ALTER TABLE `addfooddinner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addfoodeveningsnacks`
--
ALTER TABLE `addfoodeveningsnacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addfoodlunch`
--
ALTER TABLE `addfoodlunch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `add_food_breakfast`
--
ALTER TABLE `add_food_breakfast`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `loing`
--
ALTER TABLE `loing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `new_account`
--
ALTER TABLE `new_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `presentlist`
--
ALTER TABLE `presentlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pricetab`
--
ALTER TABLE `pricetab`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `takeorder`
--
ALTER TABLE `takeorder`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `temp_order`
--
ALTER TABLE `temp_order`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addfooddinner`
--
ALTER TABLE `addfooddinner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `addfoodeveningsnacks`
--
ALTER TABLE `addfoodeveningsnacks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `addfoodlunch`
--
ALTER TABLE `addfoodlunch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `add_food_breakfast`
--
ALTER TABLE `add_food_breakfast`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `loing`
--
ALTER TABLE `loing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `new_account`
--
ALTER TABLE `new_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `presentlist`
--
ALTER TABLE `presentlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pricetab`
--
ALTER TABLE `pricetab`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `takeorder`
--
ALTER TABLE `takeorder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temp_order`
--
ALTER TABLE `temp_order`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
