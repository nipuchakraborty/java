package Jfile;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;



public class Jf {

	
	public Jf(){
		
		JFileChooser jFileChooser = new JFileChooser();
		jFileChooser.setCurrentDirectory(new File("/User/alvinreyes"));
		
		int result = jFileChooser.showOpenDialog(new JFrame());
	
	
		if (result == JFileChooser.APPROVE_OPTION) {
		    File selectedFile = jFileChooser.getSelectedFile();
		 Output output=new Output();
		 output.setVisible(true);
		 ImageIcon icon =new ImageIcon(selectedFile.getAbsolutePath());
		
	     output.imagebox.setIcon(icon);
		    
		    System.out.println("Selected file: " + selectedFile.getAbsolutePath());
		}
	}
	
	
	public static void main(String[] args) {
		new Jf();
	}
}
