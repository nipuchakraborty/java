package viwe;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.border.LineBorder;
import org.eclipse.wb.swing.FocusTraversalOnArray;

import model.Mange;
import model.Semester;

import java.awt.Component;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Toolkit;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class Book_app_design extends JFrame {

	private JPanel contentPane;
	private JTextField book_name_txt;
	private JTextField writter_name_txt;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Book_app_design frame = new Book_app_design();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Book_app_design() {
		setBackground(Color.CYAN);
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\NK CHAKRABORTY\\Downloads\\html document\\A Java program to open, read, and display an image file _ alvinalexander.com_files\\scala-cookbook-oreilly-alvin-alexander.gif"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 530, 524);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GREEN);
		contentPane.setForeground(Color.YELLOW);
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0), 6, true));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Book Information");
		lblNewLabel.setFont(new Font("Cooper Black", lblNewLabel.getFont().getStyle(), 18));
		lblNewLabel.setBounds(148, 34, 191, 22);
		contentPane.add(lblNewLabel);
		
		JLabel lblBookName = new JLabel("Book Name:");
		lblBookName.setFont(new Font("Yu Gothic UI Semilight", Font.BOLD | Font.ITALIC, 13));
		lblBookName.setBounds(27, 144, 88, 22);
		contentPane.add(lblBookName);
		
		book_name_txt = new JTextField();
		book_name_txt.setBounds(148, 144, 191, 22);
		contentPane.add(book_name_txt);
		book_name_txt.setColumns(10);
		
		JLabel lblWritterName = new JLabel("Writter Name");
		lblWritterName.setFont(new Font("Yu Gothic UI Semilight", Font.BOLD | Font.ITALIC, 13));
		lblWritterName.setBounds(27, 216, 99, 22);
		contentPane.add(lblWritterName);
		
		writter_name_txt = new JTextField();
		writter_name_txt.setColumns(10);
		writter_name_txt.setBounds(148, 216, 191, 22);
		contentPane.add(writter_name_txt);
		
		JLabel lblSemester = new JLabel("Semester");
		lblSemester.setFont(new Font("Yu Gothic UI Semilight", Font.BOLD | Font.ITALIC, 13));
		lblSemester.setBounds(27, 301, 99, 22);
		contentPane.add(lblSemester);
		
		JComboBox semester_combo = new JComboBox();
		semester_combo.setToolTipText("");
		semester_combo.setBounds(148, 301, 191, 22);
		contentPane.add(semester_combo);
		model.Semester semester=new Semester();
		ArrayList<String> semTake=semester.getRoles();
		semester_combo.setModel(new DefaultComboBoxModel<>(semTake.toArray()));
		
		JButton save_btn = new JButton("Save");
		save_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String Book_name=book_name_txt.getText();
				String Writter=writter_name_txt.getText();
				String Semester= semester_combo.getModel().toString();
				Mange mange=new Mange();
				mange.Book_info(Book_name, Writter, Semester);
			}
		});
		save_btn.setBounds(353, 402, 97, 25);
		contentPane.add(save_btn);
		contentPane.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{lblNewLabel}));
	}
}
