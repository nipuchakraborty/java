package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.ImageIcon;
import javax.swing.border.MatteBorder;

public class Settings extends JInternalFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel label;
	private JDesktopPane desktopPane;


	//variable declaration;
	int w = 750;
	int h = 400;
	private JInternalFrame jInternalFrame;
	private JDesktopPane desktopen;
	JPanel header, center, footer;
	Border border = new EtchedBorder();
	Font font = new Font("courier new", Font.PLAIN, 16);
	

	public Settings() {
		setTitle("About");
		setBackground(Color.WHITE);
		setRootPaneCheckingEnabled(false);
		setFrameIcon(new ImageIcon(Settings.class.getResource("/pictureResource/images/Help.gif")));
		getContentPane().setBackground(new Color(128, 0, 0));
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 455, 779, -442);
		getContentPane().add(scrollPane);
		
		JTextPane txtpnEclipseIdeFor = new JTextPane();
		txtpnEclipseIdeFor.setText("\r\n\r\nResturant Management Application Power By Java Language\r\n\r\nVersion: 1.1.0.0\r\nBuild id: 27263486\r\nYou Can Manage Everything of your Resturant By This Application . \r\n2017 -2018 Alright Reserved(c) CopyRight 2017\r\nDeveloped by \r\nNk chakraborty\r\n0182-7263486\r\nnipuchakraborty86@hotmail.com");
		txtpnEclipseIdeFor.setBounds(10, 158, 779, 295);
		
		getContentPane().add(txtpnEclipseIdeFor);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Settings.class.getResource("/pictureResource/nk.jpg")));
		lblNewLabel.setBounds(10, 5, 113, 142);
		getContentPane().add(lblNewLabel);
		
		JTextPane txtpnResturantManagementSoftware = new JTextPane();
		txtpnResturantManagementSoftware.setBorder(new MatteBorder(3, 3, 3, 3, (Color) new Color(192, 192, 192)));
		txtpnResturantManagementSoftware.setBackground(new Color(0, 0, 139));
		txtpnResturantManagementSoftware.setForeground(new Color(255, 20, 147));
		txtpnResturantManagementSoftware.setFont(new Font("Dodger", Font.BOLD | Font.ITALIC, 20));
		txtpnResturantManagementSoftware.setText("RESTURANT MANAGEMENT SOFTWARE\r\nBY Nk Chakraborty");
		txtpnEclipseIdeFor.setEditable(false);
		txtpnResturantManagementSoftware.setEditable(false);
		//txtpnEclipseIdeFor.setEnabled(false);
		txtpnResturantManagementSoftware.setBounds(124, 82, 665, 65);
		getContentPane().add(txtpnResturantManagementSoftware);
		
		JPanel panel = new JPanel();
		panel.setBounds(-40, -50, 929, 575);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Learn For Earn Knowledge");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Yu Gothic Light", Font.BOLD, 30));
		lblNewLabel_1.setForeground(new Color(51, 0, 153));
		lblNewLabel_1.setBounds(180, 66, 550, 49);
		panel.add(lblNewLabel_1);
		setLocation(new Point(350, 100));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		//Dimention Screen
		Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();
		//header Design
		header=new JPanel();
		header.setBorder(new EtchedBorder(EtchedBorder.LOWERED,null,null));
		header.setBackground(Color.BLUE);
		header.setPreferredSize(new Dimension(screen.width, 30));
		//center Design
		center=new JPanel();
		center.setPreferredSize(new Dimension(screen.width+50,50));
		//foter design
		footer=new JPanel();
		//footer.setBackground(Color.blue);
		footer.setBackground(new Color(155, 200, 200));
		footer.setPreferredSize(new Dimension(screen.width, 50));
		//setUndecorated(true);
	 //getRootPane().setWindowDecorationStyle(JRootPane.COLOR_CHOOSER_DIALOG);
		//setLocation((screen.width - w) / 2, (screen.height - h) / 2);
		setSize(815, 494);
		show();
	}
	public  static void  Main(String []args){
		try {
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		Settings addFood=new Settings();
		addFood.setVisible(true);
		addFood.setDefaultCloseOperation(HIDE_ON_CLOSE);
	}
}
