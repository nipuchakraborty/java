package viwe.panel;
import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.*;

public class Contact extends JInternalFrame{
    private JTextArea textarea = new JTextArea ();
    private JTextField field = new JTextField ();
    private JButton buton = new JButton ("Sent");

    public Contact() {
        setLayout(new BorderLayout());

        JPanel panel1 = new JPanel (new BorderLayout(2,2));
        JPanel panel2 = new JPanel (new BorderLayout(2,2));

        JLabel label1 = new JLabel ("Sent Report");
        JLabel label2 = new JLabel ("Number");

        panel1.add(label1, BorderLayout.NORTH);
        panel1.add(new JScrollPane(textarea), BorderLayout.CENTER);

        panel2.add(label2, BorderLayout.WEST);
        panel2.add(field, BorderLayout.CENTER);
        panel2.add(buton, BorderLayout.EAST);

        add(panel1, BorderLayout.CENTER);
        add(panel2, BorderLayout.SOUTH);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(350, 300);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {   
            public void run() {   
                JFrame frame = new JFrame("Test");

                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setLocationByPlatform(true);

                Contact panel = new Contact();

                frame.getContentPane().add(panel);
                frame.pack();
                frame.setVisible(true);
            }
        });
    }
}