package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyVetoException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;

import org.jdesktop.swingx.JXDatePicker;
import javax.swing.JComboBox;
import net.miginfocom.swing.MigLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.Button;
import java.awt.Point;
import javax.swing.JPopupMenu;
import java.awt.Component;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;
import javax.swing.BoxLayout;

public class DailyReports extends JInternalFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel label;
	private JDesktopPane desktopPane;

	//main mehtod 

	//variable declaration;
	int w = 750;
	int h = 500;
	
	private JDesktopPane desktopen;
	JPanel header, center, footer;
	Border border = new EtchedBorder();
	Font font = new Font("courier new", Font.PLAIN, 16);
	private JTable table;
	

	public DailyReports() {
		setLocation(new Point(350, 100));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		setDoubleBuffered(true);
		setMaximizable(true);
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		setTitle("DailyReports");
		
	
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setBackground(new Color(128, 0, 0));
		//Dimention Screen
		Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();
		//header Design
		header=new JPanel();
		header.setBorder(new EtchedBorder(EtchedBorder.LOWERED,null,null));
		header.setBackground(Color.BLUE);
		header.setPreferredSize(new Dimension(screen.width, 50));
		//center Design
		center=new JPanel();
		center.setPreferredSize(new Dimension(screen.width+50,50));
		//foter design
		footer=new JPanel();
	
		footer.setBackground(new Color(155, 200, 200));
		footer.setPreferredSize(new Dimension(screen.width, 50));

		
		setResizable(true);
		

		setSize(857, 500);
		
		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(getContentPane(), popupMenu);
		
		JMenu mnNewMenu = new JMenu("More Option");
		popupMenu.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Minimize");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			setMinimumSize(new Dimension(0, 0));
				
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Maximaze");
		mnNewMenu.add(mntmNewMenuItem_1);
		
		JMenuItem mntmFullScreen = new JMenuItem("Exit");
		mntmFullScreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DailyReports.this.dispose();
			}
		});
		mnNewMenu.add(mntmFullScreen);
		getContentPane().setLayout(new MigLayout("", "[130px][][146px][112px][134px][-52.00px][-64.00][][][][43.00,grow][-41.00][-68.00][][]", "[73px][58px][326px,grow][][27px][]"));
		
		JLabel lblDate = new JLabel("Date::");
		lblDate.setForeground(Color.WHITE);
		lblDate.setFont(new Font("Sylfaen", Font.BOLD, 18));
		getContentPane().add(lblDate, "cell 0 0,alignx left,aligny bottom");
		
		JLabel lblChoseReports = new JLabel("Chose Reports");
		lblChoseReports.setForeground(Color.WHITE);
		lblChoseReports.setFont(new Font("Sylfaen", Font.BOLD, 18));
		getContentPane().add(lblChoseReports, "cell 0 2,growx,aligny top");
		
		JXDatePicker datePicker = new JXDatePicker();
		Date date=new Date(System.currentTimeMillis());
		datePicker.setDate(date);
		getContentPane().add(datePicker, "cell 2 0,growx,aligny bottom");
		
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Daily Reports", "Parchase Reports"}));
		
		//comboBox.addItem("Daily Reports,Parchase Reports");
		
		
		
		getContentPane().add(comboBox, "cell 2 2,growx,aligny top");
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, "cell 3 0 12 3,grow");
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		Button button_1 = new Button("Print");
		getContentPane().add(button_1, "cell 13 5,growx,aligny top");
		
		Button button = new Button("View Reports");
		getContentPane().add(button, "cell 14 5,alignx right");
		show();
	}
	public  static void  Main(String []args){
		try {
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		DailyReports addFood=new DailyReports();
		addFood.setVisible(true);
		addFood.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
