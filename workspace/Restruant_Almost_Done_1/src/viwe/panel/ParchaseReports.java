package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import controller.ParchaseCtrl;
import model.ParchaseModel;

import javax.swing.SwingConstants;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import net.miginfocom.swing.MigLayout;
import viwe.Dash;
import javax.swing.border.TitledBorder;
import com.toedter.calendar.JDateChooser;

public  class ParchaseReports extends JInternalFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel label;
	private JDesktopPane desktopPane;

	//main mehtod 
	
	//variable declaration;
	int w = 750;
	int h = 400;
	private JInternalFrame jInternalFrame;
	private JDesktopPane desktopen;
	JPanel header, center, footer;
	Border border = new EtchedBorder();
	Font font = new Font("courier new", Font.PLAIN, 16);
	private JTextField billTxt;
	private JTextField productName;
	public static JTable parchaseRep;

	

	public ParchaseReports() {
		setBackground(Color.WHITE);
		
		//setUndecorated(true);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIgnoreRepaint(true);
		setFocusTraversalPolicyProvider(true);
		
		setName("ResFram");
		
		setTitle("Restrurant Management");
		getContentPane().setBackground(new Color(47, 79, 79));
		//Dimention Screen
		Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();
		//header Design
		header=new JPanel();
		header.setBorder(new EtchedBorder(EtchedBorder.LOWERED,null,null));
		header.setBackground(Color.BLUE);
		header.setPreferredSize(new Dimension(screen.width, 30));
		//center Design
		center=new JPanel();
		center.setPreferredSize(new Dimension(screen.width+50,50));
		//foter design
		footer=new JPanel();
		
		footer.setBackground(new Color(155, 200, 200));
		footer.setPreferredSize(new Dimension(screen.width, 50));

		
		setResizable(false);
		
		setSize(1076, 610);
		getContentPane().setLayout(new MigLayout("", "[64px,grow][95px][111px][143px][136px,grow][210.00px][49px][164px][-38.00][]", "[33px][][grow][370.00][grow]"));
		
		
		
		JLabel lblBillNo = new JLabel("Bill No:");
		lblBillNo.setBackground(Color.LIGHT_GRAY);
		lblBillNo.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblBillNo.setForeground(new Color(240, 255, 240));
		getContentPane().add(lblBillNo, "cell 0 0,alignx left,aligny center");
		
		billTxt = new JTextField();
		getContentPane().add(billTxt, "cell 1 0,grow");
		billTxt.setColumns(10);
		
		JLabel lblProductName = new JLabel("Product Name");
		lblProductName.setForeground(new Color(255, 255, 255));
		lblProductName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		getContentPane().add(lblProductName, "cell 2 0,alignx left,aligny center");
		
		JLabel lblProductCategory = new JLabel("Product Category");
		lblProductCategory.setForeground(new Color(255, 255, 255));
		lblProductCategory.setFont(new Font("Tahoma", Font.PLAIN, 18));
		getContentPane().add(lblProductCategory, "cell 4 0,alignx left,aligny center");
		
		productName = new JTextField();
		getContentPane().add(productName, "cell 3 0,grow");
		productName.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Select", "Spaici", "Vegitable", "Stationary", "Fish", "Meat", "Fruit", "Others"}));
		getContentPane().add(comboBox, "cell 5 0,grow");
		ParchaseModel.load();
		
		
		
		JLabel lblDate = new JLabel("Date");
		lblDate.setForeground(Color.WHITE);
		lblDate.setFont(new Font("Tahoma", Font.PLAIN, 18));
		getContentPane().add(lblDate, "cell 7 0");
		
		JDateChooser dateChooser = new JDateChooser();
		getContentPane().add(dateChooser, "cell 8 0 2 1,grow");
		
		JDesktopPane desktopPane_1 = new JDesktopPane();
		getContentPane().add(desktopPane_1, "cell 0 2 10 2,grow");
		desktopPane_1.setLayout(new BoxLayout(desktopPane_1, BoxLayout.X_AXIS));
		
		JScrollPane scrollPane = new JScrollPane();
		desktopPane_1.add(scrollPane);
		
		parchaseRep = new JTable();
		ParchaseModel.Report();
		scrollPane.setViewportView(parchaseRep);
		show();
		
		
	
	}
	public  static void  Main(String []args){
		try {
			
			
		} catch (Exception e) {
			
		}
		ParchaseReports addFood=new ParchaseReports();
		addFood.setVisible(true);
		addFood.setDefaultCloseOperation(HIDE_ON_CLOSE);
	}
}
