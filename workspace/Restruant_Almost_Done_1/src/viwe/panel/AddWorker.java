package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import controller.AddworkerCtrl;
import model.AddWorkerModel;
import viwe.TextPrompt;

import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.ImageIcon;
import javax.swing.border.MatteBorder;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JPopupMenu;
import java.awt.Component;
import javax.swing.JMenuItem;

public class AddWorker extends JInternalFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel label;
	private JDesktopPane desktopPane;
	private JComboBox postCb ;

	//variable declaration;
	int w = 750;
	int h = 400;
	private JInternalFrame jInternalFrame;
	private JDesktopPane desktopen;
	JPanel header, center, footer;
	Border border = new EtchedBorder();
	Font font = new Font("courier new", Font.PLAIN, 16);
	private JTextField workerId;
	private JTextField nameT;
	public static  JTable WorkerDitails_Tab;
	public static JRadioButton gMale,gFmale;
	private JTextField SearchTxt;

	public AddWorker() {
		setFrameIcon(new ImageIcon(AddWorker.class.getResource("/pictureResource/images/Business.png")));
		setForeground(new Color(0, 255, 0));
		setTitle("Add Worker");
		getContentPane().setBackground(new Color(128, 0, 0));
		getContentPane().setLayout(null);
		
		JMenuItem mntmRefresh = new JMenuItem("Refresh");
		mntmRefresh.setBounds(0, 0, 95, 24);
		getContentPane().add(mntmRefresh);
		mntmRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddWorkerModel.load();
			}
		});
		
		workerId = new JTextField();
		workerId.setColumns(10);
		workerId.setBounds(141, 24, 66, 28);
		getContentPane().add(workerId);
		
		nameT = new JTextField();
		nameT.setColumns(10);
		nameT.setBounds(141, 63, 128, 28);
		getContentPane().add(nameT);
		
		 postCb = new JComboBox();
		 postCb.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 		if (postCb.getSelectedItem().equals("Select")) {
		 			JOptionPane.showMessageDialog(null, "Select Post Please !");
					
				}
		 	}
		 });
		 postCb.setModel(new DefaultComboBoxModel(new String[] {"Select", "Manager", "Cooker", "Waiter", "Sweeper"}));
		postCb.setForeground(new Color(0, 0, 255));
		postCb.setBounds(281, 24, 149, 28);
		getContentPane().add(postCb);
		
		gFmale = new JRadioButton("Female");
		gFmale.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (gFmale.isSelected()) {
					gMale.setSelected(false);
					
				}
			}
		});
		gFmale.setForeground(new Color(0, 0, 255));
		gFmale.setBounds(357, 66, 73, 23);
		getContentPane().add(gFmale);
		
		 gMale = new JRadioButton("Male");
		gMale.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (gMale.isSelected()){
					gFmale.setSelected(false);
				}
			}
		});
		gMale.setForeground(new Color(0, 0, 255));
		gMale.setBounds(281, 66, 73, 23);
		getContentPane().add(gMale);
		
		JPopupMenu popupMenu = new JPopupMenu();
		getContentPane().add(popupMenu);
		popupMenu.setBounds(0, 0, 163, 30);
		
		JLabel label_1 = new JLabel("Worker Id");
		label_1.setForeground(new Color(0, 0, 255));
		label_1.setFont(new Font("Tahoma", Font.ITALIC, 18));
		label_1.setBounds(34, 24, 119, 22);
		getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("Name");
		label_2.setForeground(new Color(0, 0, 255));
		label_2.setFont(new Font("Tahoma", Font.ITALIC, 18));
		label_2.setBounds(34, 63, 119, 22);
		getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("Post");
		label_3.setForeground(new Color(0, 0, 255));
		label_3.setFont(new Font("Tahoma", Font.ITALIC, 18));
		label_3.setBounds(217, 24, 119, 22);
		getContentPane().add(label_3);
		
		JLabel lblSearch = new JLabel("Search");
		lblSearch.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				lblSearch.setFont(new Font("Tahoma", Font.ITALIC, 19));
				lblSearch.setForeground(Color.green);	
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				lblSearch.setFont(new Font("Tahoma", Font.ITALIC, 18));
				lblSearch.setForeground(new Color(240, 248, 255));
			}
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String s=SearchTxt.getText();
				AddWorkerModel.Search(s);
			}
		});
		lblSearch.setIcon(new ImageIcon(AddWorker.class.getResource("/pictureResource/b_search.png")));
		lblSearch.setFont(new Font("Tahoma", Font.ITALIC, 18));
		lblSearch.setForeground(new Color(240, 248, 255));
		lblSearch.setBounds(516, 30, 77, 28);
		getContentPane().add(lblSearch);
		
		SearchTxt = new JTextField();
		new TextPrompt("Enter Id", SearchTxt);
		SearchTxt.setBounds(605, 29, 149, 28);
		getContentPane().add(SearchTxt);
		SearchTxt.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 130, 779, 323);
		getContentPane().add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane);
		
		WorkerDitails_Tab = new JTable();
		WorkerDitails_Tab.setForeground(new Color(0, 0, 0));
		WorkerDitails_Tab.setFont(new Font("Sitka Display", Font.BOLD, 18));
		WorkerDitails_Tab.setBackground(new Color(255, 255, 255));
		AddWorkerModel.load();
		scrollPane.setViewportView(WorkerDitails_Tab);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String workId =workerId.getText();
				String name=nameT.getText();
				
				String post=postCb.getSelectedItem().toString();
				String Gengder=null;
				if (gMale.isSelected()) {
					Gengder=gMale.getText();
				}
				else if (gFmale.isSelected()) {
					Gengder=gFmale.getText();
				}
				
				
				new AddworkerCtrl(workId,name,post,Gengder);
				workerId.setText("");
				nameT.setText("");
				
			}
		});
		
		JButton btnRemove = new JButton("Remove");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int row = WorkerDitails_Tab.getSelectedRow();

				String id = WorkerDitails_Tab.getModel().getValueAt(row, 0).toString();
				
				AddWorkerModel.RemoveWorker(id);
				AddWorkerModel.load();
				
				
			}
		});
		btnRemove.setBounds(20, 466, 97, 25);
		getContentPane().add(btnRemove);
		btnAdd.setBounds(700, 464, 89, 23);
		getContentPane().add(btnAdd);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(AddWorker.class.getResource("/pictureResource/Foood.jpeg")));
		lblNewLabel.setBounds(-142, -628, 1568, 1163);
		getContentPane().add(lblNewLabel);
		setLocation(new Point(350, 100));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		setDoubleBuffered(true);
		
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		//Dimention Screen
		Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();
		//header Design
		header=new JPanel();
		header.setBorder(new EtchedBorder(EtchedBorder.LOWERED,null,null));
		header.setBackground(Color.BLUE);
		header.setPreferredSize(new Dimension(screen.width, 30));
		//center Design
		center=new JPanel();
		center.setPreferredSize(new Dimension(screen.width+50,50));
		//foter design
		footer=new JPanel();
		//footer.setBackground(Color.blue);
		footer.setBackground(new Color(155, 200, 200));
		footer.setPreferredSize(new Dimension(screen.width, 50));
		//setUndecorated(true);
	 //getRootPane().setWindowDecorationStyle(JRootPane.COLOR_CHOOSER_DIALOG);
		//setLocation((screen.width - w) / 2, (screen.height - h) / 2);
		setSize(873, 545);
		show();
	}
	public  static void  Main(String []args){
		try {
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		AddWorker addFood=new AddWorker();
		addFood.setVisible(true);
		addFood.setDefaultCloseOperation(HIDE_ON_CLOSE);
	}
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
