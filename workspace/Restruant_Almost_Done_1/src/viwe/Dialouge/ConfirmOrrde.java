package viwe.Dialouge;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import controller.TempOderCtrl;
import model.TempOrder;

import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ConfirmOrrde extends JDialog {

	private final JPanel contentPanel = new JPanel();
	public static JTextField foodnameTax;
	public static JTextField periceTax;
	public static JTextField CategoryTax;
	public static JTextField QuntityTax;
	public static JTextField TotalTax;

/*	public static void main(String[] args) {
		try {
			ConfirmOrrde dialog = new ConfirmOrrde();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
*/
	/**
	 * Create the dialog.
	 */
	public ConfirmOrrde() {
		setRootPaneCheckingEnabled(false);
		setResizable(false);
		setTitle("Confirm Your Order Sir");
		setBounds(100, 100, 686, 370);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(Color.WHITE);
		contentPanel.setForeground(Color.WHITE);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblFoodName = new JLabel("Food Name");
			lblFoodName.setFont(new Font("Tahoma", Font.BOLD, 18));
			lblFoodName.setBounds(12, 13, 113, 35);
			lblFoodName.setForeground(Color.BLACK);
			contentPanel.add(lblFoodName);
		}
		
		foodnameTax = new JTextField();
		foodnameTax.setEditable(false);
		foodnameTax.setBounds(137, 18, 157, 27);
		contentPanel.add(foodnameTax);
		foodnameTax.setColumns(10);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setForeground(Color.BLACK);
		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblPrice.setBounds(341, 13, 113, 35);
		contentPanel.add(lblPrice);
		
		JLabel lblCategory = new JLabel("Category");
		lblCategory.setForeground(Color.BLACK);
		lblCategory.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblCategory.setBounds(12, 69, 113, 35);
		contentPanel.add(lblCategory);
		
		JLabel lblQuntity = new JLabel("Quntity");
		lblQuntity.setForeground(Color.BLACK);
		lblQuntity.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblQuntity.setBounds(341, 69, 113, 35);
		contentPanel.add(lblQuntity);
		
		periceTax = new JTextField();
		periceTax.setEditable(false);
		periceTax.setBounds(466, 21, 157, 27);
		contentPanel.add(periceTax);
		periceTax.setColumns(10);
		
		CategoryTax = new JTextField();
		CategoryTax.setEditable(false);
		CategoryTax.setBounds(137, 77, 157, 27);
		contentPanel.add(CategoryTax);
		CategoryTax.setColumns(10);
		
		QuntityTax = new JTextField();
	
		QuntityTax.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				String price=periceTax.getText();
				int Pri=Integer.parseInt(price);
				String Qun=QuntityTax.getText();
				int qunt=Integer.parseInt(Qun);
				int to=Pri*qunt;
				String total=Integer.toString(to);
				TotalTax.setText(total);
			}
		});
		QuntityTax.setBounds(466, 77, 157, 27);
		contentPanel.add(QuntityTax);
		QuntityTax.setColumns(10);
		
		TotalTax = new JTextField();
		TotalTax.setFont(new Font("SansSerif", Font.PLAIN, 20));
		TotalTax.setHorizontalAlignment(SwingConstants.CENTER);
		TotalTax.setBounds(220, 215, 216, 43);
		contentPanel.add(TotalTax);
		TotalTax.setColumns(10);
		
		JLabel lblTotalAmont = new JLabel("Total Amont");
		lblTotalAmont.setForeground(Color.BLACK);
		lblTotalAmont.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblTotalAmont.setBounds(277, 165, 113, 35);
		contentPanel.add(lblTotalAmont);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBackground(Color.WHITE);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
					String nameOfFood=foodnameTax.getText();
					String Price=periceTax.getText();
					String cateGory=CategoryTax.getText();
					String qauntity=QuntityTax.getText();
					String total=TotalTax.getText();
					TempOderCtrl ta=new TempOderCtrl(nameOfFood, Price, cateGory, qauntity, total);
					TempOrder.Totalamount();
					ConfirmOrrde.this.dispose();
					}
				});
				okButton.setForeground(Color.black);
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseEntered(MouseEvent arg0) {
						okButton.setForeground(Color.GREEN);	
					}
					@Override
					public void mouseExited(MouseEvent arg0) {
						okButton.setForeground(Color.black);
					}
				});
				okButton.setContentAreaFilled(false);
				okButton.setFocusable(false);
				okButton.setFocusTraversalKeysEnabled(false);
				okButton.setFocusPainted(false);
				okButton.setBorderPainted(false);
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						ConfirmOrrde.this.dispose();
					}
				});
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseEntered(MouseEvent arg0) {
						cancelButton.setForeground(Color.GREEN);
					}
					@Override
					public void mouseExited(MouseEvent arg0) {
						cancelButton.setForeground(Color.BLACK);
					}
				});
				
				cancelButton.setContentAreaFilled(false);
				cancelButton.setFocusable(false);
				cancelButton.setFocusTraversalKeysEnabled(false);
				cancelButton.setFocusPainted(false);
				cancelButton.setBorderPainted(false);
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
