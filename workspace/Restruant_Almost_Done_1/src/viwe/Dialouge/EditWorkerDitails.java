package viwe.Dialouge;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import org.jdesktop.swingx.JXDatePicker;

import de.javasoft.plaf.synthetica.SyntheticaBlackMoonLookAndFeel;

import javax.swing.ImageIcon;
import java.awt.event.InputMethodListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.event.InputMethodEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class EditWorkerDitails extends JFrame{

	public static JPanel contentPanel = new JPanel();
	public static JTextField workerIdTxt;
	public static JTextField name;
	public static JRadioButton rdbtnMale,rdbtnFemale ;
	public static JCheckBox chckbxAbsent,chckbxPresent;
public static  JTextField genderT;
	public static  JTextField preT;
	public static JXDatePicker dat;
	public static  JTextField da2;
	/*public static void main(String[] args) {
		try {
			EditWorkerDitails dialog = new EditWorkerDitails();
			//UIManager.setLookAndFeel(new SyntheticaBlackMoonLookAndFeel());
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * Create the dialog.
	 */
	public EditWorkerDitails() {
		/*try {
			//UIManager.setLookAndFeel(new SyntheticaBlackMoonLookAndFeel());
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		setResizable(false);
		setBounds(100, 100, 547, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblWorkerId = new JLabel("Worker Id");
		
		lblWorkerId.setBounds(12, 13, 57, 16);
		contentPanel.add(lblWorkerId);
		
		workerIdTxt = new JTextField();
		workerIdTxt.setBounds(81, 13, 116, 22);
		contentPanel.add(workerIdTxt);
		workerIdTxt.setColumns(10);
		
		JLabel workername_txt = new JLabel("Name");
		workername_txt.setBounds(12, 69, 56, 16);
		contentPanel.add(workername_txt);
		
		name = new JTextField();
		name.setBounds(81, 66, 116, 22);
		contentPanel.add(name);
		name.setColumns(10);
		
		JLabel lblGender = new JLabel("Gender");
		lblGender.setBounds(258, 16, 56, 16);
		contentPanel.add(lblGender);
		
		rdbtnMale = new JRadioButton("Male");
		rdbtnMale.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if(rdbtnMale.isSelected()){
					genderT.setText("Male");
				}
				if (rdbtnMale.isSelected()){
					rdbtnFemale.setSelected(false);
				}
			}
		});
		rdbtnMale.setBounds(439, 9, 62, 25);
		contentPanel.add(rdbtnMale);
		
		 rdbtnFemale = new JRadioButton("Female");
		 rdbtnFemale.addItemListener(new ItemListener() {
		 	public void itemStateChanged(ItemEvent arg0) {
		 		if (rdbtnFemale.isSelected()) {
					genderT.setText("Female");
				}
		 		if (rdbtnFemale.isSelected()) {
					rdbtnMale.setSelected(false);
				} 
				
		 	}
		 });
		rdbtnFemale.setBounds(439, 41, 77, 25);
		contentPanel.add(rdbtnFemale);
		
		chckbxAbsent = new JCheckBox("Absent");
		chckbxAbsent.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (chckbxAbsent.isSelected()) {
					preT.setText("Absent");
				}
				if (chckbxAbsent.isSelected()) {
					chckbxPresent.setSelected(false);
				}
			}
		});
		chckbxAbsent.setBounds(439, 122, 67, 25);
		contentPanel.add(chckbxAbsent);
		
		chckbxPresent = new JCheckBox("Present");
		chckbxPresent.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (chckbxPresent.isSelected()) {
					preT.setText("Present");
				}
				if (chckbxPresent.isSelected()) {
					chckbxAbsent.setSelected(false);
					
				}
			}
		});
		chckbxPresent.setBounds(439, 92, 71, 25);
		contentPanel.add(chckbxPresent);
		
		JLabel lblSatatus = new JLabel("Satatus");
		lblSatatus.setBounds(258, 69, 56, 16);
		contentPanel.add(lblSatatus);
		
		JLabel lblDate = new JLabel("Date");
		lblDate.setBounds(12, 148, 49, 16);
		contentPanel.add(lblDate);
		
		// dat = new JXDatePicker();
		
		/// DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		    //java.util.Date date=new java.util.Date();
		    //dat.setDate(date);
		    //String tarik=dat.getDate().toString();
		    
		
		//dat.setBounds(81, 144, 116, 24);
		//contentPanel.add(dat);
		
		genderT = new JTextField();
		genderT.setBounds(305, 14, 116, 22);
		contentPanel.add(genderT);
		genderT.setColumns(10);
		
		preT = new JTextField();
		preT.setBounds(305, 66, 116, 22);
		contentPanel.add(preT);
		preT.setColumns(10);
		
		da2 = new JTextField();
		
		da2.setBounds(81, 145, 116, 22);
		contentPanel.add(da2);
		da2.setColumns(10);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton savebtn = new JButton("Save");
				savebtn.setActionCommand("Save");
				buttonPane.add(savebtn);
				getRootPane().setDefaultButton(savebtn);
			}
			{
				JButton Delebtn = new JButton("Delete");
				Delebtn.setActionCommand("Delete");
				buttonPane.add(Delebtn);
			}
		}
	}
}
