package viwe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;

import controller.AddFoodBreakfast_Ctrl;

import net.miginfocom.swing.MigLayout;
import viwe.panel.About;
import viwe.panel.AddFood;
import viwe.panel.AddWorker;
import viwe.panel.Calculator;
import viwe.panel.Contact;
import viwe.panel.DailyReports;
import viwe.panel.Help;
import viwe.panel.Order;


import viwe.panel.Parchase;
import viwe.panel.ParchaseReports;
import viwe.panel.SalarySheet;
import viwe.panel.Sit_Plane;
import viwe.panel.WorkerDetails;
import viwe.sequrity.Login;
import viwe.sequrity.LoginSingle;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.text.ParseException;
import java.awt.event.ActionEvent;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

//import calculator1.Calculator;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Window.Type;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class Dash extends JFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane ;

	
	public static void main(String[] args) {
		 {
			 Dash frame = new Dash();
			 frame.setOpacity(1.0f);
				frame.setVisible(true);
				
				
					
					try {
						try {
							UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
							//UIManager.setLookAndFeel("de.muntjak.tinylookandfeel.TinyLookAndFeel");
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (InstantiationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
					} 
					
				catch (UnsupportedLookAndFeelException e) {
					
					e.printStackTrace();
				}
		 }
	}

	int w = 1600;
	int h = 900;
	/**
	 * @wbp.nonvisual location=1563,-36
	 */
	private final JLabel label = new JLabel("New label");
	
	public Dash() {
		setAlwaysOnTop(true);
		
		setBackground(Color.WHITE);
		label.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/delete.png")));
		//setUndecorated(true);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIgnoreRepaint(true);
		setFocusTraversalPolicyProvider(true);
		setTitle("Restrurant Management");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\Rest.png"));
		setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
	
		setBounds(0, 0, w, h);
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screen.width - w) / 2, (screen.height - h) / 2);
		setSize(1449, 857);
		getContentPane().setLayout(new MigLayout());
		setPreferredSize(new Dimension(screen.width + 50, 50));
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBorderPainted(false);
		menuBar.setForeground(new Color(255, 99, 71));
		menuBar.setBackground(new Color(255, 255, 255));
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Home");
		mnNewMenu.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/Home.gif")));
		mnNewMenu.setFont(new Font("SansSerif", Font.ITALIC, 16));
		menuBar.add(mnNewMenu);
		
		JMenu mnOpen = new JMenu("Open");
		mnOpen.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/OPEN.GIF")));
		mnNewMenu.add(mnOpen);
		
		JMenuItem mntmAddfood = new JMenuItem("AddFood");
		mntmAddfood.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/DB Location.png")));
		mntmAddfood.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				AddFood addFood=new AddFood();
				
				addFood.setVisible(true);
				desktopPane.add(addFood);
			}
		});
		mnOpen.add(mntmAddfood);
		
		JMenuItem mntmOrder = new JMenuItem("Take Order");
		mntmOrder.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/orders.png")));
		mntmOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Order order =new Order();
				order.setVisible(true);
				desktopPane.add(order);
				
			}
		});
		mnOpen.add(mntmOrder);
		
		JMenuItem mntmParchase = new JMenuItem("Parchase");
		mntmParchase.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/purchase.png")));
		mntmParchase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Parchase parchase=new Parchase();
				parchase.setVisible(true);
				desktopPane.add(parchase);
			}
		});
		mnOpen.add(mntmParchase);
		
		JMenuItem mntmSeatPlane = new JMenuItem("Seat Plane");
		mntmSeatPlane.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/find.gif")));
		mntmSeatPlane.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Sit_Plane sit_Plane=new Sit_Plane();
				sit_Plane.setVisible(true);
				desktopPane.add(sit_Plane);
			}
		});
		mnOpen.add(mntmSeatPlane);
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mntmSave.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/save_all.png")));
		mnNewMenu.add(mntmSave);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		
		JMenuItem mntmLogout = new JMenuItem("Logout");
		mntmLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new LoginSingle().setVisible(true);
				Dash.this.setVisible(false);
			}
		});
		mntmLogout.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/reload_Hover.png")));
		mnNewMenu.add(mntmLogout);
		mntmExit.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/exits.png")));
		mnNewMenu.add(mntmExit);
		
		JMenu mnEdit = new JMenu("Edit");
		mnEdit.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/edit.png")));
		mnEdit.setFont(new Font("SansSerif", Font.ITALIC, 16));
		menuBar.add(mnEdit);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Recovery");
		mntmNewMenuItem.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/backup.GIF")));
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
		viwe.sequrity.F f=new viwe.sequrity.F();
		f.setVisible(true);
			desktopPane.add(f);
			}
		});
		mnEdit.add(mntmNewMenuItem);
		
		JMenu mnWorker = new JMenu("Worker ");
		mnWorker.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/Business.png")));
		mnWorker.setFont(new Font("SansSerif", Font.ITALIC, 16));
		menuBar.add(mnWorker);
		
		JMenuItem mntmWorkerInfo = new JMenuItem("Absent And Present List");
		mntmWorkerInfo.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/Attendance.png")));
		mntmWorkerInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				WorkerDetails workerDetails=new WorkerDetails();
				workerDetails.setVisible(true);
				desktopPane.add(workerDetails);
			}
		});
		
		JMenuItem mntmAddWorkerInfo = new JMenuItem("Add Worker info");
		mntmAddWorkerInfo.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/notepad.png")));
		mntmAddWorkerInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddWorker addWorker=new AddWorker();
				addWorker.setVisible(true);
				desktopPane.add(addWorker);
			}
		});
		mnWorker.add(mntmAddWorkerInfo);
		mnWorker.add(mntmWorkerInfo);
		
		JMenuItem mntmSalarySheet = new JMenuItem("Salary Sheet");
		mntmSalarySheet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SalarySheet salarySheet=new SalarySheet();
				salarySheet.setVisible(true);
				desktopPane.add(salarySheet);
			}
		});
		mntmSalarySheet.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/ProdStocks.png")));
		mnWorker.add(mntmSalarySheet);
		
		JMenu mnCalculator = new JMenu("Calculator");
		mnCalculator.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/calc.png")));
		mnCalculator.setFont(new Font("SansSerif", Font.ITALIC, 16));
		menuBar.add(mnCalculator);
		
		JMenuItem mntmCalculator = new JMenuItem("Calculator");
		mntmCalculator.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/calc.png")));
		mntmCalculator.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			Calculator calculator=new Calculator();
			calculator.setVisible(true);
			desktopPane.add(calculator);
				
			}
		});
		mnCalculator.add(mntmCalculator);
		
		JMenu mnReports = new JMenu("Reports");
		mnReports.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/NotePad.gif")));
		mnReports.setFont(new Font("SansSerif", Font.ITALIC, 16));
		menuBar.add(mnReports);
		
		JMenuItem mntmSalesReports = new JMenuItem("Sales Reports");
		mntmSalesReports.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/invoice.png")));
		mnReports.add(mntmSalesReports);
		
		JMenuItem mntmParchaseReports = new JMenuItem("Parchase Reports");
		mntmParchaseReports.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ParchaseReports par=new ParchaseReports();
				par.setVisible(true);
				desktopPane.add(par);
			}
		});
		mntmParchaseReports.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/company reference.png")));
		mnReports.add(mntmParchaseReports);
		
		JMenuItem mntmWorkersReports = new JMenuItem("Workers Reports");
		mntmWorkersReports.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/employee.png")));
		mnReports.add(mntmWorkersReports);
		
		JMenuItem mntmExpanseReports = new JMenuItem("Expanse Reports");
		mntmExpanseReports.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/Expenses.png")));
		mnReports.add(mntmExpanseReports);
		
		JMenu mnAbout = new JMenu("About");
		mnAbout.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/help.png")));
		mnAbout.setFont(new Font("SansSerif", Font.ITALIC, 16));
		
		menuBar.add(mnAbout);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/help.png")));
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				About about=new About();
				about.setVisible(true);
				desktopPane.add(about);
			}
		});
		mnAbout.add(mntmAbout);
		
		JMenuItem mntmContact = new JMenuItem("Help");
		mntmContact.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/Help.gif")));
		mntmContact.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Help help=new Help();
				help.setVisible(true);
				desktopPane.add(help);
				
			}
		});
		mnAbout.add(mntmContact);
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		//contentPane.setLayout(new MigLayout());
		//contentPane.setLayout(new FlowLayout(FlowLayout.TRAILING));
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				image=(Toolkit.getDefaultToolkit().getImage(Dash.class.getResource("/pictureResource/BA2.png")));
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		
		desktopPane.setFocusable(false);
		desktopPane.setFocusTraversalKeysEnabled(false);
		desktopPane.setFocusCycleRoot(false);
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(null);
		
	}
}
