package viwe.sequrity;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;

import org.junit.runner.Describable;

import controller.AddFoodBreakfast_Ctrl;
import de.javasoft.plaf.synthetica.SyntheticaBlackMoonLookAndFeel;
import net.miginfocom.swing.MigLayout;
import viwe.Dash;
import viwe.panel.About;
import viwe.panel.AddFood;
import viwe.panel.AddWorker;
import viwe.panel.Calculator;
import viwe.panel.Contact;
import viwe.panel.DailyReports;
import viwe.panel.Order;


import viwe.panel.Parchase;
import viwe.panel.Sit_Plane;
import viwe.panel.WorkerDetails;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.text.ParseException;
import java.awt.event.ActionEvent;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

//import calculator1.Calculator;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Window.Type;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.border.EtchedBorder;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.event.MouseMotionAdapter;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class LoginSingle extends JFrame {

	private JPanel contentPane;
	public static  LoginSingle frame;
	public static void main(String[] args) {
		 {
			 LoginSingle frame = new LoginSingle();
			 frame.setOpacity(1.0f);
				frame.setVisible(true);
				try {
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnsupportedLookAndFeelException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
					
					
		 }
	}
	
	int w = 1600;
	int h = 900;
	@Override
	public void setLocation(int a, int arg1) {
	
		super.setLocation(500, 210);
	}
	private final JLabel label = new JLabel("New label");
	int pX, pY;
	private JLabel label_1;
	private JLabel label_2;
	private JLabel label_3;
	private JLabel label_4;
	private JLabel label_5;
	private JTextField textField;
	private JLabel label_6;
	private JTextField textField_1;
	private JLabel label_7;
	private JButton button;
	private JLabel label_8;
	private JLabel label_9;
	private JButton button_1;
	private JButton button_2;
	public LoginSingle() {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedLookAndFeelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		setBackground(new Color(0, 51, 204));
		
		label.setIcon(new ImageIcon(LoginSingle.class.getResource("/pictureResource/delete.png")));
		setUndecorated(true);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIgnoreRepaint(true);
		setFocusTraversalPolicyProvider(true);
		setName("ResFram");
		setTitle("Restrurant Management");
		
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(LoginSingle.class.getResource("/pictureResource/food-grey.png")));
		setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		
		setBounds(0, 0, w, h);
		
		setSize(621, 481);
	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		
		
		getContentPane().setLayout(new MigLayout());
		setPreferredSize(new Dimension(screen.width + 50, 60));
		contentPane = new JPanel();
		contentPane.setBackground(new Color(102, 0, 255));
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		label_1 = new JLabel("Login");
		label_1.setIcon(new ImageIcon(LoginSingle.class.getResource("/pictureResource/images/icons8_User_Menu_Male_50px.png")));
		label_1.setForeground(Color.WHITE);
		label_1.setFont(new Font("SansSerif", Font.PLAIN, 18));
		label_1.setBounds(158, 50, 133, 78);
		contentPane.add(label_1);
		
		label_2 = new JLabel("New label");
		label_2.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		label_2.setBounds(158, 122, 339, 2);
		contentPane.add(label_2);
		
		label_3 = new JLabel("User Name");
		label_3.setForeground(Color.WHITE);
		label_3.setFont(new Font("SansSerif", Font.PLAIN, 18));
		label_3.setBounds(158, 162, 112, 24);
		contentPane.add(label_3);
		
		label_4 = new JLabel("Password");
		label_4.setForeground(Color.WHITE);
		label_4.setFont(new Font("SansSerif", Font.PLAIN, 18));
		label_4.setBounds(158, 286, 112, 24);
		contentPane.add(label_4);
		
		label_5 = new JLabel("Log in ");
		label_5.setHorizontalAlignment(SwingConstants.CENTER);
		label_5.setForeground(new Color(0, 255, 102));
		label_5.setFont(new Font("Modern No. 20", Font.BOLD, 26));
		label_5.setBounds(170, 0, 275, 37);
		contentPane.add(label_5);
		
		textField = new JTextField();
		int l =8;
		
		textField.setForeground(new Color(255, 255, 255));
		textField.setColumns(10);
		textField.setBorder(null);
		textField.setBackground(new Color(102, 0, 255));
		textField.setBounds(158, 198, 262, 28);
		contentPane.add(textField);
		
		label_6 = new JLabel("New label");
		label_6.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		label_6.setBounds(158, 226, 339, 2);
		contentPane.add(label_6);
		
		textField_1 = new JPasswordField();
		textField_1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				if (textField_1.getText().length()==l) {
					JOptionPane.showMessageDialog(null, "Your Password Should be 8 digit");
				
				}
				
			}
		});
		textField_1.setForeground(new Color(255, 255, 255));
		textField_1.setColumns(10);
		textField_1.setBorder(null);
		textField_1.setBackground(new Color(102, 0, 255));
		textField_1.setBounds(158, 322, 262, 28);
		contentPane.add(textField_1);
		
		label_7 = new JLabel("New label");
		label_7.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		label_7.setBounds(158, 350, 339, 2);
		contentPane.add(label_7);
		
		button = new JButton("Register");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Login().setVisible(true);
			}
		});
		button.setRolloverEnabled(false);
		button.setRequestFocusEnabled(false);
		button.setFont(new Font("SansSerif", Font.PLAIN, 20));
		button.setFocusTraversalKeysEnabled(false);
		button.setFocusPainted(false);
		button.setBorderPainted(false);
		button.setBackground(new Color(102, 0, 255));
		button.setBounds(436, 402, 102, 28);
		contentPane.add(button);
		
		label_8 = new JLabel("Error");
		label_8.setBounds(432, 204, 55, 16);
		contentPane.add(label_8);
		
		label_9 = new JLabel("Error");
		label_9.setBounds(432, 323, 55, 16);
		contentPane.add(label_9);
		
		button_1 = new JButton("X");
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_1.setForeground(Color.GRAY);
				button_1.setFont(new Font("SansSerif", Font.BOLD, 20));
				button_1.setBackground(Color.red);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_1.setForeground(Color.BLACK);
				button_1.setFont(new Font("SansSerif", Font.PLAIN, 20));
				button_1.setBackground(new Color(102, 0, 255));
			}
		});
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		button_1.setRolloverEnabled(false);
		button_1.setRequestFocusEnabled(false);
		
		button_1.setFont(new Font("SansSerif", Font.PLAIN, 20));
		button_1.setFocusTraversalKeysEnabled(false);
		button_1.setFocusPainted(false);
		button_1.setBorderPainted(false);
		button_1.setBackground(new Color(102, 0, 255));
		button_1.setBounds(558, 0, 63, 32);
		contentPane.add(button_1);
		
		button_2 = new JButton("Login");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Dash().setVisible(true);
				LoginSingle.this.setVisible(false);
			}
		});
		button_2.setRolloverEnabled(false);
		button_2.setRequestFocusEnabled(false);
		button_2.setFont(new Font("SansSerif", Font.PLAIN, 20));
		button_2.setFocusTraversalKeysEnabled(false);
		button_2.setFocusPainted(false);
		button_2.setBorderPainted(false);
		button_2.setBackground(new Color(102, 0, 255));
		button_2.setBounds(317, 402, 90, 28);
		contentPane.add(button_2);
		
	
    

	
		
		
    setLocation((screen.width - w) / 2, (screen.height - h) / 2);
		
		
		
		
		
	}
}
