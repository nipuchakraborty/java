package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class EditWorker {
	
		public static int id;
		public static String name;
		public static String gender;
		public static String present;
		public static String  post;
		public static String date;
		public void prepare(String name ,String gender,String present,String Pos,String Date){
		
			
			
			//this.id=Id;
			this.name=name;
			this.gender=gender;
			this.present=present;
			this.post=Pos;
			this.date=Date;
		}
		public void eidtPresentList(){
			Connection con=Database.getconnection();
			String q="UPDATE `presentlist` SET `id`=?,`Name`=?,`Gender`=?,`PresentList`=?,`Post`=?,`Date`=? WHERE id=?";
			try {
				PreparedStatement pStatement=con.prepareStatement(q);
				pStatement.setInt(1, this.id);
				pStatement.setString(1, this.name);
				pStatement.setString(2, this.gender);
				pStatement.setString(3, this.present);
				pStatement.setString(4,this.post);
				pStatement.setString(5, this.date);
				pStatement.execute();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}

}}
