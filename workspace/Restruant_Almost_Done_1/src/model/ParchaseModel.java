package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.table.DefaultTableModel;

import net.proteanit.sql.DbUtils;
import viwe.panel.AddWorker;
import viwe.panel.Parchase;
import viwe.panel.ParchaseReports;

public class ParchaseModel {
	public static String  bill;
	public static String ven;
	public static String pro;
	public static String cat;
	public static int totalPric;
	public static int qun;
	public static int pric;

	public void preare(String b, String v, String pro, String cat, int qun, int pric, int total) {
		this.bill = b;
		this.ven = v;
		this.pro = pro;
		this.cat = cat;
		this.totalPric = total;
		this.qun = qun;
		this.pric = pric;

	}

	public  void insart() {
		Connection conn = Database.getconnection();
		String q2= "INSERT INTO `parchase`( `BillNo`, `VendorName`, `Productname`, `Category`, `Quntity`, `PerPrice`, `TotalPrice`) VALUES (?,?,?,?,?,?,?)";
		
		try {
			PreparedStatement pStatement = conn.prepareStatement(q2);
			pStatement.setString(1, this.bill);
			pStatement.setString(2, this.ven);
			pStatement.setString(3, this.pro);
			pStatement.setString(4, this.cat);
			pStatement.setInt(5, this.qun);
			pStatement.setInt(6, this.pric);
			pStatement.setInt(7, this.totalPric);
			pStatement.execute();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void load(){
		Connection con=Database.getconnection();
		String q="SELECT * FROM `parchase`";
		try {
			PreparedStatement pStatement=con.prepareStatement(q);
			ResultSet set=pStatement.executeQuery();
			Parchase.table.setModel(DbUtils.resultSetToTableModel(set));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void Report(){
		Connection con=Database.getconnection();
		String q="SELECT * FROM `parchase`";
		try {
			PreparedStatement pStatement=con.prepareStatement(q);
			ResultSet set=pStatement.executeQuery();
			ParchaseReports.parchaseRep.setModel(DbUtils.resultSetToTableModel(set));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

public static void Search( String Id){
	Connection con=Database.getconnection();
	String q="SELECT * FROM `addworker` WHERE  workerId=?";
	try {
		PreparedStatement statement=con.prepareStatement(q);
		statement.setString(1, Id);
		ResultSet set=statement.executeQuery();
		AddWorker.WorkerDitails_Tab.setModel(DbUtils.resultSetToTableModel(set));
		statement.execute();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
}
