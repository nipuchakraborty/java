package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.jmx.LoadBalanceConnectionGroupManager;

import net.proteanit.sql.DbUtils;
import viwe.panel.SalarySheet;
import viwe.panel.WorkerDetails;

public class SalarySheetModel {
	public static String  id;
	public static String name;
	public static String gender;
	public static String present;
	public static String  post;
	public static String date;
	public void prepare(String id,String name ,String gender,String present,String Pos,String Date){
		//System.out.println(id+name+gender+present);
		
		
		this.id=id;
		this.name=name;
		this.gender=gender;
		this.present=present;
		this.post=Pos;
		this.date=Date;
	}
public void SalarySheetInseart(){
	Connection con=Database.getconnection();
	String q="INSERT INTO `salarysheet`(`WorkerID_No`, `Woker_name`, `Worker_Post`, `Total_Salary`, `Paid_Salary`, `Due`) VALUES (?,?,?,?,?,?)";
	try {
		PreparedStatement pStatement=con.prepareStatement(q);
		pStatement.setString(1, this.id);
		pStatement.setString(2, this.name);
		pStatement.setString(3, this.gender);
		pStatement.setString(4, this.present);
		pStatement.setString(5,this.post);
		pStatement.setString(6, this.date);
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}
public static void load (){
	Connection connection=Database.getconnection();
	String q="SELECT * FROM `salarysheet` ";
	try {
		PreparedStatement preparedStatement=connection.prepareStatement(q);
		ResultSet set=preparedStatement.executeQuery();
		SalarySheet.SalarySheetTab.setModel(DbUtils.resultSetToTableModel(set));
		preparedStatement.execute();
	} catch (SQLException e) {
	
		e.printStackTrace();
	}
	}

public static void search(String  id){
	Connection connection=Database.getconnection();
	String q="SELECT * FROM `presentlist` WHERE id=?";
	try {
		PreparedStatement preparedStatement=connection.prepareStatement(q);
		preparedStatement.setString(1,id);
		ResultSet set=preparedStatement.executeQuery();
		
		SalarySheet.SalarySheetTab.setModel(DbUtils.resultSetToTableModel(set));
		preparedStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	}

public static void loadWorker (String id){
	Connection connection=Database.getconnection();
	String q="SELECT * FROM `addworker` WHERE `workerId`=?";
	try {
		PreparedStatement preparedStatement=connection.prepareStatement(q);
		preparedStatement.setString(1, id);
		ResultSet set=preparedStatement.executeQuery();
		WorkerDetails.presentList.setModel(DbUtils.resultSetToTableModel(set));
		preparedStatement.execute();
	} catch (SQLException e) {
	
		e.printStackTrace();
	}
	}
public static void NameOfWorker (String id){
	Connection connection=Database.getconnection();
	String WorkerName;
	String postion = null;
	String q="SELECT `Name`,`Post` FROM `addworker` WHERE `workerId`=?";
	
	try {
		PreparedStatement preparedStatement=connection.prepareStatement(q);
		preparedStatement.setString(1, id);
		ResultSet set=preparedStatement.executeQuery();
		while(set.next()){
		WorkerName=set.getString("Name");
		postion=set.getString("Post");
		WorkerDetails.WorkerName_txt.setText(WorkerName);
		WorkerDetails.posiotn.setText(postion);
		}
	} catch (SQLException e) {
	
		e.printStackTrace();
	}


	
	/*try {
		
		PreparedStatement preparedStatement=connection.prepareStatement(q2);
		preparedStatement.setString(1,id );
		ResultSet set=preparedStatement.executeQuery();
		while(set.next()){
		post3=set.getString("`Post`");
		WorkerDetails.posiotn.setText(post3);
		}
	} catch (SQLException e) {
	
		e.printStackTrace();
	}
	}*/
}}
