package viwe;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import allPanel.AddProductCategoryPanel;
import allPanel.AddUnitPanel;
import allPanel.Products_panel;

import javax.swing.JDesktopPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class HomeScreen extends JFrame {
	private JDesktopPane desktopPane ;

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
					//UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
					/*UIManager.setLookAndFeel(
				            UIManager.getCrossPlatformLookAndFeelClassName());*/
					HomeScreen frame = new HomeScreen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public HomeScreen() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 992, 649);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnHome = new JMenu("Home");
		menuBar.add(mnHome);
		
		JMenuItem mntmAddProductCategory = new JMenuItem("Add Product Category");
		mntmAddProductCategory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame internalFrame=new JInternalFrame("ProductCategory",true,true,true,true);
				AddProductCategoryPanel addProductCategoryPanel=new AddProductCategoryPanel();
				internalFrame.getContentPane().add(addProductCategoryPanel);
				internalFrame.pack();
				
				desktopPane.add(internalFrame);
				internalFrame.setVisible(true);
				
				
			}
		});
		mnHome.add(mntmAddProductCategory);
		
		JMenuItem mntmAddProducts = new JMenuItem("Add Products");
		mntmAddProducts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame interFrame=new JInternalFrame("Add Products", true, true, true,true);
				Products_panel products_panel=new Products_panel();
				interFrame.getContentPane().add(products_panel);
				interFrame.pack();
				desktopPane.add(interFrame);
				interFrame.setVisible(true);
			}
		});
		mnHome.add(mntmAddProducts);
		
		JMenuItem mntmAddUnit = new JMenuItem("Add Unit");
		mntmAddUnit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame iFrame=new JInternalFrame("Add Unit", true, true, true);
				AddUnitPanel unitpanel=new AddUnitPanel();
				iFrame.getContentPane().add(unitpanel);
				iFrame.pack();
				desktopPane.add(iFrame);
				iFrame.setVisible(true);
			}
		});
		mnHome.add(mntmAddUnit);
		
		JMenu mnPurchase = new JMenu("purchase");
		menuBar.add(mnPurchase);
		
		JMenu mnSales = new JMenu("Sales");
		menuBar.add(mnSales);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		desktopPane = new JDesktopPane();
		contentPane.add(desktopPane, BorderLayout.CENTER);
	}
}
