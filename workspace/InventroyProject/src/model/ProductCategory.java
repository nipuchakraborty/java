package model;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class ProductCategory {
	public String CatName;
	public int status;
	public void prepareToInsert(String CatName,int status){
		this.CatName=CatName;
		this.status=status;
	
	}
	public void store(){
		Connection connection=DbConnect.getconnection();
		String query="INSERT INTO `product_category`(`Name`, `Status`) VALUES (?,?)";
		try {
			PreparedStatement preparedStatement=connection.prepareStatement(query);
			preparedStatement.setString(1, this.CatName);
			preparedStatement.setInt(2, this.status);
			preparedStatement.execute();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
