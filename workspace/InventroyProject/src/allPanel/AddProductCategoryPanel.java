package allPanel;

import javax.swing.JPanel;
import javax.swing.JInternalFrame;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;

import model.ProductCategory;

import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AddProductCategoryPanel extends JPanel {
	private JTextField category;
	private JTable table;

	/**
	 * Create the panel.
	 */
	public AddProductCategoryPanel() {
		setLayout(new MigLayout("", "[][][][grow][][][][grow]", "[][][][][][][][grow]"));
		
		JLabel lblCategory = new JLabel("Category");
		lblCategory.setFont(new Font("Tahoma", Font.PLAIN, 18));
		add(lblCategory, "cell 0 0");
		
		category = new JTextField();
		add(category, "cell 7 0,growx");
		category.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Status");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		add(lblNewLabel, "cell 0 2,aligny top");
		
		JComboBox comboBox = new JComboBox();
		add(comboBox, "cell 7 2,growx");
		
		JButton Add = new JButton("Add");
		Add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name=category.getText();
				String combox=comboBox.getSelectedItem().toString();
				ProductCategory productCategory=new ProductCategory();
				productCategory.load();
			}
		});
		add(Add, "cell 7 3");
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, "cell 0 7 8 1,grow");
		
		table = new JTable();
		scrollPane.setViewportView(table);

	}

}
