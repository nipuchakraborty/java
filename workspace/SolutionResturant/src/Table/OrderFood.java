package Table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.omg.CORBA.PUBLIC_MEMBER;



import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.nio.file.Files;
import java.security.AllPermission;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.beans.PropertyChangeEvent;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.table.DefaultTableModel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class OrderFood extends JInternalFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel label;
	private JDesktopPane desktopPane;
	public JComboBox cat_cb;
	public JComboBox st_cb;
	public String file;
	//variable declaration;
	int w = 750;
	int h = 400;
	private JInternalFrame jInternalFrame;
	private JDesktopPane desktopen;
	JPanel header, center, footer;
	Border border = new EtchedBorder();
	Font font = new Font("courier new", Font.PLAIN, 16);
	private JTextField price_txt;
	public String status;
	public static JTable BreakfastTab;
	public static JTable dinner_tab;
	public static JTable even_Tab;
	public static JTable lunch_tab;
	public static JComboBox FoodnameCb;
	public OrderFood() {
		
		setTitle("Add Food");
		setResizable(false);
		try {
			setIcon(true);
		} catch (PropertyVetoException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		setLocation(new Point(350, 100));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		setDoubleBuffered(true);
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		getContentPane().setBackground(new Color(139, 0, 0));
		getContentPane().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				Point move=null;
				Point Down=null;
				
			}
		});
		//Dimention Screen
		Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();
		//header Design
		header=new JPanel();
		header.setBorder(new EtchedBorder(EtchedBorder.LOWERED,null,null));
		header.setBackground(new Color(150, 199, 199));
		header.setPreferredSize(new Dimension(screen.width, 30));
		//center Design
		center=new JPanel();
		center.setPreferredSize(new Dimension(screen.width+50,50));
		center.setBackground(new Color(155, 200, 200));
		//foter design
		footer=new JPanel();
		//footer.setBackground(Color.blue);
		footer.setBackground(new Color(155, 200, 200));
		footer.setPreferredSize(new Dimension(screen.width, 50));

		//getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);
		setResizable(true);
		//setUndecorated(true);
		//getRootPane().setWindowDecorationStyle(JRootPane.WHEN_FOCUSED);
		//setLocation((screen.width - w) / 2, (screen.height - h) / 2);
		setSize(854, 590);
		getContentPane().setLayout(null);
		
		JLabel lblFoodCaegory = new JLabel("Food caegory:");
		lblFoodCaegory.setBounds(58, 66, 118, 24);
		lblFoodCaegory.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
		lblFoodCaegory.setForeground(new Color(245, 255, 250));
		lblFoodCaegory.setBackground(new Color(0, 0, 128));
		getContentPane().add(lblFoodCaegory);
		
		
		
		// Combobox
		ArrayList< String > cat=new ArrayList<>();
		
		cat_cb = new JComboBox();
		cat_cb.setModel(new DefaultComboBoxModel<>(cat.toArray()));
		
		cat_cb.setBounds(243, 62, 206, 28);
		
		
		
		
		cat_cb.setFont(new Font("Tahoma", Font.ITALIC, 18));
		//cat_cb.setModel(new DefaultComboBoxModel(new String[] {"Select Category", "Brakfast", "Evening Snacks", "Lunch", "Dinner"}));
		getContentPane().add(cat_cb);
		
		JPanel brakfast_pan = new JPanel();
		brakfast_pan.setBounds(454, 0, 384, 143);
		
			brakfast_pan.setBorder(new TitledBorder(null, "BrakFast", TitledBorder.CENTER, TitledBorder.TOP, null, null));
			getContentPane().add(brakfast_pan);
			brakfast_pan.setLayout(new BoxLayout(brakfast_pan, BoxLayout.X_AXIS));
			
			JScrollPane breakfast_scroll = new JScrollPane();
			brakfast_pan.add(breakfast_scroll);
			
	
			
			breakfast_scroll.setViewportView(BreakfastTab);
		 JPanel lunch = new JPanel();
		 lunch.setBounds(454, 143, 384, 127);
		 lunch.setBorder(new TitledBorder(null, "Lunch", TitledBorder.CENTER, TitledBorder.TOP, null, null));
		 getContentPane().add(lunch);
		 lunch.setLayout(new BoxLayout(lunch, BoxLayout.X_AXIS));
		 
		 JScrollPane scrollPane_1 = new JScrollPane();
		 lunch.add(scrollPane_1);
		 
		 lunch_tab = new JTable();
		 scrollPane_1.setViewportView(lunch_tab);
		 JLabel lblFoodName = new JLabel("Food Name::");
		 lblFoodName.setBounds(70, 167, 106, 24);
		 lblFoodName.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
		 lblFoodName.setForeground(new Color(240, 255, 240));
		 lblFoodName.setBackground(new Color(0, 0, 128));
		 getContentPane().add(lblFoodName);
		
		 FoodnameCb = new JComboBox();
		 FoodnameCb.setBounds(243, 160, 206, 31);
		 
		 	 	
		 	
		 	FoodnameCb.setModel(new DefaultComboBoxModel(new String[] {"Black Tea", "Green Tea", "Milk tea", "White Tea", "Leamon Tea", "Black Coffee"}));
		 	
		 	FoodnameCb.setFont(new Font("Tahoma", Font.PLAIN, 20));
		 	getContentPane().add(FoodnameCb);
		
		JLabel lblPrice = new JLabel("Food Price::");
		lblPrice.setBounds(74, 239, 102, 24);
		lblPrice.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
		lblPrice.setForeground(new Color(245, 255, 250));
		lblPrice.setBackground(new Color(0, 0, 128));
		getContentPane().add(lblPrice);
		
		price_txt = new JTextField();
		price_txt.setBounds(243, 234, 206, 29);
		price_txt.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		getContentPane().add(price_txt);
		price_txt.setColumns(10);
		
		JPanel dinner_pan = new JPanel();
		dinner_pan.setBounds(454, 264, 384, 143);
		dinner_pan.setBorder(new TitledBorder(null, "Dinner", TitledBorder.CENTER, TitledBorder.TOP, null, null));
		getContentPane().add(dinner_pan);
		dinner_pan.setLayout(new BoxLayout(dinner_pan, BoxLayout.X_AXIS));
		
		JScrollPane dinner_scrll = new JScrollPane();
		dinner_pan.add(dinner_scrll);
		
		dinner_tab = new JTable();
		dinner_scrll.setViewportView(dinner_tab);
		
		JLabel lblStatus = new JLabel("Status");
		lblStatus.setBounds(70, 316, 176, 24);
		lblStatus.setForeground(new Color(245, 255, 250));
		lblStatus.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
		lblStatus.setBackground(new Color(0, 0, 128));
		getContentPane().add(lblStatus);
		
		
		
		
		
		st_cb = new JComboBox();
		st_cb.setBounds(243, 311, 206, 28);
		st_cb.setFont(new Font("Tahoma", Font.ITALIC, 18));
		st_cb.setModel(new DefaultComboBoxModel(new String[] {"Select One", "Available", "Unavailable"}));
		getContentPane().add(st_cb);
		
		JButton btnEditData = new JButton("Edit data");
		btnEditData.setBounds(0, 513, 109, 23);
		getContentPane().add(btnEditData);
		
		JButton btnDeleteAllData = new JButton("Delete all Data");
		btnDeleteAllData.setBounds(345, 513, 104, 23);
		btnDeleteAllData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		
		//Button
		
		JButton btnAdd = new JButton("Add");
		btnAdd.setBounds(181, 513, 113, 23);
		
		btnAdd.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				String food=FoodnameCb.getSelectedItem().toString();
			
				String price=price_txt.getText();
				
				String category=cat_cb.getSelectedItem().toString();
				status=st_cb.getSelectedItem().toString();
				if (cat_cb.getSelectedItem().equals("Brakfast")) {
					food=FoodnameCb.getSelectedItem().toString();
					
					 price=price_txt.getText();
					
					 category=cat_cb.getSelectedItem().toString();
					
				
					
					
			}
				if (cat_cb.getSelectedItem().equals("Evening Snacks")) {
					
					food=FoodnameCb.getSelectedItem().toString();
					 price=price_txt.getText();
					 category=cat_cb.getSelectedItem().toString();
				
					
					
				}
				
			if (cat_cb.getSelectedItem().equals("Lunch")) {
				 food=FoodnameCb.getSelectedItem().toString();
				 price=price_txt.getText();
				 category=cat_cb.getSelectedItem().toString();
			
				
				
			}
			if (cat_cb.getSelectedItem().equals("Dinner")) {
				 food=FoodnameCb.getSelectedItem().toString();
				 price=price_txt.getText();
				 category=cat_cb.getSelectedItem().toString();
				
				
			}}
			
		});
		
		btnAdd.setFocusable(false);
		getContentPane().add(btnAdd);
		getContentPane().add(btnDeleteAllData);
		JPanel even_panel = new JPanel();
		even_panel.setBounds(454, 406, 384, 143);
		even_panel.setBorder(new TitledBorder(null, "Evening Snakcs", TitledBorder.CENTER, TitledBorder.TOP, null, null));
		getContentPane().add(even_panel);
		even_panel.setLayout(new BoxLayout(even_panel, BoxLayout.X_AXIS));
		
		JScrollPane scrollPane = new JScrollPane();
		even_panel.add(scrollPane);
		
		even_Tab = new JTable();
		scrollPane.setViewportView(even_Tab);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(OrderFood.class.getResource("/pictureResource/Foood.jpeg")));
		lblNewLabel.setBounds(0, -1147, 838, 1741);
		getContentPane().add(lblNewLabel);

		
		show();
}
	//public ImageIcon ResizeImage (String path) {
		//ImageIcon myImage=new ImageIcon(path);
		//Image image=myImage.getImage();
		//Image newiamge=image.getScaledInstance(picture_labe.getWidth(),picture_labe.getHeight(),Image.SCALE_REPLICATE);
		//ImageIcon imageIcon=new ImageIcon(newiamge);
		//return imageIcon;
		
	//}
}
