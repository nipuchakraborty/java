package Table;

//*******************************************************************
//
//   Printer.java          In Text          Application
//
//   Authors:  Lewis and Loftus
//
//   Classes:  Printer
//             File
//             Text_File
//             Binary_File
//             Image_File
//             Print_Logger
//
//*******************************************************************


//-------------------------------------------------------------------
//
//  Class Printer demonstrates the use of an abstract class.
//
//  Methods:
//
//     public static void main (String[] args)
//
//-------------------------------------------------------------------

public class Printer {

   //===========================================================
   //  Creates two file objects and logs some printing
   //  activity.
   //===========================================================
   public static void main (String[] args) {

      byte[] logo_data  = {41, 42, 49, 44};

      Text_File report = new Text_File 
         ("Sand Reconner", 66, "One two three");

      Image_File logo = new Image_File 
         ("Number 1", 45, logo_data);

      Print_Logger daily = new Print_Logger();

      daily.log(report);
      daily.log(logo);

   } // method main

} // class Printer

//-------------------------------------------------------------------
//
//  Class File represents a generic file.  Classes derived from it
//  will give a definition for the abstract print method.
//
//  Constructors:
//
//     public File (String file_id, int file_size)
//
//  Methods:
//
//     public String name()
//     abstract public String print()
//
//-------------------------------------------------------------------

abstract class File {

   protected String id;
   protected int size;

   //===========================================================
   //  Sets up the object using the specified data.
   //===========================================================
   public File (String file_id, int file_size) {
      id = file_id;
      size = file_size;
   } // constructor File

   //===========================================================
   //  Returns the name of the file.
   //===========================================================
   public String name() {
      return id;
   } // method name

   //===========================================================
   //  Should be implemented to return the contents of the
   //  file as a string for printing.
   //===========================================================
   abstract public String print();

} // class File

//-------------------------------------------------------------------
//
//  Class Text_File represents a file that contains text.
//
//  Constructors:
//
//     public Text_File (String id, int size, String file_contents)
//
//  Methods:
//
//     public String print()
//
//-------------------------------------------------------------------

class Text_File extends File {

   protected String text;

   //===========================================================
   //  Sets up the object using the specified data.
   //===========================================================
   public Text_File (String id, int size, String file_contents) {
      super(id, size);
      text = file_contents;
   } // constructor Text_File

   //===========================================================
   //  Returns the file contents for printing.
   //===========================================================
   public String print() {
      return text;
   } // method print

} // class Text_File

//-------------------------------------------------------------------
//
//  Class Binary_File represents a file that contains binary data.
//
//  Constructors:
//
//     public Binary_File (String id, int size, byte[] file_data)
//
//  Methods:
//
//     public String print()
//
//-------------------------------------------------------------------

class Binary_File extends File {

   protected byte[] data;

   //===========================================================
   //  Sets up the object using the specified data.
   //===========================================================
   public Binary_File (String id, int size, byte[] file_data) {
      super(id, size);
      data = file_data;
   } // constructor Binary_File

   //===========================================================
   //  Returns a string version of the data for printing.
   //===========================================================
   public String print() {
      return "";
   } // method print

} // class Binary_File

//-------------------------------------------------------------------
//
//  Class Image_File represents a file that contains an image.
//
//  Constructors:
//
//     public Image_File (String id, int size, byte[] file_data)
//
//  Methods:
//
//     public String print()
//
//-------------------------------------------------------------------

class Image_File extends Binary_File {

   //===========================================================
   //  Sets up the object using the specified data.
   //===========================================================
   public Image_File (String id, int size, byte[] file_data) {
      super(id, size, file_data);
   } // constructor Image_File

   //===========================================================
   //  Returns a string version of the data for printing.
   //===========================================================
   public String print() {
      return new String (data);
   }  // method print

}  // class Image_File

//-------------------------------------------------------------------
//
//  Class Print_Logger monitors printing activity.
//
//  Methods:
//
//     public void log (File file)
//
//-------------------------------------------------------------------

class Print_Logger {

   //===========================================================
   //  Prints a standard message about printing activity.
   //===========================================================
   public void log (File file) {
      System.out.println (file.name() + " : " + file.print());
   }  // method log

} 