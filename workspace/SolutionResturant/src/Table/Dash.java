package Table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;



import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Window.Type;

public class Dash extends JFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane ;

	
	public static void main(String[] args) {
		 {
			 Dash frame = new Dash();
				frame.setVisible(true);
				
				try {
					
					//UIManager.setLookAndFeel("com.syntheticaLookAndFeel");
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnsupportedLookAndFeelException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		 }
	}

	int w = 1600;
	int h = 900;
	
	public Dash() {
		setTitle("Restrurant Management");
		//setIconImage(Toolkit.getDefaultToolkit().getImage(Dash.class.getResource("/pictureResource/food-grey.png")));
		setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
	
		setBounds(0, 0, w, h);
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screen.width - w) / 2, (screen.height - h) / 2);
		setSize(w, h);
	
		setPreferredSize(new Dimension(screen.width + 50, 50));
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("File");
		menuBar.add(mnNewMenu);
		
		JMenu mnOpen = new JMenu("Open");
		mnNewMenu.add(mnOpen);
		
		JMenuItem mntmAddfood = new JMenuItem("AddFood");
		mntmAddfood.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
			}
		});
		mnOpen.add(mntmAddfood);
		
		JMenuItem mntmDailyReport = new JMenuItem("Daily Report");
		mntmDailyReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				
				
			}
		});
		mnOpen.add(mntmDailyReport);
		
		JMenuItem mntmOrder = new JMenuItem("Take Order");
		mntmOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
			}
		});
		mnOpen.add(mntmOrder);
		
		JMenuItem mntmParchase = new JMenuItem("Parchase");
		mntmParchase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
			}
		});
		mnOpen.add(mntmParchase);
		
		JMenuItem mntmSeatPlane = new JMenuItem("Seat Plane");
		mntmSeatPlane.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
			}
		});
		mnOpen.add(mntmSeatPlane);
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mnNewMenu.add(mntmSave);
		
		JMenu mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);
		
		JMenu mnCalculator = new JMenu("Calculator");
		menuBar.add(mnCalculator);
		
		JMenuItem mntmCalculator = new JMenuItem("Calculator");
		mntmCalculator.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		mnCalculator.add(mntmCalculator);
		
		JMenu mnAbout = new JMenu("About");
		
		menuBar.add(mnAbout);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				About about=new About();
				about.setVisible(true);
				desktopPane.add(about);
			}
		});
		mnAbout.add(mntmAbout);
		
		JMenuItem mntmContact = new JMenuItem("Contact");
		mntmContact.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame jFrame=new JInternalFrame("Contact",true,true ,true ,true);
			
				
			}
		});
		mnAbout.add(mntmContact);
		
		JMenu mnUpdate = new JMenu("Update");
		menuBar.add(mnUpdate);
		
		JMenuItem mntmOpen = new JMenuItem("Open");
		mntmOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				About about=new About();
				about.setVisible(true);
				desktopPane.add(about);
				
			}
		});
		mnUpdate.add(mntmOpen);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				//image=(Toolkit.getDefaultToolkit().getImage(Dash.class.getResource("/pictureResource/Backgorund.jpg")));
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(null);
		
	}
}
