package viwe.Login;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import Model.DB;

public class LoginModels {
	public static String username;
	public static String password;
	public static  String position;
	public static String picture;
	
	
	public LoginModels(String userName, String pass, String position, String pic) {
		
		this.username=userName;//user name
		this.password=pass;//password 
		this.position=position;// postion 
		this.picture=pic;// picture 
		
	}
	public void loginModels(){
		
		DB db=new DB();
		Connection connection=db.getconnection();
		String q="SELECT   `userName`, `password`,  `position`, `picture` FROM `new_account` WHERE `userName`=? AND `password`=? AND `position`=? AND `picture`=?";
		try {
			PreparedStatement ps=connection.prepareStatement(q);
			ps.setString(1, this.username);
			ps.setString(2, this.password);
			ps.setString(3, this.position);
			ps.setString(4, this.picture);
			System.out.println(ps);
			ps.executeQuery();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}

}
