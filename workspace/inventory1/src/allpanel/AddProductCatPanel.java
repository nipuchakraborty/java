package allpanel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;

import controller.AddproductCategory;

import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;

public class AddProductCatPanel extends JPanel {
	private JTextField name_textField;
	private JTable table;

	/**
	 * Create the panel.
	 */
	public AddProductCatPanel() {
		setLayout(new MigLayout("", "[][][][][][][][grow][][][grow]", "[][][][][][][grow]"));
		
		JLabel lblName = new JLabel("Name");
		add(lblName, "cell 0 0");
		
		name_textField = new JTextField();
		add(name_textField, "cell 10 0,growx");
		name_textField.setColumns(10);
		
		JLabel lblStatus = new JLabel("Status");
		add(lblStatus, "cell 0 2");
		
		JComboBox statuscomboBox = new JComboBox();
		statuscomboBox.setModel(new DefaultComboBoxModel(new String[] {"", "Avilable", "Unavialbe"}));
		add(statuscomboBox, "cell 10 2,growx");
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			String name=name_textField.getText();
			String status=statuscomboBox.getSelectedItem().toString();
			AddproductCategory aP=new AddproductCategory(name,status);
			
			}
		});
		add(btnSave, "cell 10 4");
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, "cell 0 6 11 1,grow");
		
		table = new JTable();
		scrollPane.setViewportView(table);

	}

}
