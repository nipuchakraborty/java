package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.ComboBoxEditor;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AddProductCategory_frame extends JFrame {

	private JPanel contentPane;
	private JTextField name_txtF;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");

					AddProductCategory_frame frame = new AddProductCategory_frame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddProductCategory_frame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 621, 537);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[][grow][grow]", "[][][][][][][][grow]"));
		
		JLabel lblName = new JLabel("Name");
		contentPane.add(lblName, "cell 0 1");
		
		name_txtF = new JTextField();
		contentPane.add(name_txtF, "cell 2 1,growx");
		name_txtF.setColumns(10);
		
		JLabel lblStatus = new JLabel("Status");
		contentPane.add(lblStatus, "cell 0 4");
		
		JComboBox status_combox = new JComboBox();
		status_combox.setModel(new DefaultComboBoxModel(new String[] {"Select one", "Avilable", "Not avilable"}));
		contentPane.add(status_combox, "cell 2 4,growx");
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String pname=name_txtF.getText();
				String combo=status_combox.getSelectedItem().toString();
			
			}
		});
		contentPane.add(btnSave, "cell 2 6");
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, "cell 0 7 3 1,grow");
		
		table = new JTable();
		scrollPane.setViewportView(table);
	}

}
