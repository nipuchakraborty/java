package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import allpanel.AddProductCatPanel;

import javax.swing.JDesktopPane;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class HomeScreenJframe extends JFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
					//UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
					//UIManager.setLookAndFeel("Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel MyApp");
					//UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
					HomeScreenJframe frame = new HomeScreenJframe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public HomeScreenJframe() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 988, 666);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnHome = new JMenu("Home");
		menuBar.add(mnHome);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Add Prodouct category");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame rame= new JInternalFrame("product category",true,true,true,true);
				
				AddProductCatPanel addProductCatPanel=new AddProductCatPanel();
				
				rame.getContentPane().add(addProductCatPanel,BorderLayout.CENTER);
				rame.pack();
				desktopPane.add(rame);
			
				
				
				
				
				rame.setVisible(true);
			}

			private AddProductCategory_frame AddProductCategory_frame() {
				// TODO Auto-generated method stub
				return null;
			}
		});
		mnHome.add(mntmNewMenuItem);
		
		JMenuItem mntmAddProduct = new JMenuItem("Add Product");
		mnHome.add(mntmAddProduct);
		
		JMenuItem mntmAddUnit = new JMenuItem("Add Unit");
		mnHome.add(mntmAddUnit);
		
		JMenu mnNewMenu = new JMenu("Purchase");
		menuBar.add(mnNewMenu);
		
		JMenu mnSell = new JMenu("Sell");
		menuBar.add(mnSell);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		desktopPane = new JDesktopPane();
		desktopPane.setBackground(Color.CYAN);
		contentPane.add(desktopPane, BorderLayout.CENTER);
	}
}
