package Controller;

import Model.Product_catergory;

public class DeleteCat {

	public static void DeleteCat(String get_cat_name,String get_catST,int id ){
		int status;
		
		if(get_catST.equals("Available")){
			status=0;
		}
		else{
			status=1;
		}

		Product_catergory Product_catergory = new Product_catergory();
		Product_catergory.prepare_for_send_category(get_cat_name, status, id);
		Product_catergory.Delete(id);
		
	}

}
