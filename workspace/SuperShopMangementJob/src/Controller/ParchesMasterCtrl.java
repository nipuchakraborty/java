package Controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import Model.PharchesMaster;
import View.All_panel.Purchase_panel;


public class ParchesMasterCtrl {

	public ParchesMasterCtrl(Date date, String vendorName, String total, String paid, String due) {
		
		SimpleDateFormat is=new SimpleDateFormat("YYYY-MM-dd");
		String dat=is.format(date);
		String vn=vendorName;
		int t_am = Integer.parseInt(total);
		int p_am = Integer.parseInt(paid);
		int d_am = Integer.parseInt(due);
		int st=0;
		
		if(Purchase_panel.chckbxBalanced.isSelected()){
			st=0;
		}
		else if(paid.equals(0)){
			st=0;
		}
		else{
			st=1;
		}
		
		
		PharchesMaster pharchesMaster=new PharchesMaster();
		pharchesMaster.Prepare_to_send(dat, vn, t_am, p_am, d_am);
		pharchesMaster.inseart();
		
	}
	

}
