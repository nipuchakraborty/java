package Controller;

import Model.Product_catergory;
import Model.Products;
import Model.Unit;
import Model.Helper_Method;




public class UpdateAddProduct{
	
	public UpdateAddProduct(String product, String category, String unit, String status, int id) {
		
		int cat_id=Product_catergory.get_cat_id(category);
		int unit_id=Unit.get_unit_id(unit);
		int st=Helper_Method.changeStatus(status);
		
		Products pro=new Products();
		pro.prepare_to_inseart_database(product, cat_id, unit_id, st);
		pro.UpdateProduct(id);
		pro.load();
		
	}

}
