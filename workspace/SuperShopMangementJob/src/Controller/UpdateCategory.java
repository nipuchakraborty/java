package Controller;

import java.beans.DefaultPersistenceDelegate;

import Model.Product_catergory;
import View.All_panel.Add_category;

public class UpdateCategory {
	public UpdateCategory(String get_cat_name,String get_catST,int id ){
		int status;
		
		if(get_catST.equals("Available")){
			status=0;
		}
		else{
			status=1;
		}

		Product_catergory Product_catergory = new Product_catergory();
		Product_catergory.prepare_for_send_category(get_cat_name, status, id);
		Product_catergory.Update(id);
		
	}

}
