package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import View.All_panel.Purchase_panel;
import View.All_panel.AddUnit_panel;
import net.proteanit.sql.DbUtils;

public class Sales {
	
	private String date;
	private String vn;
	private int cate_id;
	private int pdt_id;
	private int qun;
	private int buy;
	private int sell;
	private int total_price;
	public void prepare_for_sales( int pd_id, int qnt, int buy_p, int totalprice,int sell_p){
		
		this.pdt_id=pd_id;
		this.qun=qnt;
		this.buy=buy_p;
		this.total_price=qun*sell_p;
		this.sell=sell_p;
	}
	
	
	
	public void snd_to_db_purchase(){
		Connection is=Database.getconnection();
		
		String Quary="INSERT INTO `temp`( `Product_id`, `Quantity`, `Buy_price`, `Total_price`, `Sell_price`) VALUES (?,?,?,?,?)";
		
		try{
			PreparedStatement ps=is.prepareStatement(Quary);
			ps.setInt(1, this.pdt_id);
			ps.setInt(2, this.qun);
			ps.setInt(3, this.buy);
			ps.setInt(4, this.total_price);
			ps.setInt(5, this.sell);
			ps.execute();
		}
		catch(Exception x){
			x.printStackTrace();
		}
	}
	public int SumTotal(){
		Connection is=Database.getconnection();
		int total=0;
		String sql="SELECT sum(`Total_price`) as total_price FROM `temp`";
		try{
			PreparedStatement pStatement=is.prepareStatement(sql);
			
			pStatement.execute();
			ResultSet rSet=pStatement.executeQuery();
			while (rSet.next()){
				total=rSet.getInt("sum(`Total_price`)");
			}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		return total;
	}
	
	public void Trancate_temp_table(){
		Connection is=Database.getconnection();
		
		String command="TRUNCATE TABLE `temp` ";
		
		try{
			PreparedStatement ps=is.prepareStatement(command);
			ps.execute();
		}
		catch(Exception x){
			x.printStackTrace();
		}
	}
	
	
	
	
	public void load(){
		Connection is=Database.getconnection();
		
		String command="SELECT  a.Name as Product,b.Name as Category,d.Quantity AS Quantity, d.Total_price As Total,d.Sell_price AS SalePrice, h.Name as Unit,(CASE a.status WHEN 1 THEN 'Avialable' ELSE 'NOT Available' END) as Status from products a,product_catagory b,unit h ,temp d WHERE a.unit_id=h.id AND a.Catagory_id=b.id";
		
		try{
			PreparedStatement ps=is.prepareStatement(command);
			ResultSet rs=ps.executeQuery();
			Purchase_panel.table.setModel(DbUtils.resultSetToTableModel(rs));
		}
		catch(Exception x){
			x.printStackTrace();
		}

}
}
