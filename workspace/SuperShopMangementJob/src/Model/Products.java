package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import View.All_panel.AddProduct;
import View.All_panel.AddUnit_panel;
import net.proteanit.sql.DbUtils;

public class Products {
	private String snd_name;
	private int cat_id;
	private int unit_id;
	private int st;
	
	public void prepare_to_inseart_database(String products,int Cat_id,int unit_id,int status){
		
		this.snd_name=products;
		this.st=status;
		this.unit_id=unit_id;
		this.cat_id=Cat_id;
		
		
		
		
	}
	public void Delete(int id){
		Connection is=Database.getconnection();
		
		String command="DELETE FROM `products` WHERE `id`=? AND `Name`=? AND`Status`=? AND `unit_id`=? AND`Catagory_id`=?";
		
		try{
			PreparedStatement ps=is.prepareStatement(command);
			ps.setInt(1, id);
			ps.setString(2, this.snd_name);
			ps.setInt(3, this.st);
			ps.setInt(4, this.unit_id);
			ps.setInt(5, this.cat_id);
			ps.execute();
		}
		catch(Exception x){
			x.printStackTrace();
		}
		
		
	}
	
	
	
	
	
	public void UpdateProduct(int id){
		Connection is=Database.getconnection();
		
		String Upadate_Query="UPDATE `products` SET `Name`=?,`Status`=?,`unit_id`=?,`Catagory_id`=? WHERE id=?";
		
		try{
			PreparedStatement ps=is.prepareStatement(Upadate_Query);
			ps.setString(1, this.snd_name);
			ps.setInt(2, this.st);
			ps.setInt(3, this.unit_id);
			ps.setInt(4, this.cat_id);
			ps.setInt(5, id);
			
			ps.execute();
		}
		catch(Exception x){
			x.printStackTrace();
		}
	}
	
	
	
	public void snd_to_db_product(){
		Connection is=Database.getconnection();
		
		String qrary_of_Inseart_Product="INSERT INTO `products`(`Name`, `Status`, `unit_id`, `Catagory_id`) VALUES (?,?,?,?)";
		
		try{
			PreparedStatement ps=is.prepareStatement(qrary_of_Inseart_Product);
			ps.setString(1, this.snd_name);
			ps.setInt(2, this.st);
			ps.setInt(3, this.unit_id);
			ps.setInt(4, this.cat_id);
			
			ps.execute();
		}
		catch(Exception x){
			x.printStackTrace();
		}
	}
	
	public static void load(){
		Connection is=Database.getconnection();
		
		String command="SELECT a.Name as Product,b.Name as Category,c.Name as Unit,(CASE a.status WHEN 0 THEN 'Avialable' ELSE 'Unavailable' END) as Status from products a,product_catagory b,unit c WHERE a.unit_id=c.id AND a.Catagory_id=b.id";
		
		try{
			PreparedStatement ps=is.prepareStatement(command);
			ResultSet rs=ps.executeQuery();
			AddProduct.product_table.setModel(DbUtils.resultSetToTableModel(rs));
			
		}
		catch(Exception x){
			x.printStackTrace();
		}
		
	}
	
	
public static ArrayList<String> return_product_cat_wise(int Category_id){
		
		ArrayList<String> cat_wise_product=new ArrayList<>();
		Connection is=Database.getconnection();
		String command="SELECT * FROM products WHERE Catagory_id=?";
		
		try{
			PreparedStatement ps=is.prepareStatement(command);
			ps.setInt(1, Category_id);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				cat_wise_product.add(rs.getString("Name")); 
			}
			
		}
		catch(Exception e){
			
		}
		return cat_wise_product;
	}







public static int get_pd_id(String give_pd_name){
	Connection obj=Database.getconnection();
	
	String comnd="SELECT id FROM products WHERE Name=?";
	
	int pd_id=0;
	
	try{
		PreparedStatement ps=obj.prepareStatement(comnd);
		ps.setString(1,give_pd_name);
		ResultSet rs=ps.executeQuery();
		
		while(rs.next()){
			pd_id=rs.getInt("id");	
		}
		
		
	}
	catch(Exception e){
		
	}
	
	return pd_id;
}
public static int get_unit_id_by_product(String give_pd_name){
	Connection obj=Database.getconnection();
	
	String comnd="SELECT `unit_id` FROM `products` WHERE Name=?";
	
	int un_id=0;
	
	try{
		PreparedStatement ps=obj.prepareStatement(comnd);
		ps.setString(1,give_pd_name);
		ResultSet rs=ps.executeQuery();
		
		while(rs.next()){
			un_id=rs.getInt("unit_id");	
		}
		
		
	}
	catch(Exception e){
		
	}
	
	return un_id;
}
}
