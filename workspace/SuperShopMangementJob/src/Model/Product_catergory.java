package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import View.All_panel.Add_category;
import net.proteanit.sql.DbUtils;

public class Product_catergory {
	
	private String snd_name;
	private int snd_st;
	private int id;
	
	public void prepare_for_send_category(String send_name,int send_status,int id){
		
		this.snd_name=send_name;
		this.snd_st=send_status;
		this.id=id;
		
	}
	public void UpdateProductcategory(int id){
		Connection is=Database.getconnection();
		
		String command="UPDATE `product_catagory` SET `Name`=?,`Status`=?, `Catagory_id`=?,`unit_id`=? WHERE id=?";
		
		try{
			PreparedStatement ps=is.prepareStatement(command);
			ps.setString(1, this.snd_name);
			ps.setInt(2, this.snd_st);
			ps.setInt(3, this.id);
			ps.execute();
		}
		catch(Exception x){
			x.printStackTrace();
		}
		
		
	}

public void Delete(int id){
	Connection is=Database.getconnection();
	
	String command="DELETE FROM product_catagory WHERE  `Name`=? AND `Status`=? AND `id`=?";
	
	try{
		PreparedStatement ps=is.prepareStatement(command);
		ps.setString(1, this.snd_name);
		ps.setInt(2, this.snd_st);
		ps.setInt(3, this.id);
		ps.execute();
	}
	catch(Exception x){
		x.printStackTrace();
	}
	
	
}
	public void Update(int id){
		Connection is=Database.getconnection();
		
		String command="UPDATE `product_catagory` SET `Name`=?,`Status`=? WHERE id=?";
		
		try{
			PreparedStatement ps=is.prepareStatement(command);
			ps.setString(1, this.snd_name);
			ps.setInt(2, this.snd_st);
			ps.setInt(3, this.id);
			ps.execute();
		}
		catch(Exception x){
			x.printStackTrace();
		}
		
		
	}
	public void snd_to_db_cat(){
		Connection is=Database.getconnection();
		
		String command="INSERT INTO `product_catagory`(`Name`, `Status`) VALUES (?,?)";
		
		try{
			PreparedStatement ps=is.prepareStatement(command);
			ps.setString(1, this.snd_name);
			ps.setInt(2, this.snd_st);
			ps.execute();
		}
		catch(Exception x){
			x.printStackTrace();
		}
	}
	
	public void load(){
		Connection is=Database.getconnection();
		
		String command="SELECT name ,(CASE status WHEN 1 THEN 'Available' ELSE 'Not Available' END) as Status FROM `product_catagory`";
		
		try{
			PreparedStatement ps=is.prepareStatement(command);
			ResultSet rs=ps.executeQuery();
			Add_category.table.setModel(DbUtils.resultSetToTableModel(rs));
		}
		catch(Exception x){
			x.printStackTrace();
		}
		
	}
	
public static ArrayList<String> getAllCat(){
		
		ArrayList<String> all_cat=new ArrayList<>();
		Connection is=Database.getconnection();
		String command="SELECT * FROM product_catagory";
		
		try{
			PreparedStatement ps=is.prepareStatement(command);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				all_cat.add(rs.getString("Name")); 
			}
			
		}
		catch(Exception e){
			
		}
		return all_cat;
	}



public static int get_cat_id(String give_cat_name){
	Connection obj=Database.getconnection();
	
	String comnd="SELECT id FROM product_catagory WHERE Name=?";
	
	int cat_id=0;
	
	try{
		PreparedStatement ps=obj.prepareStatement(comnd);
		ps.setString(1,give_cat_name);
		ResultSet rs=ps.executeQuery();
		
		while(rs.next()){
			cat_id=rs.getInt("id");	
		}
		
		
	}
	catch(Exception e){
		
	}
	
	return cat_id;
}


}
