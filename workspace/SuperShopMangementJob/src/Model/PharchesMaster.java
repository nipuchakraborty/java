package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.omg.CORBA.PUBLIC_MEMBER;

public class PharchesMaster {
	public static  String Vendor ;
	public String date;
	
	public String vendor ;
	public int total;
	public int paid ;
	public int due;
	public int status;
	public  void Prepare_to_send(String date, String vendor, int total,int paid ,int due) {
		this.date=date;
		this.vendor = vendor;
		this.paid=paid;
		this.due=due;
		this.status=status;
		
		
	}
	public void inseart(){
		Connection connection=Database.getconnection();
		String q="INSERT INTO `parchesmaster`(`date`, `VendorName`, `Total_amount`, `pay`, `due`, `Status`) VALUES ([?,?,?,?,?,?)";
		
		try {
			PreparedStatement preparedStatement=connection.prepareStatement(q);
			preparedStatement.setString(1, this.date);
			preparedStatement.setString(2, this.vendor);
			preparedStatement.setInt(3, this.total);
			preparedStatement.setInt(4, this.paid);
			preparedStatement.setInt(5, this.due);
			preparedStatement.setInt(6, this.status);
			preparedStatement.execute();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}

	public static int get_masterId() {
		Connection connection = Database.getconnection();
		String Sql = "SELECT id FROM parchesmaster ORDER BY id DESC limit 1";
		int getLastID = 0;
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(Sql);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				getLastID = resultSet.getInt("ID");

			}
		} catch (Exception e) {
			
		}
		return getLastID;

	}
}
