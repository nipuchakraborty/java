package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Controller.LoginCtrl;
import View.Dashboard_JFrm;

public class LoginModel {
	public static int id;
	public static String Usn;
	public static String pas;

	public void prepare(String Un, String Pa) {

		this.Usn = Un;
		this.pas = Pa;
	}

	public String SuperAddmin() {
		String User = null;
		String pass = null;
		Connection is = Database.getconnection();
		String sql = "SELECT `UserName` ,`password` FROM `SuperAddmin` WHERE `UserName`=? AND `password`=?";
		try {
			PreparedStatement pStatement = is.prepareStatement(sql);
			pStatement.setString(1, this.Usn);
			pStatement.setString(2, this.pas);
			pStatement.execute();
			ResultSet resultSet = pStatement.executeQuery();
			while (resultSet.next()) {
				User = resultSet.getString("UserName");
				pass = resultSet.getString("password");
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return User;

	}

	public String SuperAddminName() {
		
		String User = null;
		Connection is = Database.getconnection();
		String sql = "SELECT `UserName` FROM `SuperAddmin` WHERE `UserName`=?";
		try {
			PreparedStatement pStatement = is.prepareStatement(sql);
			pStatement.setString(1, this.Usn);

			pStatement.execute();
			ResultSet resultSet = pStatement.executeQuery();
			while (resultSet.next()) {
				User = resultSet.getString("UserName");

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return User;

	}

	public String SuperAddminpassword() {

		String pass = null;
		Connection is = Database.getconnection();
		String sql = "SELECT `password` FROM `SuperAddmin` WHERE `password`=?";
		try {
			PreparedStatement pStatement = is.prepareStatement(sql);

			pStatement.setString(1, this.pas);
			pStatement.execute();
			ResultSet resultSet = pStatement.executeQuery();
			while (resultSet.next()) {
				pass = resultSet.getString("`password`");
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return pass;

	}

	public String addminName() {
		String User = null;

		Connection is = Database.getconnection();
		String sql = "SELECT `UserName`  FROM `Admin` WHERE `UserName`=?";
		try {
			PreparedStatement pStatement = is.prepareStatement(sql);
			pStatement.setString(1, this.Usn);

			pStatement.execute();
			ResultSet resultSet = pStatement.executeQuery();
			while (resultSet.next()) {
				User = resultSet.getString("UserName");

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return User;

	}

	public String addminpasword() {

		String pass = null;
		Connection is = Database.getconnection();
		String sql = "SELECT  `password` FROM `Admin` WHERE  `password`=?";
		try {
			PreparedStatement pStatement = is.prepareStatement(sql);

			pStatement.setString(1, this.pas);
			pStatement.execute();
			ResultSet resultSet = pStatement.executeQuery();
			while (resultSet.next()) {

				pass = resultSet.getString("`password`");
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return pass;

	}

	public String userName() {
		String User ="";

		Connection is = Database.getconnection();
		String sql = "SELECT `UserName` FROM `User` WHERE `UserName`=?";
		try {
			PreparedStatement pStatement = is.prepareStatement(sql);
			pStatement.setString(1, this.Usn);

			pStatement.execute();
			ResultSet resultSet = pStatement.executeQuery();
			while (resultSet.next()) {
				User = resultSet.getString("UserName");

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return User;

	}

	public String userpassword() {

		String pass = null;
		Connection is = Database.getconnection();
		String sql = "SELECT `password` ,`password` FROM `User` WHERE AND`password`=?";
		try {
			PreparedStatement pStatement = is.prepareStatement(sql);

			pStatement.setString(1, this.pas);
			pStatement.execute();
			ResultSet resultSet = pStatement.executeQuery();
			while (resultSet.next()) {

				pass = resultSet.getString("password");
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return pass;

	}
	
	public String Addmin() {
		String User = null;
		String pass = null;
		Connection is = Database.getconnection();
		String sql = "SELECT `UserName`, `password` FROM `admin` WHERE `UserName`=? AND`password`=?";
		try {
			PreparedStatement pStatement = is.prepareStatement(sql);
			pStatement.setString(1, this.Usn);
			pStatement.setString(2, this.pas);
			pStatement.execute();
			ResultSet resultSet = pStatement.executeQuery();
			while (resultSet.next()) {
				User = resultSet.getString("UserName");
				pass = resultSet.getString("password");
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return User;

	}
	
	public String user() {
		String User = null;
		String pass = null;
		Connection is = Database.getconnection();
		String sql = "SELECT  `UserName`, `password` FROM `user` WHERE `UserName`=? AND `password`=?";
		try {
			PreparedStatement pStatement = is.prepareStatement(sql);
			pStatement.setString(1, this.Usn);
			pStatement.setString(2, this.pas);
			pStatement.execute();
			ResultSet resultSet = pStatement.executeQuery();
			while (resultSet.next()) {
				User = resultSet.getString("UserName");
				pass = resultSet.getString("password");
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return User;

	}
	

}
