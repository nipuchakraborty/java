package View.All_dialouge;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controller.DeleteCat;
import Controller.UpdateCategory;
import Model.Product_catergory;
import Model.Status;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class Edit_Update_Category_dialouge extends JDialog {

	private final JPanel contentPanel = new JPanel();
	public static JTextField product_cate_txt;
	public static JComboBox comboBox_Satatus;

	public Edit_Update_Category_dialouge(String get1stSelectedcomumn, String get2ndstSelectedcomumn, int id) {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel lblProductCategory = new JLabel("Product Category");
		lblProductCategory.setBounds(36, 13, 109, 16);
		contentPanel.add(lblProductCategory);

		JLabel lblStatus = new JLabel("Status");
		lblStatus.setBounds(36, 67, 56, 16);
		contentPanel.add(lblStatus);

		product_cate_txt = new JTextField();
		product_cate_txt.setBounds(174, 13, 154, 22);
		contentPanel.add(product_cate_txt);
		product_cate_txt.setColumns(10);
		product_cate_txt.setText(get1stSelectedcomumn);
		Status Status = new Status();
		ArrayList<String> status = new ArrayList<>();
		status = Status.return_status();
		comboBox_Satatus = new JComboBox();
		comboBox_Satatus.setModel(new DefaultComboBoxModel<>(status.toArray()));
		comboBox_Satatus.setBounds(174, 75, 154, 22);
		contentPanel.add(comboBox_Satatus);
		comboBox_Satatus.setSelectedItem(get2ndstSelectedcomumn);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Update");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String catName = product_cate_txt.getText();
						String st = (String) comboBox_Satatus.getSelectedItem();
						new Controller.UpdateCategory(catName, st, id);// catrgoryName
																		// ,
																		// status,
																		// id
						Product_catergory product_catergory = new Product_catergory();
						product_catergory.load();
						Edit_Update_Category_dialouge.this.dispose();

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Delete");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String catName=product_cate_txt.getText();
						String st=(String)comboBox_Satatus.getSelectedItem();
						DeleteCat deleteCat=new DeleteCat();
						deleteCat.DeleteCat(catName, st, id);
				Product_catergory product_catergory=new Product_catergory();
				product_catergory.Delete(id);
				product_catergory.load();
				Edit_Update_Category_dialouge.this.dispose();
				
						
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
