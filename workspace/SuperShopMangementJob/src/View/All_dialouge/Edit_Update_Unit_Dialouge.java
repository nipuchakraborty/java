package View.All_dialouge;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controller.Delete_Unit;
import Controller.UpdateUnit;
import Model.Product_catergory;
import Model.Status;
import Model.Unit;


import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Edit_Update_Unit_Dialouge extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField unitName;
	protected int id;

	

	public Edit_Update_Unit_Dialouge(String unit,String status,int id) {//Update method for Updating Unit 
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblUnitName = new JLabel("Unit Name::");
		lblUnitName.setBounds(12, 13, 69, 16);
		contentPanel.add(lblUnitName);
		
		unitName = new JTextField();
		unitName.setBounds(216, 13, 190, 22);
		contentPanel.add(unitName);
		unitName.setColumns(10);
		unitName.setText(unit);
		
		
		
		JLabel lblStatus = new JLabel("Status::");
		
		lblStatus.setBounds(12, 75, 56, 16);
		contentPanel.add(lblStatus);
		Status Status=new Status();
		ArrayList<String>status1=new ArrayList<>();
		status1=Status.return_status();
		JComboBox status_combox = new JComboBox();
		status_combox.setModel(new DefaultComboBoxModel<>(status1.toArray()));

		status_combox.setBounds(216, 72, 190, 22);
		status_combox.setSelectedItem(status);
		contentPanel.add(status_combox);
		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton Update_btn = new JButton("Update");
				Update_btn.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String unitNa=unitName.getText();
						String st=status_combox.getSelectedItem().toString();
						
						
						new UpdateUnit(unitNa, st, id);
						Unit.load();
						Edit_Update_Unit_Dialouge.this.dispose();
						
						
					}
				});
				Update_btn.setActionCommand("OK");
				buttonPane.add(Update_btn);
				getRootPane().setDefaultButton(Update_btn);
			}
			{
				JButton Delete_btn = new JButton("Delete");
				Delete_btn.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String unitNa=unitName.getText();
						String st=status_combox.getSelectedItem().toString();
						
						
					new Delete_Unit(unitNa, st,id);
					Unit.load();
						Edit_Update_Unit_Dialouge.this.dispose();	
					}
				});
				Delete_btn.setActionCommand("Cancel");
				buttonPane.add(Delete_btn);
			}
		}
	}

	
}
