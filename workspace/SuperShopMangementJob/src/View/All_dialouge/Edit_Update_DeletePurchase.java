package View.All_dialouge;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.sql.rowset.serial.SerialArray;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import com.toedter.calendar.JDateChooser;

import Controller.UpdatePurchase;
import Model.Product_catergory;
import Model.Products;
import Model.Temp;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Date;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Edit_Update_DeletePurchase extends JDialog {

	private final JPanel contentPanel = new JPanel();
	public JTextField Quantity_txt;
	public JTextField vendorName_txt;
	public JTextField buy_price;
	public JTextField sale_price_txt;
	public JDateChooser dateChooser;
	public JComboBox category_com;
	public JComboBox product_com;
	public JLabel Unit;

	public Edit_Update_DeletePurchase() {
		setBounds(100, 100, 943, 273);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel lblCa = new JLabel("Category");
		lblCa.setBounds(12, 13, 55, 16);
		contentPanel.add(lblCa);

		JLabel lblProductsName = new JLabel("Products Name");
		lblProductsName.setBounds(12, 81, 86, 16);
		contentPanel.add(lblProductsName);

		product_com = new JComboBox();
		product_com.setBounds(138, 76, 122, 26);
		
		contentPanel.add(product_com);

		Product_catergory products = new Product_catergory();
		ArrayList<String> cat = new ArrayList<>();
		cat = products.getAllCat();

		category_com = new JComboBox();
		category_com.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				String cat = category_com.getSelectedItem().toString();
				int cat_id = Product_catergory.get_cat_id(cat);
				ArrayList<String> prdct_by_cat_wise = new ArrayList<>();
				prdct_by_cat_wise = Products.return_product_cat_wise(cat_id);
				category_com.setModel(new DefaultComboBoxModel<>(prdct_by_cat_wise.toArray()));
				Temp Temp = new Temp();
				Temp.load();
			}
		});
		category_com.setBounds(138, 13, 122, 26);
	
		contentPanel.add(category_com);

		JLabel lblDate = new JLabel("Date");
		lblDate.setBounds(325, 18, 26, 16);
		contentPanel.add(lblDate);

		dateChooser = new JDateChooser();
		Date set = new Date();
		dateChooser.setDate(set);
		
		dateChooser.setBounds(376, 13, 119, 28);
		contentPanel.add(dateChooser);

		JLabel lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(325, 81, 55, 16);
		contentPanel.add(lblQuantity);

		Quantity_txt = new JTextField();
	
		Quantity_txt.setBounds(376, 75, 122, 28);
		contentPanel.add(Quantity_txt);
		Quantity_txt.setColumns(10);

		JLabel lblVendorName = new JLabel("Vendor Name");
		lblVendorName.setBounds(564, 18, 76, 16);
		contentPanel.add(lblVendorName);

		JLabel lblNewLabel = new JLabel("Buy Price");
		lblNewLabel.setBounds(564, 56, 55, 16);
		contentPanel.add(lblNewLabel);

		JLabel lblSalePrice = new JLabel("Sale Price");
		lblSalePrice.setBounds(564, 104, 56, 16);
		contentPanel.add(lblSalePrice);

		vendorName_txt = new JTextField();
		
		vendorName_txt.setBounds(660, 12, 122, 28);
		contentPanel.add(vendorName_txt);
		vendorName_txt.setColumns(10);

		buy_price = new JTextField();
		buy_price.setColumns(10);

		buy_price.setBounds(660, 50, 122, 28);
		contentPanel.add(buy_price);

		sale_price_txt = new JTextField();

		sale_price_txt.setColumns(10);
		sale_price_txt.setBounds(660, 98, 122, 28);
		contentPanel.add(sale_price_txt);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton upadate = new JButton("Upadate");
				upadate.addActionListener(new ActionListener() {
					
					
					public void actionPerformed(ActionEvent arg0) {
				
					}
				});
				upadate.setActionCommand("OK");
				buttonPane.add(upadate);
				getRootPane().setDefaultButton(upadate);
			}
			{
				JButton delete_btn = new JButton("Delete");
				delete_btn.setActionCommand("Cancel");
				buttonPane.add(delete_btn);
			}
		}
	}
}
