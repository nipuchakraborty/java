package View.All_dialouge;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controller.DeleteParches;
//import Controller.DeleteParches;
import Controller.UpdateAddProduct;
import Model.Product_catergory;
import Model.Products;
import Model.Status;
import Model.Unit;


import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import net.miginfocom.swing.MigLayout;

public class Edit_Update_AddProduct_Dialouge extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField product_category_text;

	
	
	public Edit_Update_AddProduct_Dialouge(String fristValue, String secendValue, String thirdValue, String forthValue, int id) {
		setBounds(100, 100, 553, 427);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setFocusTraversalPolicyProvider(true);
		contentPanel.setFocusCycleRoot(true);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new MigLayout("", "[137px][187px]", "[22px][32px][22px][22px]"));
		
		JLabel lblProductName = new JLabel("Product Name");
		contentPanel.add(lblProductName, "cell 0 0,alignx left,aligny center");
		
		JLabel lblNewLabel = new JLabel("Select Product Category");
		contentPanel.add(lblNewLabel, "cell 0 1,alignx left,aligny top");
		//Products products=new Products();
		product_category_text = new JTextField();
		contentPanel.add(product_category_text, "cell 1 0,growx,aligny top");
		
		product_category_text.setColumns(10);
		product_category_text.setText(fristValue);
		
		Product_catergory Product_catergory=new Product_catergory();
		ArrayList<String>all_categoryy=new ArrayList<>();
		all_categoryy=Product_catergory.getAllCat();
		
		JComboBox SeclectProCat_combo = new JComboBox();
		SeclectProCat_combo.setModel(new DefaultComboBoxModel<>(all_categoryy.toArray()));
		contentPanel.add(SeclectProCat_combo, "cell 1 1,growx,aligny bottom");
		
		Product_catergory product_catergory=new Product_catergory();
		product_catergory.getAllCat();
		
		
		JLabel lblSelectUnit = new JLabel("Select Unit");
		contentPanel.add(lblSelectUnit, "cell 0 2,alignx left,aligny center");
		Unit unit=new Unit();
		ArrayList<String>allunit=new ArrayList<>();
		allunit=unit.return_unit();
		JComboBox selectUnit_combo = new JComboBox();
		selectUnit_combo.setModel(new DefaultComboBoxModel<>(allunit.toArray()));
		contentPanel.add(selectUnit_combo, "cell 1 2,growx,aligny top");
		
		
		JLabel lblStatus = new JLabel("Status");
		contentPanel.add(lblStatus, "cell 0 3,alignx left,aligny center");
		Status Status=new Status();
		ArrayList<String>status=new ArrayList<>();
		status=Status.return_status();
		JComboBox Status_combo = new JComboBox();
		Status_combo.setModel(new DefaultComboBoxModel<>(status.toArray()));
		contentPanel.add(Status_combo, "cell 1 3,growx,aligny top");
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton Update = new JButton("Update");
				Update.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String pro=product_category_text.getText();
						String sc=SeclectProCat_combo.getSelectedItem().toString();
						String unit=selectUnit_combo.getSelectedItem().toString();
						String st=Status_combo.getSelectedItem().toString();
						new UpdateAddProduct(pro, sc, unit, st, id);
						
						Edit_Update_AddProduct_Dialouge.this.dispose();
						
					}
				});
				Update.setActionCommand("OK");
				buttonPane.add(Update);
				getRootPane().setDefaultButton(Update);
			}
			{
				JButton cancelButton = new JButton("Delete");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String pro=product_category_text.getText();
						String sc=SeclectProCat_combo.getSelectedItem().toString();
						String unit=selectUnit_combo.getSelectedItem().toString();
						String st=Status_combo.getSelectedItem().toString();
						new DeleteParches(pro,sc,unit,st,id);
						Edit_Update_AddProduct_Dialouge.this.dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
