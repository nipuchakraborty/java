package View.All_panel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;


import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import Controller.Get_Category_data;
import Controller.Get_unit;
import Controller.UpdateUnit;
import Model.Product_catergory;
import Model.Status;
import Model.Unit;
import View.All_dialouge.Edit_Update_Unit_Dialouge;

import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AddUnit_panel extends JPanel {
	private JTextField unitName;
	public static JTable table;
	private JComboBox status_combo;
	Unit obj=new Unit();
	/**
	 * Create the panel.
	 */
	public AddUnit_panel() {
		setLayout(new MigLayout("", "[][][grow][grow]", "[][][][][grow]"));
		
		JLabel lblAllUnits = new JLabel("All Units");
		add(lblAllUnits, "cell 3 0");
		
		JLabel lblUnitName = new JLabel("Unit Name ::");
		add(lblUnitName, "cell 1 1");
		
		unitName = new JTextField();
		add(unitName, "cell 3 1,growx");
		unitName.setColumns(10);
		
		JLabel lblStatus = new JLabel("Status ::");
		add(lblStatus, "cell 1 2");
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name=unitName.getText();
				String st=status_combo.getSelectedItem().toString();
				if(status_combo.getSelectedIndex()==0){
					JOptionPane.showMessageDialog(null,"Select Status");
				}
				else{
					Get_unit Get_unit=new Get_unit(name,st);
					obj.load();
				}
				
			}
		});
		
		
		Status Status=new Status();
		ArrayList<String>status=new ArrayList<>();
		status=Status.return_status();
		
		status_combo = new JComboBox();
		status_combo.setModel(new DefaultComboBoxModel<>(status.toArray()));
		add(status_combo, "cell 3 2,growx");
		add(btnSave, "cell 3 3");
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, "cell 0 4 4 1,grow");
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				
				int row=table.getSelectedRow();
				String fristValue=table.getModel().getValueAt(row, 0).toString();
				String secendValue=table.getModel().getValueAt(row, 1).toString();
				int id=Unit.get_unit_id(fristValue);
				Edit_Update_Unit_Dialouge edit_Update_Unit_Dialouge=new Edit_Update_Unit_Dialouge(fristValue, secendValue, id);
				edit_Update_Unit_Dialouge.setVisible(true);
				Unit.load();
			}
		});
		obj.load();
		scrollPane.setViewportView(table);
		
		

	}

}
