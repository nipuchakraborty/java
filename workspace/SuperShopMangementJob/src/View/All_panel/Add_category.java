package View.All_panel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import Controller.Get_Category_data;
import Model.Product_catergory;
import Model.Status;
import View.All_dialouge.Edit_Update_Category_dialouge;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Add_category extends JPanel {
	private JTextField name;
	public static JTable table;
	Product_catergory pCatergory = new Product_catergory();
	private JComboBox cmb;

	public Add_category() {
		setLayout(new MigLayout("", "[grow][][][][grow]", "[][][][][grow]"));

		JLabel lblCategoryName = new JLabel("Category Name ::");
		add(lblCategoryName, "cell 1 0");

		name = new JTextField();
		add(name, "cell 4 0,growx");
		name.setColumns(10);

		JLabel lblStatus = new JLabel("Status ::");
		add(lblStatus, "cell 1 1");

		JButton save = new JButton("Save");
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String Cat_name = name.getText();
				String Cat_status = cmb.getSelectedItem().toString();

				if (cmb.getSelectedIndex() == 0) {
					JOptionPane.showMessageDialog(null, "Select Status");
				} else {
					Get_Category_data Get_Category_data = new Get_Category_data(Cat_name, Cat_status);
					
					pCatergory.load();

				}
			}
		});

		Status Status = new Status();
		ArrayList<String> status = new ArrayList<>();
		status = Status.return_status();

		cmb = new JComboBox();
		cmb.setModel(new DefaultComboBoxModel<>(status.toArray()));
		add(cmb, "cell 4 1,growx");
		add(save, "cell 4 2");

		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, "cell 0 4 5 1,grow");

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row = table.getSelectedRow();
				String get1stSelectedcomumn = table.getModel().getValueAt(row, 0).toString();
				int id = Product_catergory.get_cat_id(get1stSelectedcomumn);
				String get2ndstSelectedcomumn = table.getModel().getValueAt(row, 1).toString();
				
				
				/*Edit_Update_dialouge.product_cate_txt.setText(get1stSelectedcomumn);
				Edit_Update_dialouge.comboBox_Satatus.setSelectedItem(get2ndstSelectedcomumn);*/
				Edit_Update_Category_dialouge edit_Update_Category_dialouge=new Edit_Update_Category_dialouge(get1stSelectedcomumn,get2ndstSelectedcomumn,id);
				edit_Update_Category_dialouge.setVisible(true);
				
			}
		});
		scrollPane.setViewportView(table);
		pCatergory.load();

	}

}
