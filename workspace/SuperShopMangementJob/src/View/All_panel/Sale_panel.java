package View.All_panel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import com.toedter.calendar.JDateChooser;

import Controller.Sale;
import Model.Product_catergory;
import Model.Products;
import Model.Sales;
import Model.Temp;
import Model.temp_sale;

import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JCheckBox;
import java.awt.SystemColor;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.MatteBorder;
import javax.swing.table.TableColumnModel;

import java.awt.Color;
import javax.swing.UIManager;
import java.awt.event.ItemListener;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.jar.Attributes.Name;
import java.awt.event.ItemEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.border.TitledBorder;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Sale_panel extends JPanel {
	private JTextField qun_txt;
	private JTextField customername_txt;
	public static JTable table;
	private JTextField total_amoun;
	private JTextField paid_txt;
	private JTextField due_txt;
	private JComboBox product_com;
	private JComboBox category_com ;
	private JTextField buy_p;
	private JTextField sale_p;
	/**
	 * Create the panel.
	 */
	public Sale_panel() {
		setBorder(new TitledBorder(null, "Sale Products report", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		setBackground(Color.GREEN);
		setLayout(new MigLayout("", "[60px][4px][88px][4px][56px][4px][60px][57px][12px][79px][8px][63px][4px][101px][4px][84px]", "[22px][25px][318px][25px]"));
		
		JLabel lblCategory = new JLabel("Category :");
		add(lblCategory, "cell 0 0,alignx left,aligny center");
		Product_catergory product_catergory=new Product_catergory();
		ArrayList<String> cat= new ArrayList<>();
		cat=product_catergory.getAllCat();
		
		category_com = new JComboBox();
		//comboBox.set(new ImageIcon("C:/Users/Nk chakraborty/Desktop/SDC/About.png"));
		category_com.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				String cat=category_com.getSelectedItem().toString();
				int category_id=product_catergory.get_cat_id(cat);
				ArrayList<String>product_by_cate_wise=new ArrayList<>();
				product_by_cate_wise=Products.return_product_cat_wise(category_id);
				product_com.setModel(new DefaultComboBoxModel<>(product_by_cate_wise.toArray()));
				Temp temp=new Temp();
				temp.load();
			}
		});
		category_com.setModel(new DefaultComboBoxModel(new String[] {"Select", "Fasion", "cosmetic", "Grorsary", "electronics", "electronics", "electronics", "Raw", "Vagitable", "con"}));
		add(category_com, "cell 2 0,alignx left,aligny top");
		
		JLabel lblDate = new JLabel("Date :");
		add(lblDate, "cell 4 0,alignx right,aligny center");
		
		JDateChooser dateChooser = new JDateChooser();
		Date date=new Date();
		dateChooser.setDate(date);
		add(dateChooser, "cell 6 0,growx,aligny top");
		
		customername_txt = new JTextField();
		customername_txt.setText("customer name");
		customername_txt.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				String text=customername_txt.getText();
	           customername_txt.setText("");
				
			}
		});
		
		JLabel lblSaleprice = new JLabel("Saleprice");
		add(lblSaleprice, "cell 7 0,alignx right,aligny center");
		
		sale_p = new JTextField();
		add(sale_p, "cell 9 0,growx,aligny top");
		sale_p.setColumns(10);
		
		JLabel lblCustomarName = new JLabel("Customar Name :");
		add(lblCustomarName, "cell 13 0,alignx left,aligny center");
		add(customername_txt, "cell 15 0,growx,aligny top");
		customername_txt.setColumns(10);
		
		JLabel lblProduct = new JLabel("Product :");
		add(lblProduct, "cell 0 1,alignx right,aligny center");
		
		product_com = new JComboBox();
		add(product_com, "cell 2 1,growx,aligny center");
		
		JLabel lblQuantity = new JLabel("Quantity :");
		add(lblQuantity, "cell 4 1,alignx left,aligny center");
		
		qun_txt = new JTextField();
		add(qun_txt, "cell 6 1,growx,aligny center");
		qun_txt.setColumns(10);
		
		
	
		
		JButton btnEnter = new JButton("Enter");
		btnEnter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String pd_nam=product_com.getSelectedItem().toString();
				int pd_id=Products.get_pd_id(pd_nam);
				int ex_pd_id=Temp.ck_existing_pdt(pd_id);
				System.out.println(pd_id);
				System.out.println(ex_pd_id);
				if(pd_id==ex_pd_id){
					JOptionPane.showMessageDialog(null,"This Product Already Exist !!!");
				}
				else{
					
					String pd_name=product_com.getSelectedItem().toString();
					String qun=qun_txt.getText();
					
					String buy=buy_p.getText();
					String sell=sale_p.getText();
					
					
					
				   Sale Sale=new Sale();
				   Sale.tocontroller(pd_name,qun,buy,sell);
				   
				   temp_sale temp_sale=new temp_sale();
				   int tam=temp_sale.get_total();
				   String t_amnt = new Integer(tam).toString();
				   total_amoun.setText(t_amnt);
				   temp_sale.load();
				   
				   customername_txt.setEditable(false);
				   dateChooser.setEnabled(false);
				}
				
				
				
			   
			}
		});
		
		JLabel lblBuyprice = new JLabel("Buyprice");
		add(lblBuyprice, "cell 7 1,alignx center,aligny center");
		
		buy_p = new JTextField();
		add(buy_p, "cell 9 1,growx,aligny center");
		buy_p.setColumns(10);
		add(btnEnter, "cell 11 1,alignx left,aligny top");
		
		JButton btnNew = new JButton("New");
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				customername_txt.setText("");
				qun_txt.setText("");
				buy_p.setText("");
				sale_p.setText("");
				total_amoun.setText("");
				paid_txt.setText("");
				due_txt.setText("");
				
			}
		});
		add(btnNew, "cell 13 1,alignx left,aligny top");
		
		JPanel panel_1 = new JPanel();
		add(panel_1, "cell 0 2 7 1,grow");
		panel_1.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane, "cell 0 0,grow");
		
		table = new JTable();
		table.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent arg0) {
				Sales sales=new Sales();
				sales.load();
			}
		});
		scrollPane.setColumnHeaderView(table);
		
		JPanel panel = new JPanel();
		panel.setBorder(UIManager.getBorder("ComboBox.editorBorder"));
		panel.setBackground(SystemColor.inactiveCaption);
		add(panel, "cell 7 2 9 1,grow");
		panel.setLayout(null);
		
		JLabel lblTotalamount = new JLabel("Total-Amount :");
		lblTotalamount.setBounds(10, 11, 72, 14);
		panel.add(lblTotalamount);
		
		JLabel lblPaid = new JLabel("Paid :");
		lblPaid.setBounds(10, 36, 46, 14);
		panel.add(lblPaid);
		
		JLabel lblDue = new JLabel("Due:");
		lblDue.setBounds(10, 60, 46, 14);
		panel.add(lblDue);
		
		JCheckBox chckbxBalanced = new JCheckBox("Balanced");
		chckbxBalanced.setBounds(6, 81, 97, 23);
		panel.add(chckbxBalanced);
		
		total_amoun = new JTextField();
		total_amoun.setBounds(110, 8, 86, 20);
		panel.add(total_amoun);
		total_amoun.setColumns(10);
		
		paid_txt = new JTextField();
		paid_txt.setBounds(110, 33, 86, 20);
		panel.add(paid_txt);
		paid_txt.setColumns(10);
		
		due_txt = new JTextField();
		due_txt.setBounds(110, 57, 86, 20);
		panel.add(due_txt);
		due_txt.setColumns(10);
		
		JButton btnSave = new JButton("Save");
		btnSave.setBounds(107, 180, 89, 23);
		panel.add(btnSave);
		
		JButton printBTN = new JButton("Print");
		printBTN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					MessageFormat header=new MessageFormat("Nk company");
					MessageFormat footer=new MessageFormat("Copyright@Nk");
					table.print(JTable.PrintMode.FIT_WIDTH,header,footer);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});
		add(printBTN, "cell 4 3 3 1,alignx right,aligny top");

	}
}
