package View.All_panel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;


import javax.swing.JLabel;
import javax.swing.JTextField;

import Controller.Get_product;
import Model.Products;
import Model.Product_catergory;
import Model.Status;
import Model.Unit;
import View.All_dialouge.Edit_Update_AddProduct_Dialouge;


import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JToggleButton;
import javax.swing.JFormattedTextField;
import javax.swing.JSeparator;
import javax.swing.JScrollBar;
import javax.swing.JSlider;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import java.awt.ComponentOrientation;

public class AddProduct extends JPanel {
	private  JTextField pn;
	public static  JTable product_table;
	private  JComboBox cmb;
	

	
	
	/**
	 * Create the panel.
	 */
	public AddProduct() {
		setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		setAutoscrolls(true);
		setBackground(Color.GREEN);
		setForeground(Color.GRAY);
		setLayout(new MigLayout("", "[180px][234px][257px]", "[22px][22px][22px][22px][1px][25px][265px]"));
		
		JLabel lblProductName = new JLabel("Product name ::");
		add(lblProductName, "cell 0 0,alignx center,aligny center");
		
		pn = new JTextField();
		add(pn, "cell 2 0,growx,aligny top");
		pn.setColumns(10);
		
		JLabel lblSelectProductCategory = new JLabel("Select Product Category ::");
		add(lblSelectProductCategory, "cell 0 1,alignx right,aligny center");
		
		Product_catergory Product_catergory=new Product_catergory();
		ArrayList<String>all_categoryy=new ArrayList<>();
		all_categoryy=Product_catergory.getAllCat();
		
		JComboBox comboPC = new JComboBox();
		comboPC.setModel(new DefaultComboBoxModel<>(all_categoryy.toArray()));
		add(comboPC, "cell 2 1,growx,aligny top");
		
		JLabel lblSelectUnit = new JLabel("Select Unit ::");
		add(lblSelectUnit, "cell 0 2,alignx center,aligny center");
		
		
		Unit unit=new Unit();
		ArrayList<String>allunit=new ArrayList<>();
		allunit=unit.return_unit();
		
		JComboBox combo_un = new JComboBox();
		combo_un.setModel(new DefaultComboBoxModel<>(allunit.toArray()));
		add(combo_un, "cell 2 2,growx,aligny top");
		
		
		JLabel lblStatus = new JLabel("Status ::");
		add(lblStatus, "cell 0 3,alignx left,aligny center");
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String product_name=pn.getText();
				String product_cat=comboPC.getSelectedItem().toString();
				String product_unit=combo_un.getSelectedItem().toString();
				String product_sta=cmb.getSelectedItem().toString();
			int id = 0;
				
				new Get_product(product_name,product_cat,product_unit,product_sta,id);
				
				Products.load();
				
			}
		});
		Status Status=new Status();
		ArrayList<String>status=new ArrayList<>();
		status=Status.return_status();
		
		cmb = new JComboBox();
		cmb.setModel(new DefaultComboBoxModel<>(status.toArray()));
		add(cmb, "cell 2 3,growx,aligny top");
		
		JSeparator separator = new JSeparator();
		add(separator, "cell 0 4,alignx left,growy");
		add(btnSave, "cell 2 5,alignx left,aligny top");
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, "cell 0 6 3 1,grow");
		
		product_table = new JTable();
		product_table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				int row = product_table.getSelectedRow();
				String get1stclumn = product_table.getModel().getValueAt(row, 0).toString();
				String getSecondClumn = product_table.getModel().getValueAt(row, 1).toString();
				String get3rdcolumn = product_table.getModel().getValueAt(row, 2).toString();
				String get4thcolumn = product_table.getModel().getValueAt(row, 3).toString();
				int id=Products.get_pd_id(get1stclumn);
				Edit_Update_AddProduct_Dialouge edit=new Edit_Update_AddProduct_Dialouge(get1stclumn, getSecondClumn, get3rdcolumn, get4thcolumn,id);
				edit.setVisible(true);
				Products.load();
				
			}
		});
		Products p=new Products();
		p.load();
		scrollPane.setViewportView(product_table);

	}

}
