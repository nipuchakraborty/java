package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import View.All_panel.Add_category;
import View.All_panel.Purchase_panel;
import View.All_panel.Sale_panel;

import View.All_panel.AddProduct;
import View.All_panel.AddUnit_panel;


import javax.swing.JDesktopPane;
import java.awt.Color;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.text.ParseException;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel;
import java.awt.Component;
import net.miginfocom.swing.MigLayout;
import java.awt.FlowLayout;
import java.awt.Graphics;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.CardLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import LogIN.LoINReg;
import LogIN.Log;

import java.awt.GridBagLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Window.Type;
import java.awt.ComponentOrientation;
import javax.swing.border.CompoundBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.BevelBorder;
import java.awt.Dialog.ModalExclusionType;


public class Dashboard_JFrm extends JFrame{
	private JPanel contentPane;
	private JDesktopPane desktopPane;
	private static JMenu mnHome ;

	
	public static void main(String[] args) {
		
		
		 
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				try {
					 //UIManager.setLookAndFeel("net.sourceforge.napkinlaf.NapkinLookAndFeel");
					UIManager.setLookAndFeel(new SyntheticaAluOxideLookAndFeel());
					Dashboard_JFrm frame = new Dashboard_JFrm();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Dashboard_JFrm() {
		
		try {
			UIManager.setLookAndFeel(new SyntheticaAluOxideLookAndFeel());
		} catch (UnsupportedLookAndFeelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		setBackground(Color.BLUE);
		setTitle("Shop Management");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Nk chakraborty\\Desktop\\SDC\\FROIS-01 33.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1228, 703);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		 mnHome = new JMenu("Home");
		mnHome.setIcon(new ImageIcon(Dashboard_JFrm.class.getResource("/resource/b_home.png")));
		menuBar.add(mnHome);
		
		JMenuItem mntmAddCatagory = new JMenuItem("Add Catagory");
		mntmAddCatagory.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_1, java.awt.event.InputEvent.SHIFT_MASK));

		mntmAddCatagory.setIcon(new ImageIcon(Dashboard_JFrm.class.getResource("/resource/ap.png")));
		mntmAddCatagory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				JInternalFrame JIF=new JInternalFrame("Product Category",true,true,true,true);
				Add_category Add_category=new Add_category();
				JIF.getContentPane().add(Add_category);
				JIF.pack();
				
				desktopPane.add(JIF);
				JIF.setVisible(true);
				
				
				
			}
		});
		mnHome.add(mntmAddCatagory);
		
		JMenuItem mntmAddProduct = new JMenuItem("Add Product");
		mntmAddProduct.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_2, java.awt.event.InputEvent.SHIFT_MASK));

		mntmAddProduct.setIcon(new ImageIcon(Dashboard_JFrm.class.getResource("/resource/ap.png")));
		mntmAddProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JInternalFrame JIF=new JInternalFrame("Product Category",true,true,true,true);
				AddProduct AddProduct=new AddProduct();
				JIF.getContentPane().add(AddProduct);
				JIF.pack();
				
				desktopPane.add(JIF);
				JIF.setVisible(true);
			}
		});
		mnHome.add(mntmAddProduct);
		
		JMenuItem mntmAddUnit = new JMenuItem("Add Unit");
		mntmAddUnit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_3, java.awt.event.InputEvent.SHIFT_MASK));

		mntmAddUnit.setIcon(new ImageIcon(Dashboard_JFrm.class.getResource("/resource/ap.png")));
		mntmAddUnit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame JIF=new JInternalFrame("Product Unit",true,true,true,true);
				AddUnit_panel unit=new AddUnit_panel();
				JIF.getContentPane().add(unit);
				JIF.pack();
				
				desktopPane.add(JIF);
				JIF.setVisible(true);
			}
		});
		mnHome.add(mntmAddUnit);
		
		JMenu mnBillingInfo = new JMenu("Reports");
		mnBillingInfo.setIcon(new ImageIcon(Dashboard_JFrm.class.getResource("/resource/b_report.png")));
		menuBar.add(mnBillingInfo);
		
		JMenuItem mntmCreateCashMemo = new JMenuItem("Sale");
		mntmCreateCashMemo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_4, java.awt.event.InputEvent.SHIFT_MASK));

		mntmCreateCashMemo.setIcon(new ImageIcon(""));
		mntmCreateCashMemo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame JIF=new JInternalFrame("All Units",true,true,true,true);
				Sale_panel Sale_panel=new Sale_panel();
				JIF.getContentPane().add(Sale_panel);
				JIF.pack();
				
				desktopPane.add(JIF);
				JIF.setVisible(true);
			}
		});
		mnBillingInfo.add(mntmCreateCashMemo);
		
		JMenuItem mntmP = new JMenuItem("Purchase");
		mntmP.setIcon(new ImageIcon(""));
		mntmP.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_3, java.awt.event.InputEvent.SHIFT_MASK));

		
		mntmP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame JIF=new JInternalFrame("All Units",true,true,true,true);
				Purchase_panel Purchase_panel=new Purchase_panel();
				JIF.getContentPane().add(Purchase_panel);
				JIF.pack();
				
				desktopPane.add(JIF);
				JIF.setVisible(true);
			}
		});
		mnBillingInfo.add(mntmP);
		
		JMenu mnAbout = new JMenu("About");
		mnAbout.setIcon(new ImageIcon(Dashboard_JFrm.class.getResource("/resource/b_info.png")));
		menuBar.add(mnAbout);
		
		JMenuItem mntmCustomer = new JMenuItem("Customer");
		mntmCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Log log=new Log();
				JInternalFrame jInternalFrame=new JInternalFrame("Log IN",true,true,true);
				jInternalFrame.getContentPane().add(log);
				jInternalFrame.pack();
				
				jInternalFrame.setVisible(true);
			}
		});
		mnAbout.add(mntmCustomer);
		
		JMenuItem mntmManager = new JMenuItem("Manager");
		mnAbout.add(mntmManager);
		
		JMenuItem mntmAccounted = new JMenuItem("Accounted");
		mnAbout.add(mntmAccounted);
		
		JMenuItem mntmReviweAccounts = new JMenuItem("Reviwe Accounts");
		mnAbout.add(mntmReviweAccounts);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		
		desktopPane = new JDesktopPane(){
			private Image image;
			{
				try{
					image =ImageIO.read(new File("C:\\Users\\Nk\\Downloads\\time_management_software_full_14177545-990x480.jpg"));
					
				}
				catch(IOException e){
					e.printStackTrace();
				}
			}
			@Override
			protected void paintComponent(Graphics g){
				super.paintComponent(g);
				g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
				
			}
		};
		desktopPane.setBorder(new CompoundBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)), new BevelBorder(BevelBorder.LOWERED, null, null, null, null)));
		desktopPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		/*BufferedImage img = null;
		try {
		    img = ImageIO.read(new File("C:/Users/Nk chakraborty/Desktop/SDC/Logojoy _ Make a Logo Design Online_files/my nM.jpg"));
		} catch (IOException e) {
		    e.printStackTrace();
		}
		Image dimg = img.getScaledInstance(1200, 608, Image.SCALE_SMOOTH);
		ImageIcon imageIcon = new ImageIcon(dimg);
		setContentPane(new JLabel(imageIcon));
		   
		   
		
		desktopPane.setFocusTraversalPolicyProvider(true);*/
		desktopPane.setBackground(Color.CYAN);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(null);
	}
}
