package LogIN;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import Controller.LoginCtrl;
import Model.Database;
import View.Dashboard_JFrm;
import de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel;

import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.border.TitledBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.MatteBorder;
import javax.swing.DefaultComboBoxModel;

public class Log extends JFrame implements ActionListener, MouseListener {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Log frame = new Log();
					
					//UIManager.setLookAndFeel(new SyntheticaAluOxideLookAndFeel());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	int w = 500;
	int h = 300;
	JPanel header, center, footer;

	String type[] = { "Super Admin", "Admin", "User" };
	JComboBox cb;
	JLabel l;
	private JLabel l_2;
	private JLabel l_1;
	JButton login, forgotpass;

	Border border = new EtchedBorder();
	Font font = new Font("courier new", Font.PLAIN, 16);
	JTextField userNameF, field;
	Cursor cursor = new Cursor(Cursor.HAND_CURSOR);
	Statement sta;
	Font lFont = new Font("courier new", Font.PLAIN, 15);
	JPasswordField passwordF;
	Font smallF = new Font("serif", Font.PLAIN, 12);
	JLabel link, fPassword, exitL;

	public String user;
	public String catagory1;
	private Connection con;

	public Log() {
		super("Shop management Log in Step");
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		getContentPane().setLayout(new BorderLayout());
		/*
		 * try {
		 * 
		 * 
		 * Class.forName("com.mysql.jdbc.Driver");
		 * con=DriverManager.getConnection(
		 * "jdbc:mysql://localhost:3306/shop_management","root","");
		 * System.out.println("Connection Successful");
		 * 
		 * } catch (Exception ex) { //System.exit(0);
		 * JOptionPane.showMessageDialog (null, ex.toString()); }
		 * 
		 */

		setIconImage(Toolkit.getDefaultToolkit().getImage(""));

		// set panel object
		header = new JPanel();
		header.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		header.setForeground(Color.CYAN);
		center = new JPanel();
		center.setForeground(Color.CYAN);
		footer = new JPanel();
		center.setLayout(null);

		// panel size
		center.setPreferredSize(new Dimension(screen.width + 50, 50));
		header.setPreferredSize(new Dimension(screen.width, 30));
		center.setPreferredSize(new Dimension(screen.width, 50));
		// set background color panel
		center.setBackground(new Color(155, 200, 200));
		footer.setBackground(new Color(155, 200, 200));
		center.setBackground(Color.WHITE);
		GridBagLayout gbl_header = new GridBagLayout();
		gbl_header.columnWidths = new int[]{176, 0};
		gbl_header.rowHeights = new int[]{32, 0};
		gbl_header.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_header.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		header.setLayout(gbl_header);
		// componet to center
		l = new JLabel("User Name :");
		l.setIcon(new ImageIcon(Log.class.getResource("/resource/user-32.png")));
		l.setBounds(57, 56, 146, 32);
		l.setFont(font);
		center.add(l);
		l_2 = new JLabel("Password  :");
		l_2.setIcon(new ImageIcon(Log.class.getResource("/resource/pasword.png")));
		l_2.setBounds(57, 101, 146, 32);
		l_2.setFont(font);
		center.add(l_2);

		userNameF = new JTextField();
	
		userNameF.setBounds(220, 60, 140, 25);
		userNameF.setFont(font);
		center.add(userNameF);

		passwordF = new JPasswordField();
		passwordF.setBounds(220, 100, 140, 25);
		passwordF.setFont(font);
		center.add(passwordF);

		cb = new JComboBox(type);
		cb.setModel(new DefaultComboBoxModel(new String[] {"Select user", "Super Admin", "Admin", "User"}));
		cb.setForeground(Color.DARK_GRAY);
		cb.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				cb.setForeground(Color.RED);		
			}
		});
		cb.setBounds(220, 130, 140, 25);
		cb.setFont(lFont);
		center.add(cb);

		link = new JLabel("Login");
		link.setIcon(new ImageIcon("C:\\Users\\Nk chakraborty\\Desktop\\SDC\\ic_menu_login.png"));
		link.setForeground(Color.blue);
		link.setCursor(cursor);
		link.setBounds(192, 180, 67, 36);
		link.setFont(smallF);
		center.add(link);
		link.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				
				String com = cb.getSelectedItem().toString();
				if(com.equals("Super Admin")){
					String uS = userNameF.getText();
					System.out.println(uS);
					String pa = passwordF.getText();
					System.out.println(pa);
					new LoginCtrl(uS, pa, com);
					Log.this.dispose();
				}
				else if (com.equals("Admin")) {
					String uS = userNameF.getText();
					System.out.println(uS);
					String pa = passwordF.getText();
					System.out.println(pa);
					new LoginCtrl(uS, pa, com);
				}
				else if(com.equals("User")){
					String uS = userNameF.getText();
					System.out.println(uS);
					String pa = passwordF.getText();
					System.out.println(pa);
					new LoginCtrl(uS, pa, com);
					Log.this.dispose();
					
				}
				
				else {
					JOptionPane.showMessageDialog(null, "Cheak User");
				}

			}

			public void mouseReleased(MouseEvent e) {
				//center.setBackground(Color.black);
				Log log=new Log();
				log.setVisible(false);
			}

			public void mouseEntered(MouseEvent e) {
				link.setForeground(Color.red);
				link.setFont(new Font("serif", Font.BOLD, 12));
			}

			public void mouseExited(MouseEvent e) {
				link.setForeground(Color.blue);
				link.setFont(smallF);
			}
		});

		l = new JLabel("|");
		l.setBounds(259, 186, 4, 25);
		l.setFont(new Font("serif", Font.BOLD, 12));
		center.add(l);
		fPassword = new JLabel("Forget Password");
		
		String Us = userNameF.getText();
		String pa = passwordF.getText();

		fPassword.setCursor(cursor);
		fPassword.setBounds(288, 190, 101, 17);
		fPassword.setForeground(Color.blue);
		fPassword.setFont(smallF);
		center.add(fPassword);
		fPassword.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				// System.exit(0);
				JTextField nameF=new JTextField();
				nameF.setText("Usrer Name");
				
				JPasswordField passwordF=new JPasswordField();
				passwordF.setText("New password");
			String paString=userNameF.getText();
			String paString2=passwordF.getText();
		RestPasword restPasword=new RestPasword(paString, paString2);
		restPasword.setVisible(true);
			}

			public void mouseReleased(MouseEvent e) {
				 center.setBackground(Color.black);
			}

			public void mouseEntered(MouseEvent e) {
				fPassword.setForeground(Color.red);
				fPassword.setFont(new Font("serif", Font.BOLD, 12));
			}

			public void mouseExited(MouseEvent e) {
				fPassword.setForeground(Color.blue);
				fPassword.setFont(smallF);
			}
		});

		exitL = new JLabel("Exit");
		exitL.setFocusTraversalPolicyProvider(true);
		exitL.setIcon(new ImageIcon(Log.class.getResource("/resource/s_error.png")));
		exitL.setVerticalAlignment(SwingConstants.TOP);
		exitL.setBackground(Color.BLUE);
		exitL.setCursor(cursor);
		exitL.setBounds(w - 30, 30, 85, 15);
		exitL.setForeground(Color.RED);
		exitL.setFont(new Font("Serif", Font.PLAIN, 15));
		exitL.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				System.exit(0);
			}

			public void mouseReleased(MouseEvent e) {
				center.setBackground(Color.black);
			}

			public void mouseEntered(MouseEvent e) {
				exitL.setForeground(Color.black);
				exitL.setFont(new Font("serif", Font.BOLD, 12));
			}

			public void mouseExited(MouseEvent e) {
				exitL.setForeground(Color.gray);
				exitL.setFont(smallF);
			}
		});
		getContentPane().add(header, BorderLayout.NORTH);
		
				l_1 = new JLabel("Shop management");
				l_1.setFont(new Font("serif", Font.PLAIN, 24));
				l_1.setBackground(Color.blue);
				GridBagConstraints gbc_l = new GridBagConstraints();
				gbc_l.anchor = GridBagConstraints.WEST;
				gbc_l.gridx = 0;
				gbc_l.gridy = 0;
				header.add(l_1, gbc_l);
		getContentPane().add(center, BorderLayout.CENTER);
		getContentPane().add(footer, BorderLayout.SOUTH);
		footer.setLayout(new BoxLayout(footer, BoxLayout.X_AXIS));
		footer.add(exitL);
		// end of disign.....

		setResizable(true);
		setUndecorated(true);
		//getRootPane().setWindowDecorationStyle(JRootPane.COLOR_CHOOSER_DIALOG);
		setLocation((screen.width - w) / 2, (screen.height - h) / 2);
		setSize(w, h);
		show();
	}

	public static void login(String[] args) {
		try {
			// UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			//UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
		}

		Log m = new Log();
		//m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void actionPerformed(ActionEvent e) {

	}

	public void mouseClicked(MouseEvent e) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void mousePressed(MouseEvent e) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void mouseReleased(MouseEvent e) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void mouseEntered(MouseEvent e) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void mouseExited(MouseEvent e) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

}
