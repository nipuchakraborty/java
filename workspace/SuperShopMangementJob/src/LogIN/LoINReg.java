package LogIN;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;

import Controller.LoginCtrl;

import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoINReg {

	private JFrame frame;
	private JTextField userName;
	private JPasswordField password;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoINReg window = new LoINReg();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoINReg() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 917, 451);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblUsername = new JLabel("UserName");
		lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblUsername.setBounds(117, 100, 155, 41);
		frame.getContentPane().add(lblUsername);
		
		userName = new JTextField();
		userName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		userName.setBounds(324, 92, 351, 48);
		frame.getContentPane().add(userName);
		userName.setColumns(10);
		
		password = new JPasswordField();
		password.setFont(new Font("Tahoma", Font.PLAIN, 18));
		password.setBounds(328, 194, 347, 48);
		frame.getContentPane().add(password);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblPassword.setBounds(117, 211, 155, 41);
		frame.getContentPane().add(lblPassword);
		
		JButton login_btn = new JButton("LogIN");
		login_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String uN=userName.getName();
				String pas=password.getText();
				//new LoginCtrl(uN, pas);
			}
		});
		
		login_btn.setFont(new Font("SansSerif", Font.BOLD, 18));
		login_btn.setBounds(547, 310, 113, 41);
		frame.getContentPane().add(login_btn);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.setFont(new Font("SansSerif", Font.BOLD, 18));
		btnRegister.setBounds(367, 310, 113, 41);
		frame.getContentPane().add(btnRegister);
	}
}
