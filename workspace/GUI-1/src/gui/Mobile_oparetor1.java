package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Mobile_oparetor1 extends JFrame {

	private JPanel contentPane;
	private JTextField num;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mobile_oparetor1 frame = new Mobile_oparetor1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Mobile_oparetor1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMobileNumber = new JLabel("Mobile Number :");
		lblMobileNumber.setBounds(34, 62, 96, 14);
		contentPane.add(lblMobileNumber);
		
		num = new JTextField();
		num.setBounds(119, 59, 148, 20);
		contentPane.add(num);
		num.setColumns(10);
		
		JLabel lblMobileOparetorSelection = new JLabel("Mobile Oparetor Selection ");
		lblMobileOparetorSelection.setBounds(133, 11, 134, 14);
		contentPane.add(lblMobileOparetorSelection);
		
		JButton Find = new JButton("Find");
		Find.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String number=num.getText();
				int number_for_calculate=Integer.parseInt(number);
				int []remainder=new int[11];
				if(number.length()==11){
					int i=0;
				while (number_for_calculate!=0) {
					remainder[i]=(number_for_calculate%10);
					number_for_calculate=number_for_calculate/10;
					i++;
					
				}
				if (remainder[8]==8) {
					M_oparator_output m_oparator_output=new M_oparator_output();
					m_oparator_output.setVisible(true);
					Mobile_oparetor1.this.setVisible(false);
					m_oparator_output.op_name.setText("robi");
					ImageIcon icon=new ImageIcon("C:/Users/NK CHAKRABORTY/Desktop/robi.png");
					m_oparator_output.logo.setIcon(icon);
					m_oparator_output.logo.setBounds(100, 100, 250, 220);
					
				}
				else if (remainder[8]==7) {
					M_oparator_output m_oparator_output=new M_oparator_output();
					m_oparator_output.setVisible(true);
					Mobile_oparetor1.this.setVisible(false);
					m_oparator_output.op_name.setText("GrameenPhone");
					ImageIcon icon=new ImageIcon("C:/Users/NK CHAKRABORTY/Desktop/gp.png");
					m_oparator_output.logo.setIcon(icon);
					
					
				}
				else if (remainder[8]==6) {
					M_oparator_output m_oparator_output=new M_oparator_output();
					m_oparator_output.setVisible(true);
					Mobile_oparetor1.this.setVisible(false);
					m_oparator_output.op_name.setText("Airtel");
					ImageIcon icon=new ImageIcon("C:/Users/NK CHAKRABORTY/Desktop/airtel.png");
					m_oparator_output.logo.setIcon(icon);
					
					
				}
				else if (remainder[8]==5) {
					M_oparator_output m_oparator_output=new M_oparator_output();
					m_oparator_output.setVisible(true);
					Mobile_oparetor1.this.setVisible(false);
					m_oparator_output.op_name.setText("Teltalk");
					ImageIcon icon=new ImageIcon("C:/Users/NK CHAKRABORTY/Desktop/teletalk.png");
					m_oparator_output.logo.setIcon(icon);
					
					
					
				}
				else {
					JOptionPane.showInternalMessageDialog(null, "This is not vaild Number");
				}
				
				}}
		});
		Find.setBounds(133, 154, 89, 23);
		contentPane.add(Find);
		}
}
