package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Even_0r_odd {

	private JFrame frame;
	private JTextField A;
	private String a;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Even_0r_odd window = new Even_0r_odd();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Even_0r_odd() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		JLabel lblCheckEvenOr = new JLabel("Check Even or Odd Number");
		GridBagConstraints gbc_lblCheckEvenOr = new GridBagConstraints();
		gbc_lblCheckEvenOr.insets = new Insets(0, 0, 5, 5);
		gbc_lblCheckEvenOr.gridx = 5;
		gbc_lblCheckEvenOr.gridy = 0;
		frame.getContentPane().add(lblCheckEvenOr, gbc_lblCheckEvenOr);
		
		JLabel lblEnterANumber = new JLabel("Enter a Number :");
		GridBagConstraints gbc_lblEnterANumber = new GridBagConstraints();
		gbc_lblEnterANumber.insets = new Insets(0, 0, 5, 5);
		gbc_lblEnterANumber.anchor = GridBagConstraints.EAST;
		gbc_lblEnterANumber.gridx = 3;
		gbc_lblEnterANumber.gridy = 2;
		frame.getContentPane().add(lblEnterANumber, gbc_lblEnterANumber);
		
		A = new JTextField();
		GridBagConstraints gbc_A = new GridBagConstraints();
		gbc_A.gridwidth = 3;
		gbc_A.insets = new Insets(0, 0, 5, 5);
		gbc_A.gridx = 4;
		gbc_A.gridy = 2;
		frame.getContentPane().add(A, gbc_A);
		A.setColumns(10);
		
		JButton btnCheckOut = new JButton("Check out");
		btnCheckOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				a=A.getText();
			int b=Integer.parseInt(a);
			if ( b % 2 == 0 ){
		    	  JOptionPane.showMessageDialog(null,"This is an even number");}
		      else{
		    	  JOptionPane.showMessageDialog(null,"This is an odd number");
			}
				
			}
		});
		GridBagConstraints gbc_btnCheckOut = new GridBagConstraints();
		gbc_btnCheckOut.insets = new Insets(0, 0, 0, 5);
		gbc_btnCheckOut.gridx = 5;
		gbc_btnCheckOut.gridy = 4;
		frame.getContentPane().add(btnCheckOut, gbc_btnCheckOut);
	}

}
