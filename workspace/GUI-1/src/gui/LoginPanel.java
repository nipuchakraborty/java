package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JFormattedTextField;
import javax.swing.JToggleButton;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JSlider;
import javax.swing.JTree;
import java.awt.Font;
import javax.swing.JEditorPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;



public class LoginPanel {

	private JFrame frmLoginPanel;
	private JTextField user;
	private JPasswordField pass;
	private JTextField reg_u;
	private JTextField reg_p;
	private JLabel result;
	private String reg_user;
	private String reg_pass;
	private static LoginPanel window;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window = new LoginPanel();
					window.frmLoginPanel.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginPanel() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLoginPanel = new JFrame();
		frmLoginPanel.getContentPane().setFont(new Font("Tycho", Font.PLAIN, 11));
		frmLoginPanel.getContentPane().setForeground(Color.BLACK);
		frmLoginPanel.setBackground(Color.MAGENTA);
		frmLoginPanel.setForeground(Color.RED);
		frmLoginPanel.setResizable(false);
		frmLoginPanel.setTitle("Login Panel");
		frmLoginPanel.setIconImage(Toolkit.getDefaultToolkit().getImage("E:\\MD ARIF UDDIN\\ARIF\\IMAGE\\Icons\\aun (13).png"));
		frmLoginPanel.setBounds(100, 100, 600, 300);
		frmLoginPanel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frmLoginPanel.getContentPane().setLayout(springLayout);
		
		JLabel lblLoginPanel = new JLabel("Login Panel");
		springLayout.putConstraint(SpringLayout.NORTH, lblLoginPanel, 10, SpringLayout.NORTH, frmLoginPanel.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblLoginPanel, 121, SpringLayout.WEST, frmLoginPanel.getContentPane());
		frmLoginPanel.getContentPane().add(lblLoginPanel);
		
		JLabel lblSignUpHere = new JLabel("Sign Up Here !");
		springLayout.putConstraint(SpringLayout.NORTH, lblSignUpHere, 0, SpringLayout.NORTH, lblLoginPanel);
		springLayout.putConstraint(SpringLayout.EAST, lblSignUpHere, -118, SpringLayout.EAST, frmLoginPanel.getContentPane());
		frmLoginPanel.getContentPane().add(lblSignUpHere);
		
		JLabel lblUsername = new JLabel("Username :");
		springLayout.putConstraint(SpringLayout.NORTH, lblUsername, 42, SpringLayout.NORTH, frmLoginPanel.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblUsername, 37, SpringLayout.WEST, frmLoginPanel.getContentPane());
		frmLoginPanel.getContentPane().add(lblUsername);
		
		user = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, user, 12, SpringLayout.SOUTH, lblLoginPanel);
		springLayout.putConstraint(SpringLayout.EAST, user, 119, SpringLayout.EAST, lblUsername);
		frmLoginPanel.getContentPane().add(user);
		user.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password :");
		springLayout.putConstraint(SpringLayout.NORTH, lblPassword, 23, SpringLayout.SOUTH, lblUsername);
		springLayout.putConstraint(SpringLayout.WEST, lblPassword, 0, SpringLayout.WEST, lblUsername);
		frmLoginPanel.getContentPane().add(lblPassword);
		
		pass = new JPasswordField();
		springLayout.putConstraint(SpringLayout.WEST, user, 0, SpringLayout.WEST, pass);
		springLayout.putConstraint(SpringLayout.NORTH, pass, 17, SpringLayout.SOUTH, user);
		springLayout.putConstraint(SpringLayout.WEST, pass, 8, SpringLayout.EAST, lblPassword);
		springLayout.putConstraint(SpringLayout.EAST, pass, 101, SpringLayout.EAST, lblPassword);
		frmLoginPanel.getContentPane().add(pass);
		
		JLabel lblUserName = new JLabel("User Name :");
		springLayout.putConstraint(SpringLayout.NORTH, lblUserName, 0, SpringLayout.NORTH, lblUsername);
		springLayout.putConstraint(SpringLayout.WEST, lblUserName, 111, SpringLayout.EAST, user);
		frmLoginPanel.getContentPane().add(lblUserName);
		
		reg_u = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, reg_u, 12, SpringLayout.SOUTH, lblSignUpHere);
		springLayout.putConstraint(SpringLayout.WEST, reg_u, 12, SpringLayout.EAST, lblUserName);
		springLayout.putConstraint(SpringLayout.EAST, reg_u, 137, SpringLayout.EAST, lblUserName);
		frmLoginPanel.getContentPane().add(reg_u);
		reg_u.setColumns(10);
		
		JLabel lblPassword_1 = new JLabel("Password :");
		springLayout.putConstraint(SpringLayout.WEST, lblPassword_1, 0, SpringLayout.WEST, lblUserName);
		springLayout.putConstraint(SpringLayout.SOUTH, lblPassword_1, 0, SpringLayout.SOUTH, lblPassword);
		frmLoginPanel.getContentPane().add(lblPassword_1);
		
		reg_p = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, reg_p, 17, SpringLayout.SOUTH, reg_u);
		springLayout.putConstraint(SpringLayout.WEST, reg_p, 18, SpringLayout.EAST, lblPassword_1);
		springLayout.putConstraint(SpringLayout.EAST, reg_p, 120, SpringLayout.EAST, lblPassword_1);
		frmLoginPanel.getContentPane().add(reg_p);
		reg_p.setColumns(10);
		
		JButton SignUp = new JButton("Sign Up !!!");
		SignUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reg_user=reg_u.getText();
				reg_pass=reg_p.getText();
				
				reg_u.setText("");
				reg_p.setText("");

				JOptionPane.showMessageDialog(null,"Successfully Registered");			
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, SignUp, 37, SpringLayout.SOUTH, reg_p);
		springLayout.putConstraint(SpringLayout.WEST, SignUp, 0, SpringLayout.WEST, lblSignUpHere);
		frmLoginPanel.getContentPane().add(SignUp);
		
		JButton Login = new JButton("Login");
		Login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String U=user.getText();
				String P=pass.getText();
				

				
				if(U.equals(reg_user) && P.equals(reg_pass)){
					Sign_in_window as=new Sign_in_window();
					as.setVisible(true);
					
					window.frmLoginPanel.setVisible(false);
					
					
				}
				else{
					JOptionPane.showMessageDialog(null,"Wrong input");
				}
				
				
				
				
				
			}
		});
		Login.setFont(new Font("Tycho", Font.PLAIN, 11));
		Login.setSelectedIcon(new ImageIcon("E:\\MD ARIF UDDIN\\ARIF\\IMAGE\\Icons\\aun (9).png"));
		springLayout.putConstraint(SpringLayout.NORTH, Login, 0, SpringLayout.NORTH, SignUp);
		springLayout.putConstraint(SpringLayout.EAST, Login, 0, SpringLayout.EAST, lblLoginPanel);
		frmLoginPanel.getContentPane().add(Login);
		
		result = new JLabel("result");
		springLayout.putConstraint(SpringLayout.NORTH, result, 18, SpringLayout.SOUTH, Login);
		springLayout.putConstraint(SpringLayout.WEST, result, 59, SpringLayout.WEST, frmLoginPanel.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, result, 110, SpringLayout.SOUTH, Login);
		springLayout.putConstraint(SpringLayout.EAST, result, 107, SpringLayout.EAST, user);
		frmLoginPanel.getContentPane().add(result);
	}
	protected void setvisible(boolean b) {
		// TODO Auto-generated method stub
		
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
