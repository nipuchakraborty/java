package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;
import java.awt.Cursor;
import javax.swing.border.LineBorder;

public class Div {

	private JFrame frame;
	private JTextField X;
	private JTextField Y;
	private JLabel lblstInput;
	private JLabel lblndInput;
	private JButton btnResult;
	private JTextArea res;
	private JButton btnReset;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Div window = new Div();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Div() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblAddintion = new JLabel("Substraction");
		lblAddintion.setBackground(Color.MAGENTA);
		lblAddintion.setBounds(166, 11, 173, 29);
		frame.getContentPane().add(lblAddintion);
		
		X = new JTextField();
		X.setSelectedTextColor(Color.BLUE);
		X.setSelectionColor(Color.YELLOW);
		X.setDisabledTextColor(Color.MAGENTA);
		X.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		X.setForeground(Color.WHITE);
		X.setText("1st input here");
		X.setBounds(147, 51, 142, 20);
		frame.getContentPane().add(X);
		X.setColumns(10);
		
		Y = new JTextField();
		Y.setToolTipText("awefgdsfsdsdfsdadfaf\r\n");
		Y.setForeground(Color.LIGHT_GRAY);
		Y.setText("2nd input here\r\n");
		Y.setBounds(147, 82, 142, 20);
		frame.getContentPane().add(Y);
		Y.setColumns(10);
		
		lblstInput = new JLabel("1st input :");
		lblstInput.setBounds(74, 54, 64, 14);
		frame.getContentPane().add(lblstInput);
		
		lblndInput = new JLabel("2nd input :");
		lblndInput.setBounds(74, 85, 63, 14);
		frame.getContentPane().add(lblndInput);
		
		btnResult = new JButton("Divitiion");
		btnResult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String a=X.getText();
				int a_num=Integer.parseInt(a);
				
				String b=Y.getText();
				int b_num=Integer.parseInt(b);
				
				int c=b_num/a_num;
				String c_string=Integer.toString(c);
				res.setText(c_string);
				
				
				
				
			}
		});
		btnResult.setSelectedIcon(new ImageIcon("C:\\Users\\Arif\\Desktop\\shere\\meeting-icon-30.png"));
		btnResult.setIcon(null);
		btnResult.setBounds(181, 191, 69, 23);
		frame.getContentPane().add(btnResult);
		
		res = new JTextArea();
		res.setBounds(319, 65, 89, 22);
		frame.getContentPane().add(res);
		
		JButton btnAddition = new JButton("Addition");
		btnAddition.setAutoscrolls(true);
		btnAddition.setDisplayedMnemonicIndex(-5);
		btnAddition.setFocusable(false);
		btnAddition.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnAddition.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String a=X.getText();
				int a_num=Integer.parseInt(a);
				
				String b=Y.getText();
				int b_num=Integer.parseInt(b);
				
				int c=b_num+a_num;
				String c_string=Integer.toString(c);
				res.setText(c_string);
			}
		});
		btnAddition.setBounds(25, 191, 69, 23);
		frame.getContentPane().add(btnAddition);
		
		JButton btnSubtraction = new JButton("Subtraction\r\n");
		btnSubtraction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String a=X.getText();
				int a_num=Integer.parseInt(a);
				
				String b=Y.getText();
				int b_num=Integer.parseInt(b);
				
				int c=a_num-b_num;
				String c_string=Integer.toString(c);
				res.setText(c_string);
			}
		});
		btnSubtraction.setBounds(104, 191, 69, 23);
		frame.getContentPane().add(btnSubtraction);
		
		JButton btnMultipication = new JButton("Multipication");
		btnMultipication.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String a=X.getText();
				int a_num=Integer.parseInt(a);
				
				String b=Y.getText();
				int b_num=Integer.parseInt(b);
				
				int c=a_num*b_num;
				String c_string=Integer.toString(c);
				res.setText(c_string);
			}
		});
		btnMultipication.setBounds(260, 191, 69, 23);
		frame.getContentPane().add(btnMultipication);
		
		JButton btnModulus = new JButton("Modulus");
		btnModulus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String a=X.getText();
				int a_num=Integer.parseInt(a);
				
				String b=Y.getText();
				int b_num=Integer.parseInt(b);
				
				int c=a_num%b_num;
				String c_string=Integer.toString(c);
				res.setText(c_string);
			}
		});
		btnModulus.setBounds(342, 191, 69, 23);
		frame.getContentPane().add(btnModulus);
		
		JLabel lblResult = new JLabel("Result");
		lblResult.setBounds(340, 40, 46, 14);
		frame.getContentPane().add(lblResult);
		
		btnReset = new JButton("RESET");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				X.setText("");
				Y.setText("");
				res.setText("");
			}
		});
		btnReset.setBounds(147, 225, 103, 23);
		frame.getContentPane().add(btnReset);
	}
}
