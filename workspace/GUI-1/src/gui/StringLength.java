package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class StringLength {

	private JFrame frame;
	private JTextField a;
	private JTextArea length;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StringLength window = new StringLength();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public StringLength() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JLabel lblGetASentence = new JLabel("GET A Sentence Length");
		springLayout.putConstraint(SpringLayout.NORTH, lblGetASentence, 10, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblGetASentence, 138, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblGetASentence, 251, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblGetASentence);
		
		JLabel lblTypeASentence = new JLabel("Type a sentence :: ");
		springLayout.putConstraint(SpringLayout.NORTH, lblTypeASentence, 74, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblTypeASentence, 27, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblTypeASentence, -313, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(lblTypeASentence);
		
		a = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, a, 0, SpringLayout.NORTH, lblTypeASentence);
		springLayout.putConstraint(SpringLayout.WEST, a, 17, SpringLayout.EAST, lblTypeASentence);
		springLayout.putConstraint(SpringLayout.EAST, a, 412, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(a);
		a.setColumns(10);
		
		JButton btnGetLength = new JButton("Get Length");
		btnGetLength.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String x=a.getText();
				int o=x.length();
				String out=Integer.toString(o);
				length.setText(out);
				a.setText("");
				
			}
		});
		springLayout.putConstraint(SpringLayout.SOUTH, a, -28, SpringLayout.NORTH, btnGetLength);
		springLayout.putConstraint(SpringLayout.NORTH, btnGetLength, 98, SpringLayout.SOUTH, lblGetASentence);
		springLayout.putConstraint(SpringLayout.WEST, btnGetLength, 0, SpringLayout.WEST, lblGetASentence);
		frame.getContentPane().add(btnGetLength);
		
		length = new JTextArea();
		springLayout.putConstraint(SpringLayout.NORTH, length, 43, SpringLayout.SOUTH, btnGetLength);
		springLayout.putConstraint(SpringLayout.WEST, length, 153, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, length, 266, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(length);
	}
}
