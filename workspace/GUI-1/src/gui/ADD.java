package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class ADD {

	private JFrame frame;
	private JTextField X;
	private JTextField Y;
	private JLabel lblstInput;
	private JLabel lblndInput;
	private JButton btnResult;
	private JTextArea res;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ADD window = new ADD();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ADD() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblAddintion = new JLabel("ADDINTION");
		lblAddintion.setBackground(Color.MAGENTA);
		lblAddintion.setBounds(166, 11, 173, 29);
		frame.getContentPane().add(lblAddintion);
		
		X = new JTextField();
		X.setForeground(Color.LIGHT_GRAY);
		X.setText("1st input here");
		X.setBounds(147, 51, 142, 20);
		frame.getContentPane().add(X);
		X.setColumns(10);
		
		Y = new JTextField();
		Y.setToolTipText("awefgdsfsdsdfsdadfaf\r\n");
		Y.setForeground(Color.LIGHT_GRAY);
		Y.setText("2nd input here\r\n");
		Y.setBounds(147, 82, 142, 20);
		frame.getContentPane().add(Y);
		Y.setColumns(10);
		
		lblstInput = new JLabel("1st input :");
		lblstInput.setBounds(74, 54, 64, 14);
		frame.getContentPane().add(lblstInput);
		
		lblndInput = new JLabel("2nd input :");
		lblndInput.setBounds(74, 85, 63, 14);
		frame.getContentPane().add(lblndInput);
		
		btnResult = new JButton("Result");
		btnResult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String a=X.getText();
				int a_num=Integer.parseInt(a);
				
				String b=Y.getText();
				int b_num=Integer.parseInt(b);
				
				int c=a_num+b_num;
				String c_string=Integer.toString(c);
				res.setText(c_string);
				
				
				
				
			}
		});
		btnResult.setSelectedIcon(new ImageIcon("C:\\Users\\Arif\\Desktop\\shere\\meeting-icon-30.png"));
		btnResult.setIcon(null);
		btnResult.setBounds(166, 137, 89, 23);
		frame.getContentPane().add(btnResult);
		
		res = new JTextArea();
		res.setBounds(319, 65, 89, 22);
		frame.getContentPane().add(res);
	}
}
