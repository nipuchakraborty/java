package viwe;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Manage;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InfoDesign extends JFrame {

	private JPanel contentPane;
	private JTextField name_txt;
	private JTextField brith_date_txt;
	private JTextField titel_txt;
	public JRadioButton female_radio;
	public JRadioButton male_radio;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InfoDesign frame = new InfoDesign();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InfoDesign() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 556);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(32, 178, 170));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBrithdayInformation = new JLabel("Brithday Information");
		lblBrithdayInformation.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblBrithdayInformation.setBounds(125, 25, 202, 27);
		contentPane.add(lblBrithdayInformation);
		
		JLabel lblBrithDayDate = new JLabel("Brith day Date");
		lblBrithDayDate.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblBrithDayDate.setBounds(12, 180, 146, 27);
		contentPane.add(lblBrithDayDate);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblName.setBounds(12, 111, 146, 27);
		contentPane.add(lblName);
		
		JLabel lblTitel = new JLabel("Titel");
		lblTitel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblTitel.setBounds(12, 246, 146, 27);
		contentPane.add(lblTitel);
		
		JLabel lblGender = new JLabel("Gender");
		lblGender.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblGender.setBounds(12, 325, 96, 27);
		contentPane.add(lblGender);
		
		male_radio = new JRadioButton("Male");
		male_radio.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
			
			}
		});
		male_radio.setBackground(new Color(255, 0, 255));
		male_radio.setBounds(95, 328, 63, 25);
		contentPane.add(male_radio);
		
		female_radio = new JRadioButton("female");
		female_radio.setBackground(new Color(255, 0, 255));
		female_radio.setBounds(210, 328, 74, 25);
		contentPane.add(female_radio);
		
		name_txt = new JTextField();
		name_txt.setBackground(new Color(245, 245, 220));
		name_txt.setBounds(168, 115, 202, 22);
		contentPane.add(name_txt);
		name_txt.setColumns(10);
		
		brith_date_txt = new JTextField();
		brith_date_txt.setBackground(new Color(255, 235, 205));
		brith_date_txt.setColumns(10);
		brith_date_txt.setBounds(168, 184, 202, 22);
		contentPane.add(brith_date_txt);
		
		titel_txt = new JTextField();
		titel_txt.setBackground(new Color(255, 228, 196));
		titel_txt.setColumns(10);
		titel_txt.setBounds(168, 250, 202, 22);
		contentPane.add(titel_txt);
		
		JButton save_btn = new JButton("Save");
		save_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name=name_txt.getText();
				String bd=brith_date_txt.getText();
				String titel=titel_txt.getText();
				String[]radio=new String[2];
				radio[0]=male_radio.getText();
				radio[1]=female_radio.getText();
				
				Manage manage=new Manage();
				manage.Birthinfo_manage(name, bd, titel,radio.toString());
				
			}
		});
		save_btn.setBounds(279, 417, 97, 25);
		contentPane.add(save_btn);
	}
}
