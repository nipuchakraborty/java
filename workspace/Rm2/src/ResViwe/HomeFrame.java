package ResViwe;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import java.awt.Color;
import javax.swing.JTabbedPane;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.border.MatteBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JMenuItem;
import javax.swing.JCheckBox;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import java.awt.Insets;
import javax.swing.border.LineBorder;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import java.awt.Font;

public class HomeFrame extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomeFrame frame = new HomeFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public HomeFrame() {
		//setBackground(new Color(0, 0, 205));
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1401, 762);
		setUndecorated(true);
		JMenuBar menuBar = new JMenuBar();
		menuBar.setMargin(new Insets(3, 3, 3, 3));
		menuBar.setBackground(new Color(0, 0, 128));
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("File");
		mnNewMenu.setForeground(new Color(255, 255, 0));
		mnNewMenu.setBackground(new Color(220, 20, 60));
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmOpen = new JMenuItem("Open");
		mntmOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser jF=new JFileChooser();
				jF.setCurrentDirectory(new File(System.getProperty("user.home")));
				 FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images", "*.jpg","*.gif","*.png","*.txt","*.doc","*.All");
				 jF.addChoosableFileFilter(filter);
				 int result = jF.showSaveDialog(null);
			}
		});
		mnNewMenu.add(mntmOpen);
		
		JMenuItem mntmSetting = new JMenuItem("Setting");
		mnNewMenu.add(mntmSetting);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mnNewMenu.add(mntmAbout);
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mnNewMenu.add(mntmSave);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mnNewMenu.add(mntmExit);
		
		JMenu mnEdit = new JMenu("Edit");
		mnEdit.setForeground(new Color(255, 255, 0));
		mnEdit.setBackground(new Color(153, 50, 204));
		menuBar.add(mnEdit);
		
		JMenuItem mntmAddItem = new JMenuItem("Add Item");
		mnEdit.add(mntmAddItem);
		
		JMenuItem mntmAddPrice = new JMenuItem("Add price");
		mnEdit.add(mntmAddPrice);
		
		JMenuItem mntmAddStatus = new JMenuItem("Add Status");
		mnEdit.add(mntmAddStatus);
		
		JMenuItem mntmRemoveItem = new JMenuItem("Remove Item");
		mnEdit.add(mntmRemoveItem);
		
		JMenu mnDishes = new JMenu("Dishes");
		mnDishes.setForeground(new Color(255, 255, 0));
		menuBar.add(mnDishes);
		
		JMenu mnManagment = new JMenu("Managment");
		mnManagment.setForeground(new Color(255, 255, 0));
		menuBar.add(mnManagment);
		
		JMenu mnDailyActivites = new JMenu("Daily activites");
		mnDailyActivites.setForeground(new Color(255, 255, 0));
		menuBar.add(mnDailyActivites);
		
		JMenu mnAboutUs = new JMenu("About us");
		mnAboutUs.setForeground(new Color(255, 255, 0));
		menuBar.add(mnAboutUs);
		
		JMenuItem mntmAbout_1 = new JMenuItem("About");
		mntmAbout_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				About about=new About();
				about.setVisible(true);
			}
		});
		mnAboutUs.add(mntmAbout_1);
		contentPane = new JPanel();
		contentPane.setBorder(new MatteBorder(3, 3, 3, 3, (Color) Color.GRAY));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 0, 205));
		contentPane.add(panel, "cell 0 0,grow");
		panel.setLayout(new MigLayout("", "[826px][][][][][][][][][grow]", "[488px,grow]"));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		panel.add(tabbedPane, "cell 0 0,grow");
		
		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addTab("Breakfast", null, tabbedPane_1, null);
		
		JInternalFrame internalFrame_4 = new JInternalFrame("Breakfast");
		internalFrame_4.getContentPane().setForeground(new Color(0, 0, 205));
		internalFrame_4.getContentPane().setBackground(new Color(0, 0, 139));
		internalFrame_4.getContentPane().setLayout(null);
		
		JLabel l1 = new JLabel("");
		l1.setForeground(new Color(255, 255, 0));
		l1.setBorder(new LineBorder(new Color(0, 0, 0)));
		l1.setBackground(new Color(0, 0, 0));
		l1.setBounds(35, 13, 52, 56);
		l1.setText("Tea");
		l1.setHorizontalTextPosition(JLabel.CENTER);
		l1.setVerticalTextPosition(JLabel.BOTTOM);
		l1.setIcon(new ImageIcon(HomeFrame.class.getResource("/picture/1495295838_Coffee.png")));
		internalFrame_4.getContentPane().add(l1);
		
		JLabel l2 = new JLabel();
		l2.setForeground(new Color(255, 255, 0));
		l2.setText("Cheken Chap");
		l2.setHorizontalTextPosition(JLabel.CENTER);
		l2.setVerticalTextPosition(JLabel.BOTTOM);
		l2.setIcon(new ImageIcon(HomeFrame.class.getResource("/picture/1495295893_Turkey.png")));
		l2.setBounds(12, 109, 88, 68);
		internalFrame_4.getContentPane().add(l2);
		
		JCheckBox ch1 = new JCheckBox("");
		ch1.setBackground(new Color(0, 0, 139));
		ch1.setBounds(112, 32, 25, 25);
		internalFrame_4.getContentPane().add(ch1);
		
		JCheckBox ch2 = new JCheckBox("");
		ch2.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		ch2.setBackground(new Color(0, 0, 139));
		ch2.setBounds(112, 122, 25, 25);
		internalFrame_4.getContentPane().add(ch2);
		
		JButton btnOrder = new JButton("Order");
		btnOrder.setFont(new Font("SansSerif", Font.BOLD, 18));
		btnOrder.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null), new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0))));
		btnOrder.setFocusable(false);
		btnOrder.setFocusCycleRoot(true);
		btnOrder.setIgnoreRepaint(true);
		btnOrder.setInheritsPopupMenu(true);
		btnOrder.setIconTextGap(5);
		btnOrder.setHorizontalTextPosition(SwingConstants.LEFT);
		btnOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (ch1.isSelected()){
					String item=l1.getText();
					System.out.println(item);
				
				}
				if (ch2.isSelected()){
					String item2=l2.getText();
					System.out.println(item2);
				
				}
			}
		});
		btnOrder.setIcon(new ImageIcon(HomeFrame.class.getResource("/picture/1495497606_Click.png")));
		btnOrder.setBounds(675, 505, 114, 56);
		internalFrame_4.getContentPane().add(btnOrder);
		tabbedPane_1.addTab("Menu Chart", null, internalFrame_4, null);
		internalFrame_4.setVisible(true);
		
		JTabbedPane tabbedPane_2 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addTab("Dinner", null, tabbedPane_2, null);
		
		JInternalFrame internalFrame_3 = new JInternalFrame("Dinner");
		internalFrame_3.getContentPane().setBackground(new Color(0, 0, 139));
		tabbedPane_2.addTab("Menu chart", null, internalFrame_3, null);
		internalFrame_3.setVisible(true);
		
		JTabbedPane tabbedPane_4 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addTab("Lunch", null, tabbedPane_4, null);
		
		JInternalFrame internalFrame_2 = new JInternalFrame("Luch");
		internalFrame_2.getContentPane().setBackground(new Color(0, 0, 139));
		tabbedPane_4.addTab("Menu Chart", null, internalFrame_2, null);
		internalFrame_2.setVisible(true);
		
		JTabbedPane tabbedPane_5 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addTab("Spcial dish", null, tabbedPane_5, null);
		
		JInternalFrame internalFrame_1 = new JInternalFrame("Spcial dish");
		internalFrame_1.getContentPane().setBackground(new Color(0, 0, 139));
		tabbedPane_5.addTab("Menu Chart", null, internalFrame_1, null);
		internalFrame_1.setVisible(true);
		
		JTabbedPane tabbedPane_6 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addTab("New tab", null, tabbedPane_6, null);
		
		JInternalFrame internalFrame = new JInternalFrame("New JInternalFrame");
		internalFrame.getContentPane().setBackground(new Color(0, 0, 139));
		tabbedPane_6.addTab("New tab", null, internalFrame, null);
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, "cell 2 0 8 1,grow");
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Name of Dish", "Price", "Date"
			}
		));
		table.getColumnModel().getColumn(0).setPreferredWidth(91);
		scrollPane.setViewportView(table);
		internalFrame.setVisible(true);
	}
}
