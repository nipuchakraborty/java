package Restruant;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.crypto.AEADBadTagException;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import Restruant.LoginModel.Model;
import net.miginfocom.swing.MigLayout;
import java.awt.Color;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Window.Type;
import java.awt.Dimension;
import java.awt.Font;



public class Login extends JFrame{
	
    JButton button ;
    JLabel label;
    private JTextField username;
    private JPasswordField passwordF;
    
    public Login(){
    super("Longin");
    setFont(new Font("Dialog", Font.BOLD, 18));
    setType(Type.UTILITY);
    getContentPane().setBackground(new Color(0, 0, 139));
    getContentPane().setForeground(new Color(204, 204, 255));
    setModalExclusionType(ModalExclusionType.TOOLKIT_EXCLUDE);
    setBackground(new Color(51, 102, 153));
    setResizable(false);
    getContentPane().setLayout(null);
    label = new JLabel();
    label.setBounds(169, 17, 128, 128);
    label.setIcon(new ImageIcon(Login.class.getResource("/javax/swing/plaf/basic/icons/image-delayed.png")));
    getContentPane().add(label);
    button = new JButton("Browse");
    button.setFont(new Font("SansSerif", Font.PLAIN, 18));
    button.setBounds(179, 157, 107, 28);
    getContentPane().add(button);
    
    button.addActionListener(new ActionListener() {

        public void actionPerformed(ActionEvent e) {
        
          JFileChooser file = new JFileChooser();
          file.setCurrentDirectory(new File(System.getProperty("user.home")));
          //filter the files
          FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images", "jpg","gif","png");
          file.addChoosableFileFilter(filter);
          int result = file.showSaveDialog(null);
           //if the user click on save in Jfilechooser
          if(result == JFileChooser.APPROVE_OPTION){
              File selectedFile = file.getSelectedFile();
              String path = selectedFile.getAbsolutePath();
              label.setIcon(ResizeImage(path));
          }
           //if the user click on save in Jfilechooser


          else if(result == JFileChooser.CANCEL_OPTION){
              System.out.println("No File Select");
          }
        }
    });
    
    JLabel lblUserName = new JLabel("User name");
    lblUserName.setFont(new Font("SansSerif", Font.BOLD, 18));
    lblUserName.setBounds(7, 233, 99, 16);
    lblUserName.setForeground(new Color(255, 255, 255));
    lblUserName.setBackground(new Color(0, 0, 205));
    getContentPane().add(lblUserName);
    
    JLabel lblPassword = new JLabel("password");
    lblPassword.setFont(new Font("SansSerif", Font.BOLD, 18));
    lblPassword.setBounds(7, 265, 99, 16);
    lblPassword.setForeground(new Color(255, 255, 255));
    lblPassword.setBackground(new Color(0, 255, 102));
    getContentPane().add(lblPassword);
    
    username = new JTextField();
    username.setBounds(148, 227, 247, 28);
    getContentPane().add(username);
    username.setColumns(10);
    
    passwordF = new JPasswordField();
    passwordF.setBounds(148, 259, 247, 28);
    getContentPane().add(passwordF);
    
    JComboBox comboBox = new JComboBox();
    comboBox.setBounds(148, 291, 155, 26);
    comboBox.setPreferredSize(new Dimension(31, 26));
    comboBox.setModel(new DefaultComboBoxModel(new String[] {"Select Your Post", "Manager", "Accounted", "Worker"}));
    getContentPane().add(comboBox);
    
    JLabel lblPost = new JLabel("Post");
    lblPost.setFont(new Font("SansSerif", Font.BOLD, 18));
    lblPost.setBounds(7, 296, 62, 16);
    lblPost.setForeground(new Color(255, 255, 255));
    lblPost.setBackground(new Color(0, 0, 139));
    getContentPane().add(lblPost);
    
    JButton btnNewAccount = new JButton("New account");
    btnNewAccount.setBounds(296, 334, 99, 28);
    btnNewAccount.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent arg0) {
    		NewAccount newAccount =new NewAccount();
    		newAccount.setVisible(true);
    	}
    });
    getContentPane().add(btnNewAccount);
    
    JButton btnLogin = new JButton("Login");
    btnLogin.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent arg0) {
    		String un=username.getText();
    		String pa=passwordF.getText();
    		String poS=comboBox.getSelectedItem().toString();
    	}
    });
    btnLogin.setBounds(326, 299, 69, 28);
    getContentPane().add(btnLogin);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setLocationRelativeTo(null);
    setSize(464,421);
    setVisible(true);
    }
     

	// Methode to resize imageIcon with the same size of a Jlabel
    public ImageIcon ResizeImage(String ImagePath)
    {
        ImageIcon MyImage = new ImageIcon(ImagePath);
        Image img = MyImage.getImage();
        Image newImg = img.getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon image = new ImageIcon(newImg);
        return image;
    }
    
    public static void main(String[] args){
        new Login();
    }
   }