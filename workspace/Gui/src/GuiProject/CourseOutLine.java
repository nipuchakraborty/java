package GuiProject;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;

public class CourseOutLine extends JFrame {

	private JPanel contentPane;
	private JTextField ClassName;
	private JTextField Show;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CourseOutLine frame = new CourseOutLine();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CourseOutLine() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 545, 472);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblFfiCourseLayout = new JLabel("FFI course Layout");
		lblFfiCourseLayout.setFont(new Font("SansSerif", Font.PLAIN, 16));
		lblFfiCourseLayout.setBounds(188, 25, 158, 16);
		contentPane.add(lblFfiCourseLayout);
		
		JLabel lblEnterClass = new JLabel("Enter Class:");
		lblEnterClass.setForeground(Color.BLUE);
		lblEnterClass.setFont(new Font("SansSerif", Font.PLAIN, 17));
		lblEnterClass.setBounds(25, 104, 112, 27);
		contentPane.add(lblEnterClass);
		
		ClassName = new JTextField();
		ClassName.setBounds(134, 105, 340, 35);
		contentPane.add(ClassName);
		ClassName.setColumns(10);
		
		Show = new JTextField();
		Show.setForeground(new Color(255, 0, 255));
		Show.setBounds(134, 152, 343, 247);
		contentPane.add(Show);
		Show.setColumns(10);
		
		JButton Click = new JButton("Click");
		Click.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String cString=ClassName.getText();
				cString=cString.toLowerCase();
			
				if (cString.equals(cString)){
				Show.setText("introduction\n"+"instaltion\n"+"Class\n"+"Variables");
			}
				else if (cString.equals("Second Class")) {
					Show.setText("Variables\n"+"Method\n"+"Oparator\n"+"Quiz");

				}
				else {
					Show.setText("you Class Not take!");
				}
				}
		});
		Click.setBounds(6, 262, 90, 80);
		contentPane.add(Click);
		
		JMenuBar Menu = new JMenuBar();
		Menu.setToolTipText("File \r\nOpen \r\nExit");
		Menu.setBounds(6, 6, 111, 23);
		contentPane.add(Menu);
		
		JLabel label = new JLabel("");
		Menu.add(label);
	}
}
