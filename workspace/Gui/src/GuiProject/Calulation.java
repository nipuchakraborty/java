package GuiProject;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Calulation {

	private JFrame frame;
	private JTextField FirstNumber;
	private JTextField SecoundNumber;
	private JTextField Answer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Calulation window = new Calulation();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	double firstNum;
	double secNum;
	String oparation;
	
	public Calulation() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 701, 555);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		FirstNumber = new JTextField();
		FirstNumber.setFont(new Font("SansSerif", Font.BOLD, 16));
		FirstNumber.setForeground(Color.MAGENTA);
		FirstNumber.setBounds(310, 80, 127, 33);
		frame.getContentPane().add(FirstNumber);
		FirstNumber.setColumns(10);
		
		JLabel lblCalculation = new JLabel("CALCULATION");
		lblCalculation.setBounds(113, 80, 188, -17);
		frame.getContentPane().add(lblCalculation);
		
		JLabel lblEnterTheFirst = new JLabel("Enter the first Number ");
		lblEnterTheFirst.setForeground(Color.RED);
		lblEnterTheFirst.setFont(new Font("SansSerif", Font.PLAIN, 18));
		lblEnterTheFirst.setBounds(33, 76, 227, 37);
		frame.getContentPane().add(lblEnterTheFirst);
		
		JLabel lblEnterTheSecound = new JLabel("Enter the Secound Number ");
		lblEnterTheSecound.setForeground(Color.RED);
		lblEnterTheSecound.setFont(new Font("SansSerif", Font.BOLD, 16));
		lblEnterTheSecound.setBounds(33, 193, 211, 46);
		frame.getContentPane().add(lblEnterTheSecound);
		
		SecoundNumber = new JTextField();
		SecoundNumber.setForeground(Color.MAGENTA);
		SecoundNumber.setFont(new Font("SansSerif", Font.BOLD, 16));
		SecoundNumber.setBounds(310, 204, 127, 35);
		frame.getContentPane().add(SecoundNumber);
		SecoundNumber.setColumns(10);
		
		JButton sum = new JButton("Sum");
		sum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			String fist=FirstNumber.getText();
			String secon=SecoundNumber.getText();
			int first=Integer.parseInt(fist);
			int second=Integer.parseInt(secon);
			int result=first+second;
			
			Answer.setText(String.valueOf(result));
			
			
			
			
			//problemm##########################
			
			//double first=Double.valueOf(firstNum);
			//double secound=Double.valueOf(secNum);
			
			
			//double answer=Double.valueOf(Answer);
			//problemm##########################
			}
		});
		sum.setBounds(33, 402, 90, 28);
		frame.getContentPane().add(sum);
		
		JButton sub = new JButton("Sub");
		sub.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String fist=FirstNumber.getText();
				String secon=SecoundNumber.getText();
				//Answer.setText(fist-secon);
			}
		});
		sub.setBounds(127, 402, 90, 28);
		frame.getContentPane().add(sub);
		
		JButton mul = new JButton("Mul");
		mul.setBounds(229, 402, 90, 28);
		frame.getContentPane().add(mul);
		
		JButton div = new JButton("Div");
		div.setBounds(331, 402, 90, 28);
		frame.getContentPane().add(div);
		
		JButton mod = new JButton("Mod");
		mod.setBounds(433, 402, 90, 28);
		frame.getContentPane().add(mod);
		
		JButton btnSwap = new JButton("Swap");
		btnSwap.setBounds(534, 402, 90, 28);
		frame.getContentPane().add(btnSwap);
		
		JLabel lblClickAButton = new JLabel("Click A Button ");
		lblClickAButton.setFont(new Font("SansSerif", Font.BOLD | Font.ITALIC, 22));
		lblClickAButton.setBounds(260, 308, 177, 52);
		frame.getContentPane().add(lblClickAButton);
		
		Answer = new JTextField();
		Answer.setBounds(468, 66, 167, 175);
		frame.getContentPane().add(Answer);
		Answer.setColumns(10);
		
		JLabel lblYourResult = new JLabel("Your Result ");
		lblYourResult.setFont(new Font("SansSerif", Font.PLAIN, 19));
		lblYourResult.setBounds(496, 251, 103, 28);
		frame.getContentPane().add(lblYourResult);
	}
}
