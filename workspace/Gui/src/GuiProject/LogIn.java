package GuiProject;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.UIManager;

public class LogIn extends JFrame {

	private JPanel contentPane;
	private JTextField UserName;
	private JTextField password;
	private JTextField regName;
	private JTextField regpassword;
	private String Regname;
	private String Regpass;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LogIn frame = new LogIn();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LogIn() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\NkChakraborty\\Downloads\\Aritel_BD_Logo.jpg"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 816, 419);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Log In pannel");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblNewLabel.setBounds(252, 13, 241, 37);
		contentPane.add(lblNewLabel);
		
		JLabel lblUsername = new JLabel("UserName");
		lblUsername.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblUsername.setBounds(33, 93, 111, 37);
		contentPane.add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblPassword.setBounds(33, 170, 111, 37);
		contentPane.add(lblPassword);
		
		UserName = new JTextField();
		UserName.setForeground(UIManager.getColor("ToolBar.dockingForeground"));
		UserName.setBounds(206, 93, 182, 31);
		contentPane.add(UserName);
		UserName.setColumns(10);
		
		password = new JTextField();
		password.setColumns(10);
		password.setBounds(206, 179, 182, 31);
		contentPane.add(password);
		
		JButton Login = new JButton("Lon in");
		Login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name=UserName.getText();
				String pass=password.getText();
				if(name.equals(Regname)&&pass.equals(Regpass)){
					Mobile_oparator mobile_oprator=new Mobile_oparator();
					mobile_oprator.setVisible(true);
					LogIn.this.setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, "Sorry!");
				}
				
			}

			
		});
		Login.setBounds(289, 240, 97, 25);
		contentPane.add(Login);
		
		JLabel label = new JLabel("Password");
		label.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		label.setBounds(431, 170, 111, 37);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("UserName");
		label_1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		label_1.setBounds(431, 93, 111, 37);
		contentPane.add(label_1);
		
		regName = new JTextField();
		regName.setColumns(10);
		regName.setBounds(604, 93, 182, 31);
		contentPane.add(regName);
		
		regpassword = new JTextField();
		regpassword.setColumns(10);
		regpassword.setBounds(604, 179, 182, 31);
		contentPane.add(regpassword);
		
		JButton Signup = new JButton("Sign Up");
		Signup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Regname=regName.getText();
				Regpass=regpassword.getText();
				regName.setText("");
				regpassword.setText("");
				//UserName.setText(Regname);
				//password.setText(Regpass);
				
				JOptionPane.showMessageDialog(null, " Now Can Log in! ");	
				
			}
		});
		Signup.setBounds(687, 240, 97, 25);
		contentPane.add(Signup);
	}

}
