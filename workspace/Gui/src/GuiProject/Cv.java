package GuiProject;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.JScrollBar;

public class Cv extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField textField_14;
	private JTextField textField_10;
	private JTextField textField_15;
	private JTextField textField_16;
	private JTextField textField_17;
	private JTextField textField_18;
	private JTextField textField_19;
	private JTextField textField_20;
	private JTextField textField_21;
	private JTextField textField_22;
	private JTextField textField_23;
	private JTextField textField_24;
	private JTextField textField_25;
	private JTextField textField_26;
	private JTextField textField_27;
	private JTextField textField_28;
	private JTextField textField_29;
	private JTextField textField_30;
	private JTextField textField_31;
	private JTextField textField_32;
	private JTextField textField_33;
	private JTextField textField_34;
	private JTextField textField_35;
	private JTextField textField_36;
	private JTextField textField_37;
	private JTextField textField_38;
	private JTextField textField_39;
	private JTextField textField_40;
	private JTextField textField_41;
	private JTextField textField_42;
	private JTextField textField_43;
	private JTextField textField_44;
	private JTextField textField_45;
	private JTextField textField_46;
	private JTextField textField_47;
	private JTextField textField_48;
	private JTextField textField_49;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cv frame = new Cv();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Cv() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 699, 1283);
		contentPane = new JPanel();
		contentPane.setForeground(SystemColor.desktop);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMakingACv = new JLabel("MAKING A CV ");
		lblMakingACv.setFont(new Font("Tahoma", Font.PLAIN, 23));
		lblMakingACv.setForeground(Color.BLUE);
		lblMakingACv.setBounds(259, 13, 181, 32);
		contentPane.add(lblMakingACv);
		
		JLabel lblName = new JLabel("NAME:");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 19));
		lblName.setBounds(30, 76, 94, 16);
		contentPane.add(lblName);
		
		JLabel lblAddress = new JLabel("ADDRESS:");
		lblAddress.setFont(new Font("Tahoma", Font.PLAIN, 19));
		lblAddress.setBounds(30, 115, 94, 16);
		contentPane.add(lblAddress);
		
		JLabel lblFatherName = new JLabel("FATHER NAME:");
		lblFatherName.setFont(new Font("Tahoma", Font.PLAIN, 19));
		lblFatherName.setBounds(30, 154, 168, 16);
		contentPane.add(lblFatherName);
		
		JLabel lblMotherName = new JLabel("MOTHER NAME:");
		lblMotherName.setFont(new Font("Tahoma", Font.PLAIN, 19));
		lblMotherName.setBounds(30, 187, 138, 16);
		contentPane.add(lblMotherName);
		
		JLabel lblBloodGroup = new JLabel("BLOOD GROUP:");
		lblBloodGroup.setFont(new Font("Tahoma", Font.PLAIN, 19));
		lblBloodGroup.setBounds(30, 222, 138, 16);
		contentPane.add(lblBloodGroup);
		
		JLabel lblEducationInformation = new JLabel("EDUCATION INFORMATION");
		lblEducationInformation.setFont(new Font("Tahoma", Font.PLAIN, 19));
		lblEducationInformation.setBounds(208, 267, 250, 16);
		contentPane.add(lblEducationInformation);
		
		JLabel lblPrimarySchoolName = new JLabel("PRIMARY SCHOOL NAME :");
		lblPrimarySchoolName.setBounds(30, 314, 168, 32);
		contentPane.add(lblPrimarySchoolName);
		
		JLabel lblGpa = new JLabel("GPA:");
		lblGpa.setBounds(30, 346, 56, 16);
		contentPane.add(lblGpa);
		
		JLabel lblYear = new JLabel("YEAR:");
		lblYear.setBounds(413, 349, 56, 16);
		contentPane.add(lblYear);
		
		JLabel lblBoard = new JLabel("BOARD:");
		lblBoard.setBounds(30, 375, 56, 16);
		contentPane.add(lblBoard);
		
		JLabel lblExamName = new JLabel("EXAM NAME:");
		lblExamName.setBounds(413, 378, 75, 16);
		contentPane.add(lblExamName);
		
		textField = new JTextField();
		textField.setBounds(190, 319, 435, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(102, 343, 116, 22);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(509, 343, 116, 22);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(102, 372, 116, 22);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(509, 372, 116, 22);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblHighSchoolName = new JLabel("HIGH SCHOOL NAME :");
		lblHighSchoolName.setBounds(30, 438, 168, 32);
		contentPane.add(lblHighSchoolName);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(190, 443, 435, 22);
		contentPane.add(textField_5);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(102, 467, 116, 22);
		contentPane.add(textField_6);
		
		JLabel label_1 = new JLabel("GPA:");
		label_1.setBounds(30, 470, 56, 16);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("BOARD:");
		label_2.setBounds(30, 499, 56, 16);
		contentPane.add(label_2);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(102, 496, 116, 22);
		contentPane.add(textField_7);
		
		JLabel label_3 = new JLabel("YEAR:");
		label_3.setBounds(402, 473, 56, 16);
		contentPane.add(label_3);
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(509, 467, 116, 22);
		contentPane.add(textField_8);
		
		JLabel label_4 = new JLabel("EXAM NAME:");
		label_4.setBounds(402, 499, 75, 16);
		contentPane.add(label_4);
		
		textField_9 = new JTextField();
		textField_9.setColumns(10);
		textField_9.setBounds(509, 496, 116, 22);
		contentPane.add(textField_9);
		
		textField_11 = new JTextField();
		textField_11.setColumns(10);
		textField_11.setBounds(102, 528, 116, 22);
		contentPane.add(textField_11);
		
		JLabel label_5 = new JLabel("GPA:");
		label_5.setBounds(30, 531, 56, 16);
		contentPane.add(label_5);
		
		JLabel label_6 = new JLabel("BOARD:");
		label_6.setBounds(30, 560, 56, 16);
		contentPane.add(label_6);
		
		textField_12 = new JTextField();
		textField_12.setColumns(10);
		textField_12.setBounds(102, 557, 116, 22);
		contentPane.add(textField_12);
		
		JLabel label_7 = new JLabel("YEAR:");
		label_7.setBounds(402, 531, 56, 16);
		contentPane.add(label_7);
		
		textField_13 = new JTextField();
		textField_13.setColumns(10);
		textField_13.setBounds(509, 528, 116, 22);
		contentPane.add(textField_13);
		
		JLabel label_8 = new JLabel("EXAM NAME:");
		label_8.setBounds(402, 560, 75, 16);
		contentPane.add(label_8);
		
		textField_14 = new JTextField();
		textField_14.setColumns(10);
		textField_14.setBounds(509, 557, 116, 22);
		contentPane.add(textField_14);
		
		JLabel lblDiplomaEducationInformation = new JLabel("DIPLOMA EDUCATION INFORMATION ");
		lblDiplomaEducationInformation.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblDiplomaEducationInformation.setForeground(Color.MAGENTA);
		lblDiplomaEducationInformation.setBounds(169, 602, 351, 16);
		contentPane.add(lblDiplomaEducationInformation);
		
		JLabel lblInstituteName = new JLabel("INSTITUTE NAME :");
		lblInstituteName.setBounds(50, 646, 138, 16);
		contentPane.add(lblInstituteName);
		
		textField_10 = new JTextField();
		textField_10.setBounds(248, 76, 289, 22);
		contentPane.add(textField_10);
		textField_10.setColumns(10);
		
		textField_15 = new JTextField();
		textField_15.setColumns(10);
		textField_15.setBounds(248, 111, 289, 22);
		contentPane.add(textField_15);
		
		textField_16 = new JTextField();
		textField_16.setColumns(10);
		textField_16.setBounds(248, 146, 289, 22);
		contentPane.add(textField_16);
		
		textField_17 = new JTextField();
		textField_17.setColumns(10);
		textField_17.setBounds(248, 181, 289, 22);
		contentPane.add(textField_17);
		
		textField_18 = new JTextField();
		textField_18.setColumns(10);
		textField_18.setBounds(248, 216, 289, 22);
		contentPane.add(textField_18);
		
		textField_19 = new JTextField();
		textField_19.setBounds(168, 643, 338, 22);
		contentPane.add(textField_19);
		textField_19.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("SEMESTER ");
		lblNewLabel.setBounds(259, 699, 67, 22);
		contentPane.add(lblNewLabel);
		
		JLabel lblYear_1 = new JLabel("YEAR");
		lblYear_1.setBounds(173, 699, 56, 22);
		contentPane.add(lblYear_1);
		
		JLabel lblGpa_1 = new JLabel("GPA");
		lblGpa_1.setBounds(398, 699, 42, 22);
		contentPane.add(lblGpa_1);
		
		JLabel lblDepartment = new JLabel("DEPARTMENT");
		lblDepartment.setBounds(50, 699, 94, 22);
		contentPane.add(lblDepartment);
		
		JLabel lblBoard_1 = new JLabel("BOARD");
		lblBoard_1.setBounds(512, 699, 56, 22);
		contentPane.add(lblBoard_1);
		
		textField_20 = new JTextField();
		textField_20.setBounds(49, 734, 95, 32);
		contentPane.add(textField_20);
		textField_20.setColumns(10);
		
		textField_21 = new JTextField();
		textField_21.setColumns(10);
		textField_21.setBounds(156, 734, 95, 32);
		contentPane.add(textField_21);
		
		textField_22 = new JTextField();
		textField_22.setColumns(10);
		textField_22.setBounds(259, 734, 95, 32);
		contentPane.add(textField_22);
		
		textField_23 = new JTextField();
		textField_23.setColumns(10);
		textField_23.setBounds(366, 734, 95, 32);
		contentPane.add(textField_23);
		
		textField_24 = new JTextField();
		textField_24.setColumns(10);
		textField_24.setBounds(473, 734, 95, 32);
		contentPane.add(textField_24);
		
		textField_25 = new JTextField();
		textField_25.setColumns(10);
		textField_25.setBounds(49, 779, 95, 32);
		contentPane.add(textField_25);
		
		textField_26 = new JTextField();
		textField_26.setColumns(10);
		textField_26.setBounds(156, 779, 95, 32);
		contentPane.add(textField_26);
		
		textField_27 = new JTextField();
		textField_27.setColumns(10);
		textField_27.setBounds(259, 779, 95, 32);
		contentPane.add(textField_27);
		
		textField_28 = new JTextField();
		textField_28.setColumns(10);
		textField_28.setBounds(366, 779, 95, 32);
		contentPane.add(textField_28);
		
		textField_29 = new JTextField();
		textField_29.setColumns(10);
		textField_29.setBounds(473, 779, 95, 32);
		contentPane.add(textField_29);
		
		textField_30 = new JTextField();
		textField_30.setColumns(10);
		textField_30.setBounds(49, 835, 95, 32);
		contentPane.add(textField_30);
		
		textField_31 = new JTextField();
		textField_31.setColumns(10);
		textField_31.setBounds(156, 835, 95, 32);
		contentPane.add(textField_31);
		
		textField_32 = new JTextField();
		textField_32.setColumns(10);
		textField_32.setBounds(259, 835, 95, 32);
		contentPane.add(textField_32);
		
		textField_33 = new JTextField();
		textField_33.setColumns(10);
		textField_33.setBounds(366, 835, 95, 32);
		contentPane.add(textField_33);
		
		textField_34 = new JTextField();
		textField_34.setColumns(10);
		textField_34.setBounds(473, 835, 95, 32);
		contentPane.add(textField_34);
		
		textField_35 = new JTextField();
		textField_35.setColumns(10);
		textField_35.setBounds(49, 887, 95, 32);
		contentPane.add(textField_35);
		
		textField_36 = new JTextField();
		textField_36.setColumns(10);
		textField_36.setBounds(156, 887, 95, 32);
		contentPane.add(textField_36);
		
		textField_37 = new JTextField();
		textField_37.setColumns(10);
		textField_37.setBounds(259, 887, 95, 32);
		contentPane.add(textField_37);
		
		textField_38 = new JTextField();
		textField_38.setColumns(10);
		textField_38.setBounds(366, 887, 95, 32);
		contentPane.add(textField_38);
		
		textField_39 = new JTextField();
		textField_39.setColumns(10);
		textField_39.setBounds(473, 887, 95, 32);
		contentPane.add(textField_39);
		
		textField_40 = new JTextField();
		textField_40.setColumns(10);
		textField_40.setBounds(50, 933, 95, 32);
		contentPane.add(textField_40);
		
		textField_41 = new JTextField();
		textField_41.setColumns(10);
		textField_41.setBounds(157, 933, 95, 32);
		contentPane.add(textField_41);
		
		textField_42 = new JTextField();
		textField_42.setColumns(10);
		textField_42.setBounds(260, 933, 95, 32);
		contentPane.add(textField_42);
		
		textField_43 = new JTextField();
		textField_43.setColumns(10);
		textField_43.setBounds(367, 933, 95, 32);
		contentPane.add(textField_43);
		
		textField_44 = new JTextField();
		textField_44.setColumns(10);
		textField_44.setBounds(474, 933, 95, 32);
		contentPane.add(textField_44);
		
		textField_45 = new JTextField();
		textField_45.setColumns(10);
		textField_45.setBounds(49, 978, 95, 32);
		contentPane.add(textField_45);
		
		textField_46 = new JTextField();
		textField_46.setColumns(10);
		textField_46.setBounds(156, 978, 95, 32);
		contentPane.add(textField_46);
		
		textField_47 = new JTextField();
		textField_47.setColumns(10);
		textField_47.setBounds(259, 978, 95, 32);
		contentPane.add(textField_47);
		
		textField_48 = new JTextField();
		textField_48.setColumns(10);
		textField_48.setBounds(366, 978, 95, 32);
		contentPane.add(textField_48);
		
		textField_49 = new JTextField();
		textField_49.setColumns(10);
		textField_49.setBounds(473, 978, 95, 32);
		contentPane.add(textField_49);
	}
}
