package MobileSoftwer;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Reg extends JFrame {

	private JPanel contentPane;
	private JTextField firstName;
	private JTextField lastName;
	private JTextField userName;
	private JTextField Createpassword;
	private JTextField confirmPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Reg frame = new Reg();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Reg() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 604, 597);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblRegistration = new JLabel("Registration");
		lblRegistration.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblRegistration.setBounds(201, 13, 145, 16);
		contentPane.add(lblRegistration);
		
		JLabel lblCusomerName = new JLabel("Customer Name");
		lblCusomerName.setBounds(41, 78, 102, 16);
		contentPane.add(lblCusomerName);
		
		firstName = new JTextField();
		firstName.setBounds(155, 75, 145, 22);
		contentPane.add(firstName);
		firstName.setColumns(10);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(312, 78, 74, 16);
		contentPane.add(lblLastName);
		
		lastName = new JTextField();
		lastName.setBounds(398, 78, 116, 22);
		contentPane.add(lastName);
		lastName.setColumns(10);
		
		JLabel lblUserName = new JLabel("User Name");
		lblUserName.setBounds(41, 122, 102, 16);
		contentPane.add(lblUserName);
		
		userName = new JTextField();
		userName.setBounds(155, 119, 145, 22);
		contentPane.add(userName);
		userName.setColumns(10);
		
		Createpassword = new JTextField();
		Createpassword.setColumns(10);
		Createpassword.setBounds(219, 169, 145, 22);
		contentPane.add(Createpassword);
		
		confirmPassword = new JTextField();
		confirmPassword.setColumns(10);
		confirmPassword.setBounds(219, 216, 145, 22);
		contentPane.add(confirmPassword);
		
		JLabel lblCreateANew = new JLabel("create a New password");
		lblCreateANew.setBounds(41, 172, 145, 16);
		contentPane.add(lblCreateANew);
		
		JLabel lblConfirmPassword = new JLabel("Confirm password");
		lblConfirmPassword.setBounds(41, 219, 145, 16);
		contentPane.add(lblConfirmPassword);
		
		JButton btnNewButton = new JButton("Registration");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				firstName.getText();
				lastName.getText();
				userName.getText();
				Createpassword.getText();
				confirmPassword.getText();
				JOptionPane.showMessageDialog(null, "Your Registraion is Successfull! ");
				MobileOparator mb=new MobileOparator();
				mb.frame.setVisible(true);
				Reg.this.setVisible(false);
				
			}
		});
		btnNewButton.setBounds(293, 251, 132, 25);
		contentPane.add(btnNewButton);
	}
}
