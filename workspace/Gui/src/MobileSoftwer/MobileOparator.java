package MobileSoftwer;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.UIManager;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MobileOparator {

	public JFrame frame;
	private JTextField LogName_txt;
	private JPasswordField LogPass_txt;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MobileOparator window = new MobileOparator();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MobileOparator() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(UIManager.getColor("ToolBar.dockingForeground"));
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\NkChakraborty\\Downloads\\xth.jpg"));
		frame.setBounds(100, 100, 450, 319);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLogin.setBounds(155, 13, 56, 22);
		frame.getContentPane().add(lblLogin);
		
		JLabel lblCustomerName = new JLabel("Customer Name ");
		lblCustomerName.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblCustomerName.setForeground(UIManager.getColor("activeCaptionText"));
		lblCustomerName.setBounds(12, 77, 156, 33);
		frame.getContentPane().add(lblCustomerName);
		
		LogName_txt = new JTextField();
		LogName_txt.setBounds(173, 84, 247, 22);
		frame.getContentPane().add(LogName_txt);
		LogName_txt.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setForeground(UIManager.getColor("Viewport.foreground"));
		lblPassword.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblPassword.setBounds(12, 148, 107, 33);
		frame.getContentPane().add(lblPassword);
		
		LogPass_txt = new JPasswordField();
		LogPass_txt.setBounds(173, 155, 247, 22);
		frame.getContentPane().add(LogPass_txt);
		
		JButton logIn_btn = new JButton("LogIn");
		logIn_btn.setBounds(323, 190, 97, 25);
		frame.getContentPane().add(logIn_btn);
		
		JButton regtrastion_btn = new JButton("Regtrastion");
		regtrastion_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Reg reg=new Reg();
				reg.setVisible(true);
				
			}
		});
		regtrastion_btn.setBounds(313, 228, 107, 25);
		frame.getContentPane().add(regtrastion_btn);
		
		JLabel lblIfYouNot = new JLabel("If you not regtration yet?");
		lblIfYouNot.setBounds(119, 232, 182, 16);
		frame.getContentPane().add(lblIfYouNot);
	}
}
