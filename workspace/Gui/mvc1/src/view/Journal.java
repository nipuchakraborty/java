package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.JournalController;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Journal extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	public static JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Journal frame = new Journal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Journal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 525, 415);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[][][][grow]", "[][][][][][][][]"));
		
		JLabel lblTotal = new JLabel("Total");
		lblTotal.setFont(new Font("SansSerif", Font.PLAIN, 16));
		contentPane.add(lblTotal, "cell 0 0");
		
		textField_2 = new JTextField();
		contentPane.add(textField_2, "cell 3 0,growx");
		textField_2.setColumns(10);
		
		
		
		JLabel lblAccointName = new JLabel("Accoint Name");
		lblAccointName.setFont(new Font("SansSerif", Font.PLAIN, 16));
		contentPane.add(lblAccointName, "cell 0 2");
		
		textField = new JTextField();
		contentPane.add(textField, "cell 3 2,growx");
		textField.setColumns(10);
		
		JLabel lblAccointType = new JLabel("Accoint type");
		lblAccointType.setFont(new Font("SansSerif", Font.PLAIN, 16));
		contentPane.add(lblAccointType, "cell 0 4");
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Select One", "Debit", "Credit"}));
		contentPane.add(comboBox, "cell 3 4,growx");
		
		JLabel lblAmount = new JLabel("Amount");
		lblAmount.setFont(new Font("SansSerif", Font.PLAIN, 16));
		contentPane.add(lblAmount, "cell 0 6");
		
		textField_1 = new JTextField();
		contentPane.add(textField_1, "cell 3 6,growx");
		textField_1.setColumns(10);
		
		JButton btnNewButton = new JButton("Submit");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String total=textField_2.getText();
				String name=textField.getText();
				String acc_type=comboBox.getSelectedItem().toString();
				String amount=textField_1.getText();
				if(acc_type.equals("Select One")){
					JOptionPane.showMessageDialog(null, "please select valid account type");
				}
				else {
					new JournalController(total, acc_type, name, amount);
				}
				
			}
		});
		contentPane.add(btnNewButton, "cell 3 7");
		textField_2.setText("50000");
		textField_2.setEditable(false);
	}

}
