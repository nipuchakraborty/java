package controller;

import view.Journal;

public class JournalController {
	
	public int current_total;
	public JournalController(String total,String acc_type,String acc_name,String amount){
		
		int total_for_calculate=Integer.parseInt(total);
		int amount_for_calculate=Integer.parseInt(amount);
		if(acc_type.equals("Debit")){
			
			 current_total=total_for_calculate+amount_for_calculate;
		}
		else{
			 current_total=total_for_calculate-amount_for_calculate;
		}
		String current=String.valueOf(current_total);
		
		Journal.textField_2.setText(current);
		System.out.println();
	}

}
