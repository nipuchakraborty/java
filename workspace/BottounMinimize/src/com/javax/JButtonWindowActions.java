package com.javax;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import net.miginfocom.swing.MigLayout;
class JButtonWindowActions extends JFrame
{
JButton button1,button2,button3;
 public JButtonWindowActions()
 {
 setTitle("JButtonWindowActions Demo");
 setVisible(true);
 setDefaultCloseOperation(EXIT_ON_CLOSE);
 setOpacity(0.7f);
	setSize(166, 292);
 button1=new JButton("Maximize/Restore");
 button1.setBounds(7, 11, 135, 25);
 button1.addActionListener(new ActionListener(){
   public void actionPerformed(ActionEvent ae)
   {
    if(getExtendedState()==NORMAL)
    setExtendedState(MAXIMIZED_BOTH);
    else
    setExtendedState(NORMAL);
   }
  });
 button2=new JButton("Minimize");
 button2.setBounds(146, 11, 83, 25);
 button2.addActionListener(new ActionListener(){
   public void actionPerformed(ActionEvent ae)
   {
    setState(ICONIFIED);
   }
  });
 button3=new JButton("Close");
 button3.setBounds(233, 11, 63, 25);
 button3.addActionListener(new ActionListener(){
   public void actionPerformed(ActionEvent ae)
   {
   dispose();
   }
  });
 getContentPane().setLayout(null);
  // add buttons to frame
 getContentPane().add(button1);
 getContentPane().add(button2);
 getContentPane().add(button3);
  // pack the frame
 pack();
 }
 public static void main(String args[])
 {
 new JButtonWindowActions();
 }
}