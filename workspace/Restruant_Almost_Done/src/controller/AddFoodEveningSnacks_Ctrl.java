package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import model.AddfoodEvening_Snacks_model;
import model.Database;
import net.proteanit.sql.DbUtils;
import viwe.panel.AddFood;

public class AddFoodEveningSnacks_Ctrl {
	public AddFoodEveningSnacks_Ctrl(String name,String price,String category,String amount,String status,String path){
		int a=Integer.parseInt(amount);
		AddfoodEvening_Snacks_model addfoodEvening_Snacks_model=new AddfoodEvening_Snacks_model();
		
		int pri=Integer.parseInt(price);
		addfoodEvening_Snacks_model.Store(name, pri, category,a,status,path);
		addfoodEvening_Snacks_model.Quary();
	}
	
}
