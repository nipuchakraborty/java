package model;

import java.awt.Image;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import net.proteanit.sql.DbUtils;
import viwe.panel.AddFood;
import viwe.panel.Order;
//import viwe.panel.Order;
import viwe.panel.WorkerDetails;

public  class TakeOrder {
	public String foodname;
	public int price;
	public int quntity;
	public String date;
	
	public void Store(String food,int price,int quntity,String date){
		this.foodname =food;
		this.price=price;
		this.quntity=quntity;
		this.date=date;
	}
	
	
	//////brakfasttable qurry
	
public static void breakFastDataLoad(){
	
	Connection connection=Database.getconnection();
	String qury="SELECT  `Name`, `Price`, `Category`, `Status` FROM `add_food_breakfast`";
	
	
	try {
		PreparedStatement pStatement=connection.prepareStatement(qury);
		ResultSet rSet=pStatement.executeQuery();
		
		
		AddFood.BreakfastTab.setModel(DbUtils.resultSetToTableModel(rSet));
		
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}


////view data 
public static void breakFastDataLoadView(){
	
	Connection connection=Database.getconnection();
	String qury="SELECT  `Name`, `Price`, `Category`, `Status` FROM `breakfast_view`";
	
	
	try {
		PreparedStatement pStatement=connection.prepareStatement(qury);
		ResultSet rSet=pStatement.executeQuery();
		
		
		AddFood.BreakfastTab.setModel(DbUtils.resultSetToTableModel(rSet));
		
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}
////////////////////////////////////////////////////////////////////////////////////







public static void DnnerDataLoad(){
	
	Connection connection=Database.getconnection();
	String qury="SELECT `Food Name`, `Price`, `Category`, `Status` FROM `addfooddinner` ";
	try {
		PreparedStatement pStatement=connection.prepareStatement(qury);
		ResultSet rSet=pStatement.executeQuery();
		AddFood.dinner_tab.setModel(DbUtils.resultSetToTableModel(rSet));
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
	
}


public static void DnnerDataLoad_view(){
	
	Connection connection=Database.getconnection();
	String qury="SELECT  `Food Name`, `Price`, `Category`, `Status` FROM `dinner_view` ";
	try {
		PreparedStatement pStatement=connection.prepareStatement(qury);
		ResultSet rSet=pStatement.executeQuery();
		AddFood.dinner_tab.setModel(DbUtils.resultSetToTableModel(rSet));
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
	
}





public static void EveningsnakcsDataLoad(){
	
	Connection connection=Database.getconnection();
	String qury="SELECT  `foodname`, `Price`, `Category`, `Status` FROM `addfoodeveningsnacks` ";
	try {
		PreparedStatement pStatement=connection.prepareStatement(qury);
		ResultSet rSet=pStatement.executeQuery();

		AddFood.even_Tab.setModel(DbUtils.resultSetToTableModel(rSet));
		
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}
public static void EveningsnakcsDataLoad_View(){
	
	Connection connection=Database.getconnection();
	String qury="SELECT  `foodname`, `Price`, `Category`, `Status` FROM `evening` ";
	try {
		PreparedStatement pStatement=connection.prepareStatement(qury);
		ResultSet rSet=pStatement.executeQuery();

		AddFood.even_Tab.setModel(DbUtils.resultSetToTableModel(rSet));
		
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}






public static void lunchDataLoad(){
	
	Connection connection=Database.getconnection();
	String qury="SELECT  `FoodName`, `Price`, `Category`, `Status` FROM `addfoodlunch`";
	try {
		PreparedStatement pStatement=connection.prepareStatement(qury);
		ResultSet rSet=pStatement.executeQuery();
		AddFood.lunch_tab.setModel(DbUtils.resultSetToTableModel(rSet));
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}
public static void lunchDataLoad_View(){
	
	Connection connection=Database.getconnection();
	String qury="SELECT `FoodName`, `Price`, `Category`, `Status` FROM `addfoodlunch`";
	try {
		PreparedStatement pStatement=connection.prepareStatement(qury);
		ResultSet rSet=pStatement.executeQuery();
		AddFood.lunch_tab.setModel(DbUtils.resultSetToTableModel(rSet));
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}
public static void trancate(){
	Connection conn=Database.getconnection();
	String q1="TRUNCATE addfoodeveningsnacks;";

 
 
 
 try {
	PreparedStatement pset=conn.prepareStatement(q1);
	
	ResultSet rSet=pset.executeQuery();
	
} catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}}
 public static void trancate2(){
		Connection conn=Database.getconnection();
		
		String q2 ="TRUNCATE addfooddinner;";

	 
	 
	 
	 try {
		
		PreparedStatement pset2=conn.prepareStatement(q2);
		
		
		boolean resultSet=pset2.execute();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
	
}
 public static void trancate3(){
		Connection conn=Database.getconnection();
	

	 String q3=" TRUNCATE addfoodlunch;";


	 
	 
	 
	 try {
		
		PreparedStatement pset3=conn.prepareStatement(q3);
		;
		
		ResultSet resultSet=pset3.executeQuery();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}
 
 
 
 
 ///Oreder List
 
 
 public static void breakFastOrder(){
		
		Connection connection=Database.getconnection();
		String qury="SELECT  `Name`, `Price`, `Category`,  `Status` FROM `breakfast_view`";
		
		
		try {
			PreparedStatement pStatement=connection.prepareStatement(qury);
			ResultSet rSet=pStatement.executeQuery();
			
			
			Order.breakfast_table.setModel(DbUtils.resultSetToTableModel(rSet));
			
			pStatement.execute();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}
 public static void DnnerOrder(){
		
		Connection connection=Database.getconnection();
		String qury="SELECT  `Food Name`, `Price`, `Category`,  `Status` FROM `dinner_view`";
		try {
			PreparedStatement pStatement=connection.prepareStatement(qury);
			ResultSet rSet=pStatement.executeQuery();
			Order.dinner_table.setModel(DbUtils.resultSetToTableModel(rSet));
			pStatement.execute();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
	}
 public static void EveningsnakcsOrder(){
		
		Connection connection=Database.getconnection();
		String qury="SELECT  `foodname`, `Price`, `Category`, `Status`  FROM `addfoodeveningsnacks`";
		try {
			PreparedStatement pStatement=connection.prepareStatement(qury);
			ResultSet rSet=pStatement.executeQuery();

			Order.snacks_table.setModel(DbUtils.resultSetToTableModel(rSet));
			
			pStatement.execute();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}
 public static void lunchOrder(){
		
		Connection connection=Database.getconnection();
		String qury="SELECT  `FoodName`, `Price`, `Category`, `Status` FROM `addfoodlunch` ";
		try {
			PreparedStatement pStatement=connection.prepareStatement(qury);
			ResultSet rSet=pStatement.executeQuery();
			Order.luch_table.setModel(DbUtils.resultSetToTableModel(rSet));
			pStatement.execute();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}
 public static void  pic(){
		//String  name = null;
		Connection obj=Database.getconnection();
		
		String comnd="SELECT  `picturepath` FROM `add_food_breakfast` WHERE `Name`= 'Lemon Tea'";
		
	
		
		try{
			PreparedStatement ps=obj.prepareStatement(comnd);
			//ps.setString(1,Foodname);
			ps.execute();
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()){
				//name=rs.getString("Name");	
			}
			
			
		}
		catch(Exception e){
			
		}
		
		//return name;
	}
 public static void saveAndDel(){
		Connection conn=Database.getconnection();
	

	 String q3=" TRUNCATE TABLE `temp_order` ";

	 
	 try {
		
		PreparedStatement pset3=conn.prepareStatement(q3);
		
		pset3.executeUpdate();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}
 
 
	public static void getOrderBreakfast (String name){
		
			Connection connection=Database.getconnection();
			String foodname;
			int  price = 0;
			String category = null;
			String path=null;
			
			
			String q="SELECT `Name`, `Price`, `Category`,  `picturepath` FROM `add_food_breakfast` WHERE `Name`=?";
			
			try {
				PreparedStatement preparedStatement=connection.prepareStatement(q);
				preparedStatement.setString(1, name);
				ResultSet set=preparedStatement.executeQuery();
				while(set.next()){
					
				foodname=set.getString("Name");
				price=set.getInt("Price");
				String foodP=Integer.toString(price);
				category=set.getString("Category");
				path=set.getString("picturepath");
				
				Order.FoodName.setText(foodname);
				
				Order.UnitPrice.setText(foodP);
				Order.Category.setText(category);
				ImageIcon myIcon=new ImageIcon(path);
				Image img= myIcon.getImage();
				Image im=img.getScaledInstance(Order.pic.getWidth(), Order.pic.getHeight(), Image.SCALE_REPLICATE);
				ImageIcon imageIcon=new ImageIcon(im);
				Order.pic.setIcon(new ImageIcon(path));
				
				
				/*WorkerDetails.WorkerName_txt.setText(WorkerName);
				WorkerDetails.posiotn.setText(postion);
				WorkerDetails.g.setText(Gender);*/
				
				
				}
			} catch (SQLException e) {
			
				e.printStackTrace();
			}
 
	}
	
	
	
	public static void getOrderlunch (String name){
		
		Connection connection=Database.getconnection();
		String foodname;
		int  price = 0;
		String category = null;
		String path=null;
		
		
		String q="SELECT  `FoodName`, `Price`, `Category`,  `picturepath` FROM `addfoodlunch` WHERE `FoodName`=?";
		
		try {
			PreparedStatement preparedStatement=connection.prepareStatement(q);
			preparedStatement.setString(1, name);
			ResultSet set=preparedStatement.executeQuery();
			while(set.next()){
				
			foodname=set.getString("FoodName");
			price=set.getInt("Price");
			String foodP=Integer.toString(price);
			category=set.getString("Category");
			path=set.getString("picturepath");
			
			Order.FoodName.setText(foodname);
			
			Order.UnitPrice.setText(foodP);
			Order.Category.setText(category);
			ImageIcon myIcon=new ImageIcon(path);
			Image img= myIcon.getImage();
			Image im=img.getScaledInstance(Order.pic.getWidth(), Order.pic.getHeight(), Image.SCALE_REPLICATE);
			ImageIcon imageIcon=new ImageIcon(im);
			Order.pic.setIcon(new ImageIcon(path));
			
			
			/*WorkerDetails.WorkerName_txt.setText(WorkerName);
			WorkerDetails.posiotn.setText(postion);
			WorkerDetails.g.setText(Gender);*/
			
			
			}
		} catch (SQLException e) {
		
			e.printStackTrace();
		}

}
	
public static void getOrderSnacks (String name){
		
		Connection connection=Database.getconnection();
		String foodname;
		int  price = 0;
		String category = null;
		String path=null;
		
		
		String q="SELECT `foodname`, `Price`, `Category`,   `picturepath` FROM `addfoodeveningsnacks`  WHERE `foodname`=?";
		
		try {
			PreparedStatement preparedStatement=connection.prepareStatement(q);
			preparedStatement.setString(1, name);
			ResultSet set=preparedStatement.executeQuery();
			while(set.next()){
				
			foodname=set.getString("foodname");
			price=set.getInt("Price");
			String foodP=Integer.toString(price);
			category=set.getString("Category");
			path=set.getString("picturepath");
			
			Order.FoodName.setText(foodname);
			
			Order.UnitPrice.setText(foodP);
			Order.Category.setText(category);
			ImageIcon myIcon=new ImageIcon(path);
			Image img= myIcon.getImage();
			Image im=img.getScaledInstance(Order.pic.getWidth(), Order.pic.getHeight(), Image.SCALE_REPLICATE);
			ImageIcon imageIcon=new ImageIcon(im);
			Order.pic.setIcon(new ImageIcon(path));
			
			
		
			
			
			}
		} catch (SQLException e) {
		
			e.printStackTrace();
		}

}

public static void getOrderDinner (String name){
	
	Connection connection=Database.getconnection();
	String foodname;
	int  price = 0;
	String category = null;
	String path=null;
	
	
	String q="SELECT  `Food Name`, `Price`, `Category`,  `picturepath` FROM `addfooddinner` WHERE `Food Name`=?";
	
	try {
		PreparedStatement preparedStatement=connection.prepareStatement(q);
		preparedStatement.setString(1, name);
		ResultSet set=preparedStatement.executeQuery();
		while(set.next()){
			
		foodname=set.getString("Food Name");
		price=set.getInt("Price");
		String foodP=Integer.toString(price);
		category=set.getString("Category");
		path=set.getString("picturepath");
		
		Order.FoodName.setText(foodname);
		
		Order.UnitPrice.setText(foodP);
		Order.Category.setText(category);
		ImageIcon myIcon=new ImageIcon(path);
		Image img= myIcon.getImage();
		Image im=img.getScaledInstance(Order.pic.getWidth(), Order.pic.getHeight(), Image.SCALE_REPLICATE);
		ImageIcon imageIcon=new ImageIcon(im);
		Order.pic.setIcon(new ImageIcon(path));
		
		
	
		
		
		}
	} catch (SQLException e) {
	
		e.printStackTrace();
	}

}
	public static ImageIcon ResizeImage(String ImagePath) {
		ImageIcon MyImage = new ImageIcon(ImagePath);
		
		Image img = MyImage.getImage();
		Image newImg = img.getScaledInstance(Order.pic.getWidth(), Order.pic.getHeight(), Image.SCALE_REPLICATE);
		ImageIcon image = new ImageIcon(newImg);
	
		return image;
	}
}
