package model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import net.proteanit.sql.DbUtils;
import viwe.panel.AddFood;
import viwe.panel.SalesReport;
import viwe.panel.WorkerDetails;

public class SalesReportModel {
public static String name;
public static String table;
public static int tatal;
public static String tak;
public static java.sql.Date date;

	public void preapare(String TableNo,String name,int total,String tak2, java.sql.Date date){
		
		this.table=TableNo;
		this.name=name;
		this.tatal=total;
		this.date=date;
		this.tak=tak2;
		
		
		
	}
	public void inseart(){
		
			Connection conn=Database.getconnection();
			String Quarry="INSERT INTO `sales_repors`( `TableNo`, `CustomerName`, `Sale_price`,`taked_by`,`Date`) VALUES (?,?,?,?,?)";
			try {
				PreparedStatement pStatement=conn.prepareStatement(Quarry);
				pStatement.setString(1, this.table);
				pStatement.setString(2, this.name);
				pStatement.setInt(3, this.tatal);
				pStatement.setString(4, this.tak);
				pStatement.setDate(5, this.date);
				
				pStatement.execute();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			
			
		
	}
	public static void Load(){
		Connection conn=Database.getconnection();
		String Quarry ="SELECT  `TableNo`, `CustomerName`, `Sale_price`,`taked_by`, `Date` FROM `sales_repors`  ";
		try {
			PreparedStatement pStatement=conn.prepareStatement(Quarry);
			ResultSet set =pStatement.executeQuery();
			SalesReport.table.setModel(DbUtils.resultSetToTableModel(set));
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Databse Could not Loads");
			e.printStackTrace();
		}
		
		
	}
	public static void search(Date date){
		Connection connection=Database.getconnection();
		String q="SELECT  `TableNo`, `CustomerName`, `Sale_price`, `Date` FROM `sales_repors` WHERE Date=?";
		try {
			PreparedStatement preparedStatement=connection.prepareStatement(q);
			preparedStatement.setDate(1,date);
			ResultSet set=preparedStatement.executeQuery();
			
			SalesReport.table.setModel(DbUtils.resultSetToTableModel(set));
			preparedStatement.execute();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	public static void searchName(String Cn){
		Connection connection=Database.getconnection();
		String q="SELECT  `TableNo`, `CustomerName`, `Sale_price`, `Date` FROM `sales_repors` WHERE CustomerName like ?";
		try {
			PreparedStatement preparedStatement=connection.prepareStatement(q);
			preparedStatement.setString(1,"%"+Cn+"%");
			ResultSet set=preparedStatement.executeQuery();
			
			SalesReport.table.setModel(DbUtils.resultSetToTableModel(set));
			preparedStatement.execute();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
}
