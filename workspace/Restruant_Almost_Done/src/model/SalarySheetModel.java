package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;

import com.mysql.jdbc.jmx.LoadBalanceConnectionGroupManager;

import net.proteanit.sql.DbUtils;
import viwe.panel.SalarySheet;
import viwe.panel.WorkerDetails;

public class SalarySheetModel {
	public static String  id;
	public static String name;
	public static String gender;
	public static String status;
	public static String  post;
	public static int basic;
	public static int due;
	public static int paid;
	public static int bonus;
	public static int Total;
	public static java.sql.Date date;
	
	public void prepare(String workerId,String name ,String gender,String workerPost,int Basic_Salary,int paid, int due,int total,int bonus,java.sql.Date date){
		
		
		this.id=workerId;
		this.name=name;
		this.gender=gender;
		this.post=workerPost;
		this.status=status;
		this.basic=Basic_Salary;
		this.paid=paid;
		this.due=due;
		this.bonus=bonus;
		this.Total=total;
		this.date=date;
	}
public void SalarySheetInseart(){
	Connection con=Database.getconnection();
	String q="INSERT INTO `salarysheet`(`WorkerID_No`, `Woker_name`, `Gender`, `Worker_Post`,  `Basic_Salary`, `Paid_Salary`, `Due`, `Bonus`, `Total_Salary`, `Date`) VALUES  (?,?,?,?,?,?,?,?,?,?)";
	try {
		PreparedStatement p=con.prepareStatement(q);
		p.setString(1, this.id);
		p.setString(2, this.name);
		p.setString(3, this.gender);
		p.setString(4, this.post);
		
		p.setInt(5, this.basic);
		p.setInt(6, this.paid);
		p.setInt(7, this.due);
		p.setInt(8,this.bonus);
		p.setInt(9, this.Total);
		p.setDate(10, this.date);
		p.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}
public static void load (){
	Connection connection=Database.getconnection();
	String q="SELECT  `WorkerID_No`, `Woker_name`, `Gender`, `Worker_Post`, `Basic_Salary`, `Paid_Salary`, `Due`, `Bonus`, `Total_Salary`, `Date` FROM `salarysheet`";
	try {
		PreparedStatement preparedStatement=connection.prepareStatement(q);
		ResultSet set=preparedStatement.executeQuery();
		SalarySheet.SalarySheetTab.setModel(DbUtils.resultSetToTableModel(set));
		preparedStatement.execute();
	} catch (SQLException e) {
	
		e.printStackTrace();
	}
	}

public static void search(String  id){
	Connection connection=Database.getconnection();
	String q="SELECT  `WorkerID_No`, `Woker_name`, `Gender`, `Worker_Post`,  `Basic_Salary`, `Paid_Salary`, `Due`, `Bonus`, `Total_Salary`, `Date` FROM `salarysheet` WHERE WorkerID_No=?";
	try {
		PreparedStatement preparedStatement=connection.prepareStatement(q);
		preparedStatement.setString(1,id);
		ResultSet set=preparedStatement.executeQuery();
		
		SalarySheet.SalarySheetTab.setModel(DbUtils.resultSetToTableModel(set));
		preparedStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	}

public static void loadWorker (String id){
	Connection connection=Database.getconnection();
	String q="SELECT * FROM `addworker` WHERE `workerId`=?";
	try {
		PreparedStatement preparedStatement=connection.prepareStatement(q);
		preparedStatement.setString(1, id);
		ResultSet set=preparedStatement.executeQuery();
		WorkerDetails.presentList.setModel(DbUtils.resultSetToTableModel(set));
		preparedStatement.execute();
	} catch (SQLException e) {
	
		e.printStackTrace();
	}
	}
public static void NameOfWorker (String id){
	Connection connection=Database.getconnection();
	String WorkerName;
	String postion = null;
	String Gender=null;
	int  basic ;
	String q="SELECT  `Name`, `Post`, `Gender`, `Basic_Salary` FROM `addworker` WHERE `workerId`=?";
	
	try {
		PreparedStatement preparedStatement=connection.prepareStatement(q);
		preparedStatement.setString(1, id);
		ResultSet set=preparedStatement.executeQuery();
		while(set.next()){
		WorkerName=set.getString("Name");
		postion=set.getString("Post");
		Gender=set.getString("Gender");
		
		basic=set.getInt("Basic_Salary");
		
		String b=Integer.toString(basic);
		SalarySheet.WorkerName_txt.setText(WorkerName);
		SalarySheet.posiotn.setText(postion);
		SalarySheet.genderTxt.setText(Gender);
		SalarySheet.Salary_amount.setText(b);
		
		}
	} catch (SQLException e) {
	
		e.printStackTrace();
	}


	
	
}

public static void Dlete(String name) {
	Connection connection = Database.getconnection();
	String qury = "DELETE FROM `salarysheet` WHERE `WorkerID_No`=?";
	try {
		PreparedStatement p = connection.prepareStatement(qury);
		p.setString(1, name);
		
		
		
		
		p.executeUpdate();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
}
}
