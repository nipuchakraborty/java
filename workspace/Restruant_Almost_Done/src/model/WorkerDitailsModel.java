package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.jmx.LoadBalanceConnectionGroupManager;

import net.proteanit.sql.DbUtils;
import viwe.panel.WorkerDetails;

public class WorkerDitailsModel {
	public static String  id;
	public static String name;
	public static String gender;
	public static String present;
	public static String  post;
	public static java.sql.Date date;
	public void prepare(String id,String name ,String gender,String present,String Pos,java.sql.Date date){
		//System.out.println(id+name+gender+present);
		
		
		this.id=id;
		this.name=name;
		this.gender=gender;
		this.present=present;
		this.post=Pos;
		this.date=date;
	}
public void PresentList(){
	Connection con=Database.getconnection();
	String q="INSERT INTO `presentlist`(`id`, `Name`, `Gender`, `PresentList`, `Post`, `Date`) VALUES (?,?,?,?,?,?)";
	try {
		PreparedStatement pStatement=con.prepareStatement(q);
		pStatement.setString(1, this.id);
		pStatement.setString(2, this.name);
		pStatement.setString(3, this.gender);
		pStatement.setString(4, this.present);
		pStatement.setString(5,this.post);
		pStatement.setDate(6, this.date);
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}
public static void load (){
	Connection connection=Database.getconnection();
	String q="SELECT * FROM `presentList`";
	try {
		PreparedStatement preparedStatement=connection.prepareStatement(q);
		ResultSet set=preparedStatement.executeQuery();
		WorkerDetails.presentList.setModel(DbUtils.resultSetToTableModel(set));
		preparedStatement.execute();
	} catch (SQLException e) {
	
		e.printStackTrace();
	}
	}

public static void search(String  id){
	Connection connection=Database.getconnection();
	String q="SELECT * FROM `presentlist` WHERE id=?";
	try {
		PreparedStatement preparedStatement=connection.prepareStatement(q);
		preparedStatement.setString(1,id);
		ResultSet set=preparedStatement.executeQuery();
		
		WorkerDetails.presentList.setModel(DbUtils.resultSetToTableModel(set));
		preparedStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	}

public static void loadWorker (String id){
	Connection connection=Database.getconnection();
	String q="SELECT * FROM `addworker` WHERE `workerId`=?";
	try {
		PreparedStatement preparedStatement=connection.prepareStatement(q);
		preparedStatement.setString(1, id);
		ResultSet set=preparedStatement.executeQuery();
		WorkerDetails.presentList.setModel(DbUtils.resultSetToTableModel(set));
		preparedStatement.execute();
	} catch (SQLException e) {
	
		e.printStackTrace();
	}
	}
public static void NameOfWorker (String id){
	Connection connection=Database.getconnection();
	String WorkerName;
	String postion = null;
	String Gender = null;
	
	String q="SELECT  `Name`, `Post`, `Gender`  FROM `addworker` WHERE `workerId`=?";
	
	try {
		PreparedStatement preparedStatement=connection.prepareStatement(q);
		preparedStatement.setString(1, id);
		ResultSet set=preparedStatement.executeQuery();
		while(set.next()){
			
		WorkerName=set.getString("Name");
		postion=set.getString("Post");
		Gender=set.getString("Gender");
		
		
		
		WorkerDetails.WorkerName_txt.setText(WorkerName);
		WorkerDetails.posiotn.setText(postion);
		WorkerDetails.g.setText(Gender);
		
		
		}
	} catch (SQLException e) {
	
		e.printStackTrace();
	}


	
	/*try {
		
		PreparedStatement preparedStatement=connection.prepareStatement(q2);
		preparedStatement.setString(1,id );
		ResultSet set=preparedStatement.executeQuery();
		while(set.next()){
		postion=set.getString("`Post`");
		WorkerDetails.posiotn.setText(postion);
		}
	} catch (SQLException e) {
	
		e.printStackTrace();
	}
	*/
}
public static void Dlete(String name) {
	Connection connection = Database.getconnection();
	String qury = "DELETE FROM `presentlist` WHERE `Name`=?";
	try {
		PreparedStatement p = connection.prepareStatement(qury);
		p.setString(1, name);
		
		
		
		p.executeUpdate();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
}
}
