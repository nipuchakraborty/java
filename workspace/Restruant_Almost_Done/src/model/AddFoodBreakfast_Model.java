package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import net.proteanit.sql.DbUtils;
import viwe.panel.AddFood;

public class AddFoodBreakfast_Model {
	public String Name;
	public int Price;
	public String Category;
	public int Amount;
	public String staus;
	public String path;
	
	public void Store(String Name,int Price, String Category,int Amount ,String Staus,String path){
		this.Name=Name;
		this.Price=Price;
		this.Category=Category;
		this.staus=Staus;
		this.Amount=Amount;
		this.path=path;
	}
	public void Quary(){
		Connection conn=Database.getconnection();
		String Quarry="INSERT INTO `add_food_breakfast`(`Name`, `Price`, `Category`,`Amount_Of_Food`, `Status`,`picturepath`) VALUES (?,?,?,?,?,?)";
		try {
			PreparedStatement pStatement=conn.prepareStatement(Quarry);
			pStatement.setString(1, this.Name);
			pStatement.setInt(2, this.Price);
			pStatement.setString(3, this.Category);
			pStatement.setInt(4, this.Amount);
			pStatement.setString(5, this.staus);
			pStatement.setString(6, this.path);
			pStatement.execute();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Databse Could not Loads");
			e.printStackTrace();
		}
		
		
	}
	public static void Load(){
		Connection conn=Database.getconnection();
		String Quarry ="SELECT  `Name`, `Price`, `Category`, `Amount_Of_Food`, `Status` FROM `breakfast_view`";
		try {
			PreparedStatement pStatement=conn.prepareStatement(Quarry);
			ResultSet set =pStatement.executeQuery();
			AddFood.BreakfastTab.setModel(DbUtils.resultSetToTableModel(set));
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Databse Could not Loads");
			e.printStackTrace();
		}
		}
		
		public static void Dlete(String name) {
			Connection connection = Database.getconnection();
			String qury = "DELETE FROM `add_food_breakfast` WHERE `Name`=?";
			try {
				PreparedStatement p = connection.prepareStatement(qury);
				p.setString(1, name);
				
				
				
				p.executeUpdate();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		
	}
}
