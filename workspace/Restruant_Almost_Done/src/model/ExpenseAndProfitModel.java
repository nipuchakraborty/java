package model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.proteanit.sql.DbUtils;
import viwe.panel.Order;
import viwe.panel.Parchase;
import viwe.panel.ParchaseReports;
public class ExpenseAndProfitModel {
	public static void load(String id){
		Connection con=Database.getconnection();
		String q="SELECT `BillNo`, `VendorName`, `Productname`, `Category`, `Quntity`, `PerPrice`, `TotalPrice`, `Date` FROM `parchase` WHERE `BillNo`=?";
		try {
			PreparedStatement p=con.prepareStatement(q);
			p.setString(1, id);
			ResultSet set=p.executeQuery();
			ParchaseReports.Expense.setModel(DbUtils.resultSetToTableModel(set));
			p.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public static void loadReportP(){
		Connection con=Database.getconnection();
		String q="SELECT `BillNo`, `VendorName`, `Productname`, `Category`, `PerPrice`, `TotalPrice`, `Date` FROM `parchase` ";
		try {
			PreparedStatement p=con.prepareStatement(q);
			ResultSet set=p.executeQuery();
			ParchaseReports.Expense.setModel(DbUtils.resultSetToTableModel(set));
			p.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public static void load_data(java.sql.Date date){
		Connection con=Database.getconnection();
		String sal="SELECT  `Date` ,`Sale_price`  FROM `sales_repors` WHERE `Date`=? ORDER BY Date";
		try {
			PreparedStatement statement=con.prepareStatement(sal);
			statement.setDate(1, date);
			ResultSet set=statement.executeQuery();
			ParchaseReports.Profit.setModel(DbUtils.resultSetToTableModel(set));
			statement.execute();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
	}
	public static void load(){
		Connection con=Database.getconnection();
		String sal="SELECT  `Date` ,`Sale_price`  FROM `sales_repors` ORDER BY Date";
		try {
			PreparedStatement statement=con.prepareStatement(sal);
			
			ResultSet set=statement.executeQuery();
			ParchaseReports.Profit.setModel(DbUtils.resultSetToTableModel(set));
			statement.execute();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
	}
	
	public static void Totalamount (){
		String Total;
		Connection con=Database.getconnection();
		String sum="SELECT SUM(`TotalPrice`) FROM parchase";
		
		try {
			PreparedStatement statement=con.prepareStatement(sum);
			ResultSet set=statement.executeQuery();
			while (set.next()) {
				Total=set.getString("SUM(`TotalPrice`)") ;
				ParchaseReports.total_parchase.setText(Total);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void TotalSale(){
		String Total;
		Connection con=Database.getconnection();
		String sum="SELECT SUM(`Sale_price`) FROM sales_repors";
		
		try {
			PreparedStatement statement=con.prepareStatement(sum);
			ResultSet set=statement.executeQuery();
			while (set.next()) {
				Total=set.getString("SUM(`Sale_price`)") ;
				ParchaseReports.total_sale.setText(Total);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	}
