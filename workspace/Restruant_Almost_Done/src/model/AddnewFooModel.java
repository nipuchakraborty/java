package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.experimental.theories.Theories;

import com.mysql.jdbc.StatementInterceptor;

public class AddnewFooModel {
public static String name;
public static String cat;

	public  void prepare(String cat, String Food) {
		
		this.name=Food;
		this.cat=cat;
		
	}

	public void inseart (){
		Connection c=Database.getconnection();
		String q="INSERT INTO `AddFood_name_Cat`(`Category`, `FoodName`) VALUES (?,?)";
		
		try {
			PreparedStatement p=c.prepareStatement(q);
			p.setString(1, this.cat);
			p.setString(2, this.name);
			p.execute();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}
	public static ArrayList<String> getdata(String cat){
		ArrayList< String > F=new ArrayList<>();
		Connection c=Database.getconnection();
		String q=" SELECT * FROM AddFood_name_Cat WHERE `Category`=?";
		
		try {
			PreparedStatement p=c.prepareStatement(q);
			
			p.setString(1, cat);
			ResultSet rs=p.executeQuery();
			while(rs.next()){
				F.add(rs.getString("Category")); 
			
							}
			} catch (Exception e) {
			// TODO: handle exception
		}
		return F;
	
}}
