package model;

import java.awt.Image;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ImageIcon;

import org.junit.experimental.theories.Theories;

import net.proteanit.sql.DbUtils;
import viwe.panel.AddWorker;
import viwe.panel.Order;

public class AddWorkerModel {
	public static String workerId ;
	public static String Name;
	public static String Post;
	public static String gender;
	public static int basicSlary;
	public static String con;
	public static String add;
	public static String ema;
	public static String picture;
	
	
	public  void prepareAddWorker(String workerId ,String Name,String Post,String gender ,String contact, String address, String email, String pic,int ba){
		this.workerId=workerId;
		this.Name=Name;
		this.Post=Post;
		this.gender=gender;
		this.basicSlary=ba;
		this.con=contact;
		this.add=address;
		this.ema=email;
		this.picture=pic;
		
	}
public void insertWorker(){
	Connection con=Database.getconnection();
	String q="INSERT INTO `addworker`(`workerId`, `Name`, `Post`, `Gender`,`Contact_no`, `Address`, `Email`, `Picture`,`Basic_Salary`) VALUES (?,?,?,?,?,?,?,?,?)";
	try {
		PreparedStatement statement=con.prepareStatement(q);
		statement.setString(1, this.workerId);
		statement.setString(2, this.Name);
		statement.setString(3, this.Post);
		statement.setString(4, this.gender);
		statement.setString(5, this.con);
		statement.setString(6, this.add);
		statement.setString(7, this.ema);
		statement.setString(8, this.picture);
		statement.setInt(9, this.basicSlary);
		statement.execute();
		
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}
public static void load(){
	Connection con=Database.getconnection();
	String q="SELECT `workerId`, `Name`, `Post`, `Gender`, `Contact_no`, `Address`, `Email`,  `Basic_Salary` FROM `addworker` ";
	try {
		PreparedStatement statement=con.prepareStatement(q);
		ResultSet set=statement.executeQuery();
		AddWorker.WorkerDitails_Tab.setModel(DbUtils.resultSetToTableModel(set));
		statement.execute();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}

public static void Search( String Id){
	Connection con=Database.getconnection();
	String q="SELECT * FROM `addworker` WHERE  workerId=?";
	try {
		PreparedStatement statement=con.prepareStatement(q);
		statement.setString(1, Id);
		ResultSet set=statement.executeQuery();
		AddWorker.WorkerDitails_Tab.setModel(DbUtils.resultSetToTableModel(set));
		statement.execute();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
public static void RemoveWorker( String Id){
	Connection con=Database.getconnection();
	String q="DELETE FROM `addworker` WHERE `workerId`=?";
	try {
		PreparedStatement statement=con.prepareStatement(q);
		statement.setString(1, Id);
		
		
		statement.executeUpdate();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
public static void getworker (String name){
	
	Connection connection=Database.getconnection();
	String nameW;
	String gender=null;
	String contact=null;
	String address=null;
	String Eamil=null;
	int BasicSalary=0;
	String post = null;
	String path=null;
	
	
	String q="SELECT  `Name`, `Post`, `Gender`, `Contact_no`, `Address`, `Email`, `Picture`, `Basic_Salary` FROM `addworker` WHERE `Name`=?";
	
	try {
		PreparedStatement preparedStatement=connection.prepareStatement(q);
		preparedStatement.setString(1, name);
		ResultSet set=preparedStatement.executeQuery();
		while(set.next()){
			
		nameW=set.getString("Name");
		post=set.getString("Post");
		gender=set.getString("Gender");
		contact=set.getString("Contact_no");
		address=set.getString("Address");
		Eamil=set.getString("Email");
		path=set.getString("Picture");
		BasicSalary=set.getInt("Basic_Salary");
		String b=Integer.toString(BasicSalary);
		AddWorker.W_name.setText(nameW);
		
		AddWorker.post.setText(post);
		
		AddWorker.Gen_txt.setText(gender);
		
		
		AddWorker.contact.setText(contact);
		
		AddWorker.emai_id_txt.setText(Eamil);
		AddWorker.Address.setText(address);
		
		AddWorker.B_salary.setText(b);
		
		ImageIcon myIcon=new ImageIcon(path);
		Image img= myIcon.getImage();
		Image im=img.getScaledInstance(AddWorker.pic_L.getWidth(), AddWorker.pic_L.getHeight(), Image.SCALE_SMOOTH);
		ImageIcon imageIcon=new ImageIcon(im);
		AddWorker.pic_L.setIcon(new ImageIcon(path));
		
		
		}
	} catch (SQLException e) {
	
		e.printStackTrace();
	}

}

}
