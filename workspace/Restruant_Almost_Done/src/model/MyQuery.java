package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.Query;


public class MyQuery {
    
 
    
    public ArrayList<TableMethod> BindTable(){
        
   ArrayList<TableMethod> list = new ArrayList<TableMethod>();
   Connection connection=Database.getconnection();
   Statement st;
   ResultSet rs;
   
   try {
   st = connection.createStatement();
   rs = st.executeQuery("SELECT  `Name`, `Price`, `Category`, `Status`, `Picture` FROM `breakfast_view`");
   
   TableMethod p;
   while(rs.next()){
   p = new TableMethod(
   
   rs.getString("Name"),
   rs.getInt("Price"),
   rs.getString("Category"),
   rs.getString("Status"),
   rs.getBytes("Picture"),
  rs.getInt("Cat_id")
   );
   list.add(p);
   }
   
   } catch (SQLException ex) {
   Logger.getLogger(MyQuery.class.getName()).log(Level.SEVERE, null, ex);
   }
   return list;
   }
}