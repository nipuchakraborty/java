package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;
import viwe.panel.AddFood;

public class AddFoodLuch_Model {
	public String Name;
	public int Price;
	public String Category;
	public String staus;
	public int amount;
	public String path;
	public void Store(String Name,int Price, String Category,int amont,String Staus,String path){
		this.Name=Name;
		this.Price=Price;
		this.Category=Category;
		this.staus=Staus;
		this.amount=amont;
		this.path=path;
	}
	public void Quary(){
		Connection conn=Database.getconnection();
		String Quarry="INSERT INTO `addfoodlunch`(`FoodName`, `Price`, `Category`, `Amount_Of_Food`, `Status`, `picturepath`) VALUES (?,?,?,?,?,?)";
		try {
			PreparedStatement pStatement=conn.prepareStatement(Quarry);
			pStatement.setString(1, this.Name);
			pStatement.setInt(2, this.Price);
			pStatement.setString(3, this.Category);
			pStatement.setInt(4, this.amount);
			pStatement.setString(5, this.staus);
			pStatement.setString(6, this.path);
			pStatement.execute();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
	}
	public static void Load(){
		Connection conn=Database.getconnection();
		String Quarry ="SELECT  `FoodName`, `Price`, `Category`, `Amount_Of_Food`, `Status` FROM `lunch`  ";
		try {
			PreparedStatement pStatement=conn.prepareStatement(Quarry);
			ResultSet set =pStatement.executeQuery();
			AddFood.lunch_tab.setModel(DbUtils.resultSetToTableModel(set));
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Databse Could not Loads");
			e.printStackTrace();
		}
		
		
	}
	public static void Dlete(String name) {
		Connection connection = Database.getconnection();
		String qury = "DELETE FROM `addfoodlunch` WHERE `FoodName`=?";
		try {
			PreparedStatement p = connection.prepareStatement(qury);
			p.setString(1, name);
			
			
			
			p.executeUpdate();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	
}
}
