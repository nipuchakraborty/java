/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPasswordField;

import viwe.Dash;
import javax.swing.JLabel;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ImageIcon;
import java.awt.Font;

//import model.Reg;

/**
 *
 * @author Fardin
 */
public class LoginFrame extends javax.swing.JFrame {

    /**
     * Creates new form LoginFrame
     */
	MessageDailog dailog=new MessageDailog();
    public LoginFrame() {
    	getContentPane().setBackground(Color.DARK_GRAY);
        initComponents();
        setBounds(300,100,800,550);
    }

    /**
     * Wow just login and enjoy 
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        contant = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        user_txt = new javax.swing.JTextField();
        pass_lb = new javax.swing.JLabel();
       
        jSeparator2 = new javax.swing.JSeparator();
        password_txt = new javax.swing.JPasswordField();
        jLabel4 = new javax.swing.JLabel();
        role_com = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jButton1.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		String user=user_txt.getText();
        		String pass=password_txt.getText();
        		String role=role_com.getSelectedItem().toString();
        		
        		// database
        		String login=Reg.getRegistration(user, pass);
        		String u=Reg.getUser(pass);
        		String p=Reg.getPassword(user);
        		String position=Reg.getPosition(user);
        		if (pass.equals(p) && role.equals(position) && user.equals(u)) {
					
        			dailog.message_lb.setText("sign in");
        			dailog.setBounds(300, 200, 497, 137);
        			dailog.setVisible(true);
        			new Dash().setVisible(true);
        			dailog.setVisible(false);
        			LoginFrame.this.dispose();
        			
				}
        		else {
        			forget_lb.show();
					dailog.message_lb.setText("wrong..!");
					 dailog.setBounds(300, 200, 497, 137);
					dailog.setVisible(true);
				}
        		
        	}
        });
        close_lb = new javax.swing.JLabel();
        forget_lb = new javax.swing.JLabel();
      // forget_lb.hide();
        sidebar = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        sign_in_lb = new javax.swing.JLabel();
        sign_up_lb = new javax.swing.JLabel();

        jLabel2.setBackground(new java.awt.Color(0, 0, 0));
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("User");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 0));
        setUndecorated(true);
        setResizable(false);

        contant.setBackground(new Color(255, 255, 255));
        contant.setForeground(new java.awt.Color(255, 255, 255));

        jLabel1.setBackground(new java.awt.Color(51, 51, 51));
        jLabel1.setFont(new java.awt.Font("Siyam Rupali", 0, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(102, 102, 102));
        jLabel1.setText("User");

        jSeparator1.setBackground(new java.awt.Color(153, 153, 153));

        user_txt.setBackground(new Color(255, 255, 255));
        user_txt.setFont(new java.awt.Font("Siyam Rupali", 0, 12)); // NOI18N
        user_txt.setForeground(new Color(0, 0, 0));
        user_txt.setBorder(null);

        pass_lb.setBackground(new java.awt.Color(51, 51, 51));
        pass_lb.setFont(new java.awt.Font("Siyam Rupali", 0, 12)); // NOI18N
        pass_lb.setForeground(new java.awt.Color(102, 102, 102));
        pass_lb.setText("Password");
        pass_lb.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseEntered(MouseEvent arg0) {
        		((JPasswordField) password_txt).setEchoChar((char)0);
        	}
        	@Override
        	public void mouseExited(MouseEvent arg0) {
        		((JPasswordField) password_txt).setEchoChar('*');
        	}
        });
      

        jSeparator2.setBackground(new java.awt.Color(153, 153, 153));

        password_txt.setBackground(new Color(255, 255, 255));
        password_txt.setFont(new java.awt.Font("Siyam Rupali", 0, 12)); // NOI18N
        password_txt.setForeground(new Color(0, 0, 0));
        password_txt.setBorder(null);

        jLabel4.setBackground(new java.awt.Color(51, 51, 51));
        jLabel4.setFont(new java.awt.Font("SimSun", 0, 36)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(102, 102, 102));
        jLabel4.setText("Sign in");

        role_com.setBackground(new java.awt.Color(51, 51, 51));
        role_com.setFont(new java.awt.Font("Siyam Rupali", 0, 12)); // NOI18N
        role_com.setForeground(new java.awt.Color(153, 153, 153));
        role_com.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select One", "Owner", "Mangaer" }));

        jButton1.setBackground(new java.awt.Color(51, 51, 51));
        jButton1.setFont(new java.awt.Font("Siyam Rupali", 0, 12)); // NOI18N
        jButton1.setForeground(new java.awt.Color(153, 153, 153));
        jButton1.setText("submit");

        close_lb.setBackground(new java.awt.Color(51, 51, 51));
        close_lb.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        close_lb.setForeground(new java.awt.Color(102, 102, 102));
        close_lb.setText("X");
        close_lb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                close_lbMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                close_lbMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                close_lbMouseExited(evt);
            }
        });

        forget_lb.setFont(new java.awt.Font("Siyam Rupali", 0, 12)); // NOI18N
        forget_lb.setForeground(new java.awt.Color(204, 0, 0));
        forget_lb.setText("Forget Password ?");
        forget_lb.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent arg0) {
        		
        		ForgetFrame frame=new ForgetFrame();
        		frame.setBounds(350,100,750,550);
        		frame.setVisible(true);
        		LoginFrame.this.dispose();
        		
        	}
        	@Override
        	public void mouseEntered(MouseEvent arg0) {
        		 forget_lb.setFont(new java.awt.Font("Siyam Rupali", 0, 14));
        	}
        	@Override
        	public void mouseExited(MouseEvent arg0) {
        		 forget_lb.setFont(new java.awt.Font("Siyam Rupali", 0, 12));
        	}
        });

        javax.swing.GroupLayout contantLayout = new javax.swing.GroupLayout(contant);
        contantLayout.setHorizontalGroup(
        	contantLayout.createParallelGroup(Alignment.TRAILING)
        		.addGroup(contantLayout.createSequentialGroup()
        			.addContainerGap(78, Short.MAX_VALUE)
        			.addGroup(contantLayout.createParallelGroup(Alignment.LEADING)
        				.addGroup(contantLayout.createSequentialGroup()
        					.addGap(39)
        					.addGroup(contantLayout.createParallelGroup(Alignment.TRAILING, false)
        						.addGroup(contantLayout.createSequentialGroup()
        							.addComponent(forget_lb)
        							.addPreferredGap(ComponentPlacement.RELATED, 205, Short.MAX_VALUE)
        							.addComponent(jButton1, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
        						.addComponent(jSeparator2, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        						.addComponent(jSeparator1, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        						.addComponent(pass_lb, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
        						.addComponent(jLabel1, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
        						.addComponent(password_txt, Alignment.LEADING)
        						.addComponent(role_com, Alignment.LEADING, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        						.addComponent(user_txt))
        					.addContainerGap())
        				.addComponent(close_lb, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)))
        		.addGroup(Alignment.LEADING, contantLayout.createSequentialGroup()
        			.addGap(108)
        			.addComponent(jLabel4, GroupLayout.PREFERRED_SIZE, 198, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(206, Short.MAX_VALUE))
        );
        contantLayout.setVerticalGroup(
        	contantLayout.createParallelGroup(Alignment.LEADING)
        		.addGroup(contantLayout.createSequentialGroup()
        			.addGroup(contantLayout.createParallelGroup(Alignment.TRAILING)
        				.addGroup(contantLayout.createSequentialGroup()
        					.addComponent(close_lb, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
        					.addGap(95))
        				.addGroup(contantLayout.createSequentialGroup()
        					.addComponent(jLabel4, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
        					.addGap(18)))
        			.addComponent(jLabel1, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(user_txt, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(jSeparator1, GroupLayout.PREFERRED_SIZE, 7, GroupLayout.PREFERRED_SIZE)
        			.addGap(11)
        			.addComponent(pass_lb, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(password_txt, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(jSeparator2, GroupLayout.PREFERRED_SIZE, 2, GroupLayout.PREFERRED_SIZE)
        			.addGap(37)
        			.addComponent(role_com, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
        			.addGap(18)
        			.addGroup(contantLayout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(jButton1, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
        				.addComponent(forget_lb))
        			.addContainerGap(49, Short.MAX_VALUE))
        );
        contant.setLayout(contantLayout);

        sidebar.setBackground(new java.awt.Color(102, 102, 102));
        sidebar.setForeground(new java.awt.Color(255, 255, 255));
        sidebar.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                sidebarMouseDragged(evt);
            }
        });
        sidebar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                sidebarMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                sidebarMouseReleased(evt);
            }
        });

        jLabel6.setBackground(new java.awt.Color(102, 102, 102));
        jLabel6.setFont(new Font("SimSun", Font.PLAIN, 34)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(153, 153, 153));
        jLabel6.setText(" Restaurant  ");

        sign_in_lb.setFont(new java.awt.Font("Siyam Rupali", 0, 18)); // NOI18N
        sign_in_lb.setForeground(new java.awt.Color(153, 153, 153));
        sign_in_lb.setText("Sign in");
        sign_in_lb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                sign_in_lbMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                sign_in_lbMouseExited(evt);
            }
        });
        
        label = new JLabel("");
        label.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Male_User_100px.png"));

        javax.swing.GroupLayout sidebarLayout = new javax.swing.GroupLayout(sidebar);
        sidebarLayout.setHorizontalGroup(
        	sidebarLayout.createParallelGroup(Alignment.LEADING)
        		.addGroup(sidebarLayout.createSequentialGroup()
        			.addGroup(sidebarLayout.createParallelGroup(Alignment.LEADING)
        				.addGroup(sidebarLayout.createSequentialGroup()
        					.addContainerGap()
        					.addComponent(jLabel6))
        				.addGroup(sidebarLayout.createSequentialGroup()
        					.addGap(48)
        					.addGroup(sidebarLayout.createParallelGroup(Alignment.LEADING)
        						.addComponent(label, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
        						.addComponent(sign_in_lb, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE))))
        			.addContainerGap(28, Short.MAX_VALUE))
        );
        sidebarLayout.setVerticalGroup(
        	sidebarLayout.createParallelGroup(Alignment.LEADING)
        		.addGroup(sidebarLayout.createSequentialGroup()
        			.addGap(32)
        			.addComponent(jLabel6)
        			.addGap(18)
        			.addComponent(label)
        			.addPreferredGap(ComponentPlacement.RELATED, 151, Short.MAX_VALUE)
        			.addComponent(sign_in_lb, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
        			.addGap(83))
        );
        sidebar.setLayout(sidebarLayout);

        sign_up_lb.setFont(new java.awt.Font("Siyam Rupali", 0, 18)); // NOI18N
        sign_up_lb.setForeground(new java.awt.Color(153, 153, 153));
        sign_up_lb.setText("Sign up");
        sign_up_lb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sign_up_lbMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                sign_up_lbMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                sign_up_lbMouseExited(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(sign_up_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(90, 90, 90)
                .addComponent(contant, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(sidebar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 433, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(contant, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(sign_up_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(143, 143, 143))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(sidebar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    int xx,xy;
    private void sidebarMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sidebarMousePressed
        setOpacity((float)0.8);
        xx=evt.getX();
        xy=evt.getY();
    }//GEN-LAST:event_sidebarMousePressed

    private void sidebarMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sidebarMouseDragged
      int x=evt.getXOnScreen();
      int y=evt.getYOnScreen();
      this.setLocation(x-xx, y-xy);
    }//GEN-LAST:event_sidebarMouseDragged

    private void sidebarMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sidebarMouseReleased
         setOpacity((float)1.0);
    }//GEN-LAST:event_sidebarMouseReleased

    private void close_lbMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_close_lbMouseClicked
        LoginFrame.this.dispose();
    }//GEN-LAST:event_close_lbMouseClicked

    private void close_lbMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_close_lbMouseEntered
        close_lb.setForeground(Color.red);
    }//GEN-LAST:event_close_lbMouseEntered

    private void close_lbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_close_lbMouseExited
        close_lb.setForeground(new java.awt.Color(102,102,102));
    }//GEN-LAST:event_close_lbMouseExited

    private void sign_up_lbMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sign_up_lbMouseClicked
        RegFrame rf=new RegFrame();
        rf.setBounds(350,100,800,550);
        rf.setVisible(true);
        LoginFrame.this.dispose();
    }//GEN-LAST:event_sign_up_lbMouseClicked

    private void sign_up_lbMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sign_up_lbMouseEntered
        sign_up_lb.setForeground(Color.WHITE);
    }//GEN-LAST:event_sign_up_lbMouseEntered

    private void sign_up_lbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sign_up_lbMouseExited
         sign_up_lb.setForeground(new java.awt.Color(153,153,153));
    }//GEN-LAST:event_sign_up_lbMouseExited

    private void sign_in_lbMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sign_in_lbMouseEntered
        sign_in_lb.setForeground(Color.WHITE);
    }//GEN-LAST:event_sign_in_lbMouseEntered

    private void sign_in_lbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sign_in_lbMouseExited
         sign_in_lb.setForeground(new java.awt.Color(153,153,153));
    }//GEN-LAST:event_sign_in_lbMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            
                new LoginFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel close_lb;
    private javax.swing.JPanel contant;
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> role_com;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel pass_lb;
    private javax.swing.JLabel jLabel4;
    public static javax.swing.JLabel forget_lb;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPasswordField password_txt;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField user_txt;
    private javax.swing.JPanel sidebar;
    private javax.swing.JLabel sign_in_lb;
    private javax.swing.JLabel sign_up_lb;
    private JLabel label;
}
