package login;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MessageDailog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JLabel close_lb;
	public JLabel message_lb ;
	public JButton ok_btn;
	public JButton cancel_btn ;
	private JLabel lblX;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			MessageDailog dialog = new MessageDailog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public MessageDailog() {
		setUndecorated(true);
		setBounds(100, 100, 497, 137);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(Color.GRAY);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
		panel.setBorder(null);
		panel.setForeground(new Color(0, 0, 0));
		panel.setBounds(0, 0, 497, 22);
		contentPanel.add(panel);
		panel.setLayout(null);
		
		close_lb = new JLabel("X");
		close_lb.setForeground(Color.LIGHT_GRAY);
		close_lb.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				MessageDailog.this.dispose();
			}
			@Override
			public void mouseEntered(MouseEvent arg0) {
				close_lb.setForeground(Color.RED);
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				close_lb.setForeground(Color.BLACK);
			}
		});
		close_lb.setFont(new Font("Tahoma", Font.PLAIN, 12));
		close_lb.setBounds(497, 0, 15, 22);
		panel.add(close_lb);
		{
			lblX = new JLabel("X");
			lblX.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lblX.setForeground(Color.WHITE);
			lblX.setBounds(482, 5, 15, 14);
			panel.add(lblX);
		}
		{
			message_lb = new JLabel("");
			message_lb.setForeground(new Color(255, 255, 255));
			message_lb.setBounds(43, 45, 427, 59);
			contentPanel.add(message_lb);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBackground(Color.GRAY);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				 ok_btn= new JButton("OK");
				 ok_btn.addActionListener(new ActionListener() {
				 	public void actionPerformed(ActionEvent arg0) {
				 		MessageDailog.this.dispose();
				 	}
				 });
				ok_btn.setForeground(new Color(255, 255, 255));
				ok_btn.setBackground(Color.GRAY);
				ok_btn.setActionCommand("OK");
				buttonPane.add(ok_btn);
				getRootPane().setDefaultButton(ok_btn);
			}
			{
				cancel_btn= new JButton("Cancel");
				cancel_btn.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						MessageDailog.this.dispose();
					}
				});
				cancel_btn.setForeground(new Color(255, 255, 255));
				cancel_btn.setBackground(Color.GRAY);
				cancel_btn.setActionCommand("Cancel");
				buttonPane.add(cancel_btn);
			}
		}
	}
}
