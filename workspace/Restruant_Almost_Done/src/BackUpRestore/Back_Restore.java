package BackUpRestore;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;
import net.miginfocom.swing.MigLayout;
import viwe.panel.Order;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;

import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import controller.AddnewFoodCrl;
import controller.TempOderCtrl;
import model.TakeOrder;
import model.TempOrder;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class Back_Restore extends JFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane;
	public static JLabel lblFoodName;
	public static JTextField fileLocation;
	public static String path;
	static Point mouseDownScreenCoords;
	static Point mouseDownCompCoords;
	private JButton btnBrowse;
	//public String path;
	public String filename;
	public Back_Restore() {
		
		mouseDownScreenCoords = null;
		mouseDownCompCoords = null;
		setResizable(false);
		setUndecorated(true);
		setBackground(new Color(0, 100, 0));

		setLocation(new Point(400, 200));

		setVisible(true);

		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));

		setTitle("Restore & Backup Data");

		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

		setSize(555, 298);
		getContentPane().setLayout(new MigLayout());

		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));

		desktopPane = new JDesktopPane() {
			private Image image;
			{
				
				image = (Toolkit.getDefaultToolkit()
						.getImage(Back_Restore.class.getResource("/pictureResource/BA3.png")));
			}

			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
			}
		};

		desktopPane.setFocusable(false);
		desktopPane.setFocusTraversalKeysEnabled(false);
		desktopPane.setFocusCycleRoot(false);
		desktopPane.setBorder(null);

		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(null);

		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Back_Restore.this.dispose();
			}
		});
		cancelButton.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Cancel_16px_1.png"));
		cancelButton.setBounds(452, 201, 91, 39);
		desktopPane.add(cancelButton);

		JButton okButton = new JButton("Ok");
		okButton.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Checked_16px.png"));
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			Process p	=null;
			try {
				Runtime runtime=Runtime.getRuntime();
				p=runtime.exec("‪C:/xampp/mysql/bin/mysqldump.exe  -uroot -p121 -resturantmanagement>"+path);
				//mysqldump –uroot –pmyrootpassword db_test > db_test.sql
				int proCom=p.waitFor();
				if (proCom==0) {
				JOptionPane.showMessageDialog(null, "done");	
					
				}
				else{
					JOptionPane.showMessageDialog(null, "sorry");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			}
		});
		okButton.setBounds(373, 201, 67, 39);
		desktopPane.add(okButton);

		lblFoodName = new JLabel("File Location");
		lblFoodName.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblFoodName.setForeground(Color.WHITE);
		lblFoodName.setBounds(6, 117, 134, 34);
		desktopPane.add(lblFoodName);

		fileLocation = new JTextField();
		fileLocation.setBounds(191, 123, 226, 28);
		desktopPane.add(fileLocation);
		fileLocation.setColumns(10);

		btnBrowse = new JButton("Browse");
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser f=new JFileChooser();
				f.showOpenDialog(f);
				String date =new SimpleDateFormat("yyyy-mm-dd").format(new Date());
				try {
					File file=f.getSelectedFile();
					path=file.getAbsolutePath();
					path=path.replace('\\', '/');
					path=path + "" +date+".sql";
					fileLocation.setText(path);
					
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		btnBrowse.setBounds(434, 123, 90, 28);
		desktopPane.add(btnBrowse);
		
		JLabel lblRestre = new JLabel("RESTORE");
		lblRestre.setForeground(new Color(240, 230, 140));
		lblRestre.setFont(new Font("STEAK", Font.BOLD, 20));
		lblRestre.setBounds(215, 44, 121, 34);
		desktopPane.add(lblRestre);
		addMouseListener(new MouseListener() {
			public void mouseReleased(MouseEvent e) {

				mouseDownScreenCoords = null;
				mouseDownCompCoords = null;
			}

			public void mousePressed(MouseEvent e) {
				mouseDownScreenCoords = e.getLocationOnScreen();
				mouseDownCompCoords = e.getPoint();
			}

			public void mouseExited(MouseEvent e) {
			}

			public void mouseEntered(MouseEvent e) {
			}

			public void mouseClicked(MouseEvent e) {

			}
		});
		addMouseMotionListener(new MouseMotionListener() {
			public void mouseMoved(MouseEvent e) {
			}

			public void mouseDragged(MouseEvent e) {
				Point currCoords = e.getLocationOnScreen();
				setLocation(mouseDownScreenCoords.x + (currCoords.x - mouseDownScreenCoords.x) - mouseDownCompCoords.x,
						mouseDownScreenCoords.y + (currCoords.y - mouseDownScreenCoords.y) - mouseDownCompCoords.y);
			}
		});

	}
}
