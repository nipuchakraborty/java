package viwe.Dialouge;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;
import net.miginfocom.swing.MigLayout;
import viwe.panel.Order;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.text.ParseException;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import controller.EditWorkerCtrl;
import controller.TempOderCtrl;
import model.TakeOrder;
import model.TempOrder;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.ImageIcon;


public class EditAddWorker extends JFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane ;
	public static JLabel lblFoodName;
	public static JTextField idTxt;
	public static JTextField Gender;
	public static JTextField workerNametxt;
	public static JTextField basicTxt;
	public static String path;
	 static Point mouseDownScreenCoords;
     static Point mouseDownCompCoords;
     public static JTextField Post;
	
	public EditAddWorker() {
		mouseDownScreenCoords = null;
        mouseDownCompCoords = null;
		setResizable(false);
		setUndecorated(true);
		setBackground(new Color(0, 100, 0));
		
		setLocation(new Point(200, 50));
		
		
		setVisible(true);
		
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		
		
		
		setTitle("Confirm Your Order Sir");
		
	
		
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		
		setSize(455, 457);
		getContentPane().setLayout(new MigLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				image=(Toolkit.getDefaultToolkit().getImage(EditAddWorker.class.getResource("/pictureResource/BA3.png")));
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		
		desktopPane.setFocusable(false);
		desktopPane.setFocusTraversalKeysEnabled(false);
		desktopPane.setFocusCycleRoot(false);
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(null);
		
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EditAddWorker.this.dispose();
			}
		});
		cancelButton.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Cancel_16px_1.png"));
		cancelButton.setBounds(269, 416, 87, 28);
		desktopPane.add(cancelButton);
		
		JButton okButton = new JButton("Ok");
		okButton.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Checked_16px.png"));
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String workerId=idTxt.getText();
				String GenderS=Gender.getText();
				String name=workerNametxt.getText();
				
				String basicS=basicTxt.getText();
				String pos=Post.getText();
				
			
			new EditWorkerCtrl(workerId, name, pos, GenderS, basicS);
			
				EditAddWorker.this.dispose();
			}
		});
		okButton.setBounds(194, 416, 63, 28);
		desktopPane.add(okButton);
		
		lblFoodName = new JLabel("Worker Id");
		lblFoodName.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblFoodName.setForeground(Color.WHITE);
		lblFoodName.setBounds(20, 69, 128, 26);
		desktopPane.add(lblFoodName);
		
		idTxt = new JTextField();
		idTxt.setBounds(194, 71, 167, 28);
		desktopPane.add(idTxt);
		idTxt.setColumns(10);
		
		JLabel lblPrice = new JLabel("Gender");
		lblPrice.setForeground(Color.WHITE);
		lblPrice.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblPrice.setBounds(20, 178, 95, 26);
		desktopPane.add(lblPrice);
		
		Gender = new JTextField();
		Gender.setHorizontalAlignment(SwingConstants.CENTER);
		Gender.setBounds(194, 180, 167, 28);
		desktopPane.add(Gender);
		Gender.setColumns(10);
		
		workerNametxt = new JTextField();
		workerNametxt.setBounds(194, 119, 167, 28);
		desktopPane.add(workerNametxt);
		workerNametxt.setColumns(10);
		
		JLabel lblCategory = new JLabel("Worker Name");
		lblCategory.setForeground(Color.WHITE);
		lblCategory.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblCategory.setBounds(20, 117, 128, 26);
		desktopPane.add(lblCategory);
		
		JLabel lblQuntity = new JLabel("Basic Salry");
		lblQuntity.setForeground(Color.WHITE);
		lblQuntity.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblQuntity.setBounds(20, 233, 106, 26);
		desktopPane.add(lblQuntity);
		
		basicTxt = new JTextField();
		basicTxt.setFont(new Font("SansSerif", Font.PLAIN, 14));
		basicTxt.setHorizontalAlignment(SwingConstants.CENTER);
		basicTxt.setBounds(194, 234, 167, 28);
		desktopPane.add(basicTxt);
		basicTxt.setColumns(10);
		
		JLabel lblPost = new JLabel("Post");
		lblPost.setForeground(Color.WHITE);
		lblPost.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblPost.setBounds(20, 291, 106, 26);
		desktopPane.add(lblPost);
		
		Post = new JTextField();
		Post.setHorizontalAlignment(SwingConstants.CENTER);
		Post.setFont(new Font("SansSerif", Font.PLAIN, 14));
		Post.setColumns(10);
		Post.setBounds(194, 292, 167, 28);
		desktopPane.add(Post);
		addMouseListener(new MouseListener(){
            public void mouseReleased(MouseEvent e) {
            	
                mouseDownScreenCoords = null;
                mouseDownCompCoords = null;
            }
            public void mousePressed(MouseEvent e) {
                mouseDownScreenCoords = e.getLocationOnScreen();
                mouseDownCompCoords = e.getPoint();
            }
            public void mouseExited(MouseEvent e) {
            }
            public void mouseEntered(MouseEvent e) {
            }
            public void mouseClicked(MouseEvent e) {
            	
            }
        });
        addMouseMotionListener(new MouseMotionListener(){
            public void mouseMoved(MouseEvent e) {
            }
            public void mouseDragged(MouseEvent e) {
                Point currCoords = e.getLocationOnScreen();
                setLocation(mouseDownScreenCoords.x + (currCoords.x - mouseDownScreenCoords.x) - mouseDownCompCoords.x,
                              mouseDownScreenCoords.y + (currCoords.y - mouseDownScreenCoords.y) - mouseDownCompCoords.y);
            }
        });
		
	}
}
