package viwe.Dialouge;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;
import net.miginfocom.swing.MigLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.text.ParseException;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import controller.EditWorkerCtrl;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;


public class EditWorkerDitails extends JFrame {

	public static JPanel contentPane;
	public static JDesktopPane desktopPane ;
	public static  JTextField workerIdTxt;
	public static JTextField name;
	public static JTextField da2;
	public static JTextField preT;
	public static JTextField genderT;
	public JCheckBox chckbxAbsent;
	private JLabel lblX;
	static Point mouseDownScreenCoords;
	static Point mouseDownCompCoords;
	private JLabel lblPosition;
	public static  JTextField pos;
	public EditWorkerDitails() {
		mouseDownScreenCoords = null;
        mouseDownCompCoords = null;
		setResizable(false);
		setBackground(new Color(0, 100, 0));
	setUndecorated(true);
		setLocation(new Point(200, 50));
		
		setIgnoreRepaint(true);
		
		
		setVisible(true);
		
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		
		setTitle("Edit Worker Information");
		
	
		
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		
		setSize(763, 457);
		getContentPane().setLayout(new MigLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				image=(Toolkit.getDefaultToolkit().getImage(EditWorkerDitails.class.getResource("/pictureResource/BA4.png")));
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		
		desktopPane.setFocusable(false);
		desktopPane.setFocusTraversalKeysEnabled(false);
		desktopPane.setFocusCycleRoot(false);
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(null);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EditWorkerDitails.this.dispose();
			}
		});
		btnCancel.setBounds(424, 301, 97, 25);
		desktopPane.add(btnCancel);
		
		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String worker=workerIdTxt.getText();
				
				String nam=name.getText();
				String G=genderT.getText();
				String status=preT.getText();
				String po=pos.getText();
				new EditWorkerCtrl(worker, nam, G, status, po);
			
				
				
				
			}
		});
		btnOk.setBounds(315, 301, 97, 25);
		desktopPane.add(btnOk);
		
		workerIdTxt = new JTextField();
		workerIdTxt.setColumns(10);
		workerIdTxt.setBounds(159, 82, 116, 22);
		desktopPane.add(workerIdTxt);
		
		name = new JTextField();
		name.setColumns(10);
		name.setBounds(159, 135, 116, 22);
		desktopPane.add(name);
		
		JLabel label = new JLabel("Worker Id");
		label.setBounds(90, 82, 57, 16);
		desktopPane.add(label);
		
		JLabel label_1 = new JLabel("Name");
		label_1.setBounds(90, 138, 56, 16);
		desktopPane.add(label_1);
		
		JLabel label_2 = new JLabel("Date");
		label_2.setBounds(90, 217, 49, 16);
		desktopPane.add(label_2);
		
		da2 = new JTextField();
		da2.setEditable(false);
		da2.setColumns(10);
		da2.setBounds(151, 214, 116, 22);
		desktopPane.add(da2);
		
		preT = new JTextField();
		preT.setColumns(10);
		preT.setBounds(383, 135, 116, 22);
		desktopPane.add(preT);
		
		genderT = new JTextField();
		genderT.setEditable(false);
		genderT.setColumns(10);
		genderT.setBounds(383, 83, 116, 22);
		desktopPane.add(genderT);
		
		JCheckBox chckbxPresent = new JCheckBox("Present");
		chckbxPresent.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (chckbxPresent.isSelected()) {
					preT.setText("Present");
				}
				if (chckbxPresent.isSelected()) {
					chckbxAbsent.setSelected(false);
					
				}
			}
		});
		chckbxPresent.setBounds(511, 134, 71, 25);
		desktopPane.add(chckbxPresent);
		
		 chckbxAbsent = new JCheckBox("Absent");
		chckbxAbsent.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (chckbxAbsent.isSelected()) {
					preT.setText("Absent");
				}
				if (chckbxAbsent.isSelected()) {
					chckbxPresent.setSelected(false);
				}
			}
		});
		chckbxAbsent.setBounds(511, 169, 67, 25);
		desktopPane.add(chckbxAbsent);
		
		JLabel label_3 = new JLabel("Satatus");
		label_3.setBounds(315, 135, 56, 16);
		desktopPane.add(label_3);
		
		JLabel label_4 = new JLabel("Gender");
		label_4.setBounds(315, 82, 56, 16);
		desktopPane.add(label_4);
		
		lblX = new JLabel("X");
		lblX.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				EditWorkerDitails.this.dispose();	
			}
		});
		lblX.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblX.setForeground(Color.RED);
		lblX.setHorizontalAlignment(SwingConstants.CENTER);
		lblX.setBounds(729, 6, 28, 26);
		desktopPane.add(lblX);
		
		lblPosition = new JLabel("Position");
		lblPosition.setBounds(315, 217, 56, 16);
		desktopPane.add(lblPosition);
		
		pos = new JTextField();
		pos.setBounds(387, 211, 116, 22);
		desktopPane.add(pos);
		pos.setColumns(10);
		
		 addMouseListener(new MouseListener(){
             public void mouseReleased(MouseEvent e) {
             	
                 mouseDownScreenCoords = null;
                 mouseDownCompCoords = null;
             }
             public void mousePressed(MouseEvent e) {
                 mouseDownScreenCoords = e.getLocationOnScreen();
                 mouseDownCompCoords = e.getPoint();
             }
             public void mouseExited(MouseEvent e) {
             }
             public void mouseEntered(MouseEvent e) {
             }
             public void mouseClicked(MouseEvent e) {
             	
             }
         });
         addMouseMotionListener(new MouseMotionListener(){
             public void mouseMoved(MouseEvent e) {
             }
             public void mouseDragged(MouseEvent e) {
                 Point currCoords = e.getLocationOnScreen();
                 setLocation(mouseDownScreenCoords.x + (currCoords.x - mouseDownScreenCoords.x) - mouseDownCompCoords.x,
                               mouseDownScreenCoords.y + (currCoords.y - mouseDownScreenCoords.y) - mouseDownCompCoords.y);
             }
         });
     }
	}

