package viwe.Dialouge;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;
import net.miginfocom.swing.MigLayout;
import viwe.panel.Order;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.text.ParseException;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import controller.AddnewFoodCrl;
import controller.TempOderCtrl;
import model.TakeOrder;
import model.TempOrder;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;


public class AddNewFoodName extends JFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane ;
	public static JLabel lblFoodName;
	public static JTextField foodnameTax;
	public static String path;
	 static Point mouseDownScreenCoords;
     static Point mouseDownCompCoords;
 	public JComboBox catCb;
	public AddNewFoodName() {
		mouseDownScreenCoords = null;
        mouseDownCompCoords = null;
		setResizable(false);
		setUndecorated(true);
		setBackground(new Color(0, 100, 0));
		
		setLocation(new Point(200, 50));
		
		
		setVisible(true);
		
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		
		
		
		setTitle("Confirm Your Order Sir");
		
	
		
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		
		setSize(396, 288);
		getContentPane().setLayout(new MigLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				image=(Toolkit.getDefaultToolkit().getImage(AddNewFoodName.class.getResource("/pictureResource/BA3.png")));
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		
		desktopPane.setFocusable(false);
		desktopPane.setFocusTraversalKeysEnabled(false);
		desktopPane.setFocusCycleRoot(false);
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(null);
		
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddNewFoodName.this.dispose();
			}
		});
		cancelButton.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Cancel_16px_1.png"));
		cancelButton.setBounds(261, 201, 91, 25);
		desktopPane.add(cancelButton);
		
		JButton okButton = new JButton("Ok");
		okButton.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Checked_16px.png"));
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String Foonae=foodnameTax.getText();
				String cat=catCb.getSelectedItem().toString();
				new AddnewFoodCrl(cat, Foonae);
				AddNewFoodName.this.dispose();
				
			}
		});
		okButton.setBounds(182, 201, 67, 25);
		desktopPane.add(okButton);
		
		lblFoodName = new JLabel("Food name");
		lblFoodName.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblFoodName.setForeground(Color.WHITE);
		lblFoodName.setBounds(20, 69, 109, 34);
		desktopPane.add(lblFoodName);
		
		foodnameTax = new JTextField();
		foodnameTax.setBounds(126, 75, 226, 28);
		desktopPane.add(foodnameTax);
		foodnameTax.setColumns(10);
		
		JLabel lblCategory = new JLabel("Category");
		lblCategory.setForeground(Color.WHITE);
		lblCategory.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblCategory.setBounds(20, 129, 109, 34);
		desktopPane.add(lblCategory);
		
		 catCb = new JComboBox();
		 catCb.setEditable(true);
		catCb.setModel(new DefaultComboBoxModel(new String[] {"Select Category", "Breakfast", "Evening Snacks", "Lunch", "Dinner"}));
		catCb.setBounds(129, 129, 223, 34);
		desktopPane.add(catCb);
		addMouseListener(new MouseListener(){
            public void mouseReleased(MouseEvent e) {
            	
                mouseDownScreenCoords = null;
                mouseDownCompCoords = null;
            }
            public void mousePressed(MouseEvent e) {
                mouseDownScreenCoords = e.getLocationOnScreen();
                mouseDownCompCoords = e.getPoint();
            }
            public void mouseExited(MouseEvent e) {
            }
            public void mouseEntered(MouseEvent e) {
            }
            public void mouseClicked(MouseEvent e) {
            	
            }
        });
        addMouseMotionListener(new MouseMotionListener(){
            public void mouseMoved(MouseEvent e) {
            }
            public void mouseDragged(MouseEvent e) {
                Point currCoords = e.getLocationOnScreen();
                setLocation(mouseDownScreenCoords.x + (currCoords.x - mouseDownScreenCoords.x) - mouseDownCompCoords.x,
                              mouseDownScreenCoords.y + (currCoords.y - mouseDownScreenCoords.y) - mouseDownCompCoords.y);
            }
        });
		
	}
}
