package viwe.Dialouge;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;
import net.miginfocom.swing.MigLayout;
import viwe.panel.Order;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.text.ParseException;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import controller.TempOderCtrl;
import model.TakeOrder;
import model.TempOrder;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.ImageIcon;


public class ConfirmOrrde extends JFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane ;
	public static JLabel lblFoodName;
	public static JTextField foodnameTax;
	public static JTextField periceTax;
	public static JTextField CategoryTax;
	public static JTextField QuntityTax;
	public static JTextField TotalTax;
	public static JLabel pic;
	public static String path;
	private JLabel lblNewLabel;
	 static Point mouseDownScreenCoords;
     static Point mouseDownCompCoords;
	
	public ConfirmOrrde() {
		mouseDownScreenCoords = null;
        mouseDownCompCoords = null;
		setResizable(false);
		setUndecorated(true);
		setBackground(new Color(0, 100, 0));
		
		setLocation(new Point(200, 50));
		
		
		setVisible(true);
		
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		
		
		
		setTitle("Confirm Your Order Sir");
		
	
		
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		
		setSize(763, 457);
		getContentPane().setLayout(new MigLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				image=(Toolkit.getDefaultToolkit().getImage(ConfirmOrrde.class.getResource("/pictureResource/BA3.png")));
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		
		desktopPane.setFocusable(false);
		desktopPane.setFocusTraversalKeysEnabled(false);
		desktopPane.setFocusCycleRoot(false);
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(null);
		
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConfirmOrrde.this.dispose();
			}
		});
		cancelButton.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Cancel_16px_1.png"));
		cancelButton.setBounds(636, 375, 87, 28);
		desktopPane.add(cancelButton);
		
		JButton okButton = new JButton("Ok");
		okButton.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Checked_16px.png"));
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nameOfFood=foodnameTax.getText();
				String Price=periceTax.getText();
				String cateGory=CategoryTax.getText();
				String qauntity=QuntityTax.getText();
				String total=TotalTax.getText();
				TempOderCtrl ta=new TempOderCtrl(nameOfFood, Price, cateGory, qauntity, total);
				TempOrder.Totalamount();
				ConfirmOrrde.this.dispose();
			}
		});
		okButton.setBounds(561, 375, 63, 28);
		desktopPane.add(okButton);
		
		lblFoodName = new JLabel("Food name");
		lblFoodName.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblFoodName.setForeground(Color.WHITE);
		lblFoodName.setBounds(20, 69, 109, 34);
		desktopPane.add(lblFoodName);
		
		foodnameTax = new JTextField();
		
		foodnameTax.setEditable(false);
		foodnameTax.setBounds(126, 75, 167, 28);
		desktopPane.add(foodnameTax);
		foodnameTax.setColumns(10);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setForeground(Color.WHITE);
		lblPrice.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblPrice.setBounds(23, 198, 69, 34);
		desktopPane.add(lblPrice);
		
		periceTax = new JTextField();
		periceTax.setEditable(false);
		periceTax.setHorizontalAlignment(SwingConstants.CENTER);
		periceTax.setBounds(126, 198, 167, 28);
		desktopPane.add(periceTax);
		periceTax.setColumns(10);
		
		CategoryTax = new JTextField();
		CategoryTax.setEditable(false);
		CategoryTax.setBounds(126, 129, 167, 28);
		desktopPane.add(CategoryTax);
		CategoryTax.setColumns(10);
		
		JLabel lblCategory = new JLabel("Category");
		lblCategory.setForeground(Color.WHITE);
		lblCategory.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblCategory.setBounds(20, 129, 109, 34);
		desktopPane.add(lblCategory);
		
		QuntityTax = new JTextField();
		QuntityTax.setHorizontalAlignment(SwingConstants.CENTER);
		QuntityTax.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				String price=periceTax.getText();
				int Pri=Integer.parseInt(price);
				String Qun=QuntityTax.getText();
				int qunt=Integer.parseInt(Qun);
				int to=Pri*qunt;
				String total=Integer.toString(to);
				TotalTax.setText(total);
			}
		});
		QuntityTax.setBounds(124, 260, 169, 28);
		desktopPane.add(QuntityTax);
		QuntityTax.setColumns(10);
		
		JLabel lblQuntity = new JLabel("Quntity");
		lblQuntity.setForeground(Color.WHITE);
		lblQuntity.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblQuntity.setBounds(20, 260, 109, 34);
		desktopPane.add(lblQuntity);
		
		TotalTax = new JTextField();
		TotalTax.setFont(new Font("SansSerif", Font.BOLD, 16));
		TotalTax.setHorizontalAlignment(SwingConstants.CENTER);
		TotalTax.setBounds(122, 366, 156, 43);
		desktopPane.add(TotalTax);
		TotalTax.setColumns(10);
		
		JLabel lblTotal = new JLabel("Total");
		lblTotal.setHorizontalAlignment(SwingConstants.CENTER);
		lblTotal.setForeground(Color.WHITE);
		lblTotal.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblTotal.setBounds(6, 369, 109, 34);
		desktopPane.add(lblTotal);
		
		pic = new JLabel("");
		
		pic.setBounds(359, 75, 366, 288);
		desktopPane.add(pic);
		
		lblNewLabel = new JLabel("Confirm Your Order Sir !");
		lblNewLabel.setFont(new Font("SansSerif", Font.BOLD, 40));
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(130, 6, 460, 52);
		desktopPane.add(lblNewLabel);
		addMouseListener(new MouseListener(){
            public void mouseReleased(MouseEvent e) {
            	
                mouseDownScreenCoords = null;
                mouseDownCompCoords = null;
            }
            public void mousePressed(MouseEvent e) {
                mouseDownScreenCoords = e.getLocationOnScreen();
                mouseDownCompCoords = e.getPoint();
            }
            public void mouseExited(MouseEvent e) {
            }
            public void mouseEntered(MouseEvent e) {
            }
            public void mouseClicked(MouseEvent e) {
            	
            }
        });
        addMouseMotionListener(new MouseMotionListener(){
            public void mouseMoved(MouseEvent e) {
            }
            public void mouseDragged(MouseEvent e) {
                Point currCoords = e.getLocationOnScreen();
                setLocation(mouseDownScreenCoords.x + (currCoords.x - mouseDownScreenCoords.x) - mouseDownCompCoords.x,
                              mouseDownScreenCoords.y + (currCoords.y - mouseDownScreenCoords.y) - mouseDownCompCoords.y);
            }
        });
		
	}
	public static ImageIcon ResizeImage(String ImagePath) {
		ImageIcon MyImage = new ImageIcon(ImagePath);
		
		Image img = MyImage.getImage();
		Image newImg = img.getScaledInstance(pic.getWidth(), pic.getHeight(), Image.SCALE_REPLICATE);
		ImageIcon image = new ImageIcon(newImg);
	
		return image;
	}
}
