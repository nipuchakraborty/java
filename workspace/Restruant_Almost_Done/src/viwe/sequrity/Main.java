package viwe.sequrity;

    import java.awt.Point;
    import java.awt.event.MouseEvent;
    import java.awt.event.MouseListener;
    import java.awt.event.MouseMotionListener;
    import javax.swing.JFrame;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.border.MatteBorder;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.event.MouseAdapter;
    public class Main
    {
        static Point mouseDownScreenCoords;
        static Point mouseDownCompCoords;
        public static void main(String[] args)
        {
            final JFrame f = new JFrame("Hello");
            f.getContentPane().setBackground(Color.WHITE);
            mouseDownScreenCoords = null;
            mouseDownCompCoords = null;
            f.setUndecorated(true);
            f.setVisible(true);
            f.setBounds(500, 100, 750, 600);
            f.getContentPane().setLayout(null);
            
            JLabel lblExit = new JLabel("Exit");
            lblExit.setBounds(0, 0, 38, 25);
            f.getContentPane().add(lblExit);
            lblExit.addMouseListener(new MouseAdapter() {
            	@Override
            	public void mouseClicked(MouseEvent arg0) {
            		System.exit(0);
            	}
            });
            lblExit.setFont(new Font("Tahoma", Font.BOLD, 20));
            lblExit.setForeground(Color.RED);
            
            JPanel panel = new JPanel();
            panel.setBackground(new Color(102, 102, 102));
            panel.setBounds(0, 0, 312, 626);
            f.getContentPane().add(panel);
            panel.setLayout(new MigLayout("", "[][][][][][]", "[][][][][][][][][][][][][][][][][]"));
            
            JLabel lblNewLabel = new JLabel("");
            panel.add(lblNewLabel, "cell 2 6 3 1");
            lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Male_User_104px.png"));
            
            JLabel lblUser = new JLabel("User");
            lblUser.setFont(new Font("Tahoma", Font.PLAIN, 30));
            lblUser.setForeground(Color.WHITE);
            panel.add(lblUser, "cell 3 7 2 1,alignx center,aligny center");
            
            
            f.addMouseListener(new MouseListener(){
                public void mouseReleased(MouseEvent e) {
                	
                    mouseDownScreenCoords = null;
                    mouseDownCompCoords = null;
                }
                public void mousePressed(MouseEvent e) {
                    mouseDownScreenCoords = e.getLocationOnScreen();
                    mouseDownCompCoords = e.getPoint();
                }
                public void mouseExited(MouseEvent e) {
                }
                public void mouseEntered(MouseEvent e) {
                }
                public void mouseClicked(MouseEvent e) {
                	
                }
            });
            f.addMouseMotionListener(new MouseMotionListener(){
                public void mouseMoved(MouseEvent e) {
                }
                public void mouseDragged(MouseEvent e) {
                    Point currCoords = e.getLocationOnScreen();
                    f.setLocation(mouseDownScreenCoords.x + (currCoords.x - mouseDownScreenCoords.x) - mouseDownCompCoords.x,
                                  mouseDownScreenCoords.y + (currCoords.y - mouseDownScreenCoords.y) - mouseDownCompCoords.y);
                }
            });
        }
    }