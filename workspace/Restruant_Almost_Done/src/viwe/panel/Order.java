package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;
import net.miginfocom.swing.MigLayout;
import viwe.Printsupport;
import viwe.Printsupport.MyPrintable;
import viwe.Dialouge.ConfirmOrrde;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JTabbedPane;
import com.toedter.calendar.JDateChooser;

import controller.SalesReportCtrl;
import controller.TempOderCtrl;
import model.TakeOrder;
import model.TempOrder;

import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.border.TitledBorder;
import org.jdesktop.swingx.JXDatePicker;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.border.LineBorder;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.border.MatteBorder;
import javax.swing.border.EtchedBorder;


public class Order extends JInternalFrame {

	private JPanel contentPane;
	public JDesktopPane desktopPane ;
	private JTabbedPane tabbedPane;
	private JLabel lblOrderList;
	private JScrollPane scrollPane;
	
	private JButton btnPrint;
	private JTabbedPane tabbedPane_1;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel panel_3;
	private JScrollPane scrollPane_1;
	
	private JScrollPane scrollPane_2;

	private JScrollPane scrollPane_3;

	private JScrollPane scrollPane_4;
	
	private JLabel lblName;
	private JLabel lblTableNo;
	private JLabel lblDate;
	private JTextField name_text;
	private JComboBox tableNo;
	private JXDatePicker datePicker_1;
	//all table 
	public static  JTable BillTab;
	public static JTable breakfast_table;
	public static JTable dinner_table;
	public static JTable luch_table;
	public static JTable snacks_table;
	private JLabel lblTotalAmount;
	public static  JTextField takaL;
	private JButton btnSave;
	private JButton btnRemove;
	public static String name ;
	public static String p;
	public static java.sql.Date date2;
	public static java.util.Date date;
	public static JPanel Total;
	public static JLabel lblFoodInforamtion;
	public static JLabel pic;
	public static JLabel lblPrice;
	public static JTextField UnitPrice;
	public static JLabel lblQuntity;
	public static JLabel lblNewLabel_1;
	public static JLabel lblCategory;
	public static JTextField Totalta;
	public static JTextField Category;
	public static JTextField quntity;
	public static JTextField FoodName;
	public static JTextField taked_by;
	
	
	
	
	public Order() {
		setResizable(true);
		setFrameIcon(new ImageIcon(Order.class.getResource("/pictureResource/heavy_food_meal_dinner_lunch_restaurant-128.png")));
		setBackground(new Color(0, 100, 0));
		
		setLocation(new Point(220, 0));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		setDoubleBuffered(true);
		setMaximizable(true);
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		
		
		setTitle("ORDER MENU");
		
	
		
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		
		setSize(1191, 753);
		getContentPane().setLayout(new MigLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				image=(Toolkit.getDefaultToolkit().getImage(Order.class.getResource("/pictureResource/BA4.png")));
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		
		desktopPane.setFocusable(false);
		desktopPane.setFocusTraversalKeysEnabled(false);
		desktopPane.setFocusCycleRoot(false);
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(null);
		
		lblOrderList = new JLabel("Order List");
		lblOrderList.setBounds(14, 7, 131, 39);
		lblOrderList.setHorizontalAlignment(SwingConstants.CENTER);
		lblOrderList.setFont(new Font("SansSerif", Font.PLAIN, 30));
		lblOrderList.setForeground(Color.WHITE);
		desktopPane.add(lblOrderList);
		
		lblName = new JLabel("Customer Name");
		lblName.setBounds(672, 19, 142, 24);
		lblName.setHorizontalAlignment(SwingConstants.CENTER);
		lblName.setForeground(Color.WHITE);
		lblName.setFont(new Font("SansSerif", Font.BOLD, 18));
		desktopPane.add(lblName);
		
		name_text = new JTextField("");
		
		name_text.setBounds(818, 19, 100, 28);
		desktopPane.add(name_text);
		name_text.setColumns(10);
		
		lblDate = new JLabel("Date");
		lblDate.setBounds(942, 19, 40, 24);
		lblDate.setHorizontalAlignment(SwingConstants.CENTER);
		lblDate.setForeground(Color.WHITE);
		lblDate.setFont(new Font("SansSerif", Font.BOLD, 18));
		desktopPane.add(lblDate);
		
		datePicker_1 = new JXDatePicker();
		datePicker_1.setBounds(1014, 13, 151, 28);
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		java.util.Date date5 = new Date(System.currentTimeMillis());
		datePicker_1.setDate(date5);
		
		datePicker_1.getEditor().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			}
		});
		datePicker_1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
			
				//textField_1.setText(text+" day."+mainDate);
			}
		});
		desktopPane.add(datePicker_1);
		JDateChooser datePicker=new JDateChooser();
		
		lblTableNo = new JLabel("Table No");
		lblTableNo.setBounds(672, 55, 77, 24);
		lblTableNo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTableNo.setForeground(Color.WHITE);
		lblTableNo.setFont(new Font("SansSerif", Font.BOLD, 18));
		desktopPane.add(lblTableNo);
		
		tableNo = new JComboBox();
		tableNo.setBounds(818, 56, 120, 26);
		tableNo.setModel(new DefaultComboBoxModel(new String[] {"Select Table", "Table No1", "Table No 2", "Table No 3", "Table No 4", "Table No 5", "Table No 6", "Table No 7", "Table No 8"}));
		desktopPane.add(tableNo);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(659, 123, 506, 521);
		scrollPane.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0), 0, true), "Bill", TitledBorder.CENTER, TitledBorder.TOP, null, Color.RED));
		desktopPane.add(scrollPane);
		
		BillTab = new JTable();
		TempOrder.getDataFromAddFood();
		BillTab.setBorder(null);
		scrollPane.setViewportView(BillTab);
		
		tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane_1.setBounds(7, 51, 637, 333);
		desktopPane.add(tabbedPane_1);
		
		panel = new JPanel();
		tabbedPane_1.addTab("BreakFast", null, panel, null);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		scrollPane_4 = new JScrollPane();
		panel.add(scrollPane_4);
		
		breakfast_table = new JTable();
		breakfast_table.setBackground(new Color(255, 228, 181));
		TakeOrder.breakFastOrder();
		breakfast_table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row = breakfast_table.getSelectedRow();

				name = breakfast_table.getModel().getValueAt(row, 0).toString();
				TakeOrder.getOrderBreakfast(name);
				/*String price = breakfast_table.getModel().getValueAt(row, 1).toString();

				String category = breakfast_table.getModel().getValueAt(row, 2).toString();
				
				String pic2 = breakfast_table.getModel().getValueAt(row, 4).toString();
				
				
				new ConfirmOrrde().setVisible(true);*/
				
				/*ConfirmOrrde.foodnameTax.setText(name);
				ConfirmOrrde.periceTax.setText(price);
				ConfirmOrrde.CategoryTax.setText(category);
				ConfirmOrrde.pic.setIcon(new ImageIcon(pic2));
				ConfirmOrrde.ResizeImage(pic2);*/
				
				
				
				
			}
		});
		scrollPane_4.setViewportView(breakfast_table);
		
		panel_1 = new JPanel();
		tabbedPane_1.addTab("Lunch", null, panel_1, null);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		scrollPane_2 = new JScrollPane();
		panel_1.add(scrollPane_2, BorderLayout.CENTER);
		
		luch_table = new JTable();
		luch_table.setBackground(new Color(255, 228, 181));
		TakeOrder.lunchOrder();
		luch_table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row = luch_table.getSelectedRow();

				String name = luch_table.getModel().getValueAt(row, 0).toString();
				TakeOrder.getOrderlunch(name);
/*
				String price = luch_table.getModel().getValueAt(row, 1).toString();

				String category = luch_table.getModel().getValueAt(row, 2).toString();
				String path=luch_table.getModel().getValueAt(row,4).toString();*/
				
				
				/*new ConfirmOrrde().setVisible(true);
				ConfirmOrrde.foodnameTax.setText(name);
				ConfirmOrrde.periceTax.setText(price);
				ConfirmOrrde.CategoryTax.setText(category);
				ConfirmOrrde.pic.setIcon(new ImageIcon(path));
				ConfirmOrrde.ResizeImage(path);*/
			}
		});
		scrollPane_2.setViewportView(luch_table);
		
		panel_2 = new JPanel();
		tabbedPane_1.addTab("Eving Snacks", null, panel_2, null);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		scrollPane_3 = new JScrollPane();
		panel_2.add(scrollPane_3, BorderLayout.CENTER);
		
		snacks_table = new JTable();
		snacks_table.setBackground(new Color(255, 222, 173));
		TakeOrder.EveningsnakcsOrder();
		snacks_table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row = snacks_table.getSelectedRow();

				String name = snacks_table.getModel().getValueAt(row, 0).toString();
				
				TakeOrder.getOrderSnacks(name);
				/*String price = snacks_table.getModel().getValueAt(row, 1).toString();

				String category = snacks_table.getModel().getValueAt(row, 2).toString();
				String path=snacks_table.getModel().getValueAt(row,4).toString();

				new ConfirmOrrde().setVisible(true);
				ConfirmOrrde.foodnameTax.setText(name);
				ConfirmOrrde.periceTax.setText(price);
				ConfirmOrrde.CategoryTax.setText(category);
				ConfirmOrrde.pic.setIcon(new ImageIcon(path));*/
			}
		});
		scrollPane_3.setViewportView(snacks_table);
		
		panel_3 = new JPanel();
		tabbedPane_1.addTab("Dinner", null, panel_3, null);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		scrollPane_1 = new JScrollPane();
		panel_3.add(scrollPane_1, BorderLayout.CENTER);
		
		dinner_table = new JTable();
		dinner_table.setBackground(new Color(255, 222, 173));
		TakeOrder.DnnerOrder();
		dinner_table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row = dinner_table.getSelectedRow();

				String name = dinner_table.getModel().getValueAt(row, 0).toString();
				TakeOrder.getOrderDinner(name);

				/*String price = dinner_table.getModel().getValueAt(row, 1).toString();

				String category = dinner_table.getModel().getValueAt(row, 2).toString();
				String path=dinner_table.getModel().getValueAt(row,4 ).toString();

				new ConfirmOrrde().setVisible(true);
				ConfirmOrrde.foodnameTax.setText(name);
				ConfirmOrrde.periceTax.setText(price);
				ConfirmOrrde.CategoryTax.setText(category);	
				ConfirmOrrde.pic.setIcon(new ImageIcon(path));*/
			}
		});
		scrollPane_1.setViewportView(dinner_table);
		
		btnPrint = new JButton("Print");
		btnPrint.setBounds(1092, 650, 73, 28);
		btnPrint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Printsupport p=new Printsupport();
				p.getTableData(BillTab);
				String total=takaL.getText();
				try {
					MessageFormat heading = new MessageFormat("Bill");
					
					MessageFormat footer = new MessageFormat("Total:"+total);
					BillTab.print(JTable.PrintMode.FIT_WIDTH,heading,footer);
					BillTab.printAll(getGraphics());
					BillTab.print();
				} catch (Exception E) {
					E.printStackTrace();
				}
			}
		});
		String name = name_text.getText();
		
		Total = new JPanel();
		Total.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(192, 192, 192)));
		Total.setBounds(7, 388, 637, 309);
		Total.setBackground(new Color(192, 192, 192));
		desktopPane.add(Total);
		Total.setLayout(null);
		
		lblFoodInforamtion = new JLabel("Food Informtion");
		lblFoodInforamtion.setBounds(261, 287, 86, 16);
		Total.add(lblFoodInforamtion);
		
		pic = new JLabel("");
		pic.setBounds(293, 7, 240, 202);
		//pic.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\milkTea.jpg"));
		
		Total.add(pic);
		
		lblPrice = new JLabel("Unit Price:");
		lblPrice.setBounds(7, 7, 90, 26);
		lblPrice.setFont(new Font("SansSerif", Font.PLAIN, 20));
		Total.add(lblPrice);
		
		UnitPrice = new JTextField();
		UnitPrice.setFont(new Font("SansSerif", Font.PLAIN, 16));
		UnitPrice.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		UnitPrice.setBounds(95, 7, 122, 28);
		UnitPrice.setBackground(Color.GRAY);
		UnitPrice.setEditable(false);
		Total.add(UnitPrice);
		UnitPrice.setColumns(10);
		
		lblQuntity = new JLabel("Quntity:");
		lblQuntity.setBounds(7, 105, 68, 26);
		lblQuntity.setFont(new Font("SansSerif", Font.PLAIN, 20));
		Total.add(lblQuntity);
		
		Totalta = new JTextField();
		Totalta.setFont(new Font("SansSerif", Font.PLAIN, 16));
		Totalta.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		Totalta.setBounds(95, 173, 122, 28);
		Totalta.setEditable(false);
		Totalta.setColumns(10);
		Totalta.setBackground(Color.GRAY);
		Total.add(Totalta);
		
		lblCategory = new JLabel("Category:");
		lblCategory.setBounds(7, 53, 87, 26);
		lblCategory.setFont(new Font("SansSerif", Font.PLAIN, 20));
		Total.add(lblCategory);
		
		Category = new JTextField();
		Category.setFont(new Font("SansSerif", Font.PLAIN, 16));
		Category.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		Category.setBounds(95, 55, 122, 28);
		Category.setEditable(false);
		Category.setColumns(10);
		Category.setBackground(Color.GRAY);
		Total.add(Category);
		
		lblNewLabel_1 = new JLabel("Total:");
		lblNewLabel_1.setBounds(7, 171, 50, 26);
		lblNewLabel_1.setFont(new Font("SansSerif", Font.PLAIN, 20));
		Total.add(lblNewLabel_1);
		
		quntity = new JTextField();
		quntity.setFont(new Font("SansSerif", Font.PLAIN, 16));
		quntity.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		quntity.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				String Up=UnitPrice.getText();
				int up=Integer.parseInt(Up);
				String Qu=quntity.getText();
				int q=Integer.parseInt(Qu);
				int t=up*q;
				String T =Integer.toString(t);
				Totalta.setText(T);
			
				
			}
		});
		quntity.setBounds(95, 107, 122, 28);
		quntity.setColumns(10);
		quntity.setBackground(Color.GRAY);
		Total.add(quntity);
		
		FoodName = new JTextField();
		FoodName.setFont(new Font("SansSerif", Font.PLAIN, 16));
		FoodName.setBorder(null);
		FoodName.setHorizontalAlignment(SwingConstants.CENTER);
		FoodName.setBounds(333, 213, 122, 28);
		FoodName.setBackground(new Color(192, 192, 192));
		Total.add(FoodName);
		FoodName.setColumns(10);
		
		JButton btnOk_1 = new JButton("ok");
		btnOk_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nameOfFood=FoodName.getText();
				String Price=UnitPrice.getText();
				String cateGory=Category.getText();
				String qauntity=quntity.getText();
				String total=Totalta.getText();
				TempOderCtrl ta=new TempOderCtrl(nameOfFood, Price, cateGory, qauntity, total);
				TempOrder.Totalamount();
				
			}
		});
		btnOk_1.setBounds(503, 245, 90, 28);
		Total.add(btnOk_1);
		
		
		lblTotalAmount = new JLabel("Total Amount");
		lblTotalAmount.setBounds(672, 648, 116, 26);
		lblTotalAmount.setFont(new Font("SansSerif", Font.PLAIN, 20));
		lblTotalAmount.setForeground(Color.WHITE);
		desktopPane.add(lblTotalAmount);
		
		takaL = new JTextField();
		takaL.setBounds(818, 650, 100, 28);
		desktopPane.add(takaL);
		takaL.setColumns(10);
		
		btnSave = new JButton("Save");
		btnSave.setBounds(942, 650, 55, 28);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (name_text.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Sorry You Should Type Customer name");
					
				}
				else if (tableNo.getSelectedItem().equals("Select Table")) {
					
					JOptionPane.showMessageDialog(null, "Sorry You should select table");
				}
				else {
					
					
					
					String CN=name_text.getText();
					String Tn=tableNo.getSelectedItem().toString();
					System.out.println(Tn);
					 date=datePicker_1.getDate();
					 date2=new java.sql.Date(date.getTime());
					String total=takaL.getText();
					String tak=taked_by.getText();
					new SalesReportCtrl(Tn, CN, total,tak, date2);
					TakeOrder.saveAndDel();
					TempOrder.getDataFromAddFood();
					takaL.setText("");
					
				}
	
				
			}
		});
		desktopPane.add(btnSave);
		
		btnRemove = new JButton("Remove");
		btnRemove.setBounds(1014, 650, 74, 28);
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int row = BillTab.getSelectedRow();

				String name = BillTab.getModel().getValueAt(row, 0).toString();

				
			
				TempOderCtrl.Del(name);
				TempOrder.getDataFromAddFood();
			}
		});
		desktopPane.add(btnRemove);
		btnPrint.setIcon(new ImageIcon(Order.class.getResource("/pictureResource/images/print.png")));
		desktopPane.add(btnPrint);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(7, 701, 1154, 14);
		desktopPane.add(tabbedPane);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new MatteBorder(1, 5, 0, 5, (Color) new Color(255, 255, 255)));
		panel_5.setBounds(646, 7, 14, 709);
		desktopPane.add(panel_5);
		
		taked_by = new JTextField();
		taked_by.setBounds(1024, 55, 141, 28);
		desktopPane.add(taked_by);
		taked_by.setColumns(10);
		
		JLabel lblTakedBy = new JLabel("Taked by ");
		lblTakedBy.setHorizontalAlignment(SwingConstants.CENTER);
		lblTakedBy.setForeground(Color.WHITE);
		lblTakedBy.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblTakedBy.setBounds(928, 55, 100, 24);
		desktopPane.add(lblTakedBy);
		
	}
}
