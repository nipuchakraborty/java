package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;
import net.miginfocom.swing.MigLayout;
import viwe.Printsupport;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;

import model.ExpenseAndProfitModel;

import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.border.MatteBorder;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class ParchaseReports extends JInternalFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane ;
	private JTextField biilT;
	private JLabel lblDate;
	private JDateChooser dateChooser;
	private JLabel lblFrom;
	private JDateChooser dateChooser_1;
	private JScrollPane scrollPane;
	public static  JTable Expense;
	private JButton btnSearch;
	public static  JTable Profit;
	public static JTextField total_parchase;
	public static  JTextField total_sale;
	private JTextField textField_2;
	private JButton btnRefresh;
	private JButton button;
	
	
	public ParchaseReports() {
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				ExpenseAndProfitModel.Totalamount();
				ExpenseAndProfitModel.TotalSale();
			}
		});
		setBackground(new Color(0, 100, 0));
		setTitle("Parchase");
		setLocation(new Point(220, 0));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		setDoubleBuffered(true);
		setMaximizable(true);
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		
	
		
		setTitle("Expenses Report");
		
	
		
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		
		setSize(1191, 753);
		getContentPane().setLayout(new MigLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				image=(Toolkit.getDefaultToolkit().getImage(ParchaseReports.class.getResource("/pictureResource/BA3.png")));
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		
		desktopPane.setFocusable(false);
		desktopPane.setFocusTraversalKeysEnabled(false);
		desktopPane.setFocusCycleRoot(false);
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(null);
		
		JLabel label = new JLabel("Bill No:");
		label.setBounds(7, 7, 64, 22);
		label.setForeground(new Color(240, 255, 240));
		label.setFont(new Font("Tahoma", Font.BOLD, 18));
		label.setBackground(Color.LIGHT_GRAY);
		desktopPane.add(label);
		
		biilT = new JTextField();
		biilT.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				String b=biilT.getText();
				ExpenseAndProfitModel.load(b);
			}
		});
		biilT.setBounds(127, 7, 184, 28);
		desktopPane.add(biilT);
		biilT.setColumns(10);
		
		lblDate = new JLabel("To");
		lblDate.setBounds(340, 13, 22, 22);
		lblDate.setForeground(new Color(240, 255, 240));
		lblDate.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblDate.setBackground(Color.LIGHT_GRAY);
		desktopPane.add(lblDate);
		
		dateChooser = new JDateChooser();
		dateChooser.setBounds(374, 7, 184, 31);
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		java.util.Date date2 = new Date(System.currentTimeMillis());
		dateChooser.setDate(date2);
		desktopPane.add(dateChooser);
		
		lblFrom = new JLabel("From");
		lblFrom.setBounds(648, 10, 46, 22);
		lblFrom.setForeground(new Color(240, 255, 240));
		lblFrom.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblFrom.setBackground(Color.LIGHT_GRAY);
		desktopPane.add(lblFrom);
		
		dateChooser_1 = new JDateChooser();
		
		dateChooser_1.setBounds(768, 7, 184, 28);
		DateFormat df2 = new SimpleDateFormat("dd/MM/yyyy");
		java.util.Date date = new Date(System.currentTimeMillis());
		dateChooser_1.setDate(date2);
		
		
		
		desktopPane.add(dateChooser_1);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(17, 61, 556, 617);
		scrollPane.setBorder(new TitledBorder(null, "Expenses", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(255, 255, 255)));
		desktopPane.add(scrollPane);
		
		Expense = new JTable();
		
		ExpenseAndProfitModel.loadReportP();
		scrollPane.setViewportView(Expense);
		
		btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				java.util.Date d=dateChooser_1.getDate();
				java.sql.Date sqDate=new Date(d.getTime());
				ExpenseAndProfitModel.load_data(sqDate);	
			}
		});
		btnSearch.setBounds(1041, 12, 87, 28);
		btnSearch.setIcon(new ImageIcon(ParchaseReports.class.getResource("/pictureResource/b_search.png")));
		desktopPane.add(btnSearch);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBorder(new TitledBorder(null, "Profit", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(255, 255, 255)));
		scrollPane_1.setBounds(599, 61, 551, 611);
		desktopPane.add(scrollPane_1);
		
		Profit = new JTable();
		ExpenseAndProfitModel.load();
		scrollPane_1.setViewportView(Profit);
		
		JButton btnPrint = new JButton("Print");
		btnPrint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Printsupport p=new Printsupport();
				p.getTableData(Profit);
				
				try {
					MessageFormat heading = new MessageFormat("Sales Report");
					
					MessageFormat footer = new MessageFormat("Sales Report");
					Profit.print(JTable.PrintMode.FIT_WIDTH,heading,footer);
					Profit.printAll(getGraphics());
					Profit.print();
				} catch (Exception E) {
					E.printStackTrace();
				}
			}
			
		});
		btnPrint.setIcon(new ImageIcon(ParchaseReports.class.getResource("/pictureResource/images/print.png")));
		btnPrint.setBounds(1056, 688, 90, 28);
		desktopPane.add(btnPrint);
		
		JLabel lblTotal = new JLabel("Total cost");
		lblTotal.setForeground(new Color(255, 255, 255));
		lblTotal.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblTotal.setBounds(143, 688, 85, 24);
		desktopPane.add(lblTotal);
		
		total_parchase = new JTextField();
		total_parchase.setBounds(240, 690, 122, 28);
		desktopPane.add(total_parchase);
		total_parchase.setColumns(10);
		
		JLabel lblTotalSale = new JLabel("Total Sale");
		lblTotalSale.setForeground(Color.WHITE);
		lblTotalSale.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblTotalSale.setBounds(808, 692, 86, 24);
		desktopPane.add(lblTotalSale);
		
		total_sale = new JTextField();
		total_sale.setColumns(10);
		total_sale.setBounds(906, 688, 122, 28);
		desktopPane.add(total_sale);
		
		JLabel lblProfit = new JLabel("Profit");
		lblProfit.setForeground(Color.WHITE);
		lblProfit.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblProfit.setBounds(484, 690, 74, 24);
		desktopPane.add(lblProfit);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(572, 688, 122, 28);
		desktopPane.add(textField_2);
		
		JButton btnShow = new JButton("show");
		btnShow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String p1=total_parchase.getText();
				int p=Integer.parseInt(p1);
				String s1=total_sale.getText();
				int s=Integer.parseInt(s1);
				int profit=s-p;
				String profitR=Integer.toString(profit);
				textField_2.setText(profitR);
			}
		});
		btnShow.setBounds(706, 688, 90, 28);
		desktopPane.add(btnShow);
		
		btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ExpenseAndProfitModel.loadReportP();
			}
		});
		btnRefresh.setBounds(382, 688, 90, 28);
		desktopPane.add(btnRefresh);
		
		button = new JButton("Print");
		button.setIcon(new ImageIcon(ParchaseReports.class.getResource("/pictureResource/images/print.png")));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Printsupport p=new Printsupport();
				p.getTableData(Expense);
				
				try {
					MessageFormat heading = new MessageFormat("Expense Report");
					
					MessageFormat footer = new MessageFormat("Expense Report");
					Expense.print(JTable.PrintMode.FIT_WIDTH,heading,footer);
					Expense.printAll(getGraphics());
					Expense.print();
				} catch (Exception E) {
					E.printStackTrace();
				}
			}
			
		});
		button.setBounds(27, 688, 90, 28);
		desktopPane.add(button);
		
	}
}
