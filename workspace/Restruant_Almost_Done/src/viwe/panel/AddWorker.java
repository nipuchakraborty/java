package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;
import net.miginfocom.swing.MigLayout;
import viwe.TextPrompt;
import viwe.Dialouge.EditAddWorker;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.text.ParseException;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;

import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import controller.AddworkerCtrl;
import model.AddWorkerModel;
import model.TakeOrder;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.border.MatteBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

public class AddWorker extends JInternalFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane;
	private JTextField workerId;
	private JTextField nameT;
	private JLabel lblWorkerPost;
	private JLabel lblGender;
	private JRadioButton gFmale;
	private JRadioButton gMale;
	private JComboBox postCb;
	private JScrollPane scrollPane;
	private JLabel lblNewLabel;
	private JTextField SearchTxt;
	public static  JTable WorkerDitails_Tab;
	private JButton btnAdd;
	private JLabel lblBasicSalary;
	public static JTextField basicslary;
	public static JButton btnRemove;
	public static JButton btnRefresh;
	public static JPanel panel;
	public static JTextField W_name;
	public static JTextField Address;
	public static JTextField contact;
	public static JTextField emai_id_txt;
	public static JTextField B_salary;
	public static JTextField post;
	public static JLabel lblContactNo_1;
	public static JTextField contact_no;
	public static JLabel email;
	public static JTextField email_id;
	public static JLabel lblAddress_1;
	public static JTextField address_txt;
	public static JButton btnAddPic;
	public static JTextField pict_txt;
	public static String path;
	public static String name;
	private JLabel gL;
	public static JLabel pic_L;
	public static JTextField Gen_txt;

	public AddWorker() {
		setResizable(true);
		setBorder(new MatteBorder(3, 3, 3, 3, (Color) Color.PINK));
		setFrameIcon(new ImageIcon(AddWorker.class.getResource("/pictureResource/images/emp.png")));
		setBackground(new Color(0, 100, 0));
		
		setLocation(new Point(220, 0));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);

		setVisible(true);
		setDoubleBuffered(true);
		setMaximizable(true);

		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));

		setTitle("Add New  Worker");
	
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

		setSize(1207, 753);
		getContentPane().setLayout(new MigLayout());

		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));

		desktopPane = new JDesktopPane() {
			private Image image;
			{
				
				image = (Toolkit.getDefaultToolkit().getImage(AddWorker.class.getResource("/pictureResource/BA2.png")));
			}

			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
			}
		};

		desktopPane.setFocusable(false);
		desktopPane.setFocusTraversalKeysEnabled(false);
		desktopPane.setFocusCycleRoot(false);
		desktopPane.setBorder(null);

		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(new MigLayout("", "[126px][][165px,grow][][][][][][grow][][grow][][][][][grow][][][][grow][grow][][][][][][][][][][][118.00,grow][][22.00][]", "[26.00px][][37px][][31.00][grow][][][][grow][]"));
		
		JLabel lblWorkerId = new JLabel("Worker Id");
		lblWorkerId.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblWorkerId.setForeground(Color.WHITE);
		desktopPane.add(lblWorkerId, "cell 0 0,grow");
		
		workerId = new JTextField();
		TextPrompt textPrompt=new TextPrompt("Enter Worker Id", workerId);
		workerId.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				
				
			}
		});
		desktopPane.add(workerId, "cell 2 0,alignx left,growy");
		workerId.setColumns(10);
		
		JLabel lblWorkerName = new JLabel("Worker name");
		lblWorkerName.setForeground(Color.WHITE);
		lblWorkerName.setFont(new Font("SansSerif", Font.BOLD, 20));
		desktopPane.add(lblWorkerName, "cell 6 0,alignx left,aligny top");
		
		nameT = new JTextField();
		desktopPane.add(nameT, "cell 10 0,grow");
		nameT.setColumns(10);
		
		lblNewLabel = new JLabel("Search");
		lblNewLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String s=SearchTxt.getText();
				AddWorkerModel.Search(s);
			}
		});
		lblNewLabel.setForeground(Color.GREEN);
		lblNewLabel.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblNewLabel.setIcon(new ImageIcon(AddWorker.class.getResource("/pictureResource/images/search.png")));
		desktopPane.add(lblNewLabel, "cell 15 0");
		
		SearchTxt = new JTextField();
		desktopPane.add(SearchTxt, "cell 19 0 15 1,grow");
		SearchTxt.setColumns(10);
		
		lblWorkerPost = new JLabel("Worker Post");
		lblWorkerPost.setForeground(Color.WHITE);
		lblWorkerPost.setFont(new Font("SansSerif", Font.BOLD, 20));
		desktopPane.add(lblWorkerPost, "cell 0 2,grow");
		
		postCb = new JComboBox();
		postCb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (postCb.getSelectedItem().equals("Select")) {
					JOptionPane.showMessageDialog(null, "Sorry Invaild Selection");
					
				}
			}
		});
		postCb.setModel(new DefaultComboBoxModel(new String[] {"Select", "Manager", "Cooker", "Waiter", "Sweeper"}));
		desktopPane.add(postCb, "cell 2 2,growx");
		
		lblGender = new JLabel("Gender");
		lblGender.setForeground(Color.WHITE);
		lblGender.setFont(new Font("SansSerif", Font.BOLD, 20));
		desktopPane.add(lblGender, "cell 6 2");
		
		gMale = new JRadioButton("Male");
		gMale.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (gMale.isSelected()){
					gFmale.setSelected(false);
				}
			}
		});
		gMale.setFont(new Font("SansSerif", Font.PLAIN, 18));
		gMale.setForeground(Color.WHITE);
		desktopPane.add(gMale, "cell 8 2");
		
		lblBasicSalary = new JLabel("Basic Salary");
		lblBasicSalary.setForeground(Color.WHITE);
		lblBasicSalary.setFont(new Font("SansSerif", Font.BOLD, 20));
		desktopPane.add(lblBasicSalary, "cell 15 2");
		
		basicslary = new JTextField();
		desktopPane.add(basicslary, "cell 19 2 15 1,grow");
		basicslary.setColumns(10);
		
		lblContactNo_1 = new JLabel("Contact No:");
		lblContactNo_1.setForeground(Color.WHITE);
		lblContactNo_1.setFont(new Font("SansSerif", Font.BOLD, 20));
		desktopPane.add(lblContactNo_1, "cell 0 3");
		
		contact_no = new JTextField();
		desktopPane.add(contact_no, "cell 2 3,growx");
		contact_no.setColumns(10);
		
		email = new JLabel("Email");
		email.setForeground(Color.WHITE);
		email.setFont(new Font("SansSerif", Font.BOLD, 20));
		desktopPane.add(email, "cell 6 3");
		
		email_id = new JTextField();
		email_id.setColumns(10);
		desktopPane.add(email_id, "cell 8 3 3 1,growx");
		
		lblAddress_1 = new JLabel("address");
		lblAddress_1.setForeground(Color.WHITE);
		lblAddress_1.setFont(new Font("SansSerif", Font.BOLD, 20));
		desktopPane.add(lblAddress_1, "cell 15 3");
		
		address_txt = new JTextField();
		desktopPane.add(address_txt, "cell 20 3 14 1,growx");
		address_txt.setColumns(10);
		
		btnAddPic = new JButton("Add pic");
		btnAddPic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser file = new JFileChooser();
				file.setCurrentDirectory(new File(System.getProperty("user.home")));
			
				FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images", "jpg", "gif", "png");
				file.addChoosableFileFilter(filter);
				int result = file.showSaveDialog(null);
				
				if (result == JFileChooser.APPROVE_OPTION) {
					File selectedFile = file.getSelectedFile();
					 path = selectedFile.getAbsolutePath();
					pict_txt.setText(path);
					
				}
			

				else if (result == JFileChooser.CANCEL_OPTION) {
					JOptionPane.showMessageDialog(null, "Are you Confirm you don't import pic");
					
				}
			}
				
			
		});
		desktopPane.add(btnAddPic, "cell 0 4");
		
		pict_txt = new JTextField();
		pict_txt.setBorder(null);
		pict_txt.setEnabled(false);
		pict_txt.setBackground(new Color(204, 0, 51));
		desktopPane.add(pict_txt, "cell 2 4,grow");
		pict_txt.setColumns(10);
		
		scrollPane = new JScrollPane();
		desktopPane.add(scrollPane, "cell 0 5 15 5,grow");
		
		WorkerDitails_Tab = new JTable();
		WorkerDitails_Tab.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row = WorkerDitails_Tab.getSelectedRow();

				name = WorkerDitails_Tab.getModel().getValueAt(row, 1).toString();
				AddWorkerModel.getworker(name);
			}
		});
		WorkerDitails_Tab.setBackground(new Color(244, 164, 96));
		WorkerDitails_Tab.setForeground(new Color(0, 0, 0));
		AddWorkerModel.load();
		scrollPane.setViewportView(WorkerDitails_Tab);
		
		btnAdd = new JButton("Add");
		btnAdd.setBackground(Color.PINK);
		btnAdd.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Add_File_20px_1.png"));
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String post = null;
				String workId =workerId.getText();
				String name=nameT.getText();
				String ba=basicslary.getText();
				
				//last add by me
				String contact=contact_no.getText();
				String tikana= address_txt.getText();
				String email=email_id.getText();
				String photo=pict_txt.getText();
				
				if (postCb.getSelectedItem().equals("Select")) {
					JOptionPane.showMessageDialog(null, "Sorry Invaild Selection");
					
				}
				else {
					 post=postCb.getSelectedItem().toString();
				}
				
				String Gengder=null;
				if (gFmale.isSelected()) {
					Gengder=gFmale.getText();
				}
				
				else if (gMale.isSelected()) {
					Gengder=gMale.getText();
				}
				else if (!gFmale.isSelected() ) {
					JOptionPane.showMessageDialog(null, "Select Gender");
				}
				else if (!gFmale.isSelected()) {
					JOptionPane.showMessageDialog(null, "Select Gender");
				}
				new AddworkerCtrl(workId,name,post,Gengder,contact,tikana,email,photo,ba);
				workerId.setText("");
				nameT.setText("");
				if (gMale.isSelected()) {
					gMale.setSelected(false);
				}
				if (gFmale.isSelected()) {
					gFmale.setSelected(false);
					
				}
				
				
				
			}
		});
		
		btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddWorkerModel.load();
			}
		});
		
		panel = new JPanel();
		panel.setBackground(new Color(102, 0, 204));
		desktopPane.add(panel, "cell 15 5 19 5,grow");
		panel.setLayout(null);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("SansSerif", Font.PLAIN, 19));
		lblName.setForeground(Color.GREEN);
		lblName.setBounds(30, 24, 50, 25);
		panel.add(lblName);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setForeground(Color.GREEN);
		lblAddress.setFont(new Font("SansSerif", Font.PLAIN, 19));
		lblAddress.setBounds(30, 87, 72, 25);
		panel.add(lblAddress);
		
		JLabel lblContactNo = new JLabel("Contact No");
		lblContactNo.setForeground(Color.GREEN);
		lblContactNo.setFont(new Font("SansSerif", Font.PLAIN, 19));
		lblContactNo.setBounds(30, 162, 94, 25);
		panel.add(lblContactNo);
		
		JLabel lblEamilId = new JLabel("Eamil Id");
		lblEamilId.setForeground(Color.GREEN);
		lblEamilId.setFont(new Font("SansSerif", Font.PLAIN, 19));
		lblEamilId.setBounds(30, 238, 69, 25);
		panel.add(lblEamilId);
		
		JLabel lblBasicSalary_1 = new JLabel("Basic Salary");
		lblBasicSalary_1.setForeground(Color.GREEN);
		lblBasicSalary_1.setFont(new Font("SansSerif", Font.PLAIN, 19));
		lblBasicSalary_1.setBounds(30, 323, 104, 25);
		panel.add(lblBasicSalary_1);
		
		JLabel lblPost = new JLabel("post");
		lblPost.setForeground(Color.GREEN);
		lblPost.setFont(new Font("SansSerif", Font.PLAIN, 19));
		lblPost.setBounds(30, 432, 104, 25);
		panel.add(lblPost);
		
		 pic_L = new JLabel("picture");
		pic_L.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		pic_L.setForeground(new Color(255, 255, 255));
		pic_L.setHorizontalAlignment(SwingConstants.CENTER);
		pic_L.setBounds(307, 6, 157, 134);
		panel.add(pic_L);
		
		W_name = new JTextField();
		W_name.setEditable(false);
		W_name.setBounds(134, 25, 161, 28);
		panel.add(W_name);
		W_name.setColumns(10);
		
		Address = new JTextField();
		Address.setEditable(false);
		Address.setColumns(10);
		Address.setBounds(134, 88, 161, 28);
		panel.add(Address);
		
		contact = new JTextField();
		contact.setEditable(false);
		contact.setColumns(10);
		contact.setBounds(136, 163, 248, 28);
		panel.add(contact);
		
		emai_id_txt = new JTextField();
		emai_id_txt.setEditable(false);
		emai_id_txt.setColumns(10);
		emai_id_txt.setBounds(136, 239, 248, 28);
		panel.add(emai_id_txt);
		
		B_salary = new JTextField();
		B_salary.setEditable(false);
		B_salary.setColumns(10);
		B_salary.setBounds(134, 324, 250, 28);
		panel.add(B_salary);
		
		post = new JTextField();
		post.setEditable(false);
		post.setColumns(10);
		post.setBounds(134, 433, 250, 28);
		panel.add(post);
		
		gL = new JLabel("Gender");
		gL.setForeground(Color.GREEN);
		gL.setFont(new Font("SansSerif", Font.PLAIN, 19));
		gL.setBounds(30, 377, 104, 25);
		panel.add(gL);
		
		Gen_txt = new JTextField();
		Gen_txt.setEditable(false);
		Gen_txt.setColumns(10);
		Gen_txt.setBounds(134, 378, 250, 28);
		panel.add(Gen_txt);
		desktopPane.add(btnRefresh, "cell 0 10");
		
		btnRemove = new JButton("Remove");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int row = WorkerDitails_Tab.getSelectedRow();

				String id= WorkerDitails_Tab.getModel().getValueAt(row, 0).toString();

				/*String name = WorkerDitails_Tab.getModel().getValueAt(row, 1).toString();

				String post= WorkerDitails_Tab.getModel().getValueAt(row, 2).toString();
				
				String Gender = WorkerDitails_Tab.getModel().getValueAt(row, 3).toString();
				String basic=WorkerDitails_Tab.getModel().getValueAt(row,4 ).toString();*/
				AddWorkerModel.RemoveWorker(id);
				AddWorkerModel.load();
				
				
			}
		});
		btnRemove.setBackground(Color.PINK);
		btnRemove.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Trash_Can_20px.png"));
		desktopPane.add(btnRemove, "cell 31 10");
		desktopPane.add(btnAdd, "cell 33 10");
		
		gFmale = new JRadioButton("Female");
		gFmale.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (gFmale.isSelected()){
					gMale.setSelected(false);
				}
				
			}
		});
		gFmale.setFont(new Font("SansSerif", Font.PLAIN, 18));
		gFmale.setForeground(Color.WHITE);
		desktopPane.add(gFmale, "cell 10 2");

	}
}
