package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;
import net.miginfocom.swing.MigLayout;
import viwe.Dialouge.AddNewFoodName;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.text.ParseException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;

import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import controller.AddFoodDinner_ctrl;
import controller.AddFoodEveningSnacks_Ctrl;
import controller.AddFoodLunch_ctrl;
import model.AddFoodBreakfast_Model;
import model.AddFoodDinner;
import model.AddFoodLuch_Model;
import model.AddfoodEvening_Snacks_model;
import model.TakeOrder;
import model.WorkerDitailsModel;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JButton;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.ImageIcon;
import javax.swing.border.MatteBorder;
import javax.swing.SwingConstants;


public class AddFood extends JInternalFrame {

	public static JPanel contentPane;
	public static JDesktopPane desktopPane ;
	public static JLabel label;
	public static JLabel label_1;
	public static JLabel label_2;
	public static JLabel label_3;
	public static JComboBox cat_cb;
	public static JComboBox FoodnameCb;
	public static JTextField price_txt;
	public static JComboBox st_cb;
	public static JScrollPane scrollPane;
	public static JScrollPane scrollPane_1;
	public static JScrollPane scrollPane_2;
	public static JScrollPane scrollPane_3;
	public static JTable BreakfastTab;
	public static JTable lunch_tab;
	public static JTable even_Tab;
	public static JTable dinner_tab;
	public static JButton btnNewButton_1;
	public String status;
	private JLabel lblAddAmountOf;
	private JTextField amount_txt;
	private JLabel lblInseartPicture;
	private JLabel PicL ;
	public static String path;
	private JButton btnAddNew;
	
	public AddFood() {
		try {
			setSelected(true);
		} catch (PropertyVetoException e1) {
			
			e1.printStackTrace();
		}
		
		setFrameIcon(new ImageIcon(AddFood.class.getResource("/pictureResource/images/wordpad.png")));
		
		setBackground(new Color(0, 100, 0));
		setTitle("Add Foods");
		setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
		setLocation(new Point(220, 0));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		setDoubleBuffered(true);
		setMaximizable(true);
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		
		
		
	
		
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		
		setSize(1191, 753);
		getContentPane().setLayout(new MigLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				image=(Toolkit.getDefaultToolkit().getImage(AddFood.class.getResource("/pictureResource/BA3.png")));
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		
		desktopPane.setFocusable(false);
		desktopPane.setFocusTraversalKeysEnabled(false);
		desktopPane.setFocusCycleRoot(false);
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(null);
		
		label = new JLabel("Food caegory:");
		label.setBounds(19, 63, 126, 24);
		label.setForeground(new Color(245, 255, 250));
		label.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
		label.setBackground(new Color(0, 0, 128));
		desktopPane.add(label);
		ArrayList<String >beakfast=new ArrayList<>();
		beakfast.add("Black Tea");
		beakfast.add("Milk Tea");
		beakfast.add("Lemon Tea");
		beakfast.add("Milk Coffe");
		beakfast.add("Black Coffe");
		beakfast.add("Vegitable Bargar");
		beakfast.add("Chaekn Bargar Bargar");
		beakfast.add("Sendwich");
		beakfast.add("Lemon Juice");
		//Dinner Item List
		ArrayList<String >Dinner=new ArrayList<>();
		Dinner.add("White Rice");
		Dinner.add("Black Rice");
		Dinner.add("Brown Rice");
		Dinner.add("Biriany");
		Dinner.add("Fish Fry");
		Dinner.add("Fish Sup");
		Dinner.add("Meat");
		Dinner.add("Maton");
		Dinner.add("Cheaken Cari");
		Dinner.add("Cheaken Sup");
		Dinner.add("Cheaken Rose");
		Dinner.add("Cheaken Dosa");
		Dinner.add("Cheaken Cari");
		Dinner.add("Baji");
		Dinner.add("Chhenagaja");
		Dinner.add("Chingri Malai curry");
		Dinner.add("Dal");
		Dinner.add("Shobji");
		///Snakcs ItemList
		ArrayList<String >Snacks=new ArrayList<>();
		Snacks.add("Tea & Coffee");
		Snacks.add("");
		Snacks.add("Black Tea");
		Snacks.add("Milk Tea");
		Snacks.add("Lemon Tea");
		Snacks.add("Milk Coffe");
		Snacks.add("Black Coffe");
		Snacks.add("Bangladeshi Roti");
		Snacks.add("Bread with Jelli");
		Snacks.add("Protha with Egg");
		Snacks.add("Protha with vegitable");
		Snacks.add("Sandwich");
		Snacks.add("Pizza");
		Snacks.add("Shobji");
		Snacks.add("Lemon Juice");
		Snacks.add("Mango Juice");
		Snacks.add("Apple Juice");
		Snacks.add("Papaya Juice");
		Snacks.add("Painapple Juice");
		ArrayList<String >Lunch=new ArrayList<>();
		Lunch.add("Black Rice");
		Lunch.add("White Rice");
		Lunch.add("Biriany");
		Lunch.add("Kacci Biriany");
		Lunch.add("Cheaken Biriany");
		Lunch.add("Vegitable Biriany");
		Lunch.add("Rui Fish");
		Lunch.add("Ilish Fish");
		Lunch.add("Meat");
		Lunch.add("Beef");
		Lunch.add("Khababab");
		
		cat_cb = new JComboBox();
		cat_cb.setBounds(199, 56, 205, 32);
		cat_cb.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (cat_cb.getSelectedItem().equals("Dinner")) {
					 FoodnameCb.setModel(new DefaultComboBoxModel<>(Dinner.toArray()));
				}
				if (cat_cb.getSelectedItem().equals("Breakfast")) {
					
					FoodnameCb.setModel(new DefaultComboBoxModel<>(beakfast.toArray()));
				}
				if (cat_cb.getSelectedItem().equals("Evening Snacks")) {
					FoodnameCb.setModel(new DefaultComboBoxModel<>(Snacks.toArray()));
				}
				if (cat_cb.getSelectedItem().equals("Lunch")) {
					FoodnameCb.setModel(new DefaultComboBoxModel<>(Lunch.toArray()));
				}	
			}
		});
		cat_cb.setModel(new DefaultComboBoxModel(new String[] {"Select Category", "Breakfast", "Evening Snacks", "Lunch", "Dinner"}));
		cat_cb.setFont(new Font("Tahoma", Font.ITALIC, 18));
		desktopPane.add(cat_cb);
		
		scrollPane_3 = new JScrollPane();
		scrollPane_3.setFont(new Font("SansSerif", Font.PLAIN, 15));
		scrollPane_3.setBounds(479, 7, 569, 150);
		scrollPane_3.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Breakfast", TitledBorder.CENTER, TitledBorder.TOP, null, Color.WHITE));
		desktopPane.add(scrollPane_3);
		
		BreakfastTab = new JTable();
		BreakfastTab.setFont(new Font("SansSerif", Font.PLAIN, 15));
		AddFoodBreakfast_Model.Load();
		scrollPane_3.setViewportView(BreakfastTab);
		
		label_1 = new JLabel("Food Name::");
		label_1.setBounds(19, 135, 106, 24);
		label_1.setForeground(new Color(240, 255, 240));
		label_1.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
		label_1.setBackground(new Color(0, 0, 128));
		desktopPane.add(label_1);
		
		FoodnameCb = new JComboBox();
		FoodnameCb.setBounds(199, 126, 205, 35);
		FoodnameCb.setFont(new Font("Tahoma", Font.PLAIN, 20));
		desktopPane.add(FoodnameCb);
		
		scrollPane_2 = new JScrollPane();
		scrollPane_2.setFont(new Font("SansSerif", Font.PLAIN, 12));
		scrollPane_2.setBounds(479, 157, 573, 150);
		scrollPane_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Lunch", TitledBorder.CENTER, TitledBorder.TOP, null, Color.WHITE));
		desktopPane.add(scrollPane_2);
		
		lunch_tab = new JTable();
		lunch_tab.setFont(new Font("SansSerif", Font.PLAIN, 15));
		AddFoodLuch_Model.Load();
		scrollPane_2.setViewportView(lunch_tab);
		
		label_2 = new JLabel("Food Price::");
		label_2.setBounds(19, 193, 102, 24);
		label_2.setForeground(new Color(245, 255, 250));
		label_2.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
		label_2.setBackground(new Color(0, 0, 128));
		desktopPane.add(label_2);
		
		price_txt = new JTextField();
		price_txt.setBounds(199, 185, 205, 34);
		price_txt.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		price_txt.setColumns(10);
		desktopPane.add(price_txt);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(479, 312, 573, 150);
		scrollPane_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Evening Snacks", TitledBorder.CENTER, TitledBorder.TOP, null, Color.WHITE));
		desktopPane.add(scrollPane_1);
		
		even_Tab = new JTable();
		even_Tab.setFont(new Font("SansSerif", Font.PLAIN, 15));
		AddfoodEvening_Snacks_model.Load();
		scrollPane_1.setViewportView(even_Tab);
		
		lblAddAmountOf = new JLabel("Add Amount of Food::");
		lblAddAmountOf.setBounds(7, 264, 188, 24);
		lblAddAmountOf.setForeground(new Color(245, 255, 250));
		lblAddAmountOf.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
		lblAddAmountOf.setBackground(new Color(0, 0, 128));
		desktopPane.add(lblAddAmountOf);
		
		amount_txt = new JTextField();
		amount_txt.setBounds(199, 256, 205, 34);
		amount_txt.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		amount_txt.setColumns(10);
		desktopPane.add(amount_txt);
		
		label_3 = new JLabel("Status");
		label_3.setBounds(7, 327, 51, 24);
		label_3.setForeground(new Color(245, 255, 250));
		label_3.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
		label_3.setBackground(new Color(0, 0, 128));
		desktopPane.add(label_3);
		
		st_cb = new JComboBox();
		st_cb.setBounds(199, 320, 205, 32);
		st_cb.setModel(new DefaultComboBoxModel(new String[] {"Select One", "Available", "Unavaiable"}));
		st_cb.setFont(new Font("Tahoma", Font.ITALIC, 18));
		desktopPane.add(st_cb);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(479, 468, 573, 169);
		scrollPane.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Dinner", TitledBorder.CENTER, TitledBorder.TOP, null, Color.WHITE));
		desktopPane.add(scrollPane);
		
		dinner_tab = new JTable();
		dinner_tab.setFont(new Font("SansSerif", Font.PLAIN, 15));
		AddFoodDinner.Load();
		scrollPane.setViewportView(dinner_tab);
		
	
		
		PicL = new JLabel("");
		PicL.setBorder(new MatteBorder(3, 3, 3, 3, (Color) new Color(255, 255, 255)));
		PicL.setBounds(199, 383, 194, 140);
		desktopPane.add(PicL);
		
		lblInseartPicture = new JLabel("Inseart Picture");
		lblInseartPicture.setForeground(new Color(245, 255, 250));
		lblInseartPicture.setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
		lblInseartPicture.setBackground(new Color(0, 0, 128));
		lblInseartPicture.setBounds(7, 468, 138, 24);
		desktopPane.add(lblInseartPicture);
		
		JButton btnInseart = new JButton("Inseart......");
		btnInseart.setHorizontalAlignment(SwingConstants.LEADING);
		btnInseart.setIcon(new ImageIcon(AddFood.class.getResource("/pictureResource/inseart.png")));
		btnInseart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnInseart.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {

						JFileChooser file = new JFileChooser();
						file.setCurrentDirectory(new File(System.getProperty("user.home")));
					
						FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images", "jpg", "gif", "png");
						file.addChoosableFileFilter(filter);
						int result = file.showSaveDialog(null);
						
						if (result == JFileChooser.APPROVE_OPTION) {
							File selectedFile = file.getSelectedFile();
							 path = selectedFile.getAbsolutePath();
							
							PicL.setIcon(ResizeImage(path));
						}
					

						else if (result == JFileChooser.CANCEL_OPTION) {
							JOptionPane.showMessageDialog(null, "Are you Confirm you don't import pic");
							
						}
					}
				});
				
			}
		});
		btnInseart.setBounds(318, 554, 90, 28);
		desktopPane.add(btnInseart);
		btnNewButton_1 = new JButton("Add Food");
		btnNewButton_1.setIcon(new ImageIcon(AddFood.class.getResource("/pictureResource/images/edit.png")));
		btnNewButton_1.setBounds(299, 609, 100, 36);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String food=FoodnameCb.getSelectedItem().toString();
				
				String price=price_txt.getText();
				
				String category=cat_cb.getSelectedItem().toString();
				status=st_cb.getSelectedItem().toString();
				if (cat_cb.getSelectedItem().equals("Breakfast")) {
					food=FoodnameCb.getSelectedItem().toString();
					
					 price=price_txt.getText();
					
					 category=cat_cb.getSelectedItem().toString();
					 String a=amount_txt.getText();
					
					new controller.AddFoodBreakfast_Ctrl(food, price, category, a,status,path);
					
					TakeOrder.breakFastDataLoad();
			}
				if (cat_cb.getSelectedItem().equals("Evening Snacks")) {
					
					food=FoodnameCb.getSelectedItem().toString();
					 price=price_txt.getText();
					 category=cat_cb.getSelectedItem().toString();
					 String a=amount_txt.getText();
					 new AddFoodEveningSnacks_Ctrl(food, price,category,a, status,path);
					 AddfoodEvening_Snacks_model.Load();
					
				}
				
			if (cat_cb.getSelectedItem().equals("Lunch")) {
				 food=FoodnameCb.getSelectedItem().toString();
				 price=price_txt.getText();
				 category=cat_cb.getSelectedItem().toString();
				 String a=amount_txt.getText();
				new AddFoodLunch_ctrl(food, price, category,a, status,path);
				AddFoodLuch_Model.Load();
				
			}
			if (cat_cb.getSelectedItem().equals("Dinner")) {
				 food=FoodnameCb.getSelectedItem().toString();
				 price=price_txt.getText();
				 category=cat_cb.getSelectedItem().toString();
				 String a=amount_txt.getText();
				new AddFoodDinner_ctrl(food, price, category,a, status,path);
				AddFoodDinner.Load();
			}}
				
			
		});
		desktopPane.add(btnNewButton_1);
		
		btnAddNew = new JButton("Add new");
		btnAddNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddNewFoodName add=new AddNewFoodName();
				add.setVisible(true);
			}
		});
		btnAddNew.setIcon(new ImageIcon(AddFood.class.getResource("/pictureResource/images/add.gif")));
		btnAddNew.setBounds(197, 609, 103, 36);
		desktopPane.add(btnAddNew);
		
		JButton btnNewButton = new JButton("Delete");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int row = BreakfastTab.getSelectedRow();

				String id = BreakfastTab.getModel().getValueAt(row, 0).toString();
				AddFoodBreakfast_Model.Dlete(id);
				AddFoodBreakfast_Model.Load();
			}
		});
		btnNewButton.setBounds(1060, 114, 90, 28);
		desktopPane.add(btnNewButton);
		
		JButton button = new JButton("Delete");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int row = lunch_tab.getSelectedRow();

				String id = lunch_tab.getModel().getValueAt(row, 0).toString();
				AddFoodLuch_Model.Dlete(id);
				AddFoodLuch_Model.Load();
			}
		});
		button.setBounds(1060, 261, 90, 28);
		desktopPane.add(button);
		
		JButton button_1 = new JButton("Delete");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int row = even_Tab.getSelectedRow();

				String id = even_Tab.getModel().getValueAt(row, 0).toString();
				AddfoodEvening_Snacks_model.Dlete(id);
			AddfoodEvening_Snacks_model.Load();
			}
		});
		button_1.setBounds(1064, 421, 90, 28);
		desktopPane.add(button_1);
		
		JButton button_2 = new JButton("Delete");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int row = dinner_tab.getSelectedRow();

				String id = dinner_tab.getModel().getValueAt(row, 0).toString();
				AddFoodDinner.Dlete(id);
				AddFoodDinner.Load();
			}
		});
		button_2.setBounds(1064, 598, 90, 28);
		desktopPane.add(button_2);
		
	}
	public ImageIcon ResizeImage(String ImagePath) {
		ImageIcon MyImage = new ImageIcon(ImagePath);
		
		Image img = MyImage.getImage();
		Image newImg = img.getScaledInstance(PicL.getWidth(), PicL.getHeight(), Image.SCALE_REPLICATE);
		ImageIcon image = new ImageIcon(newImg);
	
		return image;
	}
}
