package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;
import net.miginfocom.swing.MigLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.text.ParseException;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;


public class About extends JInternalFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane ;
	private JLabel label_1;
	private JLabel lblDevelopedBy;
	private JLabel lblNkChakraborty;
	private JLabel lblStudentOfBangaldesh;
	private JLabel lblComputerScience;
	private JLabel lblResturantManagementSoftware;
	private JLabel lblAllRight;
	private JLabel lblNewLabel;
	private JLabel lblNipuchakrabortyhotmailcom;
	private JLabel lblFacebook;
	private JLabel lblTwitter;
	private JLabel lblNkchakrabortytwittercom;
	private JLabel label_2;
	private JLabel lblNewLabel_1;
	
	
	public About() {
		setResizable(true);
		setFrameIcon(new ImageIcon(About.class.getResource("/pictureResource/images/Help.gif")));
		setBackground(new Color(0, 100, 0));
		setTitle("Parchase");
		setLocation(new Point(220, 0));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		setDoubleBuffered(true);
		setMaximizable(true);
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		
	
		
		setTitle("About");
		
	
		
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		
		setSize(1200, 765);
		getContentPane().setLayout(new MigLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				image=(Toolkit.getDefaultToolkit().getImage(About.class.getResource("/pictureResource/BA4.png")));
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		
		desktopPane.setFocusable(false);
		desktopPane.setFocusTraversalKeysEnabled(false);
		desktopPane.setFocusCycleRoot(false);
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(null);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(About.class.getResource("/pictureResource/nk.jpg")));
		label.setForeground(new Color(240, 255, 240));
		label.setFont(new Font("Tahoma", Font.BOLD, 18));
		label.setBackground(Color.LIGHT_GRAY);
		label.setBounds(526, 23, 113, 142);
		desktopPane.add(label);
		
		label_1 = new JLabel("Version 1.1.0");
		label_1.setForeground(Color.WHITE);
		label_1.setFont(new Font("Segoe UI Symbol", Font.PLAIN, 18));
		label_1.setBounds(12, 13, 102, 25);
		desktopPane.add(label_1);
		
		lblDevelopedBy = new JLabel("Developed by");
		lblDevelopedBy.setHorizontalAlignment(SwingConstants.CENTER);
		lblDevelopedBy.setForeground(new Color(128, 0, 128));
		lblDevelopedBy.setFont(new Font("Segoe UI Symbol", Font.PLAIN, 26));
		lblDevelopedBy.setBounds(506, 165, 158, 36);
		desktopPane.add(lblDevelopedBy);
		
		lblNkChakraborty = new JLabel("Nk Chakraborty");
		lblNkChakraborty.setHorizontalAlignment(SwingConstants.CENTER);
		lblNkChakraborty.setForeground(new Color(128, 0, 128));
		lblNkChakraborty.setFont(new Font("Segoe UI Symbol", Font.PLAIN, 18));
		lblNkChakraborty.setBounds(526, 198, 124, 25);
		desktopPane.add(lblNkChakraborty);
		
		lblStudentOfBangaldesh = new JLabel("Student Of Bangaldesh Sweden Polytechnic Institute ,Kaptai");
		lblStudentOfBangaldesh.setHorizontalAlignment(SwingConstants.CENTER);
		lblStudentOfBangaldesh.setForeground(new Color(128, 0, 128));
		lblStudentOfBangaldesh.setFont(new Font("Segoe UI Symbol", Font.PLAIN, 18));
		lblStudentOfBangaldesh.setBounds(356, 225, 471, 25);
		desktopPane.add(lblStudentOfBangaldesh);
		
		lblComputerScience = new JLabel("Computer Science & Technology");
		lblComputerScience.setHorizontalAlignment(SwingConstants.CENTER);
		lblComputerScience.setForeground(new Color(128, 0, 128));
		lblComputerScience.setFont(new Font("Segoe UI Symbol", Font.PLAIN, 18));
		lblComputerScience.setBounds(458, 254, 259, 25);
		desktopPane.add(lblComputerScience);
		
		lblResturantManagementSoftware = new JLabel("Resturant management Software .Specially  Designed & Devloped For All Resturant ");
		lblResturantManagementSoftware.setHorizontalAlignment(SwingConstants.CENTER);
		lblResturantManagementSoftware.setForeground(new Color(128, 0, 128));
		lblResturantManagementSoftware.setFont(new Font("Segoe UI Symbol", Font.PLAIN, 18));
		lblResturantManagementSoftware.setBounds(244, 281, 684, 25);
		desktopPane.add(lblResturantManagementSoftware);
		
		lblAllRight = new JLabel("2017-2018 All Right Reserved");
		lblAllRight.setHorizontalAlignment(SwingConstants.CENTER);
		lblAllRight.setForeground(Color.WHITE);
		lblAllRight.setFont(new Font("Segoe UI Symbol", Font.PLAIN, 18));
		lblAllRight.setBounds(12, 605, 253, 25);
		desktopPane.add(lblAllRight);
		
		lblNewLabel = new JLabel("Mail");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Message_50px.png"));
		lblNewLabel.setBounds(221, 508, 90, 50);
		desktopPane.add(lblNewLabel);
		
		lblNipuchakrabortyhotmailcom = new JLabel("http://www.facebook.com/Nkchakraborty");
		lblNipuchakrabortyhotmailcom.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNipuchakrabortyhotmailcom.setForeground(new Color(128, 0, 128));
		lblNipuchakrabortyhotmailcom.setBounds(244, 388, 325, 22);
		desktopPane.add(lblNipuchakrabortyhotmailcom);
		
		lblFacebook = new JLabel("Facebook");
		lblFacebook.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblFacebook.setForeground(Color.WHITE);
		lblFacebook.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Facebook_48px.png"));
		lblFacebook.setBounds(96, 374, 136, 48);
		desktopPane.add(lblFacebook);
		
		lblTwitter = new JLabel("Twitter");
		lblTwitter.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblTwitter.setForeground(Color.WHITE);
		lblTwitter.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Twitter_64px.png"));
		lblTwitter.setBounds(133, 437, 132, 64);
		desktopPane.add(lblTwitter);
		
		lblNkchakrabortytwittercom = new JLabel("http://www.Twitter.com/Nkchakraborty");
		lblNkchakrabortytwittercom.setForeground(new Color(128, 0, 128));
		lblNkchakrabortytwittercom.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNkchakrabortytwittercom.setBounds(277, 447, 309, 22);
		desktopPane.add(lblNkchakrabortytwittercom);
		
		label_2 = new JLabel("Nipuchakraborty86@gmail.com");
		label_2.setForeground(new Color(128, 0, 128));
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label_2.setBounds(327, 514, 302, 32);
		desktopPane.add(label_2);
		
		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\Rest.png"));
		lblNewLabel_1.setBounds(788, 363, 341, 286);
		desktopPane.add(lblNewLabel_1);
		
	}
}
