package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;
import net.miginfocom.swing.MigLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import controller.SalarySheetCtrl;
import controller.WorkerDitails_ctrl;
import model.SalarySheetModel;
import model.WorkerDitailsModel;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import org.jdesktop.swingx.JXDatePicker;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;


public class SalarySheet extends JInternalFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane ;
	public static  JTextField Salary_amount;
	public static  JTextField Paid_Salary;
	public static  JTextField due;
	public static  JTextField search_txt;
	public static  JTextField id_txt;
	public static  JTextField WorkerName_txt;
	public static  JTextField posiotn;
	public static  JTextField genderTxt;
	public static  JTable SalarySheetTab;
	public static  JTextField Bonus_txt;
	public static  JTextField Total_paindTxt;
	
	
	public static JXDatePicker datePicker;
	public java.util.Date date;
	public java.sql.Date sqldate;
	
	
	public SalarySheet() {
		setFrameIcon(new ImageIcon(SalarySheet.class.getResource("/pictureResource/images/icons8_User_Menu_Male_50px.png")));
		setBackground(new Color(0, 100, 0));
		setTitle("Parchase");
		setLocation(new Point(220, 0));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		setDoubleBuffered(true);
		setMaximizable(true);
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		
		setName("ResFram");
		
		setTitle("Salary Sheet");
		
	
		
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		
		setSize(1191, 753);
		getContentPane().setLayout(new MigLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				image=(Toolkit.getDefaultToolkit().getImage(SalarySheet.class.getResource("/pictureResource/BA3.png")));
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		
		desktopPane.setFocusable(false);
		desktopPane.setFocusTraversalKeysEnabled(false);
		desktopPane.setFocusCycleRoot(false);
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(new MigLayout("", "[108.00][grow][][][][][grow][][][][][][][][][][grow][grow][][][grow][][][][][][][][][grow]", "[][][][][][][][][][][grow][]"));
		
		JLabel searchL = new JLabel("Search");
		searchL.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String getd = search_txt.getText();
				SalarySheetModel.search(getd);
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				searchL.setForeground(Color.GREEN);
				searchL.setFont(new Font("SansSerif", Font.BOLD, 12));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				searchL.setForeground(Color.WHITE);
				searchL.setFont(new Font("SansSerif", Font.ITALIC, 12));
			}
			
		});
		
		JLabel lblSalarySheet = new JLabel("Salary Sheet");
		lblSalarySheet.setFont(new Font("SansSerif", Font.PLAIN, 30));
		lblSalarySheet.setForeground(Color.WHITE);
		lblSalarySheet.setHorizontalAlignment(SwingConstants.CENTER);
		desktopPane.add(lblSalarySheet, "cell 0 0,alignx center");
		searchL.setIcon(new ImageIcon(SalarySheet.class.getResource("/pictureResource/images/search_hover.png")));
		searchL.setForeground(Color.WHITE);
		searchL.setFont(new Font("SansSerif", Font.ITALIC, 12));
		desktopPane.add(searchL, "cell 26 1");
		
		search_txt = new JTextField();
		
		search_txt.setColumns(10);
		desktopPane.add(search_txt, "cell 29 1,growx");
		
		JLabel label = new JLabel("Worker Id");
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Tahoma", Font.PLAIN, 18));
		desktopPane.add(label, "cell 0 2");
		
		id_txt = new JTextField();
		id_txt.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				String id=id_txt.getText();
				SalarySheetModel.NameOfWorker(id);
				
			}
		});
		id_txt.setColumns(10);
		desktopPane.add(id_txt, "cell 1 2 6 1,growx");
		
		JLabel label_2 = new JLabel("Date");
		label_2.setForeground(Color.WHITE);
		label_2.setFont(new Font("Tahoma", Font.ITALIC, 18));
		desktopPane.add(label_2, "cell 10 2");
		
		 datePicker = new JXDatePicker();
		 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date date5 = new Date(System.currentTimeMillis());
			datePicker.setDate(date5);
		desktopPane.add(datePicker, "cell 11 2 6 1");
		
		JLabel lblGender = new JLabel("Gender");
		lblGender.setFont(new Font("SansSerif", Font.BOLD, 16));
		lblGender.setForeground(Color.WHITE);
		desktopPane.add(lblGender, "cell 17 2");
		
		genderTxt = new JTextField();
		desktopPane.add(genderTxt, "cell 20 2,growx");
		genderTxt.setColumns(10);
		
		JLabel bonus_1 = new JLabel("Bonus");
		bonus_1.setForeground(Color.WHITE);
		bonus_1.setFont(new Font("SansSerif", Font.BOLD, 16));
		desktopPane.add(bonus_1, "cell 23 2");
		
		Bonus_txt = new JTextField();
		Bonus_txt.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				String b=Bonus_txt.getText();
				int b2=Integer.parseInt(b);
				String To=Total_paindTxt.getText();
				int total=Integer.parseInt(To);
				int totalAmount=b2+total;
				String TotalPAind=Integer.toString(totalAmount);
				Total_paindTxt.setText(TotalPAind);
			}
		});
		desktopPane.add(Bonus_txt, "cell 25 2,growx");
		Bonus_txt.setColumns(10);
		
		JLabel lblBasicSalary = new JLabel("Basic Salary");
		lblBasicSalary.setFont(new Font("SansSerif", Font.BOLD, 12));
		lblBasicSalary.setForeground(Color.WHITE);
		desktopPane.add(lblBasicSalary, "cell 26 2");
		
		Salary_amount = new JTextField();
		Salary_amount.setColumns(10);
		desktopPane.add(Salary_amount, "cell 29 2,growx");
		
		JLabel label_5 = new JLabel("Paid Salary");
		label_5.setFont(new Font("SansSerif", Font.BOLD, 12));
		label_5.setForeground(Color.WHITE);
		desktopPane.add(label_5, "cell 26 3");
		
		Paid_Salary = new JTextField();
		Paid_Salary.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				String s=Salary_amount.getText();
				int basic=Integer.parseInt(s);
				String pa=Paid_Salary.getText();
				int p=Integer.parseInt(pa);
				int due=basic-p;
				String du=Integer.toString(due);
				SalarySheet.due.setText(du);
				Total_paindTxt.setText(pa);
			}
		});
		Paid_Salary.setColumns(10);
		desktopPane.add(Paid_Salary, "cell 29 3,growx");
		
		JLabel label_1 = new JLabel("Name");
		label_1.setForeground(Color.WHITE);
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		desktopPane.add(label_1, "cell 0 4");
		
		WorkerName_txt = new JTextField();
		WorkerName_txt.setColumns(10);
		desktopPane.add(WorkerName_txt, "cell 1 4 6 1,growx");
		
		JLabel label_3 = new JLabel("Post");
		label_3.setForeground(Color.WHITE);
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		desktopPane.add(label_3, "cell 10 4");
		
		posiotn = new JTextField();
		posiotn.setColumns(10);
		desktopPane.add(posiotn, "cell 11 4 6 1,growx");
		
		JLabel lblTatalPaid = new JLabel("Tatal paid");
		lblTatalPaid.setForeground(Color.WHITE);
		lblTatalPaid.setFont(new Font("SansSerif", Font.BOLD, 16));
		desktopPane.add(lblTatalPaid, "cell 23 4");
		
		Total_paindTxt = new JTextField();
		desktopPane.add(Total_paindTxt, "cell 25 4,growx");
		Total_paindTxt.setColumns(10);
		
		JLabel label_6 = new JLabel("Due");
		label_6.setFont(new Font("SansSerif", Font.BOLD, 12));
		label_6.setForeground(Color.WHITE);
		desktopPane.add(label_6, "cell 26 4");
		
		due = new JTextField();
		due.setColumns(10);
		desktopPane.add(due, "cell 29 4,growx");
		
		JScrollPane scrollPane = new JScrollPane();
		desktopPane.add(scrollPane, "cell 0 5 30 6,grow");
		
		SalarySheetTab = new JTable();
		SalarySheetModel.load();
		scrollPane.setViewportView(SalarySheetTab);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String id = id_txt.getText();
				
				String name = WorkerName_txt.getText();
				String g =genderTxt.getText();
				String poS=posiotn.getText();
				
				String BasicS=Salary_amount.getText();
				
				String paid=Paid_Salary.getText();
				
				String Due=due.getText();
				
				String total=Total_paindTxt.getText();
				
				String bonus=Bonus_txt.getText();
				
				date=datePicker.getDate();
				sqldate=new java.sql.Date(date.getTime());
			
				SalarySheetCtrl salar=new SalarySheetCtrl(id, name, g, poS, BasicS, paid, Due, total, bonus,sqldate);
				
				
			}
		});
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			int row=SalarySheetTab.getSelectedRow();
			String id= SalarySheetTab.getModel().getValueAt(row, 0).toString();
			SalarySheetModel.Dlete(id);
			SalarySheetModel.load();
			}
		});
		desktopPane.add(btnDelete, "cell 28 11");
		desktopPane.add(btnSave, "cell 29 11,alignx right");
		
	}
}
