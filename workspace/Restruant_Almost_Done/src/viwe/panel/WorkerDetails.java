package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;
import net.miginfocom.swing.MigLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JLabel;
import java.awt.Font;
import viwe.TextPrompt;
import viwe.Dialouge.EditWorkerDitails;

import javax.swing.text.JTextComponent;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;

import controller.EditWorkerCtrl;
import controller.WorkerDitails_ctrl;
import model.WorkerDitailsModel;

import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.border.MatteBorder;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;


public class WorkerDetails extends JInternalFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane ;
	public static JTextField WorkerName_txt;
	public static  JTextField posiotn;
	public static  JTextField id_txt;
	public static JTextField search_txt;
	public static  JTable presentList;
	public static  JTextField g;
	public static  Date sqlDate;
	public static java.util.Date date;
	public static JCheckBox present_ch;
	public WorkerDetails() {
		setResizable(true);
		setBackground(new Color(0, 100, 0));
		
		setLocation(new Point(220, 0));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		setDoubleBuffered(true);
		setMaximizable(true);
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		
		setName("ResFram");
		
		setTitle("Absent or Present List");
		
	
		
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		
		setSize(1191, 753);
		getContentPane().setLayout(new MigLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				image=(Toolkit.getDefaultToolkit().getImage(WorkerDetails.class.getResource("/pictureResource/BA4.png")));
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		
		desktopPane.setFocusable(false);
		desktopPane.setFocusTraversalKeysEnabled(false);
		desktopPane.setFocusCycleRoot(false);
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(new MigLayout("", "[157.00px][152.00px][158.00px][189.00px][54px][127px,grow][127px][73px][97px][][]", "[39px][11px][13px][-27.00px][4px][-11.00][][-7.00][-17.00px][12.00px][][][][][][grow][]"));
		
		JLabel label = new JLabel("Worker Id");
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Tahoma", Font.ITALIC, 18));
		desktopPane.add(label, "cell 0 2 1 7,growx,aligny top");
		
		search_txt = new JTextField();
		TextPrompt textPrompt2 = new TextPrompt("Enter Id", search_txt);
		search_txt.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				//za = search_txt.getText();
				
				//WorkerDitailsModel.search(za);
			}
		});
		
		JLabel searchL = new JLabel("Search");
		searchL.setForeground(Color.WHITE);
		searchL.setFont(new Font("SansSerif", Font.ITALIC, 12));
		searchL.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String getd = search_txt.getText();
				WorkerDitailsModel.search(getd);
			

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				searchL.setForeground(Color.GREEN);
				searchL.setFont(new Font("SansSerif", Font.BOLD, 12));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				searchL.setForeground(Color.WHITE);
				searchL.setFont(new Font("SansSerif", Font.ITALIC, 12));
			}
		});
		
		JLabel lblGender = new JLabel("Gender");
		lblGender.setHorizontalAlignment(SwingConstants.CENTER);
		lblGender.setForeground(Color.WHITE);
		lblGender.setFont(new Font("Tahoma", Font.PLAIN, 18));
		desktopPane.add(lblGender, "cell 5 2,alignx center");
		searchL.setIcon(new ImageIcon(WorkerDetails.class.getResource("/pictureResource/images/search.png")));
		searchL.setForeground(Color.WHITE);
		searchL.setFont(new Font("SansSerif", Font.ITALIC, 12));
		desktopPane.add(searchL, "cell 9 2,growx");
		desktopPane.add(search_txt, "cell 10 2,growx,aligny top");
		search_txt.setColumns(10);
		
		g = new JTextField();
		g.setEditable(false);
		desktopPane.add(g, "cell 5 6,growx");
		g.setColumns(10);
		
		JLabel label_1 = new JLabel("Name");
		label_1.setForeground(Color.WHITE);
		label_1.setFont(new Font("Tahoma", Font.ITALIC, 18));
		desktopPane.add(label_1, "cell 0 9,alignx left,aligny top");
		
		WorkerName_txt = new JTextField();
		WorkerName_txt.setEditable(false);
		WorkerName_txt.setColumns(10);
		desktopPane.add(WorkerName_txt, "cell 1 9,growx,aligny top");
		
		JLabel label_2 = new JLabel("Date");
		label_2.setForeground(Color.WHITE);
		label_2.setFont(new Font("Tahoma", Font.ITALIC, 18));
		desktopPane.add(label_2, "cell 2 2 1 7,growx,aligny top");
		
		JLabel label_3 = new JLabel("Post");
		label_3.setForeground(Color.WHITE);
		label_3.setFont(new Font("Tahoma", Font.ITALIC, 18));
		desktopPane.add(label_3, "cell 2 9,growx,aligny top");
		
		JDateChooser dateChooser = new JDateChooser();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		java.util.Date date2 = new Date(System.currentTimeMillis());
		dateChooser.setDate(date2);
		desktopPane.add(dateChooser, "cell 3 2 1 7,growx,aligny top");
		
		posiotn = new JTextField();
		posiotn.setEditable(false);
		posiotn.setColumns(10);
		desktopPane.add(posiotn, "cell 3 9,growx,aligny top");
		
		id_txt = new JTextField();
		id_txt.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				String id=id_txt.getText();
				WorkerDitailsModel.NameOfWorker(id);
			}
		});
		
		
		desktopPane.add(id_txt, "cell 1 2,growx,aligny top");
		id_txt.setColumns(10);
		
		JCheckBox absent_ch = new JCheckBox("Absent");
		absent_ch.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (absent_ch.isSelected()){
					present_ch.setSelected(false);
				}
			}
		});
		absent_ch.setForeground(Color.WHITE);
		absent_ch.setBackground(new Color(0, 0, 128));
		desktopPane.add(absent_ch, "cell 6 3 1 6,alignx left,growy");
		present_ch = new JCheckBox("Present");
		present_ch.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (present_ch.isSelected()){
					absent_ch.setSelected(false);
				}
			}
		});
		present_ch.setForeground(Color.WHITE);
		present_ch.setBackground(new Color(0, 0, 128));
		desktopPane.add(present_ch, "cell 6 9,alignx left,aligny top");
		
		JLabel lblSelectStatus = new JLabel("Select Status");
		lblSelectStatus.setForeground(Color.WHITE);
		lblSelectStatus.setFont(new Font("SansSerif", Font.PLAIN, 20));
		lblSelectStatus.setBorder(new MatteBorder(0, 0, 2, 0, (Color) Color.WHITE));
		desktopPane.add(lblSelectStatus, "cell 6 0 1 3,growx,aligny bottom");
		
		JLabel lblWorkerDitails = new JLabel("Attendance");
		lblWorkerDitails.setFont(new Font("SansSerif", Font.PLAIN, 30));
		lblWorkerDitails.setForeground(Color.GREEN);
		lblWorkerDitails.setHorizontalAlignment(SwingConstants.CENTER);
		desktopPane.add(lblWorkerDitails, "cell 3 0 3 1,alignx center,aligny top");
		
		JScrollPane scrollPane = new JScrollPane();
		desktopPane.add(scrollPane, "cell 0 12 11 4,grow");
		
		presentList = new JTable();
		WorkerDitailsModel.load();
		scrollPane.setViewportView(presentList);
		scrollPane.setViewportView(presentList);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String id = id_txt.getText();
				
				String name = WorkerName_txt.getText();
				
				String poS=posiotn.getText();

				
				date=dateChooser.getDate();
				sqlDate=new java.sql.Date(date.getTime());

				
				String gender=g.getText();
				String present = null;
				if (present_ch.isSelected()) {
					present = present_ch.getText();
				} else if (absent_ch.isSelected()) {
					present = absent_ch.getText();

				} else {
					JOptionPane.showMessageDialog(null, "Select Absent Or Present!");
				}
				
				new WorkerDitails_ctrl(id,name, gender, present,poS ,sqlDate );
				WorkerDitailsModel.load();
			}
		});
		
		JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int row = presentList.getSelectedRow();

				String id = presentList.getModel().getValueAt(row, 0).toString();
				String Name = presentList.getModel().getValueAt(row, 1).toString();
				String Gender = presentList.getModel().getValueAt(row, 2).toString();
				String present = presentList.getModel().getValueAt(row, 3).toString();
				String Dat = presentList.getModel().getValueAt(row, 5).toString();
				 String pos=presentList.getModel().getValueAt(row,
				 4).toString();
				// String date=presentList.getModel().getValueAt(row,
				// 5).toString();
				EditWorkerDitails editWorkerDitails = new EditWorkerDitails();
				editWorkerDitails.setVisible(true);
				editWorkerDitails.workerIdTxt.setText(id);
				editWorkerDitails.name.setText(Name);
				editWorkerDitails.genderT.setText(Gender);
				editWorkerDitails.preT.setText(present);
				editWorkerDitails.da2.setText(Dat);
				editWorkerDitails.pos.setText(pos);
				
				//java.util.Date date=new java.util.Date(Dat);
				//java.sql.Date sql=new Date(date.getTime());
				 //new EditWorkerCtrl(id,Name,Gender,present,pos);
			}
		});
		
		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				WorkerDitailsModel.load();	
			}
		});
		desktopPane.add(btnRefresh, "cell 8 16");
		desktopPane.add(btnEdit, "cell 9 16");
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int row = presentList.getSelectedRow();

				String id = presentList.getModel().getValueAt(row, 1).toString();
				WorkerDitailsModel.Dlete(id);
				WorkerDitailsModel.load();	
			}
		});
		desktopPane.add(btnDelete, "flowx,cell 10 16");
		desktopPane.add(btnSave, "cell 10 16,alignx right,aligny center");
		
	}
}
