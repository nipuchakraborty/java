package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;

import org.jdesktop.swingx.JXDatePicker;

import net.miginfocom.swing.MigLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;

import controller.ParchaseCtrl;
import model.ParchaseModel;

import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.border.TitledBorder;
import javax.swing.border.MatteBorder;
import javax.swing.SwingConstants;


public class Parchase extends JInternalFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane ;
	private JTextField billTxt;
	private JLabel label_1;
	
	private JTextField vendorName;
	private JLabel label_2;
	private JLabel lblProductName;
	private JTextField productName;
	private JLabel lblProductCategory;
	private JComboBox comboBox;
	private JLabel lblQuntity;
	private JTextField quntity_txt;
	private JLabel lblUnitPrice;
	private JTextField price_txt;
	private JTextField total_txt;
	public static  JLabel label_7;
	private JScrollPane scrollPane;
	public static JTable table;
	private JButton btnSave;
	public static  JXDatePicker dateChooser;
	java.util.Date date;
	java.sql.Date sqldate;
	private JButton btnPrint;
	private JLabel lblNewLabel;
	
	public Parchase() {
		setFrameIcon(new ImageIcon(Parchase.class.getResource("/pictureResource/images/DB Location.png")));
		setBackground(new Color(0, 100, 0));
		
		setLocation(new Point(220, 0));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		setDoubleBuffered(true);
		setMaximizable(true);
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
	
		setTitle("Parchase Products");
		
	
		
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		
		setSize(1191, 753);
		getContentPane().setLayout(new MigLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				image=(Toolkit.getDefaultToolkit().getImage(Parchase.class.getResource("/pictureResource/BA3.png")));
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(new MigLayout("", "[grow][grow][][][][][grow][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][grow]", "[][20.00,grow][][][][][][][][][][][][][][][][][][][][grow][]"));
		
		lblNewLabel = new JLabel("Parchase Product List");
		lblNewLabel.setFont(new Font("SansSerif", Font.PLAIN, 30));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(Color.WHITE);
		desktopPane.add(lblNewLabel, "cell 32 0");
		
		JLabel label = new JLabel("Bill No:");
		label.setForeground(new Color(240, 255, 240));
		label.setFont(new Font("Tahoma", Font.BOLD, 18));
		label.setBackground(Color.LIGHT_GRAY);
		desktopPane.add(label, "cell 0 1");
		
		billTxt = new JTextField();
		billTxt.setColumns(10);
		desktopPane.add(billTxt, "cell 1 1,growx");
		
		scrollPane = new JScrollPane();
		scrollPane.setBorder(new MatteBorder(4, 4, 4, 4, (Color) new Color(0, 0, 0)));
		desktopPane.add(scrollPane, "cell 7 1 31 21,grow");
		
		table = new JTable();
		table.setSurrendersFocusOnKeystroke(true);
		table.setShowVerticalLines(true);
		table.setShowHorizontalLines(true);
		table.setFillsViewportHeight(true);
		table.setColumnSelectionAllowed(true);
		table.setBackground(Color.WHITE);
		table.setForeground(Color.RED);
		table.setBorder(null);
		ParchaseModel.load();
		scrollPane.setViewportView(table);
		
		label_2 = new JLabel("Vendor Name:");
		label_2.setForeground(new Color(245, 255, 250));
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		desktopPane.add(label_2, "cell 0 3,alignx left");
		
		vendorName = new JTextField();
		vendorName.setColumns(10);
		desktopPane.add(vendorName, "cell 1 3,growx");
		
		lblProductName = new JLabel("Product Name :");
		lblProductName.setForeground(Color.WHITE);
		lblProductName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		desktopPane.add(lblProductName, "cell 0 5,alignx left");
		
		productName = new JTextField();
		productName.setColumns(10);
		desktopPane.add(productName, "cell 1 5,growx");
		
		lblProductCategory = new JLabel("Product Category :\r\n");
		lblProductCategory.setForeground(Color.WHITE);
		lblProductCategory.setFont(new Font("Tahoma", Font.PLAIN, 18));
		desktopPane.add(lblProductCategory, "cell 0 7,alignx left");
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Select", "Spaici", "Vegitable", "Stationary", "Fish", "Meat", "Fruit", "Others"}));
		desktopPane.add(comboBox, "cell 1 7,growx");
		
		label_1 = new JLabel("Date:");
		label_1.setForeground(new Color(240, 255, 240));
		label_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		label_1.setBackground(Color.LIGHT_GRAY);
		desktopPane.add(label_1, "cell 0 9");
		
		dateChooser = new JXDatePicker();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		java.util.Date date2 = new Date(System.currentTimeMillis());
		dateChooser.setDate(date2);
		desktopPane.add(dateChooser, "cell 1 9");
		
		lblUnitPrice = new JLabel("Unit Price:");
		lblUnitPrice.setForeground(Color.WHITE);
		lblUnitPrice.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblUnitPrice.setBackground(Color.WHITE);
		desktopPane.add(lblUnitPrice, "cell 0 15");
		
		price_txt = new JTextField();
		
		price_txt.setColumns(10);
		desktopPane.add(price_txt, "cell 1 15,growx");
		
		lblQuntity = new JLabel("Quntity");
		lblQuntity.setForeground(Color.WHITE);
		lblQuntity.setFont(new Font("Tahoma", Font.PLAIN, 18));
		desktopPane.add(lblQuntity, "cell 0 17,alignx left,aligny center");
		
		quntity_txt = new JTextField();
		quntity_txt.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				String Q=quntity_txt.getText();
				int q=Integer.parseInt(Q);
				String pr=price_txt.getText();
				int p=Integer.parseInt(pr);
			int to=q*p;
			String Total=Integer.toString(to);
			total_txt.setText(Total);
			}
		});
		quntity_txt.setColumns(10);
		desktopPane.add(quntity_txt, "cell 1 17,growx");
		
		label_7 = new JLabel("Total:");
		label_7.setForeground(Color.WHITE);
		label_7.setFont(new Font("Tahoma", Font.PLAIN, 18));
		desktopPane.add(label_7, "cell 0 19,alignx left");
		
		total_txt = new JTextField();
		total_txt.setColumns(10);
		desktopPane.add(total_txt, "cell 1 19,growx");
		
		btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String bill=billTxt.getText();
				String ven=vendorName.getText();
				String pro=productName.getText();
				String cat=comboBox.getSelectedItem().toString();
				date= dateChooser.getDate();
				sqldate=new java.sql.Date(date.getTime());
				String qun =quntity_txt.getText();
				String pric=price_txt.getText();
				String totalPric=total_txt.getText();
				new ParchaseCtrl( bill, ven,  pro, cat, qun,  pric, totalPric, sqldate);
				
			}
		});
		btnSave.setIcon(new ImageIcon(Parchase.class.getResource("/pictureResource/images/save_all.png")));
		desktopPane.add(btnSave, "cell 0 22");
		
		btnPrint = new JButton("Print");
		btnPrint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MessageFormat heading=new MessageFormat("Parchase List");
				MessageFormat Footer=new MessageFormat("");
				try {
					table.print(JTable.PrintMode.FIT_WIDTH,heading,Footer);
					table.printAll(getGraphics());
					table.print();
				} catch (PrinterException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		desktopPane.add(btnPrint, "cell 37 22,alignx right");
		
	}
}
