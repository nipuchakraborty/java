package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;
import net.miginfocom.swing.MigLayout;
import viwe.TextPrompt;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.text.ParseException;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import model.SalesReportModel;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import org.jdesktop.swingx.JXDatePicker;
import javax.swing.JButton;
import javax.swing.border.MatteBorder;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;


public class SalesReport extends JInternalFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane ;
	public static  JTable table;
	public static  JTextField textField;
	public static JTextField textField_1;
	private JButton btnNewButton;
	
	public SalesReport() {
		setBackground(new Color(0, 100, 0));
		setTitle("Sales Report");
		setLocation(new Point(220, 0));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		setDoubleBuffered(true);
		setMaximizable(true);
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		
		
	
		
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		
		setSize(1191, 753);
		getContentPane().setLayout(new MigLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				image=(Toolkit.getDefaultToolkit().getImage(SalesReport.class.getResource("/pictureResource/BA3.png")));
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		
		desktopPane.setFocusable(false);
		desktopPane.setFocusTraversalKeysEnabled(false);
		desktopPane.setFocusCycleRoot(false);
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(new MigLayout("", "[336px][grow][10px][][][][131.00][432.00][701px,grow][][][]", "[67px][68px][452px,grow]"));
		
		JLabel lblNewLabel = new JLabel("Sales Report");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("SansSerif", Font.PLAIN, 33));
		lblNewLabel.setForeground(Color.GREEN);
		desktopPane.add(lblNewLabel, "cell 0 0 9 1,alignx center,growy");
		
		JLabel lblSearch = new JLabel("Search");
		lblSearch.setForeground(Color.WHITE);
		
		lblSearch.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				lblSearch.setFont(new Font("SansSerif", Font.PLAIN, 36));
				lblSearch.setHorizontalAlignment(SwingConstants.CENTER);
				lblSearch.setForeground(Color.GREEN);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblSearch.setFont(new Font("Monotype Corsiva", Font.PLAIN, 36));
				lblSearch.setHorizontalAlignment(SwingConstants.CENTER);
				lblSearch.setForeground(Color.WHITE);
			}
			@Override
			public void mouseClicked(MouseEvent arg0) {
				/*Date date=datePicker.getDate();
				java.sql.Date date2=new java.sql.Date(date.getTime());
				SalesReportModel.search(date2);*/
			}
		});
		lblSearch.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Checked_50px.png"));
		lblSearch.setFont(new Font("Monotype Corsiva", Font.PLAIN, 36));
		lblSearch.setHorizontalAlignment(SwingConstants.CENTER);
		desktopPane.add(lblSearch, "cell 0 1,alignx center,aligny center");
		
		textField_1 = new JTextField();
		textField_1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				SalesReportModel.searchName(textField_1.getText());
			}
		});
		new TextPrompt("Enter Customar name",textField_1);
		textField_1.setFont(new Font("SansSerif", Font.PLAIN, 30));
		desktopPane.add(textField_1, "cell 2 1 6 1,grow");
		textField_1.setColumns(10);
		
		btnNewButton = new JButton("Refresh");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SalesReportModel.Load();
			}
		});
		btnNewButton.setFont(new Font("SansSerif", Font.PLAIN, 20));
		desktopPane.add(btnNewButton, "cell 8 1,grow");
		
		JPanel panel = new JPanel();
		desktopPane.add(panel, "cell 0 2 9 1,grow");
		panel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(new MatteBorder(3, 3, 3, 3, (Color) new Color(255, 235, 205)));
		panel.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		SalesReportModel.Load();
		scrollPane.setViewportView(table);
		
	}
}
