package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;
import net.miginfocom.swing.MigLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.text.ParseException;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import com.toedter.calendar.JDateChooser;

import model.WorkersReportsModel;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.JScrollBar;
import javax.swing.ImageIcon;
import javax.swing.JButton;


public class WorkerReports extends JInternalFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane ;
	private JLabel lblSearchReport;
	private JTextField textField;
	private JPanel panel;
	private JLabel lblWorkerReports;
	private JScrollPane scrollPane;
	public static JTable table;
	private JButton btnPrint;
	
	
	public WorkerReports() {
		setResizable(true);
		setBackground(new Color(0, 100, 0));
		setTitle("Parchase");
		setLocation(new Point(220, 0));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		setDoubleBuffered(true);
		setMaximizable(true);
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));

		
		setTitle("Daily Reports");
		
	
		
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		
		setSize(1191, 753);
		getContentPane().setLayout(new MigLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				image=(Toolkit.getDefaultToolkit().getImage(WorkerReports.class.getResource("/pictureResource/BA3.png")));
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(new MigLayout("", "[][][][111.00,grow][][][][][][][][][][][][][][][][][][][][][][][][][][grow][][][grow]", "[][][][][][25.00][14.00,grow][][][][][][][grow][][][][][][][][][][][][grow]"));
		
		lblWorkerReports = new JLabel("Worker Reports");
		lblWorkerReports.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_User_Groups_100px.png"));
		lblWorkerReports.setForeground(Color.WHITE);
		lblWorkerReports.setFont(new Font("SansSerif", Font.BOLD, 30));
		desktopPane.add(lblWorkerReports, "cell 0 0 3 1,grow");
		
		lblSearchReport = new JLabel("Search Report");
		lblSearchReport.setFont(new Font("SansSerif", Font.PLAIN, 20));
		lblSearchReport.setForeground(Color.WHITE);
		desktopPane.add(lblSearchReport, "cell 0 1");
		
		textField = new JTextField();
		desktopPane.add(textField, "cell 1 1 2 1,growx");
		textField.setColumns(10);
		
		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Worker Reports List", TitledBorder.CENTER, TitledBorder.TOP, null, Color.WHITE));
		desktopPane.add(panel, "flowx,cell 0 4 33 20,grow");
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		scrollPane = new JScrollPane();
		panel.add(scrollPane);
		
		table = new JTable();
		WorkersReportsModel.Load();
		scrollPane.setViewportView(table);
		
		btnPrint = new JButton("Print");
		btnPrint.setIcon(new ImageIcon(WorkerReports.class.getResource("/pictureResource/images/print.png")));
		desktopPane.add(btnPrint, "cell 32 24,alignx right");
		
	}
}
