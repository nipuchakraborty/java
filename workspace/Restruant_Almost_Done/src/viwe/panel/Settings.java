package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;
import net.miginfocom.swing.MigLayout;
import viwe.Dash;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.text.ParseException;
import java.awt.event.ActionEvent;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import BackUpRestore.BackUp;
import BackUpRestore.BackUpFrame;
import BackUpRestore.Back_Restore;
import login.LoginFrame;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.JSlider;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class Settings extends JInternalFrame {

	private JPanel contentPane;
	private JLabel lblGenerel;
	
	
	public Settings() {
		setResizable(true);
		setFrameIcon(new ImageIcon(Settings.class.getResource("/pictureResource/images/Help.gif")));
		setBackground(new Color(0, 100, 0));
		
		setLocation(new Point(220, 0));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		setDoubleBuffered(true);
		setMaximizable(true);
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		
	
		
		setTitle("Settings");
		
	
		
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		
		setSize(1191, 753);
		getContentPane().setLayout(new MigLayout());
		
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblGenerel = new JLabel("Background");
		lblGenerel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblGenerel.setBounds(18, 49, 116, 25);
		contentPane.add(lblGenerel);
		
			 
					JPanel panel = new JPanel();
					panel.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent arg0) {
							String black="/pictureResource/BA5.png";
							model.Settings.background(black);
							JOptionPane.showMessageDialog(null, "Restart");
							System.exit(0);
							
						}
					});
					panel.setBackground(new Color(0, 0, 0));
					panel.setBounds(28, 86, 63, 57);
					contentPane.add(panel);
					
					JPanel panel_1 = new JPanel();
					panel_1.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent arg0) {
							String red="/pictureResource/BA4.png";
							model.Settings.background(red);
							JOptionPane.showMessageDialog(null, "Restart");
							System.exit(0);	
						}
					});
					panel_1.setBackground(new Color(0, 255, 0));
					panel_1.setBounds(103, 86, 63, 57);
					contentPane.add(panel_1);
					
					JPanel panel_2 = new JPanel();
					panel_2.setBackground(new Color(0, 0, 205));
					panel_2.setBounds(190, 86, 63, 57);
					contentPane.add(panel_2);
					
					JPanel panel_3 = new JPanel();
					panel_3.setBackground(new Color(255, 0, 0));
					panel_3.setBounds(280, 86, 63, 57);
					contentPane.add(panel_3);
		
		JButton btnOk = new JButton("ok");
		btnOk.setBounds(407, 86, 97, 25);
		contentPane.add(btnOk);
		
		JButton btnNewButton = new JButton("Backup");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			BackUpFrame backUpFrame=new BackUpFrame();
			backUpFrame.setVisible(true);
			}
		});
		btnNewButton.setFont(new Font("SansSerif", Font.BOLD, 18));
		btnNewButton.setIcon(new ImageIcon(Settings.class.getResource("/pictureResource/icons8_Data_Backup_40px.png")));
		btnNewButton.setBorderPainted(false);
		btnNewButton.setFocusable(false);
		btnNewButton.setFocusTraversalKeysEnabled(false);
		btnNewButton.setFocusPainted(false);
		btnNewButton.setBackground(Color.LIGHT_GRAY);
		btnNewButton.setBounds(23, 336, 240, 142);
		contentPane.add(btnNewButton);
		
		JButton btnRstore = new JButton("Rstore");
		btnRstore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			Back_Restore back_Restore=new Back_Restore();
			back_Restore.setVisible(true);
			}
		});
		btnRstore.setFont(new Font("SansSerif", Font.BOLD, 18));
		btnRstore.setIcon(new ImageIcon(Settings.class.getResource("/pictureResource/icons8_Restore_Window_64px.png")));
		btnRstore.setFocusable(false);
		btnRstore.setFocusTraversalKeysEnabled(false);
		btnRstore.setFocusPainted(false);
		btnRstore.setBorderPainted(false);
		btnRstore.setBackground(Color.LIGHT_GRAY);
		btnRstore.setBounds(299, 336, 240, 142);
		contentPane.add(btnRstore);
		
		
		
	}
}
