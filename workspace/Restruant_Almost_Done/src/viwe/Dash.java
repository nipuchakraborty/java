package viwe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableStringConverter;

import controller.AddFoodBreakfast_Ctrl;
import login.LoginFrame;
import net.miginfocom.swing.MigLayout;
import viwe.panel.About;
import viwe.panel.AddFood;
import viwe.panel.AddWorker;
import viwe.panel.Calculator;
import viwe.panel.Contact;
import viwe.panel.WorkerReports;
import viwe.panel.Help;
import viwe.panel.Order;


import viwe.panel.Parchase;
import viwe.panel.ParchaseReports;
import viwe.panel.SalarySheet;
import viwe.panel.SalesReport;
import viwe.panel.Settings;
import viwe.panel.Sit_Plane;
import viwe.panel.Sit_Plane2;
import viwe.panel.WorkerDetails;
import viwe.sequrity.Login;
import viwe.sequrity.LoginSingle;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.sql.DatabaseMetaData;
import java.text.ParseException;
import java.awt.event.ActionEvent;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import java.awt.Dialog.ModalExclusionType;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Desktop;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import BackUpRestore.BackUp;
import BackUpRestore.BackUpFrame;
import BackUpRestore.Back_Restore;

//import calculator1.Calculator;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Window.Type;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.SpringLayout;
import javax.swing.JButton;
import javax.swing.border.MatteBorder;
import java.awt.SystemColor;
import javax.swing.JLayeredPane;

public class Dash extends JFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane ;

	
	public static void main(String[] args) {
		 {
			 Dash frame = new Dash();
			 frame.setOpacity(1.0f);
				frame.setVisible(true);
				
				
					
					try {
						try {
							UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
							//UIManager.setLookAndFeel("de.muntjak.tinylookandfeel.TinyLookAndFeel");
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (InstantiationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
					} 
					
				catch (UnsupportedLookAndFeelException e) {
					
					e.printStackTrace();
				}
		 }
	}

	int w = 1600;
	int h = 900;
	/**
	 * @wbp.nonvisual location=1563,-36
	 */
	private final JLabel label = new JLabel("New label");
	
	public Dash() {
		
		
		setBackground(Color.WHITE);
		label.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/delete.png")));
		//setUndecorated(true);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIgnoreRepaint(true);
		setFocusTraversalPolicyProvider(true);
		setTitle("Restrurant Management");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\Rest.png"));
		setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
	
		setBounds(0, 0, w, h);
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screen.width - w) / 2, (screen.height - h) / 2);
		setSize(1451, 857);
		getContentPane().setLayout(new MigLayout());
		setPreferredSize(new Dimension(screen.width + 50, 50));
		JMenuBar menuBar = new JMenuBar();
		menuBar.setInheritsPopupMenu(true);
		menuBar.setIgnoreRepaint(true);
		menuBar.setFocusTraversalPolicyProvider(true);
		menuBar.setFocusTraversalKeysEnabled(true);
		menuBar.setFocusCycleRoot(true);
		menuBar.setDoubleBuffered(true);
		menuBar.setBorder(new EmptyBorder(5, 5, 5, 5));
		menuBar.setBorderPainted(false);
		menuBar.setForeground(new Color(255, 99, 71));
		menuBar.setBackground(new Color(255, 255, 255));
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Home");
		mnNewMenu.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/Home.gif")));
		mnNewMenu.setFont(new Font("SansSerif", Font.ITALIC, 16));
		menuBar.add(mnNewMenu);
		
		JMenu mnOpen = new JMenu("Open");
		mnOpen.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/OPEN.GIF")));
		mnNewMenu.add(mnOpen);
		
		JMenuItem mntmAddfood = new JMenuItem("AddFood");
		mntmAddfood.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_1, java.awt.event.InputEvent.SHIFT_MASK));
		mntmAddfood.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/DB Location.png")));
		mntmAddfood.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				AddFood addFood=new AddFood();
				
				addFood.setVisible(true);
				desktopPane.add(addFood);
			}
		});
		mnOpen.add(mntmAddfood);
		
		JMenuItem mntmOrder = new JMenuItem("Take Order");
		mntmOrder.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_2, java.awt.event.InputEvent.SHIFT_MASK));

		mntmOrder.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/orders.png")));
		mntmOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Order order =new Order();
				order.setVisible(true);
				desktopPane.add(order);
				
			}
		});
		mnOpen.add(mntmOrder);
		
		JMenuItem mntmParchase = new JMenuItem("Parchase");
		mntmParchase.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_3, java.awt.event.InputEvent.SHIFT_MASK));

		mntmParchase.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/purchase.png")));
		mntmParchase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Parchase parchase=new Parchase();
				parchase.setVisible(true);
				desktopPane.add(parchase);
			}
		});
		mnOpen.add(mntmParchase);
		
		JMenuItem mntmSeatPlane = new JMenuItem("Seat Plane");
		mntmSeatPlane.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_4, java.awt.event.InputEvent.SHIFT_MASK));

		mntmSeatPlane.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/find.gif")));
		mntmSeatPlane.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Sit_Plane2 sit_Plane=new Sit_Plane2();
				sit_Plane.setVisible(true);
				desktopPane.add(sit_Plane);
			}
		});
		mnOpen.add(mntmSeatPlane);
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mntmSave.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/save_all.png")));
		mnNewMenu.add(mntmSave);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		
		JMenuItem mntmLogout = new JMenuItem("Logout");
		mntmLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new LoginFrame().setVisible(true);
				Dash.this.setVisible(false);
			}
		});
		mntmLogout.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/reload_Hover.png")));
		mnNewMenu.add(mntmLogout);
		mntmExit.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/exits.png")));
		mnNewMenu.add(mntmExit);
		
		JMenu mnEdit = new JMenu("Edit");
		mnEdit.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/edit.png")));
		mnEdit.setFont(new Font("SansSerif", Font.ITALIC, 16));
		menuBar.add(mnEdit);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Recovery");
		mntmNewMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.SHIFT_MASK));
		mntmNewMenuItem.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/backup.GIF")));
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new BackUpFrame().setVisible(true);
		
			
			}
		});
		
		JMenuItem mntmResizeImage = new JMenuItem("Resize Image");
		mntmResizeImage.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\resized\\icons8_Wallet_15px.png"));
		mntmResizeImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					Process p=Runtime.getRuntime().exec("C:/Program Files (x86)/Fast Image Resizer/fastimageresizer.exe");
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
				
				//"C:\Program Files (x86)\Fast Image Resizer\fastimageresizer.exe"
			
		});
		mnEdit.add(mntmResizeImage);
		mnEdit.add(mntmNewMenuItem);
		
		JMenu mnWorker = new JMenu("Worker ");
		mnWorker.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/Business.png")));
		mnWorker.setFont(new Font("SansSerif", Font.ITALIC, 16));
		menuBar.add(mnWorker);
		
		JMenuItem mntmWorkerInfo = new JMenuItem("Absent And Present List");
		mntmWorkerInfo.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/Attendance.png")));
		mntmWorkerInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				WorkerDetails workerDetails=new WorkerDetails();
				workerDetails.setVisible(true);
				desktopPane.add(workerDetails);
			}
		});
		
		JMenuItem mntmAddWorkerInfo = new JMenuItem("Add Worker info");
		mntmAddWorkerInfo.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/PaySlip.png")));
		mntmNewMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_W, java.awt.event.InputEvent.SHIFT_MASK));
		mntmNewMenuItem.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/notepad.png")));
		mntmAddWorkerInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddWorker addWorker=new AddWorker();
				addWorker.setVisible(true);
				desktopPane.add(addWorker);
			}
		});
		mnWorker.add(mntmAddWorkerInfo);
		mnWorker.add(mntmWorkerInfo);
		
		JMenuItem mntmSalarySheet = new JMenuItem("Salary Sheet");
		mntmSalarySheet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SalarySheet salarySheet=new SalarySheet();
				salarySheet.setVisible(true);
				desktopPane.add(salarySheet);
			}
		});
		mntmSalarySheet.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/ProdStocks.png")));
		mnWorker.add(mntmSalarySheet);
		
		JMenu mnCalculator = new JMenu("Calculator");
		mnCalculator.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/calc.png")));
		mnCalculator.setFont(new Font("SansSerif", Font.ITALIC, 16));
		menuBar.add(mnCalculator);
		
		JMenuItem mntmCalculator = new JMenuItem("Calculator");
		mntmCalculator.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/calc.png")));
		mntmCalculator.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			Calculator calculator=new Calculator();
			calculator.setVisible(true);
			desktopPane.add(calculator);
				
			}
		});
		mnCalculator.add(mntmCalculator);
		
		JMenu mnReports = new JMenu("Reports");
		mnReports.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/NotePad.gif")));
		mnReports.setFont(new Font("SansSerif", Font.ITALIC, 16));
		menuBar.add(mnReports);
		
		JMenuItem mntmSalesReports = new JMenuItem("Sales Reports");
		mntmSalesReports.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SalesReport s=new SalesReport();
				s.setVisible(true);
				desktopPane.add(s);
			}
		});
		mntmSalesReports.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/invoice.png")));
		mnReports.add(mntmSalesReports);
		
		JMenuItem mntmParchaseReports = new JMenuItem("Expense & Sales Reports");
		mntmParchaseReports.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ParchaseReports par=new ParchaseReports();
				par.setVisible(true);
				desktopPane.add(par);
			}
		});
		mntmParchaseReports.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/company reference.png")));
		mnReports.add(mntmParchaseReports);
		
		JMenuItem mntmWorkersReports = new JMenuItem("Workers Reports");
		mntmWorkersReports.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				WorkerReports wo=new WorkerReports();
				wo.setVisible(true);
				desktopPane.add(wo);
			}
		});
		mntmWorkersReports.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/employee.png")));
		mnReports.add(mntmWorkersReports);
		
		JMenu mnNewMenu_1 = new JMenu("Camera");
		mnNewMenu_1.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Camera_20px.png"));
		mnNewMenu_1.setFont(new Font("SansSerif", Font.ITALIC, 16));
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Open Camera");
		mntmNewMenuItem_1.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Camera_64px.png"));
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Process p=Runtime.getRuntime().exec("C:/Users/Nk Chakraborty/Desktop/MyCam/MyCam.exe");
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		mnNewMenu_1.add(mntmNewMenuItem_1);
		
		JMenu mnAbout = new JMenu("About");
		mnAbout.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/help.png")));
		mnAbout.setFont(new Font("SansSerif", Font.ITALIC, 16));
		
		menuBar.add(mnAbout);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/help.png")));
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				About about=new About();
				about.setVisible(true);
				desktopPane.add(about);
			}
		});
		mnAbout.add(mntmAbout);
		
		JMenuItem mntmContact = new JMenuItem("Help Online");
		mntmContact.setIcon(new ImageIcon(Dash.class.getResource("/pictureResource/images/Help.gif")));
		mntmContact.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					String url = "http://nkchakakraborty.blogspot.com/";

		            java.awt.Desktop.getDesktop().browse(java.net.URI.create(url));
					//Process p=Runtime.getRuntime().exec("‪C:/Users/Nk Chakraborty/Documents/help.html");
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			/*	Help help=new Help();
				help.setVisible(true);
				desktopPane.add(help);*/
				
			}
		});
		mnAbout.add(mntmContact);
		contentPane = new JPanel();
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		
		// this is background picture 
		
		desktopPane = new JDesktopPane() {
			
			
			
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				image=(Toolkit.getDefaultToolkit().getImage(Dash.class.getResource("/pictureResource/BA3.png")));//"/pictureResource/BA3.png"
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		desktopPane.setFont(new Font("SansSerif", Font.BOLD, 20));
		
		desktopPane.setFocusable(false);
		desktopPane.setFocusTraversalKeysEnabled(false);
		desktopPane.setFocusCycleRoot(false);
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		
		JButton btnNewButton = new JButton("Order");
		btnNewButton.setBackground(new Color(192, 192, 192));
		btnNewButton.setFont(new Font("SansSerif", Font.BOLD, 20));
		btnNewButton.setFocusTraversalKeysEnabled(false);
		btnNewButton.setFocusPainted(false);
		btnNewButton.setFocusable(false);
		btnNewButton.setRequestFocusEnabled(false);
		btnNewButton.setRolloverEnabled(false);
		btnNewButton.setBounds(7, 7, 211, 123);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Order order =new Order();
				order.setVisible(true);
				desktopPane.add(order);
			}
		});
		desktopPane.setLayout(null);
		btnNewButton.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Purchase_Order_64px.png"));
		desktopPane.add(btnNewButton);
		
		JButton btnWorker = new JButton("Worker");
		btnWorker.setBackground(new Color(192, 192, 192));
		btnWorker.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				WorkerDetails workerDetails=new WorkerDetails();
				workerDetails.setVisible(true);
				desktopPane.add(workerDetails);
			}
		});
		btnWorker.setFont(new Font("SansSerif", Font.BOLD, 20));
		btnWorker.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_User_Groups_100px.png"));
		btnWorker.setBounds(7, 176, 211, 123);
		desktopPane.add(btnWorker);
		
		JButton btnCalculator = new JButton("Settings");
		btnCalculator.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Settings settings=new Settings();
				settings.setVisible(true);
				desktopPane.add(settings);
			}
		});
		btnCalculator.setBackground(new Color(192, 192, 192));
		btnCalculator.setFont(new Font("SansSerif", Font.BOLD, 16));
		btnCalculator.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Settings_100px.png"));
		btnCalculator.setBounds(7, 487, 211, 123);
		desktopPane.add(btnCalculator);
		
		JButton btnSales = new JButton("Sales");
		btnSales.setBackground(new Color(192, 192, 192));
		btnSales.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SalesReport s=new SalesReport();
				s.setVisible(true);
				desktopPane.add(s);
			}
		});
		btnSales.setFont(new Font("SansSerif", Font.BOLD, 16));
		btnSales.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Report_Card_100px_1.png"));
		btnSales.setBounds(7, 330, 211, 123);
		desktopPane.add(btnSales);
		
		JButton btnAbout = new JButton("About");
		btnAbout.setBackground(new Color(192, 192, 192));
		btnAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				About about=new About();
				about.setVisible(true);
				desktopPane.add(about);
			}
		});
		btnAbout.setFont(new Font("SansSerif", Font.BOLD, 16));
		btnAbout.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_About_100px.png"));
		btnAbout.setBounds(7, 642, 211, 123);
		desktopPane.add(btnAbout);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(192, 192, 192));
		panel.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(192, 192, 192)));
		panel.setBounds(-12, -66, 230, 862);
		desktopPane.add(panel);
		
	}
}
