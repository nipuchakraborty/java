package Massagetoast;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
 
public class Toast extends JFrame {
 
    JButton btn;
 
    public Toast() {
        super("Toast Demo");
        btn = new JButton("Ok");
        setSize(400, 300);
        setLayout(new FlowLayout());
        getContentPane().add(btn);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
 
        btn.addActionListener(new ActionListener() {
 
            @Override
            public void actionPerformed(ActionEvent e) {
                final JDialog dialog = new AndroidLikeToast(Toast.this, true, "Hello javaknowledge!");
                setForeground(Color.black);
                Timer timer = new Timer(AndroidLikeToast.LENGTH_LONG, new ActionListener() {
 
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        dialog.setVisible(false);
                        dialog.dispose();
                    }
                });
                timer.setRepeats(false);
                timer.start();
 
                dialog.setVisible(true); // if modal, application will pause here
            }
        });
    }
 
    public static void main(String[] args) {
        Toast e = new Toast();
        //e.pack();
        e.setVisible(true);
    }
}