/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Fardin
 */
public class Reg {
	
	private String n;
	private String u;
	private String e;
	private String p;
	private String r;
	
	public void prepare(String name, String user, String email, String pass, String role) {
		
		this.n=name;
		this.u=user;
		this.e=email;
		this.p=pass;
		this.r=role;
	}


	public void store() {
		
		Connection conn=Database.getconnection();
		String sql="INSERT INTO `reg`(`name`, `user`, `email`, `password`, `role`) VALUES (?,?,?,?,?)";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, this.n);
			pst.setString(2, this.u);
			pst.setString(3, this.e);
			pst.setString(4, this.p);
			pst.setString(5, this.r);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static String getRegistration(String user,String password) {
		String pass = null;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `user`,`password` FROM `reg` WHERE `user`=? AND `password`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, user);
			pst.setString(2, password);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				pass=rs.getString("password");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pass;
			
	}
	public static String getPassword(String user) {
		String pass = null;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `password` FROM `reg` WHERE `user`=? ";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, user);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				pass=rs.getString("password");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pass;

	}
	
	public static String getUser(String password) {
		String user = null;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `user` FROM `reg` WHERE `password`=? ";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, password);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				user=rs.getString("user");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
		
		
		
	}
	

	public static String getPosition(String user) {
		String role = null;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `role` FROM `reg` WHERE user=? ";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, user);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				role=rs.getString("role");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return role;

	}
	
	public static String getEmail(String user) {
		String email = null;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `email` FROM `reg` WHERE `user`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, user);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				email=rs.getString("email");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return email;
		
	}
	
	public static String getName(String user) {
		String name = null;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `name` FROM `reg` WHERE `user`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, user);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				name=rs.getString("name");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return name;
		
	}
	
	public static String getUserByEmail(String email) {
		String user = null;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `user` FROM `reg` WHERE `email`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, email);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				user=rs.getString("user");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
		
	}
	
	
	public static int getId(String user) {
		
		int id=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `id` FROM `reg` WHERE user=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, user);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				id=rs.getInt("id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return id;
		
	}
	
	public void updateReg(int id) {
		
		Connection conn=Database.getconnection();
		String sql="UPDATE `reg` SET `name`=?,`user`=?,`email`=?,`password`=?,`role`=? WHERE id=?";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, this.n);
			pst.setString(2, this.u);
			pst.setString(3, this.e);
			pst.setString(4, this.p);
			pst.setString(5, this.r);
			pst.setInt(6, id);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
