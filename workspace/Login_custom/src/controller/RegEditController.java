package controller;

import model.Reg;
public class RegEditController {
	
	public RegEditController(int id, String name, String user, String email, String newPass, String role) {
		
		Reg reg=new Reg();
		reg.prepare(name, user, email, newPass, role);
		reg.updateReg(id);
		
	}

}
