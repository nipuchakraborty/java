package viwe;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import java.awt.Color;
import javax.swing.JTabbedPane;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.border.MatteBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JMenuItem;
import javax.swing.JCheckBox;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.io.File;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import java.awt.Insets;
import javax.swing.border.LineBorder;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.TitledBorder;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Home extends JFrame {

	private JPanel contentPane;
	private JTable table;
	//all quantity va
	private JTextField quantity5;
	private JTextField qauntity1;
	private JTextField qunatity2;
	private JTextField qauntity3;
	private JTextField qauntity4;
	
	
	private JInternalFrame internalFrame_4 ;
	//all Item name variable
	private JLabel l1;
	private JLabel l2;
	private JLabel l3;
	private JLabel l4;
	private JLabel l5;
	//all price variable
	private JLabel priceL1;
	private JLabel priceL2;
	private JLabel priceL3;
	private JLabel priceL4;
	private JLabel priceL5;
//all cheakBox variable 
	private JCheckBox ch1;
	private JCheckBox ch2;
	private JCheckBox ch3;
	private JCheckBox ch4;
	private JCheckBox ch5;
	JLabel lblCheakenDosha;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Home frame = new Home();
					//UIManager.setLookAndFeel(new SyntheticaAluOxideLookAndFeel());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Home() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Home.class.getResource("/pictureResource/food-grey.png")));
		setBackground(new Color(0, 0, 205));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1471, 762);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setMargin(new Insets(3, 3, 3, 3));
		menuBar.setBackground(new Color(0, 0, 128));
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("File");
		mnNewMenu.setForeground(new Color(255, 255, 0));
		mnNewMenu.setBackground(new Color(220, 20, 60));
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmOpen = new JMenuItem("Open");
		mntmOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser jF=new JFileChooser();
				jF.setCurrentDirectory(new File(System.getProperty("user.home")));
				 FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images", "*.jpg","*.gif","*.png","*.txt","*.doc","*.All");
				 jF.addChoosableFileFilter(filter);
				 int result = jF.showSaveDialog(null);
			}
		});
		mnNewMenu.add(mntmOpen);
		
		JMenuItem mntmSetting = new JMenuItem("Setting");
		mnNewMenu.add(mntmSetting);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mnNewMenu.add(mntmAbout);
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mnNewMenu.add(mntmSave);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mnNewMenu.add(mntmExit);
		
		JMenu mnEdit = new JMenu("Edit");
		mnEdit.setForeground(new Color(255, 255, 0));
		mnEdit.setBackground(new Color(153, 50, 204));
		menuBar.add(mnEdit);
		
		JMenuItem mntmAddItem = new JMenuItem("Add Item");
		mnEdit.add(mntmAddItem);
		
		JMenuItem mntmAddPrice = new JMenuItem("Add price");
		mnEdit.add(mntmAddPrice);
		
		JMenuItem mntmAddStatus = new JMenuItem("Add Status");
		mnEdit.add(mntmAddStatus);
		
		JMenuItem mntmRemoveItem = new JMenuItem("Remove Item");
		mnEdit.add(mntmRemoveItem);
		
		JMenu mnDishes = new JMenu("Dishes");
		mnDishes.setForeground(new Color(255, 255, 0));
		menuBar.add(mnDishes);
		
		JMenu mnManagment = new JMenu("Managment");
		mnManagment.setForeground(new Color(255, 255, 0));
		menuBar.add(mnManagment);
		
		JMenu mnDailyActivites = new JMenu("Daily activites");
		mnDailyActivites.setForeground(new Color(255, 255, 0));
		menuBar.add(mnDailyActivites);
		
		JMenu mnAboutUs = new JMenu("About us");
		mnAboutUs.setForeground(new Color(255, 255, 0));
		menuBar.add(mnAboutUs);
		
		JMenuItem mntmAbout_1 = new JMenuItem("About");
		mntmAbout_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				About about=new About();
				about.setVisible(true);
			}
		});
		mnAboutUs.add(mntmAbout_1);
		contentPane = new JPanel();
		contentPane.setBorder(new MatteBorder(3, 3, 3, 3, (Color) Color.GRAY));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 0, 205));
		contentPane.add(panel, "cell 0 0,grow");
		panel.setLayout(new MigLayout("", "[826px][][][][][][][][][grow]", "[488px,grow]"));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		panel.add(tabbedPane, "cell 0 0,grow");
		
		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addTab("Breakfast", null, tabbedPane_1, null);
		internalFrame_4 = new JInternalFrame("Breakfast");
		internalFrame_4.getContentPane().setForeground(new Color(0, 0, 205));
		internalFrame_4.getContentPane().setBackground(new Color(0, 0, 139));
		internalFrame_4.getContentPane().setLayout(null);
		
		l1 = new JLabel("");
		l1.setIcon(null);
		String path;
		l1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				JFileChooser jf=new JFileChooser();
				jf.setCurrentDirectory(new File(System.getProperty("user.home")));
				FileNameExtensionFilter f=new FileNameExtensionFilter("*.Images", ".jpg", ".gif", ".png");
				jf.addChoosableFileFilter(f);
				int result = jf.showSaveDialog(null);
				String path;
				if (result == JFileChooser.APPROVE_OPTION) {
					File selectedFile =jf.getSelectedFile();
					path = selectedFile.getAbsolutePath();
					l1.setIcon(ResizeImage(path));
				}
				else if (result == JFileChooser.CANCEL_OPTION) {
					System.out.println("No File Select");
				}
			}
		});
		l1.setForeground(new Color(255, 255, 0));
		l1.setBorder(new LineBorder(new Color(0, 0, 0)));
		l1.setBackground(new Color(0, 0, 0));
		l1.setBounds(35, 13, 34, 54);
		l1.setText("Tea");
		l1.setHorizontalTextPosition(JLabel.CENTER);
		l1.setVerticalTextPosition(JLabel.BOTTOM);
		
		internalFrame_4.getContentPane().add(l1);
		
		JLabel l2 = new JLabel();
		
		l2.setForeground(new Color(255, 255, 0));
		l2.setText("Cheken Chap");
		l2.setHorizontalTextPosition(JLabel.CENTER);
		l2.setVerticalTextPosition(JLabel.BOTTOM);
	
		l2.setBounds(12, 109, 76, 68);
		internalFrame_4.getContentPane().add(l2);
		
		ch1 = new JCheckBox("");
		ch1.setBackground(new Color(0, 0, 139));
		ch1.setBounds(112, 32, 25, 25);
		internalFrame_4.getContentPane().add(ch1);
		
		ch2 = new JCheckBox("");
		ch2.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		ch2.setBackground(new Color(0, 0, 139));
		ch2.setBounds(112, 122, 25, 25);
		internalFrame_4.getContentPane().add(ch2);
		
		JButton order_Btn = new JButton("Order");
		order_Btn.setFont(new Font("SansSerif", Font.BOLD, 18));
		order_Btn.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null), new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0))));
		order_Btn.setFocusable(false);
		order_Btn.setFocusCycleRoot(true);
		order_Btn.setIgnoreRepaint(true);
		order_Btn.setInheritsPopupMenu(true);
		order_Btn.setIconTextGap(5);
		
		
		ArrayList<String> menu=new ArrayList<>();
		
		order_Btn.setHorizontalTextPosition(SwingConstants.LEFT);
		order_Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				if (ch1.isSelected()){
					 String item=l1.getText();
					 String qaun=qauntity1.getText();
					 String price=priceL1.getText();
					System.out.println("Item Name:"+" "+item +" "+"Quntity: "+qaun+"Price "+":"+price);
					}
				if (ch2.isSelected()){
					 String item=l2.getText();
					 String qaun=qunatity2.getText();
					 String price=priceL2.getText();
					 System.out.println("Item Name:"+" "+item +" "+"Quntity: "+qaun+"Price "+":"+price);
				}
				if (ch3.isSelected()){
					 String item=l3.getText();
					 String qaun=qauntity3.getText();
					 String price=priceL3.getText();
					 System.out.println("Item Name:"+" "+item +" "+"Quntity: "+qaun+"Price "+":"+price);
				}
				if (ch4.isSelected()){
					 String item=l4.getText();
					 String qaun=qauntity4.getText();
					 String price=priceL4.getText();
					
				}
				if (ch5.isSelected()){
					 String item=l5.getText();
					 String qaun=quantity5.getText();
					 String price=priceL5.getText();
					 System.out.println("Item Name:"+" "+item +" "+"Quntity: "+qaun+"Price "+":"+price);
				}
				
			}
		});

		order_Btn.setBounds(675, 505, 114, 56);
		internalFrame_4.getContentPane().add(order_Btn);
		
		quantity5 = new JTextField();
		quantity5.setBounds(149, 32, 60, 28);
		internalFrame_4.getContentPane().add(quantity5);
		quantity5.setColumns(10);
		
		qauntity1 = new JTextField();
		qauntity1.setBounds(149, 122, 60, 28);
		internalFrame_4.getContentPane().add(qauntity1);
		qauntity1.setColumns(10);
		
		l3 = new JLabel();
		
		l3.setVerticalTextPosition(SwingConstants.BOTTOM);
		l3.setText("Noduls");
		l3.setHorizontalTextPosition(SwingConstants.CENTER);
		l3.setForeground(Color.YELLOW);
		l3.setBounds(12, 206, 94, 96);
		internalFrame_4.getContentPane().add(l3);
		ch3 = new JCheckBox("");
		ch3.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		ch3.setBackground(new Color(0, 0, 139));
		ch3.setBounds(112, 226, 25, 25);
		internalFrame_4.getContentPane().add(ch3);
		
		qunatity2 = new JTextField();
		qunatity2.setColumns(10);
		qunatity2.setBounds(149, 226, 60, 28);
		internalFrame_4.getContentPane().add(qunatity2);
		
		l4 = new JLabel();
		l4.setVerticalTextPosition(SwingConstants.BOTTOM);
		l4.setText("Omlet");
		l4.setHorizontalTextPosition(SwingConstants.CENTER);
		l4.setForeground(Color.YELLOW);
		l4.setBounds(12, 314, 94, 96);
		internalFrame_4.getContentPane().add(l4);
		
		ch4 = new JCheckBox("");
		ch4.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		ch4.setBackground(new Color(0, 0, 139));
		ch4.setBounds(112, 348, 25, 25);
		internalFrame_4.getContentPane().add(ch4);
		
		qauntity3 = new JTextField();
		qauntity3.setColumns(10);
		qauntity3.setBounds(149, 345, 60, 28);
		internalFrame_4.getContentPane().add(qauntity3);
		
		priceL1 = new JLabel("price 150/-");
		priceL1.setForeground(Color.YELLOW);
		priceL1.setBounds(251, 41, 114, 16);
		internalFrame_4.getContentPane().add(priceL1);
		
		priceL2 = new JLabel("price 150/-");
		priceL2.setForeground(Color.YELLOW);
		priceL2.setBounds(251, 131, 149, 16);
		internalFrame_4.getContentPane().add(priceL2);
		
		priceL3 = new JLabel("price 150/-");
		priceL3.setForeground(Color.YELLOW);
		priceL3.setBounds(251, 226, 129, 16);
		internalFrame_4.getContentPane().add(priceL3);
		
		priceL4 = new JLabel("price 150/-");
		priceL4.setForeground(Color.YELLOW);
		priceL4.setBounds(251, 354, 149, 16);
		internalFrame_4.getContentPane().add(priceL4);
		
		l5 = new JLabel();
		l5.setVerticalTextPosition(SwingConstants.BOTTOM);
		l5.setText("Cheaken dosha");
		l5.setHorizontalTextPosition(SwingConstants.CENTER);
		l5.setForeground(Color.YELLOW);
		l5.setBounds(12, 448, 94, 96);
		internalFrame_4.getContentPane().add(l5);
		
		ch5 = new JCheckBox("");
		ch5.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		ch5.setBackground(new Color(0, 0, 139));
		ch5.setBounds(112, 474, 25, 25);
		internalFrame_4.getContentPane().add(ch5);
		
		qauntity4 = new JTextField();
		qauntity4.setColumns(10);
		qauntity4.setBounds(149, 471, 60, 28);
		internalFrame_4.getContentPane().add(qauntity4);
		
		priceL5 = new JLabel("price 150/-");
		priceL5.setForeground(Color.YELLOW);
		priceL5.setBounds(251, 488, 169, 16);
		internalFrame_4.getContentPane().add(priceL5);
		tabbedPane_1.addTab("Menu Chart", null, internalFrame_4, null);
		internalFrame_4.setVisible(true);
		
		JTabbedPane tabbedPane_2 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addTab("Dinner", null, tabbedPane_2, null);
		
		JInternalFrame internalFrame_3 = new JInternalFrame("Dinner");
		internalFrame_3.getContentPane().setBackground(new Color(0, 0, 139));
		tabbedPane_2.addTab("Menu chart", null, internalFrame_3, null);
		internalFrame_3.setVisible(true);
		
		JTabbedPane tabbedPane_4 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addTab("Lunch", null, tabbedPane_4, null);
		
		JInternalFrame internalFrame_2 = new JInternalFrame("Luch");
		internalFrame_2.getContentPane().setBackground(new Color(0, 0, 139));
		tabbedPane_4.addTab("Menu Chart", null, internalFrame_2, null);
		internalFrame_2.setVisible(true);
		
		JTabbedPane tabbedPane_5 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addTab("Spcial dish", null, tabbedPane_5, null);
		
		JInternalFrame internalFrame_1 = new JInternalFrame("Spcial dish");
		internalFrame_1.getContentPane().setBackground(new Color(0, 0, 139));
		tabbedPane_5.addTab("Menu Chart", null, internalFrame_1, null);
		internalFrame_1.setVisible(true);
		
		JTabbedPane tabbedPane_6 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addTab("New tab", null, tabbedPane_6, null);
		
		JInternalFrame internalFrame = new JInternalFrame("New JInternalFrame");
		internalFrame.getContentPane().setBackground(new Color(0, 0, 139));
		tabbedPane_6.addTab("New tab", null, internalFrame, null);
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, "cell 2 0 8 1,grow");
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Name of Dish", "Price", "Date"
			}
		));
		table.getColumnModel().getColumn(0).setPreferredWidth(91);
		scrollPane.setViewportView(table);
		internalFrame.setVisible(true);
	}
	public ImageIcon ResizeImage(String ImagePath) {
		ImageIcon MyImage = new ImageIcon(ImagePath);
		Image img = MyImage.getImage();
		Image newImg = img.getScaledInstance(l1.getWidth(),l1.getHeight(), Image.SCALE_SMOOTH);
		ImageIcon image = new ImageIcon(newImg);
		return image;
}}
