package Viwe;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class LogIn extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LogIn frame = new LogIn();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LogIn() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 388);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLoginPannel = new JLabel("LogIn pannel");
		lblLoginPannel.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblLoginPannel.setBounds(156, 13, 137, 16);
		contentPane.add(lblLoginPannel);
		
		JLabel lblRole = new JLabel("Role");
		lblRole.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblRole.setBounds(42, 79, 52, 16);
		contentPane.add(lblRole);
		
		JLabel lblUserName = new JLabel("User Name");
		lblUserName.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblUserName.setBounds(42, 139, 97, 16);
		contentPane.add(lblUserName);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Verdana", Font.BOLD | Font.ITALIC, 15));
		lblPassword.setBounds(42, 203, 106, 16);
		contentPane.add(lblPassword);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(189, 77, 115, 22);
		contentPane.add(comboBox);
		Model.Role roles=new Model.Role();
		ArrayList<String>Role_data=roles.getRoles();
		comboBox.setModel(new DefaultComboBoxModel<>(Role_data.toArray()));
		
		textField = new JTextField();
		textField.setBounds(188, 137, 116, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(189, 201, 115, 22);
		contentPane.add(passwordField);
		
		JButton btnLogin = new JButton("LogIn");
		btnLogin.setBounds(260, 264, 97, 25);
		contentPane.add(btnLogin);
	}
}
