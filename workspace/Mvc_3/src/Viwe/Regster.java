package Viwe;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPasswordField;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Regster extends JFrame {


	private JPanel contentPane;
	private JPasswordField password;
	private JPasswordField confirmPassword;
	private JTextField name;
	private JLabel masaageBox;
	public JLabel picBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Regster frame = new Regster();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Regster() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 396);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(148, 0, 211));
		contentPane.setForeground(new Color(0, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblRegsterLevel = new JLabel("Regster Level");
		lblRegsterLevel.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		lblRegsterLevel.setBounds(137, 13, 108, 25);
		contentPane.add(lblRegsterLevel);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		lblName.setBounds(23, 70, 59, 25);
		contentPane.add(lblName);
		
		JLabel lblPosition = new JLabel("Password");
		lblPosition.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		lblPosition.setBounds(23, 108, 108, 25);
		contentPane.add(lblPosition);
		
		JLabel lblConfirmPassword = new JLabel("Confirm password");
		lblConfirmPassword.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		lblConfirmPassword.setBounds(23, 146, 138, 25);
		contentPane.add(lblConfirmPassword);
		
		JLabel lblPicture = new JLabel("Picture");
		lblPicture.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		lblPicture.setBounds(23, 184, 72, 25);
		contentPane.add(lblPicture);
		
		JLabel lblRoll = new JLabel("Roll");
		lblRoll.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
		lblRoll.setBounds(23, 266, 77, 25);
		contentPane.add(lblRoll);
		
		JButton regster = new JButton("Register");
		regster.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String pass1 =confirmPassword.getText().toString();
				String pass2=password.getText().toString();
				if(pass2.equals(pass1)){
					masaageBox.setText("regstraion Success!");
					/*Picture picIn=new Picture();
					String pk=ImageIcon icon=new ImageIcon(picIn);*/
					//JOptionPane.showMessageDialog(null, "Restration Is successfull!");
				}
				
				else{
					//JOptionPane.showMessageDialog(null, "Sorry cheak your password!");
					masaageBox.setText("Sorry cheak your password!");
				}
			}
		});
		regster.setBounds(301, 298, 97, 25);
		contentPane.add(regster);
		
		password = new JPasswordField();
		password.setBounds(156, 110, 168, 22);
		contentPane.add(password);
		
		confirmPassword = new JPasswordField();
		confirmPassword.setBounds(156, 148, 168, 22);
		contentPane.add(confirmPassword);
		
		picBox = new JLabel("");
		picBox.setBounds(107, 184, 108, 73);
		contentPane.add(picBox);
		
		JButton browsePic = new JButton("Browose a picture");
		browsePic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Picture picture=new Picture();
				picture.setVisible(true);
			}
		});
		browsePic.setBounds(227, 230, 147, 25);
		contentPane.add(browsePic);
		
		name = new JTextField();
		name.setBounds(156, 72, 168, 22);
		contentPane.add(name);
		name.setColumns(10);
		
		JComboBox roll = new JComboBox();
		roll.setBounds(112, 269, 103, 22);
		contentPane.add(roll);
		Model.Role roles=new Model.Role();
		ArrayList<String>Role_data=roles.getRoles();
		roll.setModel(new DefaultComboBoxModel<>(Role_data.toArray()));
		
		masaageBox = new JLabel("");
		masaageBox.setBackground(Color.BLUE);
		masaageBox.setForeground(Color.GREEN);
		masaageBox.setBounds(336, 151, 84, 16);
		contentPane.add(masaageBox);
		
	}
}
