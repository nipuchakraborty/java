package viwe;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.nio.channels.SelectableChannel;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Toolkit;

public class MainAPp extends JFrame {

	private JPanel contentPane;
	private JTextField price;
	Buy buy=new Buy();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainAPp frame = new MainAPp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainAPp() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\NK CHAKRABORTY\\Desktop\\booklet.gif"));
		setBackground(Color.BLACK);
		setAlwaysOnTop(true);
		setBounds(100, 100, 535, 511);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(Color.YELLOW);
		menuBar.setForeground(Color.RED);
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("File");
		menuBar.add(mnNewMenu);
		
		JMenuItem file = new JMenuItem("File");
		
		
		mnNewMenu.add(file);
		
		JMenuItem mntmOpen = new JMenuItem("Open");
		mnNewMenu.add(mntmOpen);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mnNewMenu.add(mntmExit);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GREEN);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSelectYourBook = new JLabel("Select your book ");
		lblSelectYourBook.setFont(new Font("Tahoma", Font.PLAIN, 36));
		lblSelectYourBook.setBounds(96, 13, 307, 81);
		contentPane.add(lblSelectYourBook);
		
		JLabel lblNewLabel = new JLabel("Book name");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel.setBounds(53, 152, 115, 22);
		contentPane.add(lblNewLabel);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblPrice.setBounds(53, 236, 56, 16);
		contentPane.add(lblPrice);
		
		JComboBox bookname = new JComboBox();
		bookname.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				
				int selected=bookname.getSelectedIndex();
				switch(selected){
					case 0: break;
					case 1:price.setText("600");break;
					
					case 2:price.setText("700");break;
					case 3:price.setText("550");break;
					case 4:price.setText("780");break;
					case 5:price.setText("390");break;
					case 6:price.setText("440");break;
				}
		
			}
		});
		bookname.setModel(new DefaultComboBoxModel(new String[] {"Selct One", "Java", "English", "Bangla", "Physics", "Accouting"}));
		bookname.setBounds(195, 154, 153, 22);
		contentPane.add(bookname);
		
		price = new JTextField();
		price.setBounds(195, 235, 116, 22);
		contentPane.add(price);
		price.setColumns(10);
		
		JLabel lblTk = new JLabel("Tk/-");
		lblTk.setBounds(336, 238, 56, 16);
		contentPane.add(lblTk);
		
		JButton knprice = new JButton("Buy");
		knprice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				buy.setVisible(true);
				MainAPp.this.setVisible(false);
				
			}
		});
		
		knprice.setBounds(306, 338, 115, 25);
		contentPane.add(knprice);
	}
		}
