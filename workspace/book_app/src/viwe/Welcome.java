package viwe;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.HierarchyBoundsAdapter;
import java.awt.event.HierarchyEvent;
import java.awt.Toolkit;

public class Welcome extends JFrame {

	protected static final int LENGTH_LONG =200;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Welcome frame = new Welcome();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Welcome() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\NK CHAKRABORTY\\Desktop\\booklet.gif"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 534, 367);
		contentPane = new JPanel();
		contentPane.setForeground(Color.RED);
		contentPane.setBackground(Color.CYAN);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Welcome");
		lblNewLabel.setOpaque(true);
		lblNewLabel.setIgnoreRepaint(true);
		lblNewLabel.setFocusTraversalPolicyProvider(true);
		lblNewLabel.setFocusCycleRoot(true);
		lblNewLabel.setDoubleBuffered(true);
		lblNewLabel.setForeground(Color.MAGENTA);
		lblNewLabel.setFont(new Font("Sitka Heading", Font.BOLD, 31));
		lblNewLabel.setBounds(190, 139, 133, 40);
		contentPane.add(lblNewLabel);
		
		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 Timer timer = new Timer(Welcome.this.LENGTH_LONG, null);
				 
				MainAPp mainAPp=new MainAPp();
				mainAPp.setVisible(true);
				Welcome.this.setVisible(false);
			}
		});
		btnStart.setBounds(205, 241, 97, 25);
		contentPane.add(btnStart);
	}

}
