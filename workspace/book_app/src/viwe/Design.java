package viwe;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import org.eclipse.wb.swing.FocusTraversalOnArray;

import controller.JSplash;
import controller.LoginController;
import model.Login;

import java.awt.Component;
import javax.swing.border.MatteBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JToggleButton;

public class Design extends JFrame {

	private JPanel contentPane;
	private JTextField name;
	private JPasswordField passwords;
	private JLabel error_msg;
	private JLabel error_masg2;
	private JLabel lblPassword ;
	private JButton btnRegister;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					 UIManager.setLookAndFeel("com.java.swing.plaf.napkinlaf.NapkinLookAndFeel");
				
					//UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
					Design frame = new Design();
					
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Design() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\NK CHAKRABORTY\\Desktop\\booklet.gif"));
		setBackground(Color.CYAN);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 560, 538);
		contentPane = new JPanel();
		contentPane.setAutoscrolls(true);
		contentPane.setBackground(Color.GREEN);
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0), 5, true));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Logn In pannel");
		lblNewLabel.setForeground(Color.MAGENTA);
		lblNewLabel.setFont(new Font("ThimbaDisplaySSi", Font.PLAIN, 18));
		lblNewLabel.setBounds(202, 37, 144, 29);
		contentPane.add(lblNewLabel);
		
		JLabel lblName = new JLabel("Name ");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblName.setForeground(Color.GREEN);
		lblName.setBounds(78, 162, 56, 16);
		contentPane.add(lblName);
		
		name = new JTextField();
		
		name.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			
			/*	if(!name.getText().equals("Nipu chakraborty")){
					error_msg.setText("Enter valid Password");
					
					
				}*/
			}
		});
		name.setBounds(175, 161, 228, 22);
		contentPane.add(name);
		name.setColumns(10);
		
		passwords = new JPasswordField();
		passwords.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				/*if (!password.getText().equals("27263486")) {
					error_masg2.setText("Enter valid Password");
				password.setEditable(isDisplayable());
				
					
				}*/
			}
		});
		passwords.setBounds(175, 235, 227, 22);
		contentPane.add(passwords);
		
		lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblPassword.setForeground(Color.GREEN);
		lblPassword.setBounds(78, 238, 100, 16);
		contentPane.add(lblPassword);
		
		
		
		
		
		
		JButton login = new JButton("Login");
		login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String user_name=name.getText();
				String password=passwords.getText();
				LoginController loginController=new LoginController();
				loginController.LogIn(user_name, password);
				Welcome welcome=new Welcome();
				welcome.setVisible(true);
				
			
				
		
		}});
		login.setBounds(332, 338, 97, 25);
		contentPane.add(login);
		
		error_msg = new JLabel("");
		error_msg.setBackground(Color.RED);
		error_msg.setForeground(Color.RED);
		error_msg.setBounds(185, 195, 218, 27);
		contentPane.add(error_msg);
		
		error_masg2 = new JLabel("");
		error_masg2.setBackground(Color.RED);
		error_masg2.setForeground(Color.RED);
		error_masg2.setBounds(185, 270, 218, 27);
		contentPane.add(error_masg2);
		
		btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String user_name=name.getText();
				String password=passwords.getText();
				LoginController loginController =new LoginController();
				loginController.Reg(user_name, password);
			}
		});
		btnRegister.setBounds(202, 336, 90, 28);
		contentPane.add(btnRegister);
		
		JLabel lblDelete = new JLabel("Delete");
		lblDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				MainAPp mainAPp=new MainAPp();
				mainAPp.setVisible(true);
			}
		});
		lblDelete.setIcon(new ImageIcon("C:\\Users\\Nk chakraborty\\Desktop\\SDC\\ic_delete.png"));
		lblDelete.setBounds(217, 405, 88, 48);
		contentPane.add(lblDelete);
		
		JToggleButton tglbtnNewToggleButton = new JToggleButton("New toggle button");
		tglbtnNewToggleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(tglbtnNewToggleButton.isSelected()){
					((JPasswordField) passwords).setEchoChar((char)0);
				}
				else{
					((JPasswordField) passwords).setEchoChar('*');
				}
			}
		});
		tglbtnNewToggleButton.setBounds(364, 284, 127, 28);
		contentPane.add(tglbtnNewToggleButton);
		
		
		
		
		contentPane.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{lblNewLabel, lblName, name, passwords, lblPassword, login, error_msg, error_masg2}));
	}
}
