package View.Login;

import javax.swing.JOptionPane;

import Model.Login_m;
import View.Dashboard_JFrm;

public class Login_cntrl {

	public Login_cntrl(String un, String ps) {
		
		Login_m Login_m=new Login_m();
		String result=Login_m.login_data_chek(un, ps);
		
		if(result==null){
			System.out.println("No");
			JOptionPane.showMessageDialog(null, "Login Failed");
			
		}
		else{
			System.out.println("yes");
			Dashboard_JFrm Dashboard_JFrm=new Dashboard_JFrm();
			Dashboard_JFrm.setVisible(true);
		}
	}

}
