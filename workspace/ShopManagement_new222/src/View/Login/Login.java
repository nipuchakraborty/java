package View.Login;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login {

	private JFrame frame;
	private JTextField user;
	private JTextField passw;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblLogin = new JLabel("Login \r\n");
		lblLogin.setBounds(203, 49, 46, 14);
		frame.getContentPane().add(lblLogin);
		
		user = new JTextField();
		user.setBounds(124, 74, 207, 20);
		frame.getContentPane().add(user);
		user.setColumns(10);
		
		JLabel lblUsername = new JLabel("Username ::");
		lblUsername.setBounds(55, 77, 59, 14);
		frame.getContentPane().add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password ::");
		lblPassword.setBounds(55, 113, 67, 14);
		frame.getContentPane().add(lblPassword);
		
		passw = new JTextField();
		passw.setColumns(10);
		passw.setBounds(124, 110, 207, 20);
		frame.getContentPane().add(passw);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String un=user.getText();
				String ps=passw.getText();
				new Login_cntrl(un,ps);
				
			}
		});
		btnLogin.setBounds(171, 159, 89, 23);
		frame.getContentPane().add(btnLogin);
	}
}
