package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import View.All_panel.Add_category;
import View.All_panel.Create_Purchase;
import View.All_panel.Create_Sale;
import View.All_panel.Create_bill;

import View.All_panel.Product_name;
import View.All_panel.Units;


import javax.swing.JDesktopPane;
import java.awt.Color;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import java.awt.GridLayout;


public class Dashboard_JFrm extends JFrame {

	private JPanel contentPane;
	private JDesktopPane desktopPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(new SyntheticaAluOxideLookAndFeel());
					Dashboard_JFrm frame = new Dashboard_JFrm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Dashboard_JFrm() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Dashboard_JFrm.class.getResource("/resource/sp (4).png")));
		setBackground(new Color(0, 51, 153));
		setTitle("Shop Management");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(230, 150, 850, 484);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnHome = new JMenu("Home");
		mnHome.setIcon(new ImageIcon(Dashboard_JFrm.class.getResource("/resource/home (2).png")));
		menuBar.add(mnHome);
		
		JMenuItem mntmAddCatagory = new JMenuItem("Add Catagory");
		mntmAddCatagory.setMnemonic(KeyEvent.VK_O);
		mntmAddCatagory.setIcon(new ImageIcon(Dashboard_JFrm.class.getResource("/resource/cat.png")));
		mntmAddCatagory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				JInternalFrame JIF=new JInternalFrame("Category",false,true,false,true);
				
				Add_category Add_category=new Add_category();
				JIF.getContentPane().add(Add_category);
				JIF.pack();
				desktopPane.add(JIF);
				setBackground (new Color(0, 51, 153));
				JIF.setVisible(true);
				
				
				
			}
		});
		mnHome.add(mntmAddCatagory);
		
		JMenuItem mntmAddProduct = new JMenuItem("Add Product");
		mntmAddProduct.setMnemonic('b');
		mntmAddProduct.setMnemonic(KeyEvent.VK_D);
		mntmAddProduct.setIcon(new ImageIcon(Dashboard_JFrm.class.getResource("/resource/sp (9).png")));
		mntmAddProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JInternalFrame JIF=new JInternalFrame("Product Category",false,true,false,true);
				Product_name Product_name=new Product_name();
				JIF.getContentPane().add(Product_name);
				JIF.pack();
				
				desktopPane.add(JIF);
				JIF.setVisible(true);
			}
		});
		mnHome.add(mntmAddProduct);
		
		JMenuItem mntmAddUnit = new JMenuItem("Add Unit");
		mntmAddUnit.setIcon(new ImageIcon(Dashboard_JFrm.class.getResource("/resource/unit.gif")));
		mntmAddUnit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame JIF=new JInternalFrame("Add Units",false,true,false,true);
				Units unit=new Units();
				JIF.getContentPane().add(unit);
				JIF.pack();
				
				desktopPane.add(JIF);
				JIF.setVisible(true);
			}
		});
		mnHome.add(mntmAddUnit);
		
		JMenu mnBillingInfo = new JMenu("Billing Info");
		mnBillingInfo.setIcon(new ImageIcon(Dashboard_JFrm.class.getResource("/resource/bill.png")));
		menuBar.add(mnBillingInfo);
		
		JMenuItem mntmCreateCashMemo = new JMenuItem("Create Bill");
		mntmCreateCashMemo.setIcon(new ImageIcon(Dashboard_JFrm.class.getResource("/resource/sp (11).png")));
		mntmCreateCashMemo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame JIF=new JInternalFrame("All Units",false,true,false,true);
				Create_Sale Create_Sale=new Create_Sale();
				JIF.getContentPane().add(Create_Sale);
				JIF.pack();
				
				desktopPane.add(JIF);
				JIF.setVisible(true);
			}
		});
		mnBillingInfo.add(mntmCreateCashMemo);
		
		JMenuItem mntmP = new JMenuItem("Create Purchase");
		mntmP.setIcon(new ImageIcon(Dashboard_JFrm.class.getResource("/resource/sp (12).png")));
		mntmP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame JIF=new JInternalFrame("All Units",false,true,false,true);
				Create_Purchase Create_Purchase=new Create_Purchase();
				JIF.getContentPane().add(Create_Purchase);
				JIF.pack();
				
				desktopPane.add(JIF);
				JIF.setVisible(true);
			}
		});
		mnBillingInfo.add(mntmP);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        try {
		        	 image = ImageIO.read(new File("C:/Users/Nk chakraborty/Desktop/u.jpg"));
		        	// image = ImageIO.read(new File( Home.class.getResource("/resource/sp (12).png"));
		        	
		        	//image = ImageIO.read(new URL("https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/396567/1160/772/m1/fpnw/wm0/flat_vector_shop-01-.jpg?1426020217&s=3550b21a0ae6ce4120676f9069322f63"));
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(new MigLayout("", "[]", "[]"));
	}
}
