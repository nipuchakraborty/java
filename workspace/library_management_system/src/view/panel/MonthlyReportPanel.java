package view.panel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;

import model.IssueBook;
import model.ReturnBook;

import javax.swing.JLabel;
import javax.swing.JTable;

public class MonthlyReportPanel extends JPanel {
	public static JTable issue_table;
	public static JTable return_table;

	/**
	 * Create the panel.
	 */
	public MonthlyReportPanel() {
		setLayout(new MigLayout("", "[grow][][][][][][][][][][][][][][][][][][grow]", "[grow][][][][][][][][][][][][][][][grow]"));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		add(panel_1, "cell 0 0 19 8,grow");
		panel_1.setLayout(new MigLayout("", "[grow]", "[][grow]"));
		
		JLabel lblIssueBook = new JLabel("Issue Book");
		panel_1.add(lblIssueBook, "cell 0 0");
		
		JScrollPane scrollPane_1 = new JScrollPane();
		panel_1.add(scrollPane_1, "cell 0 1,grow");
		
		issue_table = new JTable();
		IssueBook.load();
		scrollPane_1.setViewportView(issue_table);
		
		JPanel panel = new JPanel();
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		add(panel, "cell 0 8 19 8,grow");
		panel.setLayout(new MigLayout("", "[grow]", "[][grow]"));
		
		JLabel lblReturnBook = new JLabel("Return Book");
		panel.add(lblReturnBook, "cell 0 0");
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, "cell 0 1,grow");
		
		return_table = new JTable();
		ReturnBook.load();
		scrollPane.setViewportView(return_table);

	}

}
