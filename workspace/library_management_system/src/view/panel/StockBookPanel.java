package view.panel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import model.Book;
import model.ReturnAbleBook;
import model.ReturnBook;
import model.StockBook;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class StockBookPanel extends JPanel {
	public static JTable table;
	private JTextField serch_txt;

	/**
	 * Create the panel.
	 */
	public StockBookPanel() {
		setLayout(new MigLayout("", "[grow][][][grow][][][][][][][][][][][][][][][][][][][grow]", "[grow][][][][][][][][][][][][][][][][][grow]"));
		
//		serch_txt = new JTextField();
//		add(serch_txt, "cell 3 0 8 1,growx");
//		serch_txt.setColumns(10);
//		
//		JButton btnSearch = new JButton("search");
//		btnSearch.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent arg0) {
//				String book=serch_txt.getText();
//				System.out.println(book);
//				StockBook.loadSearch(book);
//			}
//		});
//		add(btnSearch, "cell 12 0");
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, "cell 0 1 23 16,grow");
		
		table = new JTable();
		StockBook.load();
		scrollPane.setViewportView(table);

	}

}
