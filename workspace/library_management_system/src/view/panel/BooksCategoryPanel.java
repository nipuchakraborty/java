package view.panel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import view.edit.BooksCategoryEidtDailog;
import view.message.MessageDailog;

import javax.swing.JLabel;
import javax.swing.JTextField;

import controller.BookCategoryController;
import model.BookCategory;
import model.Status;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import java.awt.Color;

public class BooksCategoryPanel extends JPanel {
	private JTextField cat_txt;
	 public static JTable table;

	/**
	 * Create the panel.
	 */
	public BooksCategoryPanel() {
		setBackground(new Color(255, 255, 255));
		setLayout(new MigLayout("", "[][][][][][][grow][][][][][][][][][][][][grow][grow][grow]", "[][][][][][][][][][][][][][grow][][][grow]"));
		
		JLabel lblCat = new JLabel("Category");
		lblCat.setFont(new Font("Serif", Font.PLAIN, 12));
		lblCat.setForeground(Color.BLACK);
		add(lblCat, "cell 3 2");
		
		cat_txt = new JTextField();
		cat_txt.setFont(new Font("Serif", Font.PLAIN, 12));
		cat_txt.setForeground(Color.BLACK);
		add(cat_txt, "cell 6 2 9 1,growx");
		cat_txt.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Status");
		lblNewLabel.setFont(new Font("Serif", Font.PLAIN, 12));
		lblNewLabel.setForeground(Color.BLACK);
		add(lblNewLabel, "cell 3 4");
		
		JComboBox status_com = new JComboBox();
		status_com.setFont(new Font("Serif", Font.PLAIN, 12));
		status_com.setForeground(Color.BLACK);
		ArrayList<String> status=Status.getStatus();
		status_com.setModel(new DefaultComboBoxModel<>(status.toArray()));
		add(status_com, "cell 6 4 9 1,growx");
		
		JButton btnSave = new JButton("save");
		btnSave.setFont(new Font("Serif", Font.PLAIN, 12));
		btnSave.setForeground(Color.BLACK);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//get data
				String category=cat_txt.getText();
				String status=status_com.getSelectedItem().toString();
				
				if (status.equals("Select One")) {
					MessageDailog dailog=new MessageDailog();
					dailog.message_label.setText("Change status.");
					dailog.setVisible(true);
				}else {
					// dailog box
					MessageDailog dailog=new MessageDailog();
					dailog.message_label.setText("Category added successfully...!");
					dailog.setVisible(true);
					
					// data send to controller
					new BookCategoryController(category, status);
					BookCategory.load();
					
					//
					cat_txt.setText("");
				}
			}
		});
		add(btnSave, "cell 6 6 2 1");
		
		JPanel panel_1 = new JPanel();
		panel_1.setForeground(new Color(0, 139, 139));
		add(panel_1, "cell 0 7 21 8,grow");
		panel_1.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane, "cell 0 0,grow");
		
		table = new JTable();
		table.setFont(new Font("Serif", Font.PLAIN, 12));
		table.setForeground(Color.BLACK);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row=table.getSelectedRow();
				String fristValue=table.getModel().getValueAt(row, 0).toString();
				String secendValue=table.getModel().getValueAt(row, 1).toString();
				
				int id=BookCategory.getCategoryId(fristValue);
				BooksCategoryEidtDailog dailog=new BooksCategoryEidtDailog(fristValue,secendValue,id);
				Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
				dailog.setBounds((dimension.width-900)/2,(dimension.height-700)/2,600,400);
				dailog.setVisible(true);
			}
		});
		BookCategory.load();
		scrollPane.setViewportView(table);

	}

}
