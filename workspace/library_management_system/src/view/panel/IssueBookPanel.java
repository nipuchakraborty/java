package view.panel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import view.message.MessageDailog;

import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JTextField;

import model.Book;
import model.BookCategory;
import model.Department;
import model.ReturnAbleBook;
import model.Semester;
import model.Shift;
import model.Student;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;

import controller.IssueBookController;
import controller.ReturnableBookController;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import org.jdesktop.swingx.JXDatePicker;

public class IssueBookPanel extends JPanel {
	private JTextField s_id_txt;
	private JTextField self_txt;
	
	public JTextField name_txt;
	public JTextField dpt_txt;
	public JTextField semester_txt;
	public JTextField shift_txt;
	public JTextField book_txt;
	
	private JComboBox book_com;
	public JLabel name_label;
	public JLabel department_label;
	public JLabel semester_label;
	public JLabel shift_label;
	public JLabel book_label;
	public JButton btnIssue;
	public JLabel pic_label;
	
	private String book;
	private JLabel lblDate;
	public JTextField date_txt;

	/**
	 * Create the panel.
	 */
	public IssueBookPanel() {
		setBorder(new CompoundBorder(new CompoundBorder(null, new EtchedBorder(EtchedBorder.LOWERED, null, null)), null));
		setLayout(null);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(322, 228, 1, 2);
		add(separator);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(9, 11, 356, 539);
		add(panel_2);
		ArrayList<String> category=BookCategory.getCategoryName();
		
		JButton btnEnter = new JButton("Enter");
		btnEnter.setBounds(126, 286, 59, 23);
		btnEnter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String student_id=s_id_txt.getText();
				int s_id=Integer.parseInt(student_id);
				
				String s_name=Student.getStudentName(s_id);
				int d_s_id=Student.getStudentId(s_name);
				
				int dpt_id=Student.getDepartmentId(d_s_id);
				String dpt=Department.getDepartment(dpt_id);
				
				int semester_id=Student.getSemesterId(d_s_id);
				String semester=Semester.getSemester(semester_id);
				
				int shift_id=Student.getShiftId(d_s_id);
				String shift=Shift.getShift(shift_id);
				
				String image=Student.getImage(d_s_id);
				
				//from return able book
				// return able id
				//retun_able_book_id!=0
//				int retun_able_id=ReturnAbleBook.getStudentId_FromReturn(s_name);
//				int retun_able_book_id=ReturnAbleBook.get_return_able_book_id(s_id);
				
				//get row from returnable book by id
				int row=ReturnAbleBook.getrowInfo(d_s_id);
						
				if (s_id!=d_s_id) {
					MessageDailog dailog=new MessageDailog();
					dailog.message_label.setText("your student id isn't currect..");
					dailog.setVisible(true);
					
					// hide component
					name_txt.hide();
					dpt_txt.hide();
					semester_txt.hide();
					shift_txt.hide();
					book_txt.hide();
					
					pic_label.hide();
					name_label.hide();
					department_label.hide();
					semester_label.hide();
					shift_label.hide();
					book_label.hide();
					btnIssue.hide();
				}
				else if (row==3) {
					MessageDailog dailog=new MessageDailog();
					dailog.message_label.setText("you already issue 3 book..");
					dailog.setVisible(true);
					
					// hide component
					name_txt.hide();
					dpt_txt.hide();
					semester_txt.hide();
					shift_txt.hide();
					book_txt.hide();
					
					pic_label.hide();
					name_label.hide();
					department_label.hide();
					semester_label.hide();
					shift_label.hide();
					book_label.hide();
					btnIssue.hide();
				}
				else {
					//showing component
					pic_label.show();
					name_label.show();
					department_label.show();
					semester_label.show();
					shift_label.show();
					book_label.show();
					
					name_txt.show();
					dpt_txt.show();
					semester_txt.show();
					shift_txt.show();
					book_txt.show();
					btnIssue.show();
					
					// showing data on component
					
					pic_label.setIcon(new ImageIcon(image));
					name_txt.setText(s_name);
					dpt_txt.setText(dpt);
					semester_txt.setText(semester);
					shift_txt.setText(shift);
					
					String all_book=book;
					book_txt.setText(all_book);
					
				}
				
				
			}
		});
		
		 book_com = new JComboBox();
		 book_com.setBounds(123, 186, 191, 20);
		 book_com.addItemListener(new ItemListener() {
		 	public void itemStateChanged(ItemEvent e) {
		 		
		 		book=book_com.getSelectedItem().toString();
		 		int b_id=Book.getBookId(book);
		 		
		 		int b_self_no=Book.getSelfNo(b_id);
		 		String self=Integer.toString(b_self_no);
				self_txt.setText(self);
				self_txt.setEditable(false);
		 	}
		 });
		 
		 JComboBox cat_com = new JComboBox();
		 cat_com.setBounds(123, 146, 191, 20);
		 cat_com.addItemListener(new ItemListener() {
		 	public void itemStateChanged(ItemEvent arg0) {
		 		String category=cat_com.getSelectedItem().toString();
		 		int cat_id=BookCategory.getCategoryId(category);
		 		
		 		ArrayList<String> b_name=new ArrayList<>();
		 		b_name.addAll(Book.giveCatIdTakeBookName(cat_id));
		 		book_com.setModel(new DefaultComboBoxModel<>(b_name.toArray()));
		 		
		 		book=book_com.getSelectedItem().toString();
		 		int b_id=Book.getBookId(book);
		 		
		 		int b_self_no=Book.getSelfNo(b_id);
		 		String self=Integer.toString(b_self_no);
				self_txt.setText(self);
				self_txt.setEditable(false);
		 		
		 	}
		 });
		 panel_2.setLayout(null);
		 
		 lblDate = new JLabel("Date");
		 lblDate.setBounds(39, 68, 51, 14);
		 panel_2.add(lblDate);
		 
		 date_txt = new JTextField();
		 date_txt.setBounds(123, 65, 191, 20);
		 date_txt.setEditable(false);
		 panel_2.add(date_txt);
		 date_txt.setColumns(10);
		 
		 JLabel lblStudentId = new JLabel("Student Id");
		 lblStudentId.setBounds(39, 108, 74, 14);
		 panel_2.add(lblStudentId);
		 
		 s_id_txt = new JTextField();
		 s_id_txt.setBounds(123, 105, 191, 20);
		 panel_2.add(s_id_txt);
		 s_id_txt.setColumns(10);
		 
		 JLabel lblCategory = new JLabel("Category");
		 lblCategory.setBounds(39, 149, 74, 14);
		 panel_2.add(lblCategory);
		 cat_com.setModel(new DefaultComboBoxModel<>(category.toArray()));
		 panel_2.add(cat_com);
		 
		 JLabel lblBook = new JLabel("Book ");
		 lblBook.setBounds(39, 189, 45, 14);
		 panel_2.add(lblBook);
		 panel_2.add(book_com);
		
		JLabel lblNewLabel = new JLabel("Self no");
		lblNewLabel.setBounds(39, 236, 51, 14);
		panel_2.add(lblNewLabel);
		
		self_txt = new JTextField();
		self_txt.setBounds(121, 233, 193, 20);
		self_txt.setEditable(false);
		panel_2.add(self_txt);
		self_txt.setColumns(10);
		panel_2.add(btnEnter);
		
		JPanel issue_panel = new JPanel();
		issue_panel.setBounds(375, 11, 366, 539);
		add(issue_panel);
		 issue_panel.setLayout(null);
		 
		 name_label = new JLabel("Name");
		 name_label.setBounds(10, 301, 75, 14);
		 issue_panel.add(name_label);
		 
		 name_txt = new JTextField();
		 name_txt.setBounds(106, 298, 250, 20);
		 name_txt.setEditable(false);
		 issue_panel.add(name_txt);
		 name_txt.setColumns(10);
		 
		  department_label = new JLabel("Department");
		  department_label.setBounds(10, 337, 75, 20);
		  issue_panel.add(department_label);
		  
		  dpt_txt = new JTextField();
		  dpt_txt.setBounds(106, 337, 250, 20);
		  dpt_txt.setEditable(false);
		  issue_panel.add(dpt_txt);
		  dpt_txt.setColumns(10);
		  
		   semester_label = new JLabel("Semester");
		   semester_label.setBounds(10, 372, 75, 20);
		   issue_panel.add(semester_label);
		   
		   semester_txt = new JTextField();
		   semester_txt.setBounds(106, 372, 250, 20);
		   semester_txt.setEditable(false);
		   issue_panel.add(semester_txt);
		   semester_txt.setColumns(10);
		   
		   shift_label = new JLabel("Shfit");
		   shift_label.setBounds(10, 406, 75, 14);
		   issue_panel.add(shift_label);
		   
		   shift_txt = new JTextField();
		   shift_txt.setBounds(106, 403, 250, 20);
		   shift_txt.setEditable(false);
		   issue_panel.add(shift_txt);
		   shift_txt.setColumns(10);
		   
		    book_label = new JLabel("Book ");
		    book_label.setBounds(10, 445, 75, 14);
		    issue_panel.add(book_label);
		    
		    book_txt = new JTextField();
		    book_txt.setBounds(106, 442, 250, 20);
		    book_txt.setEditable(false);
		    issue_panel.add(book_txt);
		    book_txt.setColumns(10);
		    
		     btnIssue = new JButton("Issue");
		     btnIssue.setBounds(106, 487, 85, 23);
		     btnIssue.addActionListener(new ActionListener() {
		     	public void actionPerformed(ActionEvent arg0) {
		     		String date=date_txt.getText();
		     		String id=s_id_txt.getText();
		     		String name=name_txt.getText();
		     		String dpt=dpt_txt.getText();
		     		String semester=semester_txt.getText();
		     		String shift=shift_txt.getText();
		     		
		     		MessageDailog dailog=new MessageDailog();
					dailog.message_label.setText(book+" is Issued Successfuly..");
					dailog.setVisible(true);
		     				
		     		new IssueBookController(date,id,name,dpt,semester,shift,book);
		     		new ReturnableBookController(date,id,name,book);
		     		ReturnAbleBook.load();
		     		
		     		// hide componet
		     		pic_label.hide();
		     		name_txt.hide();
					dpt_txt.hide();
					semester_txt.hide();
					shift_txt.hide();
					book_txt.hide();
					
					name_label.hide();
					department_label.hide();
					semester_label.hide();
					shift_label.hide();
					book_label.hide();
					btnIssue.hide();
		     		
		     	}
		     });
		     issue_panel.add(btnIssue);
		     
		     pic_label = new JLabel("");
		     pic_label.setBounds(53, 11, 256, 254);
		     issue_panel.add(pic_label);

	}
}
