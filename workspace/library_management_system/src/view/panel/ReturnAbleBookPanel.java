package view.panel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import view.edit.BookEditDailog;
import view.edit.ProfileDailog;
import view.sendMail.Mailer;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import model.Department;
import model.NotReturn;
import model.ReturnAbleBook;
import model.ReturnBook;
import model.Semester;
import model.Shift;
import model.Student;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ReturnAbleBookPanel extends JPanel {
	public static JTable table;

	/**
	 * Create the panel.
	 */
	public ReturnAbleBookPanel() {
		setLayout(new MigLayout("", "[grow][][][][][][][][][][][][][][][grow]", "[grow][][][][][][][][][][][][][][][][grow]"));
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, "cell 0 0 16 14,grow");
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row=table.getSelectedRow();
				String fristValue=table.getModel().getValueAt(row, 0).toString();
				String secendValue=table.getModel().getValueAt(row, 1).toString();
				String thirdValue=table.getModel().getValueAt(row, 2).toString();
				String fourthValue=table.getModel().getValueAt(row, 3).toString();
				
				int s_id=Integer.parseInt(fourthValue);
				String name=Student.getStudentName(s_id);
				System.out.println(s_id);
				int dpt_id=Student.getDepartmentId(s_id);
				String dpt=Department.getDepartment(dpt_id);
				
				int semester_id=Student.getSemesterId(s_id);
				String semester=Semester.getSemester(semester_id);
				
				int shift_id=Student.getShiftId(s_id);
				String shift=Shift.getShift(shift_id);
				
				String image=Student.getImage(s_id);
				
				int d_s_id=Student.getStudentId(name);
				
				
//				if (s_id==d_s_id) {
//					ProfileDailog dailog=new ProfileDailog(fristValue,secendValue,thirdValue,fourthValue);
//					dailog.id_txt.setText(fristValue);
//					dailog.name_txt.setText(name);
//					dailog.dpt_txt.setText(dpt);
//					dailog.semester_txt.setText(semester);
//					dailog.shift_txt.setText(shift);
//					dailog.pic_lb.setIcon(new ImageIcon(image));
//
//					Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
//					dailog.setBounds((dimension.width-900)/2,(dimension.height-700)/2,900,500);
//					dailog.setVisible(true);
//				}
//				else {
//					JOptionPane.showMessageDialog(null, "wrong");
//				}
//				
				
				
			}
		});
		NotReturn.load();
		scrollPane.setViewportView(table);
		
		JButton btnSend = new JButton("send");
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int row=table.getSelectedRow();
				String fristValue=table.getModel().getValueAt(row, 0).toString();
				String secendValue=table.getModel().getValueAt(row, 1).toString();
//				String thirdValue=table.getModel().getValueAt(row, 2).toString();
//				String fourthValue=table.getModel().getValueAt(row, 3).toString();
				
				int s_id=Integer.parseInt(fristValue);
				String email=Student.getEmail(s_id);
				System.out.println(s_id);
				System.out.println(email);
				Mailer.send("fs.shafi253@gmail.com","fardinkhan",email,"send","Your didn't return your issue book.");
				JOptionPane.showMessageDialog(null, "email send successfully..");
			}
		});
		add(btnSend, "cell 8 15");

	}

}
