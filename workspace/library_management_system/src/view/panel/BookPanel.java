package view.panel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import view.edit.BookEditDailog;
import view.message.MessageDailog;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import controller.BookController;
import model.Book;
import model.BookCategory;
import model.Status;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JScrollPane;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import java.awt.Color;

public class BookPanel extends JPanel {
	private JTextField b_name_txt;
	private JTextField writer_txt;
	private JTextField quentity_txt;
	private JTextField serial_no_txt;
	public static JTable table;
	private JTextField self_no_txt;
	private JTextField prize_txt;
	private JTextField issue_prize_txt;

	/**
	 * Create the panel.
	 */
	public BookPanel() {
		setForeground(Color.BLACK);
		setBackground(new Color(255, 255, 255));
		setLayout(new MigLayout("", "[][][][][][][][grow][][][][grow]", "[][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][grow][][grow]"));
		
		JLabel lblBookName = new JLabel("Book Name :");
		lblBookName.setFont(new Font("Serif", Font.PLAIN, 12));
		lblBookName.setForeground(Color.BLACK);
		add(lblBookName, "cell 3 3,alignx right");
		
		b_name_txt = new JTextField();
		b_name_txt.setFont(new Font("Serif", Font.PLAIN, 12));
		b_name_txt.setForeground(Color.BLACK);
		add(b_name_txt, "cell 7 3,growx");
		b_name_txt.setColumns(10);
		
		JLabel lblCategory = new JLabel("Category :");
		lblCategory.setFont(new Font("Serif", Font.PLAIN, 12));
		lblCategory.setForeground(Color.BLACK);
		add(lblCategory, "cell 3 5,alignx right");
		
		JComboBox cat_com = new JComboBox();
		cat_com.setFont(new Font("Serif", Font.PLAIN, 12));
		cat_com.setForeground(Color.BLACK);
		ArrayList<String> category=BookCategory.getCategoryName();
		cat_com.setModel(new DefaultComboBoxModel<>(category.toArray()));
		add(cat_com, "cell 7 5,growx");
		
		JLabel lblSelfNo = new JLabel("Writer :");
		lblSelfNo.setFont(new Font("Serif", Font.PLAIN, 12));
		lblSelfNo.setForeground(Color.BLACK);
		add(lblSelfNo, "cell 3 7,alignx right");
		
		writer_txt = new JTextField();
		writer_txt.setFont(new Font("Serif", Font.PLAIN, 12));
		writer_txt.setForeground(Color.BLACK);
		add(writer_txt, "cell 7 7,growx");
		writer_txt.setColumns(10);
		
		JLabel lblQuentity = new JLabel("Quentity :");
		lblQuentity.setFont(new Font("Serif", Font.PLAIN, 12));
		lblQuentity.setForeground(Color.BLACK);
		add(lblQuentity, "cell 3 9,alignx right");
		
		quentity_txt = new JTextField();
		quentity_txt.setFont(new Font("Serif", Font.PLAIN, 12));
		quentity_txt.setForeground(Color.BLACK);
		add(quentity_txt, "cell 7 9,growx");
		quentity_txt.setColumns(10);
		ArrayList<String> status=Status.getStatus();
		
		JLabel lblSelfNo_1 = new JLabel("Self no :");
		lblSelfNo_1.setFont(new Font("Serif", Font.PLAIN, 12));
		lblSelfNo_1.setForeground(Color.BLACK);
		add(lblSelfNo_1, "cell 3 12,alignx right");
		
		self_no_txt = new JTextField();
		self_no_txt.setFont(new Font("Serif", Font.PLAIN, 12));
		self_no_txt.setForeground(Color.BLACK);
		add(self_no_txt, "cell 7 12,growx");
		self_no_txt.setColumns(10);
		
		JLabel lblPrize = new JLabel("Prize :");
		lblPrize.setForeground(Color.BLACK);
		lblPrize.setFont(new Font("Serif", Font.PLAIN, 12));
		add(lblPrize, "cell 3 15,alignx right");
		
		prize_txt = new JTextField();
		prize_txt.setForeground(Color.BLACK);
		prize_txt.setFont(new Font("Serif", Font.PLAIN, 12));
		prize_txt.setColumns(10);
		add(prize_txt, "cell 7 15,growx");
		
		JLabel lblIssuePrize = new JLabel("Issue Prize :");
		lblIssuePrize.setForeground(Color.BLACK);
		lblIssuePrize.setFont(new Font("Serif", Font.PLAIN, 12));
		add(lblIssuePrize, "cell 3 17,alignx right");
		
		issue_prize_txt = new JTextField();
		issue_prize_txt.setForeground(Color.BLACK);
		issue_prize_txt.setFont(new Font("Serif", Font.PLAIN, 12));
		issue_prize_txt.setColumns(10);
		add(issue_prize_txt, "cell 7 17,growx");
		
		JLabel lblStatus = new JLabel("Status :");
		lblStatus.setFont(new Font("Serif", Font.PLAIN, 12));
		lblStatus.setForeground(Color.BLACK);
		add(lblStatus, "cell 3 20,alignx right");
		
		JComboBox status_com = new JComboBox();
		status_com.setFont(new Font("Serif", Font.PLAIN, 12));
		status_com.setForeground(Color.BLACK);
		status_com.setModel(new DefaultComboBoxModel<>(status.toArray()));
		add(status_com, "cell 7 20,growx");
		
		JButton btnSave = new JButton("save");
		btnSave.setFont(new Font("Serif", Font.PLAIN, 12));
		btnSave.setForeground(Color.BLACK);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//get data
				String book=b_name_txt.getText();
				String category=cat_com.getSelectedItem().toString();
				String writer=writer_txt.getText();
				String quentity=quentity_txt.getText();
				String self=self_no_txt.getText();
				String prize=prize_txt.getText();
				String issue_prize=issue_prize_txt.getText();
				String status=status_com.getSelectedItem().toString();
				
				if (category.equals("Select One")) {
					// dailog box
					MessageDailog dailog=new MessageDailog();
					dailog.message_label.setText("Select Book Category.");
					dailog.setVisible(true);
				}
				else {
					// dailog box
					MessageDailog dailog=new MessageDailog();
					dailog.message_label.setText("Book is added successfully...!");
					dailog.setVisible(true);
					
					new BookController(book,category,writer,quentity,self,prize,issue_prize,status);
					Book.load();
					
					//
					b_name_txt.setText("");
					writer_txt.setText("");
					quentity_txt.setText("");
					self_no_txt.setText("");
					prize_txt.setText("");
					issue_prize_txt.setText("");
				}
			}
		});
		add(btnSave, "cell 7 24");
		
		JPanel panel_1 = new JPanel();
		panel_1.setForeground(new Color(0, 139, 139));
		add(panel_1, "cell 0 30 12 1,grow");
		panel_1.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane, "cell 0 0,grow");
		
		table = new JTable();
		table.setFont(new Font("Serif", Font.PLAIN, 12));
		table.setForeground(Color.BLACK);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row=table.getSelectedRow();
				String fristValue=table.getModel().getValueAt(row, 0).toString();
				String secendValue=table.getModel().getValueAt(row, 1).toString();
				String thirdValue=table.getModel().getValueAt(row, 2).toString();
				String fourthValue=table.getModel().getValueAt(row, 3).toString();
				String fivethValue=table.getModel().getValueAt(row, 4).toString();
				String sixthValue=table.getModel().getValueAt(row, 5).toString();
				
				int id=Book.getBookId(fristValue);
				
			//	BookEditDailog dailog=new BookEditDailog(fristValue,secendValue,thirdValue,fourthValue,fivethValue,sixthValue,id);
				Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
				//dailog.setBounds((dimension.width-900)/2,(dimension.height-700)/2,600,400);
			//	dailog.setVisible(true);
			}
		});
		table.setRowSelectionAllowed(true);
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		Book.load();
		scrollPane.setViewportView(table);

	}

}
