package view.panel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import view.edit.ProfileDailog;
import view.frame.TextPrompt;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import model.SearchStudent;
import model.Student;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class StudentListPanel extends JPanel {
	public static JTable table;
	private JTextField search_txt;

	/**
	 * Create the panel.
	 */
	public StudentListPanel() {
		setLayout(null);
		
		search_txt = new JTextField();
		TextPrompt text1=new TextPrompt("Student Name",search_txt);
		text1.changeAlpha(128);
		search_txt.setBounds(260, 13, 256, 38);
		add(search_txt);
		search_txt.setColumns(10);
		
		JButton btnSearch = new JButton("search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String search=search_txt.getText();
				SearchStudent.loadSearch(search);
//				
//				try
//				  { 
//					 int id = Integer.parseInt(search);
//					 SearchStudent.loadSearchInt(id);
//				  }
//
//				 catch(NumberFormatException er)
//				  { 
//					 
//				  }
				}

				
		});
		btnSearch.setIcon(new ImageIcon(StudentListPanel.class.getResource("")));
		btnSearch.setBounds(524, 13, 119, 38);
		add(btnSearch);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 86, 945, 385);
		add(scrollPane);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
//				int row=table.getSelectedRow();
//				String fristValue=table.getModel().getValueAt(row, 0).toString();
//				String secendValue=table.getModel().getValueAt(row, 1).toString();
//				String thirdValue=table.getModel().getValueAt(row, 2).toString();
//				String fourthValue=table.getModel().getValueAt(row, 3).toString();
//				String fivethValue=table.getModel().getValueAt(row, 4).toString();
//				String sixthValue=table.getModel().getValueAt(row, 5).toString();
//				String seventhValue=table.getModel().getValueAt(row, 6).toString();
//				
//				int s_id=Integer.parseInt(fristValue);
//				int id=Student.getId(s_id);
//				String pic=Student.getImage(s_id);
//				
//				ProfileDailog dailog=new ProfileDailog(fristValue, secendValue, thirdValue, fourthValue,fivethValue,sixthValue,seventhValue,pic,id);
//				dailog.setVisible(true);
			}
		});
		Student.load();
		scrollPane.setViewportView(table);
		
		JButton btnView = new JButton("view");
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int row=table.getSelectedRow();
				String fristValue=table.getModel().getValueAt(row, 0).toString();
				String secendValue=table.getModel().getValueAt(row, 1).toString();
				String thirdValue=table.getModel().getValueAt(row, 2).toString();
				String fourthValue=table.getModel().getValueAt(row, 3).toString();
				String fivethValue=table.getModel().getValueAt(row, 4).toString();
				String sixthValue=table.getModel().getValueAt(row, 5).toString();
				String seventhValue=table.getModel().getValueAt(row, 6).toString();
				
				int s_id=Integer.parseInt(fristValue);
				int id=Student.getId(s_id);
				String pic=Student.getImage(s_id);
				
				ProfileDailog dailog=new ProfileDailog(fristValue, secendValue, thirdValue, fourthValue,fivethValue,sixthValue,seventhValue,pic,id);
				// hide
				dailog.image_txt.hide();
				dailog.lblImage.hide();
				dailog.btnBrowse.hide();
				dailog.btnDelete.hide();
				dailog.btnUpdate.hide();
				dailog.chckbxFemale.hide();
				dailog.chckbxMale.hide();
				
				// editable
				dailog.id_txt.setEditable(false);
				dailog.name_txt.setEditable(false);
				dailog.dpt_txt.setEditable(false);
				dailog.semester_txt.setEditable(false);
				dailog.shift_txt.setEditable(false);
				dailog.email_txt.setEditable(false);
				dailog.sex_txt.setEditable(false);
				
				// show 
				dailog.sex_txt = new JTextField();
				dailog.sex_txt.setColumns(10);
				//dailog.sex_txt.setText(sixthValue);
				dailog.sex_txt.setBounds(219, 467, 239, 30);
				dailog.contentPanel.add(dailog.sex_txt);
				
				dailog.setVisible(true);
				System.out.println(sixthValue);
			}
		});
		btnView.setBounds(467, 485, 77, 25);
		add(btnView);

	}
}
