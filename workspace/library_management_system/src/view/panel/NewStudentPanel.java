package view.panel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import view.frame.ChoseFile;
import view.frame.TextPrompt;
import view.message.MessageDailog;

import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JTextField;
import controller.StudentController;
import model.Book;
import model.Department;
import model.Semester;
import model.Shift;
import model.Student;

import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class NewStudentPanel extends JPanel {
	private JTextField student_id_txt;
	private JTextField name_txt;
	private JTextField email_txt;
	public static  JTextField pic_path_txt;

	/**
	 * Create the panel.
	 */
	public NewStudentPanel() {
		setLayout(null);
		
		JLabel lblStudentId = new JLabel("Student Id");
		lblStudentId.setBounds(45, 60, 102, 33);
		add(lblStudentId);
		
		student_id_txt = new JTextField();
		student_id_txt.setBounds(171, 60, 312, 33);
		add(student_id_txt);
		TextPrompt text1=new TextPrompt("enter student id",student_id_txt);
		text1.changeAlpha(128);
		student_id_txt.setColumns(10);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(45, 106, 102, 25);
		add(lblName);
		
		name_txt = new JTextField();
		name_txt.setBounds(171, 106, 312, 30);
		add(name_txt);
		TextPrompt textPrompt=new TextPrompt("enter your name",name_txt);
		textPrompt.changeAlpha(128);
		name_txt.setColumns(10);
		
		JLabel lblEmail = new JLabel("Department");
		lblEmail.setBounds(45, 156, 102, 30);
		add(lblEmail);
		
		JComboBox dpt_com = new JComboBox();
		dpt_com.setBounds(171, 153, 312, 33);
		ArrayList<String> department=Department.getDepartment();
		dpt_com.setModel(new DefaultComboBoxModel<>(department.toArray()));
		add(dpt_com);
		
		JLabel lblSemester = new JLabel("Semester");
		lblSemester.setBounds(45, 212, 102, 33);
		add(lblSemester);
		
		JComboBox semester_com = new JComboBox();
		semester_com.setBounds(171, 209, 312, 36);
		ArrayList<String> semseter=Semester.getSemester();
		semester_com.setModel(new DefaultComboBoxModel<>(semseter.toArray()));
		add(semester_com);
		
		JLabel lblShift = new JLabel("Shift");
		lblShift.setBounds(45, 271, 102, 34);
		add(lblShift);
		
		JComboBox shift_com = new JComboBox();
		shift_com.setBounds(171, 269, 312, 36);
		ArrayList<String> shift=Shift.getShift();
		shift_com.setModel(new DefaultComboBoxModel<>(shift.toArray()));
		add(shift_com);
		
		JLabel lblEmail_1 = new JLabel("Sex");
		lblEmail_1.setBounds(45, 318, 102, 33);
		add(lblEmail_1);
		
		JCheckBox male_chckbx = new JCheckBox("male");
		male_chckbx.setBounds(175, 318, 122, 33);
		add(male_chckbx);
		
		JCheckBox female_chckbx = new JCheckBox("female");
		female_chckbx.setBounds(361, 314, 122, 37);
		add(female_chckbx);
		
		JLabel lblEmail_2 = new JLabel("Email");
		lblEmail_2.setBounds(45, 370, 102, 31);
		add(lblEmail_2);
		
		email_txt = new JTextField();
		email_txt.setBounds(171, 367, 312, 34);
		add(email_txt);
		TextPrompt prompt=new TextPrompt("email",email_txt);
		prompt.changeAlpha(128);
		email_txt.setColumns(10);
		
		JLabel lblImage = new JLabel("Image");
		lblImage.setBounds(44, 423, 102, 30);
		add(lblImage);
		
		pic_path_txt = new JTextField();
		pic_path_txt.setBounds(170, 420, 312, 33);
		TextPrompt text3=new TextPrompt("select image", pic_path_txt);
		text3.changeAlpha(128);
		pic_path_txt.setColumns(10);
		add(pic_path_txt);
		
		JButton btnBrowse = new JButton("browse");
		btnBrowse.setBounds(511, 419, 102, 34);
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ChoseFile choseFile=new ChoseFile();
				choseFile.setVisible(true);
			}
		});
		add(btnBrowse);
		
		JButton btnSave = new JButton("submit");
		btnSave.setBounds(171, 476, 95, 33);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// get data
				String s_id=student_id_txt.getText();
				String name=name_txt.getText();
				String dpt=dpt_com.getSelectedItem().toString();
				String semester=semester_com.getSelectedItem().toString();
				String shift=shift_com.getSelectedItem().toString();
				String sex;
				String num=email_txt.getText();
				String image=pic_path_txt.getText();
				
				if (male_chckbx.isSelected()) {
					sex="Male";
					female_chckbx.setEnabled(false);
				}
				else {
					sex="Female";
					male_chckbx.setEnabled(false);
				}
				
				if (dpt.equals("Select One")) {
					MessageDailog dailog=new MessageDailog();
					dailog.message_label.setText("Select Department.");
					dailog.setVisible(true);
				}
				else if (semester.equals("Select One")) {
					MessageDailog dailog=new MessageDailog();
					dailog.message_label.setText("Select Semester.");
					dailog.setVisible(true);
				}
				else if (shift.equals("Select One")) {
					MessageDailog dailog=new MessageDailog();
					dailog.message_label.setText("Select Shift.");
					dailog.setVisible(true);
				}
				else {
					// dailog box
					MessageDailog dailog=new MessageDailog();
					dailog.message_label.setText("Registration successfull...!");
					dailog.setVisible(true);
					
					new StudentController(s_id,name,dpt,semester,shift,sex,num,image);
					Student.load();
					
					// 
					name_txt.setText("");
					student_id_txt.setText("");
					email_txt.setText("");
					pic_path_txt.setText("");
				}
			
			}
		});
		add(btnSave);
		Student.load();

	}

}
