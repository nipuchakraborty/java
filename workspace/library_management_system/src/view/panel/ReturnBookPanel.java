package view.panel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import view.message.MessageDailog;

import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;

import model.Book;
import model.Department;
import model.IssueBook;
import model.ReturnAbleBook;
import model.Semester;
import model.Shift;
import model.Student;
import model.Utility;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.awt.event.ActionEvent;
import org.jdesktop.swingx.JXDatePicker;

import controller.Return_bookController;

public class ReturnBookPanel extends JPanel {
	private JTextField id_txt;
	public JTextField book_txt;
	public JTextField name_txt;
	public JTextField dpt_txt;
	public JTextField semester_txt;
	public JTextField shift_txt;
	
	public JLabel pic_label;
	public JLabel name_label;
	public JLabel dpt_label;
	public JLabel semester_label;
	public JLabel shift_label;
	public JLabel book_label;
	
	public JButton btnReturn;
	public JLabel i_date_label;
	public JTextField i_date_txt;
	public JLabel r_date_label;
	public JTextField return_date_txt;
	
	private String book;
	/**
	 * Create the panel.
	 */
	public ReturnBookPanel() {
		setLayout(null);
		
		JLabel lblStudetnId = new JLabel("Studetn Id");
		lblStudetnId.setBounds(87, 15, 91, 17);
		add(lblStudetnId);
		
		id_txt = new JTextField();
		id_txt.setBounds(188, 12, 254, 20);
		add(id_txt);
		id_txt.setColumns(10);
		 
		 JButton btnNewButton = new JButton("search");
		 btnNewButton.setBounds(471, 11, 85, 23);
		 btnNewButton.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent e) {
		 		String student_id=id_txt.getText();
		 		int s_id=Integer.parseInt(student_id);
		 		
		 		String s_name=Student.getStudentName(s_id);
		 		int d_s_id=Student.getStudentId(s_name);
		 		
		 		int issue_student_id=IssueBook.getIssueStudentId(s_name);
		 		
		 		int dpt_id=Student.getDepartmentId(d_s_id);
		 		String dpt=Department.getDepartment(dpt_id);
		 		
		 		int semester_id=Student.getSemesterId(d_s_id);
		 		String semester=Semester.getSemester(semester_id);
		 		
		 		int shift_id=Student.getShiftId(d_s_id);
		 		String shift=Shift.getShift(shift_id);
		 		
		 		String image=Student.getImage(d_s_id);
		 		
		 		int book_id=IssueBook.getIssueBookId(d_s_id);
		 		book=Book.getBook(book_id);
//		 		int book1_id=IssueBook.getFristIssueBookId(d_s_id);
//		 		book1=Book.getBook(book1_id);
//		 		int book2_id=IssueBook.getSecendIssueBookId(d_s_id);
//		 		book2=Book.getBook(book2_id);
		 		
		 		String all_book=book;
		 		
		 		String date=IssueBook.getIssueDate(d_s_id);
		 		
		 		// return able id
		 		int retun_able_id=ReturnAbleBook.getStudentId_FromReturn(s_name);
		 		int retun_able_book_id=ReturnAbleBook.get_return_able_book_id(s_id);
		 		//&& s_id==retun_able_id && retun_able_book_id!=0
		 		//curent date
		 		String return_date=Utility.getCurrentDate();
		 		
		 		if (s_id==d_s_id) {
		 			//showing component
		 			i_date_label.show();
		 			r_date_label.show();
		 			pic_label.show();
		 			name_label.show();
		 			dpt_label.show();
		 			semester_label.show();
		 			shift_label.show();
		 			book_label.show();
		 			
		 			i_date_txt.show();
		 			return_date_txt.show();
		 			name_txt.show();
		 			dpt_txt.show();
		 			semester_txt.show();
		 			shift_txt.show();
		 			book_txt.show();
		 			btnReturn.show();
		 			
		 			// showing data on component
		 			pic_label.setIcon(new ImageIcon(image));
		 			i_date_txt.setText(date);
		 			return_date_txt.setText(return_date);
		 			name_txt.setText(s_name);
		 			dpt_txt.setText(dpt);
		 			semester_txt.setText(semester);
		 			shift_txt.setText(shift);
		 			book_txt.setText(all_book);
		 			
		 			
		 		}
	
		 		
		 		else {
		 			MessageDailog dailog=new MessageDailog();
		 			dailog.message_label.setText("your student id isn't currect..");
		 			dailog.setVisible(true);
		 			
		 			// hide component
		 			name_txt.hide();
		 			dpt_txt.hide();
		 			semester_txt.hide();
		 			shift_txt.hide();
		 			book_txt.hide();
		 			i_date_txt.hide();
		 			return_date_txt.hide();
		 			
		 			i_date_label.hide();
		 			r_date_label.hide();
		 			pic_label.hide();
		 			name_label.hide();
		 			dpt_label.hide();
		 			semester_label.hide();
		 			shift_label.hide();
		 			book_label.hide();
		 			btnReturn.hide();
		 		}
		 		
		 		
		 	}
		 });
		 add(btnNewButton);
		
		 pic_label = new JLabel("");
		 pic_label.setBounds(129, 43, 300, 225);
		add(pic_label);
		      
		      i_date_label = new JLabel("Issue Date");
		      i_date_label.setBounds(76, 298, 96, 14);
		      add(i_date_label);
		      
		      i_date_txt = new JTextField();
		      i_date_txt.setBounds(199, 292, 230, 20);
		      i_date_txt.setEditable(false);
		      add(i_date_txt);
		      i_date_txt.setColumns(10);
		      
		      r_date_label = new JLabel("Return Date");
		      r_date_label.setBounds(76, 322, 96, 14);
		      add(r_date_label);
		      
		      return_date_txt = new JTextField();
		      return_date_txt.setBounds(199, 316, 230, 20);
		      return_date_txt.setEditable(false);
		      add(return_date_txt);
		      return_date_txt.setColumns(10);
		      
		       name_label = new JLabel("Name");
		       name_label.setBounds(76, 346, 96, 14);
		       add(name_label);
		      
		      name_txt = new JTextField();
		      name_txt.setBounds(199, 340, 230, 20);
		      name_txt.setEditable(false);
		      add(name_txt);
		      name_txt.setColumns(10);
		    
		     dpt_label = new JLabel("Department");
		     dpt_label.setBounds(76, 374, 96, 14);
		     add(dpt_label);
		    
		    dpt_txt = new JTextField();
		    dpt_txt.setBounds(199, 368, 230, 20);
		    dpt_txt.setEditable(false);
		    add(dpt_txt);
		    dpt_txt.setColumns(10);
		   
		    semester_label = new JLabel("Semester");
		    semester_label.setBounds(76, 398, 96, 14);
		    add(semester_label);
		   
		   semester_txt = new JTextField();
		   semester_txt.setBounds(199, 392, 230, 20);
		   semester_txt.setEditable(false);
		   add(semester_txt);
		   semester_txt.setColumns(10);
		  
		   shift_label = new JLabel("Shift");
		   shift_label.setBounds(76, 422, 96, 14);
		   add(shift_label);
		  
		  shift_txt = new JTextField();
		  shift_txt.setBounds(199, 416, 230, 20);
		  shift_txt.setEditable(false);
		  add(shift_txt);
		  shift_txt.setColumns(10);
		 
		  book_label = new JLabel("Issue Book");
		  book_label.setBounds(76, 446, 96, 14);
		  add(book_label);
		 
		 book_txt = new JTextField();
		 book_txt.setBounds(199, 443, 230, 20);
		 book_txt.setEditable(false);
		 add(book_txt);
		 book_txt.setColumns(10);
		
		 btnReturn = new JButton("Return");
		 btnReturn.setBounds(199, 487, 96, 23);
		 btnReturn.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 		String id=id_txt.getText();
		 		String r_date=return_date_txt.getText();
		 		
		 		String name=name_txt.getText();
		 		String dpt=dpt_txt.getText();
		 		String semester=semester_txt.getText();
		 		String shift=shift_txt.getText();
		 		
		 		MessageDailog dailog=new MessageDailog();
		 		dailog.message_label.setText(book+" is Return Successfuly..");
				dailog.setVisible(true);
				new Return_bookController(id,r_date,name,dpt,semester,shift,book);
				ReturnAbleBook.load();
				
				int s_id=Integer.parseInt(id);
				ReturnAbleBook ableBook=new ReturnAbleBook();
				ableBook.delete(s_id);
				ReturnAbleBook.load();
				
				// hide component
				name_txt.hide();
				dpt_txt.hide();
				semester_txt.hide();
				shift_txt.hide();
				book_txt.hide();
				i_date_txt.hide();
				return_date_txt.hide();
				
				i_date_label.hide();
				r_date_label.hide();
				pic_label.hide();
				name_label.hide();
				dpt_label.hide();
				semester_label.hide();
				shift_label.hide();
				book_label.hide();
				btnReturn.hide();
				
		 		
		 	}
		 });
		 add(btnReturn);

	}

}
