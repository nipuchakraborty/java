package view.login;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

import com.alee.laf.WebLookAndFeel;

import controller.LoginController;
import model.Registration;
import model.Role;
import view.Desh;

import javax.swing.JCheckBox;
import javax.swing.ImageIcon;


public class Login extends JFrame implements ActionListener,MouseListener {
	
	 int w=500;
	    int h=300;
	    JPanel header,center,footer;
	    
	    JComboBox role_com;
	    JLabel l;
	    private JLabel l_3;
	    private JLabel l_2;
	    private JLabel l_1;
	    JButton login,forgotpass;
	    Border border=new EtchedBorder();
	    Font font=new Font("courier new",Font.PLAIN,16);
	    JTextField user_txt,field;
	    Cursor cursor=new Cursor(Cursor.HAND_CURSOR);
	    Statement sta;
	    Font lFont=new Font("courier new",Font.PLAIN,15);
	    JPasswordField pass_txt;
	    Font smallF=new Font("serif",Font.PLAIN,12);
	    JLabel link,fPassword;

	    public String user;
	    public String catagory1;
	    private Connection con;
	    private JLabel label;
	    private JLabel lblRegsistration;
	    
	public Login(){
		super("Libray System");
		getContentPane().setForeground(new Color(0, 139, 139));
		setForeground(new Color(0, 139, 139));
		super.setForeground(new Color(0, 128, 128));
        Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();
        Login.this.setBounds((screen.width-900)/2,(screen.height-700)/2,900,600);
        getContentPane().setLayout(new BorderLayout());
        
        

     // creating panel object
             header=new JPanel();
             header.setForeground(new Color(0, 139, 139));
             center=new JPanel();
             center.setForeground(new Color(0, 139, 139));
             footer=new JPanel();
             footer.setForeground(new Color(0, 139, 139));


     // set layout to panel
             header.setLayout(new FlowLayout());
             footer.setLayout(null);
             center.setLayout(null);
             //center.setBorder(new EtchedBorder(Color.green,Color.blue));



     // panel size
             center.setPreferredSize(new Dimension(screen.width+50,50));
             header.setPreferredSize(new Dimension(screen.width,50));
             footer.setPreferredSize(new Dimension(screen.width,50));
             
       // set background color to panel
             header.setBackground(new Color(255, 255, 255));
             footer.setBackground(new Color(255, 255, 255));
             
       //center.setBackground(new Color(50,200,200));
             center.setBackground(new Color(255, 255, 255)); 
             
             l=new JLabel("LIBRIRY SYSTEM");
             l.setIcon(new ImageIcon("E:\\importent pic\\icon\\library-icon.png"));
             l.setFont(new Font("serif",Font.PLAIN,24));
             l.setForeground(new Color(0, 139, 139));
             header.add(l);
           //component to center....
             l_1=new JLabel("User Name :");
             l_1.setForeground(new Color(0, 139, 139));
             l_1.setIcon(new ImageIcon(Login.class.getResource("/resource/user_login.png")));
                     l_1.setBounds(70, 31, 140, 25);
                     l_1.setFont(font);
                     center.add(l_1);
             l_2=new JLabel("Password  :");
             l_2.setForeground(new Color(0, 139, 139));
             l_2.setIcon(new ImageIcon(Login.class.getResource("/resource/password.jpg")));
                     l_2.setBounds(70, 77, 140, 25);
                     l_2.setFont(font);
                     center.add(l_2);

            user_txt=new JTextField();
            user_txt.setForeground(new Color(0, 139, 139));
            user_txt.setBackground(Color.WHITE);
                     user_txt.setBounds(220, 31, 140, 25);
                     user_txt.setFont(font);
                     center.add(user_txt);

            pass_txt=new JPasswordField();
            pass_txt.setForeground(new Color(0, 139, 139));
           
                     pass_txt.setBounds(220, 77, 140, 25);
                     pass_txt.setFont(font);
                     center.add(pass_txt);

            role_com=new JComboBox();
            role_com.setForeground(new Color(0, 139, 139));
    		role_com.setBounds(220, 116, 140, 25);
    		ArrayList<String> role=Role.getRole();
    		role_com.setModel(new DefaultComboBoxModel<>(role.toArray()));
                     role_com.setFont(lFont);
                     center.add(role_com);

            link=new JLabel("Login");
            link.setIcon(new ImageIcon(Login.class.getResource("/resource/login_butten.png")));
                     link.setForeground(new Color(0, 139, 139));
                     link.setCursor(cursor);
                     link.setBounds(220, 152,61, 25);
                     link.setFont(smallF);
                     center.add(link);
                     link.addMouseListener(new MouseAdapter() {
                          /*   public void mouseClicked(MouseEvent e) {
                             MainFrame obj = new MainFrame(con);
                             obj.setVisible(true);
                             setVisible(false);
                             }*/
                             public void mouseReleased(MouseEvent e) {
                            	 
                            
                             }
                             public void mouseEntered(MouseEvent e) {
                                 link.setForeground(Color.red);
                                 link.setFont(new Font("serif",Font.BOLD,12));
                             }
                             public void mouseExited(MouseEvent e) {
                            	  link.setForeground(new Color(0, 139, 139));
                                 link.setFont(smallF);
                             }
                     	@Override
                     	public void mouseClicked(MouseEvent arg0) {
                     		 link.setForeground(Color.red);
                             link.setFont(new Font("serif",Font.BOLD,12));
                             
                             String user=user_txt.getText();
                             String password=pass_txt.getText();
                             String role=role_com.getSelectedItem().toString();
                             
                             // from database
                             
                            String reg=Registration.getRegistration(user, password);
                     		String pass=Registration.getPassword(user);
                     		String user_name=Registration.getUser(pass);
                     		
                     		if (!password.equals(reg)) {
								
                     			JOptionPane.showMessageDialog(null, "username & password is wrong..!");
                     			user_txt.setBackground(Color.red);
                     			pass_txt.setBackground(Color.red);
                     			
                     			 pass_txt.addMouseListener(new MouseAdapter() {
                                 	@Override
                                 	public void mouseClicked(MouseEvent arg0) {
                                 		
                                 		pass_txt.setBackground(Color.WHITE);
                                 		
                                 	}
                                 	@Override
                                 	public void mouseExited(MouseEvent arg0) {
                                 		
                                 		pass_txt.setBackground(Color.RED);
                                 	}
                                 });
                     			 
                     			 user_txt.addMouseListener(new MouseAdapter() {
                                  	@Override
                                  	public void mouseClicked(MouseEvent arg0) {
                                  		user_txt.setBackground(Color.WHITE);
                                  		
                                  		
                                  	}
                                  	@Override
                                  	public void mouseExited(MouseEvent arg0) {
                                  		user_txt.setBackground(Color.RED);
                                  		
                                  	}
                                  });
                     			
							}
                             
                             
                     		
                     	}
                      });



            l_3=new JLabel("|");
            l_3.setForeground(new Color(0, 139, 139));
                     l_3.setBounds(291, 152,11, 25);
                     l_3.setFont(new Font("serif",Font.BOLD,12));
                     center.add(l_3);
            fPassword=new JLabel("Forget Password");
            fPassword.setIcon(new ImageIcon(Login.class.getResource("/resource/forget_pass.png")));
                     fPassword.setCursor(cursor);
                     fPassword.setBounds(301, 152,121, 25);
                     fPassword.setForeground(new Color(0, 139, 139));
                     fPassword.setFont(smallF);
                     center.add(fPassword);
                     fPassword.addMouseListener(new MouseAdapter()  {
                             public void mouseClicked(MouseEvent e) {
                                 //System.exit(0);
                             }
                             public void mouseReleased(MouseEvent e) {
                                 //center.setBackground(Color.black);
                             }
                             public void mouseEntered(MouseEvent e) {
                                 fPassword.setForeground(Color.red);
                                 fPassword.setFont(new Font("serif",Font.BOLD,12));
                             }
                             public void mouseExited(MouseEvent e) {
                            	  fPassword.setForeground(new Color(0, 139, 139));
                                 fPassword.setFont(smallF);
                             }
                             });









//            Icon icon=new ImageIcon("background.jpg");
//            l=new JLabel(icon);
//            center.add(l);







             getContentPane().add(header,BorderLayout.NORTH);
             getContentPane().add(center,BorderLayout.CENTER);
             
             label = new JLabel("");
             label.setForeground(new Color(0, 139, 139));
             label.addMouseListener(new MouseAdapter() {
             	@Override
             	public void mouseClicked(MouseEvent arg0) {
             		((JPasswordField) pass_txt).setEchoChar((char)0);
             	}
             	@Override
             	public void mouseExited(MouseEvent e) {
             		((JPasswordField) pass_txt).setEchoChar('*');
             	}
             });
             label.setIcon(new ImageIcon(Login.class.getResource("/resource/eye.png")));
             label.setBounds(368, 77, 30, 20);
             center.add(label);
             getContentPane().add(footer,BorderLayout.SOUTH);
             
             JLabel lblExit = new JLabel("Exit");
             lblExit.setForeground(new Color(0, 139, 139));
             lblExit.setFont(new Font("Sylfaen", Font.PLAIN, 12));
             lblExit.addMouseListener(new MouseAdapter() {
             	@Override
             	public void mouseEntered(MouseEvent arg0) {
             		 lblExit.setForeground(Color.red);
                     lblExit.setFont(new Font("serif",Font.BOLD,12));
             	}
             	@Override
             	public void mouseExited(MouseEvent arg0) {
             		  lblExit.setForeground(new Color(0, 139, 139));
                      lblExit.setFont(new Font("Tahoma", Font.PLAIN, 11));
             	}
             	@Override
             	public void mouseClicked(MouseEvent arg0) {
             		Login.this.dispose();
             	}
             });
             lblExit.setIcon(new ImageIcon(Login.class.getResource("/resource/close.png")));
             lblExit.setBounds(436, 22, 54, 28);
             footer.add(lblExit);
             
             lblRegsistration = new JLabel("Regsistration");
             lblRegsistration.setFont(new Font("Sylfaen", Font.PLAIN, 12));
             lblRegsistration.setForeground(new Color(0, 139, 139));
             lblRegsistration.addMouseListener(new MouseAdapter() {
             	@Override
             	public void mouseClicked(MouseEvent arg0) {
             		RegistrationDailog dailog=new RegistrationDailog();
             		dailog.setVisible(true);
             		Login.this.dispose();
             	}
             	@Override
             	public void mouseEntered(MouseEvent arg0) {
             		lblRegsistration.setForeground(Color.red);
             		lblRegsistration.setFont(new Font("serif",Font.BOLD,12));
             	}
             	@Override
             	public void mouseExited(MouseEvent arg0) {
             		 lblRegsistration.setFont(new Font("Tahoma", Font.PLAIN, 11));
                     lblRegsistration.setForeground(new Color(0, 139, 139));
             	}
             });
             lblRegsistration.setIcon(new ImageIcon(Login.class.getResource("/resource/reg.png")));
             lblRegsistration.setBounds(10, 22, 98, 28);
             footer.add(lblRegsistration);
     // end of disign.....








             setResizable(false);
             setUndecorated(true);
             getRootPane().setWindowDecorationStyle( JRootPane.COLOR_CHOOSER_DIALOG);
             setLocation((screen.width-w)/2,(screen.height-h)/2);
             setSize(500,301);
             show();
                  
             
	}
	
	public static void main(String[]args) {
		
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
