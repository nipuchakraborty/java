package view.login;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.RegistrationController;
import model.Role;
import view.message.MessageDailog;

import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RegistrationDailog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField name_txt;
	private JTextField user_txt;
	private JPasswordField pas_txt;
	private JPasswordField c_pass_txt;
	
	private JLabel mes_lb ;
	
	MessageDailog dailog=new MessageDailog();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			RegistrationDailog dialog = new RegistrationDailog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public RegistrationDailog() {
		getContentPane().setForeground(new Color(0, 139, 139));
		setForeground(new Color(0, 139, 139));
		setBackground(new Color(255, 255, 255));
		setModal(true);
		setIconImage(Toolkit.getDefaultToolkit().getImage(RegistrationDailog.class.getResource("/resource/library-icon.png")));
		setBounds(100, 100, 501, 461);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setForeground(new Color(0, 139, 139));
		contentPanel.setBackground(new Color(255, 255, 255));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setForeground(new Color(0, 139, 139));
		panel.setBackground(new Color(255, 255, 255));
		panel.setBounds(0, 0, 495, 63);
		contentPanel.add(panel);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblLibrarySystem = new JLabel("Library System");
		lblLibrarySystem.setIcon(new ImageIcon(RegistrationDailog.class.getResource("/resource/library-icon.png")));
		lblLibrarySystem.setForeground(new Color(0, 139, 139));
		lblLibrarySystem.setFont(new Font("Shonar Bangla", Font.PLAIN, 30));
		panel.add(lblLibrarySystem);
		
		JLabel lblName = new JLabel("Full Name");
		lblName.setForeground(new Color(0, 139, 139));
		lblName.setIcon(new ImageIcon(RegistrationDailog.class.getResource("/resource/name.jpg")));
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblName.setBounds(50, 96, 125, 20);
		contentPanel.add(lblName);
		
		name_txt = new JTextField();
		name_txt.setForeground(new Color(0, 139, 139));
		name_txt.setFont(new Font("Tahoma", Font.PLAIN, 13));
		name_txt.setBounds(198, 96, 214, 20);
		contentPanel.add(name_txt);
		name_txt.setColumns(10);
		
		JLabel lblU = new JLabel("User Name");
		lblU.setForeground(new Color(0, 139, 139));
		lblU.setIcon(new ImageIcon(RegistrationDailog.class.getResource("/resource/user_login.png")));
		lblU.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblU.setBounds(50, 148, 125, 20);
		contentPanel.add(lblU);
		
		user_txt = new JTextField();
		user_txt.setForeground(new Color(0, 139, 139));
		user_txt.setFont(new Font("Tahoma", Font.PLAIN, 13));
		user_txt.setBounds(198, 148, 214, 20);
		contentPanel.add(user_txt);
		user_txt.setColumns(10);
		
		JLabel lblPosition = new JLabel("Password");
		lblPosition.setForeground(new Color(0, 139, 139));
		lblPosition.setIcon(new ImageIcon(RegistrationDailog.class.getResource("/resource/password.jpg")));
		lblPosition.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblPosition.setBounds(50, 195, 125, 20);
		contentPanel.add(lblPosition);
		
		pas_txt = new JPasswordField();
		pas_txt.setForeground(new Color(0, 139, 139));
		pas_txt.setFont(new Font("Tahoma", Font.PLAIN, 13));
		pas_txt.setBounds(198, 195, 214, 20);
		contentPanel.add(pas_txt);
		
		JLabel lblConfirmPassword = new JLabel("Confirm Password");
		lblConfirmPassword.setForeground(new Color(0, 139, 139));
		lblConfirmPassword.setIcon(new ImageIcon(RegistrationDailog.class.getResource("/resource/password.jpg")));
		lblConfirmPassword.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblConfirmPassword.setBounds(50, 237, 142, 20);
		contentPanel.add(lblConfirmPassword);
		
		c_pass_txt = new JPasswordField();
		c_pass_txt.setForeground(new Color(0, 139, 139));
		c_pass_txt.setFont(new Font("Tahoma", Font.PLAIN, 13));
		c_pass_txt.setBounds(198, 237, 214, 20);
		contentPanel.add(c_pass_txt);
		
		JLabel lblPosition_1 = new JLabel("Position");
		lblPosition_1.setForeground(new Color(0, 139, 139));
		lblPosition_1.setIcon(new ImageIcon(RegistrationDailog.class.getResource("/resource/role.png")));
		lblPosition_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblPosition_1.setBounds(60, 277, 115, 26);
		contentPanel.add(lblPosition_1);
		
		JComboBox role_com = new JComboBox();
		role_com.setForeground(new Color(0, 139, 139));
		role_com.setFont(new Font("Tahoma", Font.PLAIN, 13));
		role_com.setBounds(198, 280, 214, 20);
		ArrayList<String> role=Role.getRole();
		role_com.setModel(new DefaultComboBoxModel<>(role.toArray()));
		contentPanel.add(role_com);
		
		JButton btnRegistration = new JButton("Registration");
		btnRegistration.setForeground(new Color(0, 139, 139));
		btnRegistration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String f_name=name_txt.getText();
				String user_name=user_txt.getText();
				String pass=pas_txt.getText();
				String c_pass=c_pass_txt.getText();
				String role=role_com.getSelectedItem().toString();
				
				if (pass.equals(c_pass)) {
					
				//	new RegistrationController(f_name,user_name,pass,role);
					dailog.message_label.setText("Registration successfull..."); 
					dailog.setVisible(true);
					
					name_txt.setText("");
					user_txt.setText("");
					pas_txt.setText("");
					c_pass_txt.setText("");
				}
				else {
					
					mes_lb.setText("cheek your passwor..");
					
				}
			}
		});
		btnRegistration.setBackground(new Color(255, 255, 255));
		btnRegistration.setIcon(new ImageIcon(RegistrationDailog.class.getResource("/resource/reg.png")));
		btnRegistration.setBounds(198, 318, 116, 32);
		contentPanel.add(btnRegistration);
		
		JPanel panel_1 = new JPanel();
		panel_1.setForeground(new Color(0, 139, 139));
		panel_1.setBackground(new Color(255, 255, 255));
		panel_1.setBounds(0, 361, 485, 64);
		contentPanel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblExit = new JLabel("Exit");
		lblExit.setForeground(new Color(0, 139, 139));
		lblExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				RegistrationDailog.this.dispose();
			}
		});
		lblExit.setIcon(new ImageIcon(RegistrationDailog.class.getResource("/resource/close.png")));
		lblExit.setBounds(430, 36, 55, 20);
		panel_1.add(lblExit);
		
		JLabel label_1 = new JLabel("Login");
		label_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Login login=new Login();
				login.setVisible(true);
				RegistrationDailog.this.dispose();
			}
		});
		label_1.setIcon(new ImageIcon(RegistrationDailog.class.getResource("/resource/login_butten.png")));
		label_1.setForeground(new Color(0, 139, 139));
		label_1.setFont(new Font("Serif", Font.PLAIN, 12));
		label_1.setBounds(10, 33, 61, 25);
		panel_1.add(label_1);
		
		JLabel label = new JLabel("");
		label.setForeground(new Color(0, 139, 139));
		label.setIcon(new ImageIcon(RegistrationDailog.class.getResource("/resource/eye.png")));
		label.setBounds(422, 237, 35, 20);
		contentPanel.add(label);
		
		 mes_lb = new JLabel("");
		 mes_lb.setForeground(new Color(0, 139, 139));
		mes_lb.setBounds(198, 173, 214, 20);
		contentPanel.add(mes_lb);
	}
}
