package view.neww;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import view.message.MessageDailog;

import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JTextField;

import model.Book;
import model.BookCategory;
import model.Department;
import model.Issue1;
import model.Issue2;
import model.IssueTemp;
import model.NotReturn;
import model.ReturnAbleBook;
import model.Semester;
import model.Shift;
import model.Student;
import model.Utility;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;

import controller.Issue1Controller;
import controller.IssueBookController;
import controller.IssueTempController;
import controller.NotReturn1Controller;
import controller.NotReturnController;
import controller.ReturnableBookController;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import org.jdesktop.swingx.JXDatePicker;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.text.MessageFormat;

public class IssueBookPanel2 extends JPanel {
	private JTextField s_id_txt;
	private JTextField self_txt;
	
	public JTextField name_txt;
	public JTextField dpt_txt;
	public JTextField semester_txt;
	public JTextField shift_txt;
	
	private JComboBox book_com;
	public JLabel name_label;
	public JLabel department_label;
	public JLabel semester_label;
	public JLabel shift_label;
	public JButton btnIssue;
	public JLabel pic_label;
	
	private String book;
	private JLabel lblDate;
	public JTextField date_txt;
	private JPanel panel;
	public static JTable table;
	public JTextField tatal_txt;
	private JLabel lblTotalPrize;
	public JTextField due_txt;
	public JTextField paid_txt;
	public JLabel lblPaid;
	public JLabel lblDu;
	public JLabel lblReturnDate;
	public JButton btnPrint;
	public JTextField return_date_txt;
	

	/**
	 * Create the panel.
	 */
	public IssueBookPanel2() {
		setBorder(new CompoundBorder(new CompoundBorder(null, new EtchedBorder(EtchedBorder.LOWERED, null, null)), null));
		setLayout(null);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(322, 228, 1, 2);
		add(separator);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(9, 11, 376, 272);
		add(panel_2);
		ArrayList<String> category=BookCategory.getCategoryName();
		
		JButton btnEnter = new JButton("Enter");
		btnEnter.setBounds(118, 227, 74, 32);
		btnEnter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String student_id=s_id_txt.getText();
				int s_id=Integer.parseInt(student_id);
				
				String s_name=Student.getStudentName(s_id);
				int d_s_id=Student.getStudentId(s_name);
				
				int dpt_id=Student.getDepartmentId(d_s_id);
				String dpt=Department.getDepartment(dpt_id);
				
				int semester_id=Student.getSemesterId(d_s_id);
				String semester=Semester.getSemester(semester_id);
				
				int shift_id=Student.getShiftId(d_s_id);
				String shift=Shift.getShift(shift_id);
		
				String image=Student.getImage(d_s_id);
				String date=date_txt.getText();
				
				int book_id=Book.getBookId(book);
				int issue_prize=Book.getIssuePrize(book_id);
				
				int sum=IssueTemp.getTotal();
				String total=Integer.toString(sum);
				
				// return time
				String retunr_time=Utility.getReturnDate();
				
				//System.out.println(sum);
				
				//from return able book
				// return able id
				//retun_able_book_id!=0
//				int retun_able_id=ReturnAbleBook.getStudentId_FromReturn(s_name);
//				int retun_able_book_id=ReturnAbleBook.get_return_able_book_id(s_id);
				
				//get row from returnable book by id
				//int row=ReturnAbleBook.getrowInfo(d_s_id);
				int row=NotReturn.getrowInfo(s_id);
				int row1=IssueTemp.getrowInfo();
				System.out.println(row1);
				if (s_id!=d_s_id && row==3 && row1==2) {
					MessageDailog dailog=new MessageDailog();
					dailog.message_label.setText("your student id isn't currect..");
					dailog.setVisible(true);
					
					// hide component
					name_txt.hide();
					dpt_txt.hide();
					semester_txt.hide();
					shift_txt.hide();
					//book_txt.hide();
					
					pic_label.hide();
					name_label.hide();
					department_label.hide();
					semester_label.hide();
					shift_label.hide();
				//	book_label.hide();
					btnIssue.hide();
				}
				else if (row==3) {
					MessageDailog dailog=new MessageDailog();
					dailog.message_label.setText("you already issue 3 book..");
					dailog.setVisible(true);
					
					// hide component
					name_txt.hide();
					dpt_txt.hide();
					semester_txt.hide();
					shift_txt.hide();
				//	book_txt.hide();
					
					pic_label.hide();
					name_label.hide();
					department_label.hide();
					semester_label.hide();
					shift_label.hide();
				//	book_label.hide();
					btnIssue.hide();
				}
				else {
					//showing component
					pic_label.show();
					name_label.show();
					department_label.show();
					semester_label.show();
					shift_label.show();
				//	book_label.show();
					
					name_txt.show();
					dpt_txt.show();
					semester_txt.show();
					shift_txt.show();
				//	book_txt.show();
					btnIssue.show();
					
					// showing data on component
					
					pic_label.setIcon(new ImageIcon(image));
					name_txt.setText(s_name);
					dpt_txt.setText(dpt);
					semester_txt.setText(semester);
					shift_txt.setText(shift);
					tatal_txt.setText(total);
					return_date_txt.setText(retunr_time);
					new IssueTempController(d_s_id,book,issue_prize);
					IssueTemp.load();
					
				}
				
				
			}
		});
		
		 book_com = new JComboBox();
		 book_com.setBounds(117, 147, 191, 28);
		 book_com.addItemListener(new ItemListener() {
		 	public void itemStateChanged(ItemEvent e) {
		 		
		 		book=book_com.getSelectedItem().toString();
		 		int b_id=Book.getBookId(book);
		 		
		 		int b_self_no=Book.getSelfNo(b_id);
		 		String self=Integer.toString(b_self_no);
				self_txt.setText(self);
				self_txt.setEditable(false);
				
				int sum=IssueTemp.getTotal();
				String total=Integer.toString(sum);
				tatal_txt.setText(total);
		 	}
		 });
		 
		 JComboBox cat_com = new JComboBox();
		 cat_com.setBounds(117, 97, 191, 27);
		 cat_com.addItemListener(new ItemListener() {
		 	public void itemStateChanged(ItemEvent arg0) {
		 		String category=cat_com.getSelectedItem().toString();
		 		int cat_id=BookCategory.getCategoryId(category);
		 		
		 		ArrayList<String> b_name=new ArrayList<>();
		 		b_name.addAll(Book.giveCatIdTakeBookName(cat_id));
		 		book_com.setModel(new DefaultComboBoxModel<>(b_name.toArray()));
		 		
		 		book=book_com.getSelectedItem().toString();
		 		int b_id=Book.getBookId(book);
		 		
		 		int b_self_no=Book.getSelfNo(b_id);
		 		String self=Integer.toString(b_self_no);
				self_txt.setText(self);
				self_txt.setEditable(false);
				
				int issue_prize=Book.getIssuePrize(b_id);
				String prize=Integer.toString(issue_prize);
		
		 	}
		 });
		 panel_2.setLayout(null);
		 
		 lblDate = new JLabel("Date");
		 lblDate.setBounds(33, 14, 51, 27);
		 panel_2.add(lblDate);
		 
		 date_txt = new JTextField();
		 date_txt.setBounds(117, 11, 191, 30);
		 date_txt.setEditable(false);
		 panel_2.add(date_txt);
		 date_txt.setColumns(10);
		 
		 JLabel lblStudentId = new JLabel("Student Id");
		 lblStudentId.setBounds(33, 57, 74, 27);
		 panel_2.add(lblStudentId);
		 
		 s_id_txt = new JTextField();
		 s_id_txt.setBounds(117, 54, 191, 30);
		 panel_2.add(s_id_txt);
		 s_id_txt.setColumns(10);
		 
		 JLabel lblCategory = new JLabel("Category");
		 lblCategory.setBounds(33, 100, 74, 20);
		 panel_2.add(lblCategory);
		 cat_com.setModel(new DefaultComboBoxModel<>(category.toArray()));
		 panel_2.add(cat_com);
		 
		 JLabel lblBook = new JLabel("Book ");
		 lblBook.setBounds(33, 152, 45, 20);
		 panel_2.add(lblBook);
		 panel_2.add(book_com);
		
		JLabel lblNewLabel = new JLabel("Self no");
		lblNewLabel.setBounds(33, 188, 51, 24);
		panel_2.add(lblNewLabel);
		
		self_txt = new JTextField();
		self_txt.setBounds(115, 187, 193, 27);
		self_txt.setEditable(false);
		panel_2.add(self_txt);
		self_txt.setColumns(10);
		panel_2.add(btnEnter);
		
		JButton btnNew = new JButton("New");
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				IssueTemp.truncateTemp();
				IssueTemp.load();
			}
		});
		btnNew.setBounds(228, 228, 74, 30);
		panel_2.add(btnNew);
		
		JPanel issue_panel = new JPanel();
		issue_panel.setBounds(385, 11, 495, 668);
		add(issue_panel);
		 issue_panel.setLayout(null);
		 
		 name_label = new JLabel("Name");
		 name_label.setBounds(12, 318, 133, 30);
		 issue_panel.add(name_label);
		 
		 name_txt = new JTextField();
		 name_txt.setBounds(167, 315, 280, 33);
		 name_txt.setEditable(false);
		 issue_panel.add(name_txt);
		 name_txt.setColumns(10);
		 
		  department_label = new JLabel("Department");
		  department_label.setBounds(12, 361, 133, 29);
		  issue_panel.add(department_label);
		  
		  dpt_txt = new JTextField();
		  dpt_txt.setBounds(167, 361, 280, 29);
		  dpt_txt.setEditable(false);
		  issue_panel.add(dpt_txt);
		  dpt_txt.setColumns(10);
		  
		   semester_label = new JLabel("Semester");
		   semester_label.setBounds(12, 403, 133, 30);
		   issue_panel.add(semester_label);
		   
		   semester_txt = new JTextField();
		   semester_txt.setBounds(167, 403, 280, 30);
		   semester_txt.setEditable(false);
		   issue_panel.add(semester_txt);
		   semester_txt.setColumns(10);
		   
		   shift_label = new JLabel("Shfit");
		   shift_label.setBounds(12, 449, 133, 28);
		   issue_panel.add(shift_label);
		   
		   shift_txt = new JTextField();
		   shift_txt.setBounds(167, 446, 280, 31);
		   shift_txt.setEditable(false);
		   issue_panel.add(shift_txt);
		   shift_txt.setColumns(10);
		    
		     btnIssue = new JButton("Issue");
		     btnIssue.setBounds(167, 615, 85, 30);
		     btnIssue.addActionListener(new ActionListener() {
		     	public void actionPerformed(ActionEvent arg0) {
		     		String i_date=date_txt.getText();
		     		String r_date=return_date_txt.getText();
		     		String id=s_id_txt.getText();
		     		String name=name_txt.getText();
		     		String dpt=dpt_txt.getText();
		     		String semester=semester_txt.getText();
		     		String shift=shift_txt.getText();
		     		String total=tatal_txt.getText();
		     		String paid=paid_txt.getText();
		     		String due=due_txt.getText();
		     		
		     		MessageDailog dailog=new MessageDailog();
					dailog.message_label.setText("Issued Successfuly..");
					dailog.setVisible(true);
		     		
					new Issue1Controller(i_date,r_date,id,total,paid,due);
					new NotReturn1Controller(i_date,r_date,id,total,paid,due);
					new NotReturnController();
					NotReturn.load();
					
			//		IssueTemp.copyTemp();
		    // 		new IssueBookController(date,id,name,dpt,semester,shift,book);
		     	//	new ReturnableBookController(date,id,name,book);
		     	//	ReturnAbleBook.load();
		     		
//		     		// hide componet
//		     		pic_label.hide();
//		     		name_txt.hide();
//					dpt_txt.hide();
//					semester_txt.hide();
//					shift_txt.hide();
//				//	book_txt.hide();
//					
//					name_label.hide();
//					department_label.hide();
//					semester_label.hide();
//					shift_label.hide();
//				//	book_label.hide();
//					btnIssue.hide();
					
					pic_label.setIcon(null);
					name_txt.setText("");
					dpt_txt.setText("");
					semester_txt.setText("");
					shift_txt.setText("");
					tatal_txt.setText("");
					return_date_txt.setText("");
					paid_txt.setText("");
					due_txt.setText("");
					s_id_txt.setText("");
					self_txt.setText("");
		     	}
		     });
		     issue_panel.add(btnIssue);
		     
		     pic_label = new JLabel("");
		     pic_label.setBounds(85, 13, 280, 245);
		     issue_panel.add(pic_label);
		     
		     tatal_txt = new JTextField();
		     tatal_txt.setEditable(false);
		     tatal_txt.setColumns(10);
		     tatal_txt.setBounds(167, 490, 280, 29);
		     issue_panel.add(tatal_txt);
		     
		     lblTotalPrize = new JLabel("Total Prize");
		     lblTotalPrize.setBounds(12, 493, 133, 26);
		     issue_panel.add(lblTotalPrize);
		     
		     due_txt = new JTextField();
		     due_txt.setEditable(false);
		     due_txt.setColumns(10);
		     due_txt.setBounds(167, 574, 280, 28);
		     issue_panel.add(due_txt);
		     
		     paid_txt = new JTextField();
		     paid_txt.addKeyListener(new KeyAdapter() {
		     	@Override
		     	public void keyReleased(KeyEvent arg0) {
		     		String total=tatal_txt.getText();
		     		String paid=paid_txt.getText();
		     		int total_tk=Integer.parseInt(total);
		     		int paid_tk=Integer.parseInt(paid);
		     		int due_tk=total_tk-paid_tk;
		     		String due=Integer.toString(due_tk);
		     		due_txt.setText(due);
		     	}
		     });
		     paid_txt.setColumns(10);
		     paid_txt.setBounds(167, 532, 280, 29);
		     issue_panel.add(paid_txt);
		     
		     lblPaid = new JLabel("Paid");
		     lblPaid.setBounds(12, 535, 133, 26);
		     issue_panel.add(lblPaid);
		     
		     lblDu = new JLabel("Due");
		     lblDu.setBounds(12, 577, 133, 25);
		     issue_panel.add(lblDu);
		     
		     return_date_txt = new JTextField();
		     return_date_txt.setEditable(false);
		     return_date_txt.setColumns(10);
		     return_date_txt.setBounds(165, 269, 282, 33);
		     issue_panel.add(return_date_txt);
		     
		     lblReturnDate = new JLabel("Return Date");
		     lblReturnDate.setBounds(10, 269, 135, 30);
		     issue_panel.add(lblReturnDate);
		     
		     panel = new JPanel();
		     panel.setBounds(9, 296, 375, 327);
		     add(panel);
		     panel.setLayout(null);
		     
		     JScrollPane scrollPane = new JScrollPane();
		     scrollPane.setBounds(10, 11, 355, 265);
		     panel.add(scrollPane);
		     
		     table = new JTable();
		     IssueTemp.load();
		     scrollPane.setViewportView(table);
		     
		     btnPrint = new JButton("Print");
		     btnPrint.setBounds(296, 289, 69, 30);
		     panel.add(btnPrint);
		     btnPrint.addActionListener(new ActionListener() {
		     	public void actionPerformed(ActionEvent e) {
		     		String name=name_txt.getText();
		     		try {
		     			
						MessageFormat heading=new MessageFormat("Name: "+name);
						MessageFormat center=new MessageFormat("als");
						MessageFormat footer=new MessageFormat("this is footer");
						table.print(JTable.PrintMode.FIT_WIDTH,heading,footer);
					} catch (PrinterException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
		     	}
		     });

	}
}
