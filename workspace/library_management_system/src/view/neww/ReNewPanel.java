package view.neww;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import view.frame.TextPrompt;
import view.message.MessageDailog;

import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;

import model.Book;
import model.Department;
import model.Issue1;
import model.IssueBook;
import model.NotReturn;
import model.NotReturn1;
import model.ReturnAbleBook;
import model.Semester;
import model.Shift;
import model.Student;
import model.Utility;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.awt.event.ActionEvent;
import org.jdesktop.swingx.JXDatePicker;

import com.toedter.calendar.JDateChooser;

import controller.RenewController;
import controller.Return1Controller;
import controller.Return2Controller;
import controller.Return_bookController;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.print.PrinterException;
import java.text.MessageFormat;

public class ReNewPanel extends JPanel {
	private JTextField id_txt;
	
	public JLabel pic_label;
	
	private String book;
	public static JTable table;
	private JTextField return_date_txt;
	private JLabel lblReturnDate;
	
	private int s_id;
	public JTextField name_txt;
	public JLabel label_1;
	public JLabel lblTotalDue;
	public JTextField total_txt;
	public JTextField pre_due_txt;
	public JLabel Due;
	public JTextField paid_txt;
	public JLabel lblPaid;
	public JTextField d_txt;
	public JLabel lblDue;
	public JButton btn_return;
	private JLabel lblRenewDate;
	private JDateChooser renew_date;
	private JButton btnPrint;
	/**
	 * Create the panel.
	 */
	public ReNewPanel() {
		setLayout(null);
		
		id_txt = new JTextField();
		TextPrompt text1=new TextPrompt("Student Id",id_txt);
		text1.changeAlpha(128);
		id_txt.setBounds(578, 14, 160, 36);
		add(id_txt);
		id_txt.setColumns(10);
		 
		 JButton btnNewButton = new JButton("search");
		 btnNewButton.setBounds(748, 13, 85, 37);
		 btnNewButton.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent e) {
		 		String student_id=id_txt.getText();
		 		s_id=Integer.parseInt(student_id);
		 		
		 		String s_name=Student.getStudentName(s_id);
		 		int d_s_id=Student.getStudentId(s_name);
		 		
		 		int issue_student_id=IssueBook.getIssueStudentId(s_name);
		 		
		 		int dpt_id=Student.getDepartmentId(d_s_id);
		 		String dpt=Department.getDepartment(dpt_id);
		 		
		 		int semester_id=Student.getSemesterId(d_s_id);
		 		String semester=Semester.getSemester(semester_id);
		 		
		 		int shift_id=Student.getShiftId(d_s_id);
		 		String shift=Shift.getShift(shift_id);
		 		
		 		String image=Student.getImage(d_s_id);
		 		
		 		int book_id=IssueBook.getIssueBookId(d_s_id);
		 		book=Book.getBook(book_id);
//		 		int book1_id=IssueBook.getFristIssueBookId(d_s_id);
//		 		book1=Book.getBook(book1_id);
//		 		int book2_id=IssueBook.getSecendIssueBookId(d_s_id);
//		 		book2=Book.getBook(book2_id);
		 		
		 		String all_book=book;
		 		String date=IssueBook.getIssueDate(d_s_id);
		 		
		 		int due_tk=Issue1.getDue(d_s_id);
		 		String due=Integer.toString(due_tk);
		 	//	System.out.println(due);
		 		// return able id
		 		int retun_able_id=ReturnAbleBook.getStudentId_FromReturn(s_name);
		 		int retun_able_book_id=ReturnAbleBook.get_return_able_book_id(s_id);
		 		//&& s_id==retun_able_id && retun_able_book_id!=0
		 		//curent date
		 		String return_date=Utility.getCurrentDate();
		 		
		 		//get total of not return issue
		 		int total_tk=NotReturn1.getTotal(s_id);
		 		int total_due_tk=total_tk+due_tk;
		 		String total_due=Integer.toString(total_due_tk);
		 		
		 		// get return date
		 		String r_date=NotReturn1.getReturnDate(s_id);
		 		
		 		if (s_id==d_s_id) {
		 			//showing component
		 		
		 			// showing data on component
		 			
		 			pic_label.setIcon(new ImageIcon(image));
		 			name_txt.setText(s_name);
		 			pre_due_txt.setText(due);
		 			return_date_txt.setText(r_date);
		 			total_txt.setText(total_due);
		 			NotReturn.loadForReturnById_renew(s_id);
		 			
		 		}
	
		 		
		 		else {
		 			MessageDailog dailog=new MessageDailog();
		 			dailog.message_label.setText("your student id isn't currect..");
		 			dailog.setVisible(true);
		 			
		 			// hide component
		 			
		 		}
		 		
		 		
		 	}
		 });
		 add(btnNewButton);
		
		 pic_label = new JLabel("");
		 pic_label.setBounds(12, 86, 300, 294);
		add(pic_label);
		 
		 JScrollPane scrollPane = new JScrollPane();
		 scrollPane.setBounds(29, 436, 804, 146);
		 add(scrollPane);
		 
		 table = new JTable();
		 scrollPane.setViewportView(table);
		 
		 return_date_txt = new JTextField();
		 return_date_txt.setEditable(false);
		 return_date_txt.setColumns(10);
		 return_date_txt.setBounds(476, 129, 357, 31);
		 add(return_date_txt);
		 
		 lblReturnDate = new JLabel("Return Date");
		 lblReturnDate.setBounds(343, 129, 105, 31);
		 add(lblReturnDate);
		 
		 btn_return = new JButton("Renew");
		 btn_return.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent e) {
		 		String id=id_txt.getText();
		 		int s_id=Integer.parseInt(id);
		 		String date=return_date_txt.getText();
		 		Date renew_d=renew_date.getDate();
		 		String total=total_txt.getText();
		 		String paid=paid_txt.getText();
		 		String due=d_txt.getText();
		 		new RenewController(renew_d,total,paid,due,s_id);
//		 		new Return1Controller(s_id,date);
//		 		new Return2Controller(s_id);
//		 		
//		 		NotReturn notReturn=new NotReturn();
//		 		notReturn.delete(s_id);
		 		
		 		//set text
		 		name_txt.setText("");
		 		pic_label.setIcon(null);
		 		total_txt.setText("");
		 		return_date_txt.setText("");
		 		paid_txt.setText("");
		 		d_txt.setText("");
		 	}
		 });
		 btn_return.setBounds(744, 595, 89, 38);
		 add(btn_return);
		 
		 name_txt = new JTextField();
		 name_txt.setEditable(false);
		 name_txt.setColumns(10);
		 name_txt.setBounds(476, 86, 357, 30);
		 add(name_txt);
		 
		 label_1 = new JLabel("Name");
		 label_1.setBounds(343, 84, 106, 32);
		 add(label_1);
		 
		 lblTotalDue = new JLabel("Total");
		 lblTotalDue.setBounds(343, 266, 106, 32);
		 add(lblTotalDue);
		 
		 total_txt = new JTextField();
		 total_txt.setEditable(false);
		 total_txt.setColumns(10);
		 total_txt.setBounds(476, 266, 357, 32);
		 add(total_txt);
		 
		 pre_due_txt = new JTextField();
		 pre_due_txt.setEditable(false);
		 pre_due_txt.setColumns(10);
		 pre_due_txt.setBounds(476, 216, 357, 37);
		 add(pre_due_txt);
		 
		 Due = new JLabel("Previous Due");
		 Due.setBounds(343, 216, 106, 37);
		 add(Due);
		 
		 paid_txt = new JTextField();
		 paid_txt.addKeyListener(new KeyAdapter() {
		 	@Override
		 	public void keyReleased(KeyEvent e) {
		 		String pre=total_txt.getText();
		 		int pre_tk=Integer.parseInt(pre);
		 		String p=paid_txt.getText();
		 		int p_tk=Integer.parseInt(p);
		 		int d_tk=pre_tk-p_tk;
		 		String d=Integer.toString(d_tk);
		 		d_txt.setText(d);
		 		
		 		String duee=d_txt.getText();
		 		int duee_tk=Integer.parseInt(duee);
		 		
		 	}
		 });
		 paid_txt.setColumns(10);
		 paid_txt.setBounds(476, 311, 357, 35);
		 add(paid_txt);
		 
		 lblPaid = new JLabel("Paid");
		 lblPaid.setBounds(343, 311, 106, 35);
		 add(lblPaid);
		 
		 d_txt = new JTextField();
		 d_txt.setEditable(false);
		 d_txt.setColumns(10);
		 d_txt.setBounds(476, 360, 357, 40);
		 add(d_txt);
		 
		 lblDue = new JLabel("Due");
		 lblDue.setBounds(343, 360, 106, 40);
		 add(lblDue);
		 
		 lblRenewDate = new JLabel("Renew Date");
		 lblRenewDate.setBounds(343, 173, 105, 30);
		 add(lblRenewDate);
		 
		 renew_date = new JDateChooser();
		 renew_date.setBounds(476, 173, 357, 30);
		 add(renew_date);
		 
		 btnPrint = new JButton("Print");
		 btnPrint.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 		String id=id_txt.getText();
		 		try {
	     			
					MessageFormat heading=new MessageFormat("Student Id: "+id);
					MessageFormat center=new MessageFormat("als");
					MessageFormat footer=new MessageFormat("this is footer");
					table.print(JTable.PrintMode.FIT_WIDTH,heading,footer);
				} catch (PrinterException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		 	}
		 });
		 btnPrint.setBounds(29, 595, 79, 38);
		 add(btnPrint);
		 
//		 JDateChooser dateChooser = new JDateChooser();
//		 dateChooser.setBounds(476, 142, 213, 20);
//			contentPane.add(dateChooser);

	}
}
