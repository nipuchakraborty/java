package view.neww;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import view.frame.TextPrompt;
import view.message.MessageDailog;

import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;

import model.Book;
import model.Department;
import model.Issue1;
import model.IssueBook;
import model.NotReturn;
import model.NotReturn1;
import model.ReturnAbleBook;
import model.Semester;
import model.Shift;
import model.Student;
import model.Utility;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.awt.event.ActionEvent;
import org.jdesktop.swingx.JXDatePicker;

import controller.Return1Controller;
import controller.Return2Controller;
import controller.Return_bookController;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.print.PrinterException;
import java.text.MessageFormat;

public class ReturnBookPanel2 extends JPanel {
	private JTextField id_txt;
	
	public JLabel pic_label;
	
	private String book;
	public static JTable table;
	private JTextField return_date_txt;
	private JLabel lblReturnDate;
	
	private int s_id;
	public JTextField name_txt;
	public JTextField dpt_txt;
	public JLabel label;
	public JLabel label_1;
	public JLabel label_2;
	public JLabel label_3;
	public JTextField semester_txt;
	public JTextField shift_txt;
	public JTextField due_txt;
	public JLabel Due;
	public JTextField paid_txt;
	public JLabel lblPaid;
	public JTextField d_txt;
	public JLabel lblDue;
	public JButton btn_return;
	/**
	 * Create the panel.
	 */
	public ReturnBookPanel2() {
		setLayout(null);
		
		id_txt = new JTextField();
		TextPrompt text1=new TextPrompt("Student Id",id_txt);
		text1.changeAlpha(128);
		id_txt.setBounds(587, 14, 160, 30);
		add(id_txt);
		id_txt.setColumns(10);
		 
		 JButton btnNewButton = new JButton("search");
		 btnNewButton.setBounds(757, 13, 85, 31);
		 btnNewButton.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent e) {
		 		String student_id=id_txt.getText();
		 		s_id=Integer.parseInt(student_id);
		 		
		 		String s_name=Student.getStudentName(s_id);
		 		int d_s_id=Student.getStudentId(s_name);
		 		
		 		int issue_student_id=IssueBook.getIssueStudentId(s_name);
		 		
		 		int dpt_id=Student.getDepartmentId(d_s_id);
		 		String dpt=Department.getDepartment(dpt_id);
		 		
		 		int semester_id=Student.getSemesterId(d_s_id);
		 		String semester=Semester.getSemester(semester_id);
		 		
		 		int shift_id=Student.getShiftId(d_s_id);
		 		String shift=Shift.getShift(shift_id);
		 		
		 		String image=Student.getImage(d_s_id);
		 		
		 		int book_id=IssueBook.getIssueBookId(d_s_id);
		 		book=Book.getBook(book_id);
//		 		int book1_id=IssueBook.getFristIssueBookId(d_s_id);
//		 		book1=Book.getBook(book1_id);
//		 		int book2_id=IssueBook.getSecendIssueBookId(d_s_id);
//		 		book2=Book.getBook(book2_id);
		 		
		 		String all_book=book;
		 		String date=IssueBook.getIssueDate(d_s_id);
		 		
		 		int due_tk=Issue1.getDue(s_id);
		 		String due=Integer.toString(due_tk);
		 		// return able id
		 		int retun_able_id=ReturnAbleBook.getStudentId_FromReturn(s_name);
		 		int retun_able_book_id=ReturnAbleBook.get_return_able_book_id(s_id);
		 		//&& s_id==retun_able_id && retun_able_book_id!=0
		 		//curent date
		 		String return_date=Utility.getCurrentDate();
		 		
		 		int not_return1_id=NotReturn1.getNotReurn1_id(s_id);
		 		int id=NotReturn1.getStudentId(not_return1_id);
		 		
		 		if (s_id==d_s_id && s_id==id) {
		 			//showing component
		 		
		 			// showing data on component
		 			
		 			pic_label.setIcon(new ImageIcon(image));
		 			return_date_txt.setText(return_date);
		 			name_txt.setText(s_name);
		 			dpt_txt.setText(dpt);
		 			semester_txt.setText(semester);
		 			shift_txt.setText(shift);
		 			due_txt.setText(due);
		 			NotReturn.loadForReturnById(s_id);
		 			NotReturn.load();
		 			
		 		}
		 		else {
		 			MessageDailog dailog=new MessageDailog();
		 			dailog.message_label.setText("your student id isn't currect..");
		 			dailog.setVisible(true);
		 			
		 			// hide component
		 			
		 		}
		 		
		 		
		 	}
		 });
		 add(btnNewButton);
		
		 pic_label = new JLabel("");
		 pic_label.setBounds(12, 81, 300, 264);
		add(pic_label);
		 
		 JScrollPane scrollPane = new JScrollPane();
		 scrollPane.setBounds(26, 428, 816, 155);
		 add(scrollPane);
		 
		 table = new JTable();
		 scrollPane.setViewportView(table);
		 
		 return_date_txt = new JTextField();
		 return_date_txt.setEditable(false);
		 return_date_txt.setColumns(10);
		 return_date_txt.setBounds(568, 81, 274, 28);
		 add(return_date_txt);
		 
		 lblReturnDate = new JLabel("Return Date");
		 lblReturnDate.setBounds(397, 81, 140, 28);
		 add(lblReturnDate);
		 
		 btn_return = new JButton("Return");
		 btn_return.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent e) {
		 		String id=id_txt.getText();
		 		int s_id=Integer.parseInt(id);
		 		String date=return_date_txt.getText();
		 		new Return1Controller(s_id,date);
		 		
		 		new Return2Controller(s_id);
		 		
		 		NotReturn1 notReturn1=new NotReturn1();
		 		notReturn1.delete(s_id);
		 
		 		NotReturn notReturn=new NotReturn();
		 		notReturn.delete(s_id);
		 		//set text
		 		name_txt.setText("");
		 		dpt_txt.setText("");
		 		semester_txt.setText("");
		 		shift_txt.setText("");
		 		due_txt.setText("");
		 		paid_txt.setText("");
		 		d_txt.setText("");
		 	}
		 });
		 btn_return.setBounds(742, 596, 100, 34);
		 add(btn_return);
		 
		 name_txt = new JTextField();
		 name_txt.setEditable(false);
		 name_txt.setColumns(10);
		 name_txt.setBounds(568, 124, 274, 27);
		 add(name_txt);
		 
		 dpt_txt = new JTextField();
		 dpt_txt.setEditable(false);
		 dpt_txt.setColumns(10);
		 dpt_txt.setBounds(568, 164, 274, 28);
		 add(dpt_txt);
		 
		 label = new JLabel("Department");
		 label.setBounds(397, 167, 140, 25);
		 add(label);
		 
		 label_1 = new JLabel("Name");
		 label_1.setBounds(397, 122, 140, 29);
		 add(label_1);
		 
		 label_2 = new JLabel("Semester");
		 label_2.setBounds(397, 208, 140, 28);
		 add(label_2);
		 
		 label_3 = new JLabel("Shfit");
		 label_3.setBounds(397, 252, 140, 28);
		 add(label_3);
		 
		 semester_txt = new JTextField();
		 semester_txt.setEditable(false);
		 semester_txt.setColumns(10);
		 semester_txt.setBounds(568, 205, 274, 31);
		 add(semester_txt);
		 
		 shift_txt = new JTextField();
		 shift_txt.setEditable(false);
		 shift_txt.setColumns(10);
		 shift_txt.setBounds(568, 249, 274, 31);
		 add(shift_txt);
		 
		 due_txt = new JTextField();
		 due_txt.setEditable(false);
		 due_txt.setColumns(10);
		 due_txt.setBounds(568, 293, 274, 30);
		 add(due_txt);
		 
		 Due = new JLabel("Previous Due");
		 Due.setBounds(397, 293, 140, 30);
		 add(Due);
		 
		 paid_txt = new JTextField();
		 paid_txt.addKeyListener(new KeyAdapter() {
		 	@Override
		 	public void keyReleased(KeyEvent e) {
		 		String pre=due_txt.getText();
		 		int pre_tk=Integer.parseInt(pre);
		 		String p=paid_txt.getText();
		 		int p_tk=Integer.parseInt(p);
		 		int d_tk=pre_tk-p_tk;
		 		String d=Integer.toString(d_tk);
		 		d_txt.setText(d);
		 		
		 		String duee=d_txt.getText();
		 		int duee_tk=Integer.parseInt(duee);
		 		if (pre_tk-p_tk==0) {
					btn_return.show();
				}
		 		else {
					btn_return.hide();
				}
		 		
		 	}
		 });
		 paid_txt.setColumns(10);
		 paid_txt.setBounds(568, 336, 274, 29);
		 add(paid_txt);
		 
		 lblPaid = new JLabel("Paid");
		 lblPaid.setBounds(397, 336, 140, 29);
		 add(lblPaid);
		 
		 d_txt = new JTextField();
		 d_txt.setEditable(false);
		 d_txt.setColumns(10);
		 d_txt.setBounds(568, 378, 274, 37);
		 add(d_txt);
		 
		 lblDue = new JLabel("Due");
		 lblDue.setBounds(397, 378, 140, 37);
		 add(lblDue);
		 
		 JButton btnPrint = new JButton("Print");
		 btnPrint.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent e) {
		 		String id=id_txt.getText();
		 		try {
	     			
					MessageFormat heading=new MessageFormat("Student Id: "+id);
					MessageFormat center=new MessageFormat("als");
					MessageFormat footer=new MessageFormat("this is footer");
					table.print(JTable.PrintMode.FIT_WIDTH,heading,footer);
				} catch (PrinterException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		 	}
		 });
		 btnPrint.setBounds(36, 594, 66, 36);
		 add(btnPrint);

	}
}
