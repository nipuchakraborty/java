package view.allFrame;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.BackUp;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


import javax.swing.JOptionPane;

import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class ReStoreFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	
	String Path = "";
    JFileChooser f = null;
	public static String getBackUpPath() {
		String backUpPath = "";
	    JFileChooser fc = null;
        
        if (fc == null) {
               fc = new JFileChooser();
               fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
               fc.setAcceptAllFileFilterUsed(false);
       }
       int returnVal = fc.showDialog(null, "Open");
       if (returnVal == JFileChooser.APPROVE_OPTION) {
           File file = fc.getSelectedFile();
           backUpPath = file.getAbsolutePath();
       }
      return backUpPath;
}
	
	public ReStoreFrame() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 260);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(10, 79, 313, 27);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnBrows = new JButton("Click To ReStore");
		btnBrows.setFont(new Font("SansSerif", Font.PLAIN, 14));
		btnBrows.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String backuppath=textField.getText();
				 String dbUserName="root";
					String dbPassword ="";
					String source="E:/fardin/backUp";
					String[] restoreCmd = new String[]{"C:/xampp/mysql/bin/", "--user=" + dbUserName, "--password=" + dbPassword, "-e", "source " + source};

				        Process runtimeProcess;
				        try {

				            runtimeProcess = Runtime.getRuntime().exec(restoreCmd);
				            int processComplete = runtimeProcess.waitFor();

				            if (processComplete == 0) {
				                System.out.println("Restored successfully!");
				            } else {
				                System.out.println("Could not restore the backup!");
				            }
				        } catch (Exception ex) {
				            ex.printStackTrace();
				        }
				      
			}
		});
		btnBrows.setBounds(119, 135, 144, 27);
		contentPane.add(btnBrows);
		
		JButton btnChoose = new JButton("Browse");
		btnChoose.setFont(new Font("SansSerif", Font.PLAIN, 14));
		btnChoose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				textField.setText(getBackUpPath());
			}
		});
		btnChoose.setBounds(331, 79, 93, 27);
		contentPane.add(btnChoose);
		
	}
}
