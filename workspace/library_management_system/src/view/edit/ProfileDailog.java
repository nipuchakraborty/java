package view.edit;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Student;
import view.frame.ChoseFile;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;

public class ProfileDailog extends JDialog {

	public final JPanel contentPanel = new JPanel();
	public static JTextField id_txt;
	public static JTextField name_txt;
	public static JTextField shift_txt;
	public static JTextField dpt_txt;
	public static JTextField semester_txt;
	public static JTextField email_txt;
	public static JTextField image_txt;
	public static JTextField sex_txt;
	public static JLabel pic_lb ;
	public static JLabel lblImage;
	public static JButton btnBrowse ;
	public static JButton btnUpdate ;
	public static JButton btnDelete;
	public static JCheckBox chckbxMale ;
	public static JCheckBox chckbxFemale; 

	public ProfileDailog(String fristValue, String secendValue, String thirdValue, String fourthValue, String fivethValue, String sixthValue, String seventhValue,String pic, int id) {
		setBounds(100, 100, 602, 724);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblName = new JLabel("Student Id");
		lblName.setBounds(67, 255, 102, 30);
		contentPanel.add(lblName);
		
		id_txt = new JTextField();
		id_txt.setBounds(219, 255, 239, 30);
		id_txt.setText(fristValue);
		contentPanel.add(id_txt);
		id_txt.setColumns(10);
		
		name_txt = new JTextField();
		name_txt.setColumns(10);
		name_txt.setBounds(219, 296, 239, 30);
		name_txt.setText(secendValue);
		contentPanel.add(name_txt);
		
		JLabel lblName_1 = new JLabel("Name");
		lblName_1.setBounds(67, 296, 102, 30);
		contentPanel.add(lblName_1);
		
		shift_txt = new JTextField();
		shift_txt.setColumns(10);
		shift_txt.setText(fivethValue);
		shift_txt.setBounds(219, 424, 239, 30);
		contentPanel.add(shift_txt);
		
		JLabel lblSemester = new JLabel("Shift");
		lblSemester.setBounds(67, 424, 102, 30);
		contentPanel.add(lblSemester);
		
		dpt_txt = new JTextField();
		dpt_txt.setColumns(10);
		dpt_txt.setBounds(219, 337, 239, 30);
		dpt_txt.setText(thirdValue);
		contentPanel.add(dpt_txt);
		
		JLabel lblDepart = new JLabel("Department");
		lblDepart.setBounds(67, 337, 102, 30);
		contentPanel.add(lblDepart);
		
		semester_txt = new JTextField();
		semester_txt.setColumns(10);
		semester_txt.setText(fourthValue);
		semester_txt.setBounds(219, 383, 239, 30);
		contentPanel.add(semester_txt);
		
		JLabel lblShift = new JLabel("Semester");
		lblShift.setBounds(67, 383, 102, 30);
		contentPanel.add(lblShift);
		
		email_txt = new JTextField();
		email_txt.setColumns(10);
		email_txt.setText(seventhValue);
		email_txt.setBounds(219, 510, 239, 30);
		contentPanel.add(email_txt);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(67, 510, 102, 30);
		contentPanel.add(lblEmail);
		
		JLabel lblSex = new JLabel("Sex");
		lblSex.setBounds(67, 469, 102, 30);
		contentPanel.add(lblSex);
		
	//	sex_txt.setText(sixthValue);
		
		 chckbxMale = new JCheckBox("Male");
		chckbxMale.setBounds(219, 469, 102, 23);
		contentPanel.add(chckbxMale);
		
		 chckbxFemale = new JCheckBox("Female");
		chckbxFemale.setBounds(347, 469, 111, 23);
		contentPanel.add(chckbxFemale);
		
		// image path
		
		image_txt = new JTextField();
		image_txt.setColumns(10);
		image_txt.setText(pic);
		image_txt.setBounds(219, 564, 239, 30);
		contentPanel.add(image_txt);
		
		 lblImage = new JLabel("Image");
		lblImage.setBounds(67, 564, 102, 30);
		contentPanel.add(lblImage);
		
		 btnBrowse = new JButton("browse");
		btnBrowse.setBounds(468, 564, 80, 27);
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ChoseFile choseFile=new ChoseFile();
				choseFile.setVisible(true);
			}
		});
		contentPanel.add(btnBrowse);
		
		 btnUpdate = new JButton("update");
		btnUpdate.setBounds(219, 641, 89, 23);
		contentPanel.add(btnUpdate);
		
		btnDelete = new JButton("Delete");
		btnDelete.setBounds(369, 641, 89, 23);
		contentPanel.add(btnDelete);
		
		 pic_lb = new JLabel("");
		pic_lb.setBounds(98, 13, 383, 229);
		contentPanel.add(pic_lb);
		
		JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// editable
				id_txt.setEditable(true);
				name_txt.setEditable(true);
				dpt_txt.setEditable(true);
				semester_txt.setEditable(true);
				shift_txt.setEditable(true);
				email_txt.setEditable(true);
				//sex_txt.setEnabled(true);
				
				// show
				image_txt.show();
				lblImage.show();
				btnBrowse.show();
				btnDelete.show();
				btnUpdate.show();
				chckbxFemale.show();
				chckbxMale.show();
				
				//hide 
				sex_txt.hide();
			}
		});
		btnEdit.setBounds(219, 607, 76, 25);
		contentPanel.add(btnEdit);
		
	}
}
