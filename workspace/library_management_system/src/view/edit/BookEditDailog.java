package view.edit;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import view.message.MessageDailog;

import javax.swing.JLabel;
import javax.swing.JTextField;

import controller.BookController;
import controller.BookEditController;
import model.Book;
import model.BookCategory;
import model.Status;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.JScrollPane;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;

public class BookEditDailog extends JDialog {
	private JTextField b_name_txt;
	private JTextField writer_txt;
	private JTextField quentity_txt;
	private JTextField serial_no_txt;
	private JTextField self_no_txt;

	/**
	 * Create the panel.
	 * @param sixthValue 
	 * @param fivethValue 
	 * @param fourthValue 
	 * @param thirdValue 
	 * @param secendValue 
	 * @param fristValue 
	 * @param id 
	 */
	public BookEditDailog(String fristValue, String secendValue, String thirdValue, String fourthValue, String fivethValue, String sixthValue, int id) {
		getContentPane().setBackground(new Color(255, 255, 255));
		
		getContentPane().setLayout(new MigLayout("", "[][][][][][][][grow][][][][][grow]", "[][][][][][][][][][][][][][][][][][][][][][][][][][][][][][grow][][grow]"));
		
		JLabel lblBookName = new JLabel("Book Name :");
		getContentPane().add(lblBookName, "cell 3 3,alignx right");
		
		b_name_txt = new JTextField();
		b_name_txt.setText(fristValue);
		getContentPane().add(b_name_txt, "cell 7 3 3 1,growx");
		b_name_txt.setColumns(10);
		
		JLabel lblCategory = new JLabel("Category :");
		getContentPane().add(lblCategory, "cell 3 5,alignx right");
		
		JComboBox cat_com = new JComboBox();
		cat_com.setSelectedItem(secendValue);
		ArrayList<String> category=BookCategory.getCategoryName();
		cat_com.setModel(new DefaultComboBoxModel<>(category.toArray()));
		getContentPane().add(cat_com, "cell 7 5 3 1,growx");
		
		JLabel lblSelfNo = new JLabel("Writer :");
		getContentPane().add(lblSelfNo, "cell 3 7,alignx right");
		
		writer_txt = new JTextField();
		writer_txt.setText(thirdValue);
		getContentPane().add(writer_txt, "cell 7 7 3 1,growx");
		writer_txt.setColumns(10);
		
		JLabel lblQuentity = new JLabel("Quentity :");
		getContentPane().add(lblQuentity, "cell 3 9,alignx right");
		
		quentity_txt = new JTextField();
		quentity_txt.setText(fourthValue);
		getContentPane().add(quentity_txt, "cell 7 9 3 1,growx");
		quentity_txt.setColumns(10);
		ArrayList<String> status=Status.getStatus();
		
		JLabel lblSelfNo_1 = new JLabel("Self no :");
		getContentPane().add(lblSelfNo_1, "cell 3 12,alignx right");
		
		self_no_txt = new JTextField();
		self_no_txt.setText(fivethValue);
		getContentPane().add(self_no_txt, "cell 7 12 3 1,growx");
		self_no_txt.setColumns(10);
		
		JLabel lblStatus = new JLabel("Status :");
		getContentPane().add(lblStatus, "cell 3 17,alignx right");
		
		JComboBox status_com = new JComboBox();
		status_com.setSelectedItem(sixthValue);
		status_com.setModel(new DefaultComboBoxModel<>(status.toArray()));
		getContentPane().add(status_com, "cell 7 17 3 1,growx");
		
		JButton btnSave = new JButton("save");
		btnSave.setIcon(new ImageIcon(BookEditDailog.class.getResource("/resource/save.png")));
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String book=b_name_txt.getText();
				String cat=cat_com.getSelectedItem().toString();
				String writer=writer_txt.getText();
				String quen=quentity_txt.getText();
				String self=self_no_txt.getText();
				String status=status_com.getSelectedItem().toString();
				
				new BookEditController(id,book,cat,writer,quen,self,status);
				BookEditDailog.this.dispose();
				
			}
		});
		getContentPane().add(btnSave, "cell 7 21");
		
		JButton btnCancel = new JButton("Delete");
		btnCancel.setIcon(new ImageIcon(BookEditDailog.class.getResource("/resource/Delete.png")));
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Book b=new Book();
				b.deleteCategory(id);
				b.load();
				BookEditDailog.this.dispose();
			}
		});
		getContentPane().add(btnCancel, "cell 8 21 2 1");
		Book.load();
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		getContentPane().add(panel, "cell 0 28 13 3,grow");
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblLibrarySystem = new JLabel("Library System");
		lblLibrarySystem.setFont(new Font("Rod", Font.PLAIN, 40));
		panel.add(lblLibrarySystem);

	}

}
