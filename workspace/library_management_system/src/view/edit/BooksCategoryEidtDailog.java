package view.edit;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import view.message.MessageDailog;

import javax.swing.JLabel;
import javax.swing.JTextField;

import controller.BookCategoryController;
import controller.BookCategoryEditContoller;
import model.BookCategory;
import model.Status;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class BooksCategoryEidtDailog extends JDialog {
	private JTextField cat_txt;

	/**
	 * Create the panel.
	 * @param id 
	 * @param secendValue 
	 * @param fristValue 
	 */
	public BooksCategoryEidtDailog(String fristValue, String secendValue, int id) {
		getContentPane().setLayout(new MigLayout("", "[][][][][][][grow][][][][][][][][][][][][][grow][grow][grow]", "[][][][][][][][][][][][][][grow][][][grow]"));
		
		JLabel lblCat = new JLabel("Category");
		getContentPane().add(lblCat, "cell 3 2");
		
		cat_txt = new JTextField();
		getContentPane().add(cat_txt, "cell 6 2 6 1,growx");
		cat_txt.setText(fristValue);
		cat_txt.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Status");
		getContentPane().add(lblNewLabel, "cell 3 4");
		
		JComboBox status_com = new JComboBox();
		status_com.setSelectedItem(secendValue);
		ArrayList<String> status=Status.getStatus();
		status_com.setModel(new DefaultComboBoxModel<>(status.toArray()));
		getContentPane().add(status_com, "cell 6 4 6 1,growx");
		
		JButton btnSave = new JButton("save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String cat=cat_txt.getText();
				String status=status_com.getSelectedItem().toString();
				
				new BookCategoryEditContoller(cat,status,id);
				BooksCategoryEidtDailog.this.dispose();
			}
		});
		getContentPane().add(btnSave, "cell 6 6");
		
		JButton btnCancle = new JButton("Delete");
		btnCancle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BookCategory category=new BookCategory();
				category.deleteCategory(id);
				category.load();
				BooksCategoryEidtDailog.this.dispose();
				
			}
		});
		getContentPane().add(btnCancle, "cell 10 6");
		BookCategory.load();
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, "cell 0 15 22 2,grow");
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblLibrary = new JLabel("Library System");
		lblLibrary.setFont(new Font("Rod", Font.PLAIN, 40));
		panel.add(lblLibrary);

	}

}
