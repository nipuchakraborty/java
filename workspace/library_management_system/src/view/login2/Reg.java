package view.login2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.RegistrationController;
import model.Role;
import view.message.MessageDailog;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Reg extends JFrame {

	private JPanel contentPane;
	private JTextField user_txt;
	private JPasswordField pass_txt;
	private JPasswordField c_pass_txt;
	private JLabel mes_lb;
	private JComboBox role_com;
	public JButton btnSignUp;
	
	MessageDailog dailog=new MessageDailog();

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					Reg frame = new Reg();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public Reg() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(370, 150, 546, 560);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.control);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.control);
		panel.setBounds(0, 0, 542, 138);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(Reg.class.getResource("/image/icons8-Library_100.png")));
		label.setBounds(212, 6, 114, 95);
		panel.add(label);
		
		JLabel lblBooksCombination = new JLabel("Books Combination");
		lblBooksCombination.setForeground(new Color(219, 112, 147));
		lblBooksCombination.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblBooksCombination.setBounds(148, 98, 262, 39);
		panel.add(lblBooksCombination);
		
		JLabel lblUserName = new JLabel("User name");
		lblUserName.setForeground(new Color(219, 112, 147));
		lblUserName.setBounds(166, 149, 109, 28);
		contentPane.add(lblUserName);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(new Color(255, 182, 193));
		separator.setBounds(166, 226, 321, 7);
		contentPane.add(separator);
		
		user_txt = new JTextField();
		user_txt.setForeground(new Color(0, 0, 0));
		user_txt.setFont(new Font("Siyam Rupali", Font.PLAIN, 12));
		user_txt.setBorder(null);
		user_txt.setBackground(SystemColor.control);
		user_txt.setBounds(166, 188, 321, 32);
		contentPane.add(user_txt);
		
		JLabel lblPasswrod = new JLabel("Passwrod");
		lblPasswrod.setForeground(new Color(219, 112, 147));
		lblPasswrod.setBounds(166, 244, 78, 28);
		contentPane.add(lblPasswrod);
		
		pass_txt = new JPasswordField();
		pass_txt.addMouseListener(new MouseAdapter() {
         	@Override
         	public void mouseClicked(MouseEvent arg0) {
         		((JPasswordField) pass_txt).setEchoChar('*');
         	}
         });
		pass_txt.setForeground(new Color(0, 0, 0));
		pass_txt.setFont(new Font("Siyam Rupali", Font.PLAIN, 12));
		pass_txt.setBorder(null);
		pass_txt.setBackground(SystemColor.control);
		pass_txt.setBounds(166, 287, 321, 30);
		contentPane.add(pass_txt);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBackground(new Color(255, 182, 193));
		separator_1.setBounds(166, 323, 321, 2);
		contentPane.add(separator_1);
		
		btnSignUp = new JButton("sign up");
		btnSignUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String user=user_txt.getText();
				String pass=c_pass_txt.getText();
				String role=role_com.getSelectedItem().toString();
				if (user.equals("")) {
					user_txt.setForeground(Color.red);
				}
				else if (pass.equals("")) {
					pass_txt.setForeground(Color.red);
				} 
				else if (c_pass_txt.equals("")) {
					c_pass_txt.setForeground(Color.red);
				}
				else if (role.equals("Select One")) {
					role_com.setBackground(Color.red);
				}
				else {
					new RegistrationController(user, pass, role);
					user_txt.setText("");
					pass_txt.setText("");
					c_pass_txt.setText("");
					dailog.message_label.setText("done...!");
					dailog.setVisible(true);
				}
			}
		});
		btnSignUp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnSignUp.setBounds(166, 471, 321, 42);
			}
			@Override
			public void mouseExited(MouseEvent e){
				btnSignUp.setBounds(166, 471, 321, 28);
			}
		});
		btnSignUp.setBackground(new Color(219, 112, 147));
		btnSignUp.setBounds(166, 471, 321, 28);
		contentPane.add(btnSignUp);
		
		role_com = new JComboBox();
		ArrayList<String> role=Role.getRole();
		role_com.setModel(new DefaultComboBoxModel<>(role.toArray()));
		role_com.setBackground(new Color(219, 112, 147));
		role_com.setBounds(166, 421, 321, 28);
		contentPane.add(role_com);
		
		JLabel lblConfromPasswrod = new JLabel("Confrom Passwrod");
		lblConfromPasswrod.setForeground(new Color(219, 112, 147));
		lblConfromPasswrod.setBounds(166, 333, 168, 28);
		contentPane.add(lblConfromPasswrod);
		
		c_pass_txt = new JPasswordField();
		c_pass_txt.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				String user=user_txt.getText();
				String pass=pass_txt.getText();
				String c_pass=c_pass_txt.getText();
				if (c_pass.equals(pass)) {
					c_pass_txt.setForeground(Color.black);
					mes_lb.setText("password matched");
					btnSignUp.show();
				}
				else {
					c_pass_txt.setForeground(Color.red);
					mes_lb.setText("password not match");
					mes_lb.setForeground(Color.red);
					btnSignUp.hide();
				}
			}
		});
		c_pass_txt.addMouseListener(new MouseAdapter() {
         	@Override
         	public void mouseClicked(MouseEvent arg0) {
         		((JPasswordField) c_pass_txt).setEchoChar('*');
         	}
         });
		c_pass_txt.setForeground(Color.BLACK);
		c_pass_txt.setFont(new Font("Siyam Rupali", Font.PLAIN, 12));
		c_pass_txt.setBorder(null);
		c_pass_txt.setBackground(SystemColor.menu);
		c_pass_txt.setBounds(166, 372, 321, 30);
		contentPane.add(c_pass_txt);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBackground(new Color(255, 182, 193));
		separator_2.setBounds(166, 408, 321, 2);
		contentPane.add(separator_2);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(SystemColor.control);
		panel_1.setBounds(0, 137, 139, 389);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblSignIn = new JLabel("sign in");
		lblSignIn.setForeground(new Color(219, 112, 147));
		lblSignIn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				lblSignIn.setFont(new Font("Tahoma", Font.PLAIN, 30));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblSignIn.setFont(new Font("Tahoma", Font.PLAIN, 25));
			}
			@Override
			public void mouseClicked(MouseEvent e){
				log log=new log();
				log.setVisible(true);
				Reg.this.dispose();
			}
		});
		lblSignIn.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblSignIn.setBounds(10, 47, 114, 91);
		panel_1.add(lblSignIn);
		
		JLabel lblSignUp = new JLabel("sign up");
		lblSignUp.setForeground(new Color(219, 112, 147));
		lblSignUp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				lblSignUp.setFont(new Font("Tahoma", Font.PLAIN, 30));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblSignUp.setFont(new Font("Tahoma", Font.PLAIN, 25));
			}
		});
		lblSignUp.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblSignUp.setBounds(10, 167, 114, 91);
		panel_1.add(lblSignUp);
		
		JLabel label_2 = new JLabel("");
		label_2.addMouseListener(new MouseAdapter() {
         	@Override
         	public void mouseEntered(MouseEvent arg0) {
         		((JPasswordField) c_pass_txt).setEchoChar((char)0);
         		((JPasswordField) pass_txt).setEchoChar((char)0);
         	}
         	@Override
         	public void mouseExited(MouseEvent e) {
         		((JPasswordField) c_pass_txt).setEchoChar('*');
         		((JPasswordField) pass_txt).setEchoChar('*');
         	}
         });
		label_2.setIcon(new ImageIcon(Reg.class.getResource("/image/show.png")));
		label_2.setBounds(497, 372, 28, 30);
		contentPane.add(label_2);
		
		mes_lb = new JLabel("");
		mes_lb.setBounds(304, 333, 197, 28);
		contentPane.add(mes_lb);
	}
}
