package view.login2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import de.javasoft.plaf.synthetica.SyntheticaPlainLookAndFeel;
import model.Registration;
import model.Role;
import view.Desh;
import view.message.MessageDailog;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JSeparator;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class log extends JFrame {

	private JPanel contentPane;
	private JTextField user_txt;
	private JPasswordField pass_txt;
	private JComboBox role_com;
	
	Desh page=new Desh();
	MessageDailog dailog=new MessageDailog();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(new SyntheticaPlainLookAndFeel());
					log frame = new log();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public log() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(370, 150, 537, 515);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.control);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.control);
		panel.setBounds(0, 0, 536, 142);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(log.class.getResource("/image/icons8-Library_100.png")));
		label.setBounds(212, 6, 114, 95);
		panel.add(label);
		
		JLabel lblBooksCombination = new JLabel("Books Combination");
		lblBooksCombination.setForeground(new Color(219, 112, 147));
		lblBooksCombination.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblBooksCombination.setBounds(148, 98, 262, 39);
		panel.add(lblBooksCombination);
		
		JLabel lblUserName = new JLabel("User name");
		lblUserName.setForeground(new Color(219, 112, 147));
		lblUserName.setBounds(177, 153, 109, 28);
		contentPane.add(lblUserName);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(new Color(255, 182, 193));
		separator.setBounds(177, 230, 321, 7);
		contentPane.add(separator);
		
		user_txt = new JTextField();
		user_txt.setForeground(new Color(0, 0, 0));
		user_txt.setFont(new Font("Siyam Rupali", Font.PLAIN, 12));
		user_txt.setBorder(null);
		user_txt.setBackground(SystemColor.control);
		user_txt.setBounds(177, 192, 321, 32);
		contentPane.add(user_txt);
		
		JLabel lblPasswrod = new JLabel("Passwrod");
		lblPasswrod.setForeground(new Color(219, 112, 147));
		lblPasswrod.setBounds(177, 248, 78, 28);
		contentPane.add(lblPasswrod);
		
		pass_txt = new JPasswordField();
		pass_txt.addMouseListener(new MouseAdapter() {
         	@Override
         	public void mouseEntered(MouseEvent arg0) {
         		((JPasswordField) pass_txt).setEchoChar('*');
         	}
         });
		pass_txt.setForeground(new Color(0, 0, 0));
		pass_txt.setFont(new Font("Siyam Rupali", Font.PLAIN, 12));
		pass_txt.setBorder(null);
		pass_txt.setBackground(SystemColor.control);
		pass_txt.setBounds(177, 291, 321, 30);
		contentPane.add(pass_txt);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBackground(new Color(255, 182, 193));
		separator_1.setBounds(177, 327, 321, 2);
		contentPane.add(separator_1);
		
		JButton btnSignUp = new JButton("sign in");
		btnSignUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String user=user_txt.getText();
				String pass=pass_txt.getText();
				String role=role_com.getSelectedItem().toString();
				int role_id=Role.getRoleId(role);
				
				// database
				String d_pass=Registration.getRegistration(user, pass);
				int d_role_id=Registration.getRole(d_pass);
				String d_user=Registration.getUser(pass);
				
				if (pass.equals(d_pass) && role_id==d_role_id) {
					page.setVisible(true);
					log.this.dispose();
				}
				else if (!user.equals(d_user)) {
					user_txt.setForeground(Color.red);
				}
				else if (!pass.equals(d_pass)) {
					pass_txt.setForeground(Color.red);
				}
				else {
					user_txt.setForeground(Color.red);
					pass_txt.setForeground(Color.red);
					role_com.setForeground(Color.red);
				}
			}
		});
		btnSignUp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnSignUp.setBounds(177, 402, 321, 42);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnSignUp.setBounds(177, 402, 321, 28);
			}
		});
		btnSignUp.setBackground(new Color(219, 112, 147));
		btnSignUp.setBounds(177, 402, 321, 28);
		contentPane.add(btnSignUp);
		
		role_com = new JComboBox();
		ArrayList<String> role=Role.getRole();
		role_com.setModel(new DefaultComboBoxModel<>(role.toArray()));
		role_com.setBackground(new Color(219, 112, 147));
		role_com.setBounds(177, 351, 321, 28);
		contentPane.add(role_com);
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBackground(SystemColor.menu);
		panel_1.setBounds(0, 140, 141, 361);
		contentPane.add(panel_1);
		
		JLabel label_2 = new JLabel("sign in");
		label_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				label_2.setFont(new Font("Tahoma", Font.PLAIN, 30));
			}
			@Override
			public void mouseExited(MouseEvent e){
				label_2.setFont(new Font("Tahoma", Font.PLAIN, 25));
			}
		});
		label_2.setForeground(new Color(219, 112, 147));
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 25));
		label_2.setBounds(10, 45, 114, 91);
		panel_1.add(label_2);
		
		JLabel label_3 = new JLabel("sign up");
		label_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				label_3.setFont(new Font("Tahoma", Font.PLAIN, 30));
			}
			@Override
			public void mouseExited(MouseEvent e){
				label_3.setFont(new Font("Tahoma", Font.PLAIN, 25));
			}
			@Override
			public void mouseClicked(MouseEvent e){
				Reg reg=new Reg();
				reg.setBounds(370, 150, 546, 560);
				reg.btnSignUp.hide();
				reg.setVisible(true);
				log.this.dispose();
			}
		});
		label_3.setForeground(new Color(219, 112, 147));
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 25));
		label_3.setBounds(10, 167, 114, 91);
		panel_1.add(label_3);
		
		JLabel lblForgetPassword = new JLabel("Forget Password ?");
		lblForgetPassword.setForeground(new Color(220, 20, 60));
		lblForgetPassword.setBounds(243, 442, 121, 20);
		contentPane.add(lblForgetPassword);
		
		JLabel lblShow = new JLabel("");
		lblShow.addMouseListener(new MouseAdapter() {
         	@Override
         	public void mouseEntered(MouseEvent arg0) {
         		((JPasswordField) pass_txt).setEchoChar((char)0);
         	}
         	@Override
         	public void mouseExited(MouseEvent e) {
         		((JPasswordField) pass_txt).setEchoChar('*');
         	}
         });
		lblShow.setIcon(new ImageIcon(log.class.getResource("/image/show.png")));
		lblShow.setBounds(508, 291, 28, 30);
		contentPane.add(lblShow);
	}
}
