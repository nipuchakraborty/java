package view.splash;

import java.awt.Color;
import java.awt.Toolkit;

import javax.lang.model.type.NullType;
import javax.swing.UIManager;

import com.alee.laf.WebLookAndFeel;
import com.thehowtotutorial.splashscreen.JSplash;
import com.thoughtworks.xstream.mapper.Mapper.Null;

import de.javasoft.plaf.synthetica.SyntheticaPlainLookAndFeel;
import view.Desh;
import view.login2.log;

public class Splash {

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(new SyntheticaPlainLookAndFeel());
//			 UIManager.setLookAndFeel("com.seaglasslookandfeel.SeaGlassLookAndFeel");
			// UIManager.setLookAndFeel ( WebLookAndFeel.class.getCanonicalName () );
			//UIManager.setLookAndFeel(new SyntheticaAluOxideLookAndFeel());
			JSplash splash=new JSplash(Splash.class.getResource("/image/logo2.jpg"), true, true, true, "",null,Color.DARK_GRAY,Color.GRAY);
			//setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\virus\\Desktop\\img\\18447438_453635195028574_4031731402440302689_n.jpg"));

			splash.splashOn();
			
			for(int i=0;i<100;i++){
				splash.setProgress(i, "connecting");
				Thread.sleep(50);
			}
			splash.splashOff();
			log log=new log();
			log.setBounds(370, 150, 542, 526);
			log.setVisible(true);
			
//			HomePage page=new HomePage();
//			page.setVisible(true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

