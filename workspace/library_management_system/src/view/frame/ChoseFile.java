package view.frame;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import view.panel.NewStudentPanel;

import javax.swing.JFileChooser;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class ChoseFile extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ChoseFile dialog = new ChoseFile();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ChoseFile() {
		getContentPane().setForeground(new Color(0, 139, 139));
		setForeground(new Color(0, 139, 139));
		setBounds(100, 100, 560, 409);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setForeground(new Color(0, 139, 139));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setForeground(new Color(0, 139, 139));
		fileChooser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String file=fileChooser.getSelectedFile().getAbsolutePath();
				ImageIcon icon=new ImageIcon(file);
				
				NewStudentPanel newStudent=new NewStudentPanel();
				newStudent.pic_path_txt.setText(file);
				newStudent.setVisible(true);
				
				ChoseFile.this.dispose();
			}
		});
		fileChooser.setBounds(0, 0, 544, 337);
		contentPanel.add(fileChooser);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setForeground(new Color(0, 139, 139));
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setForeground(new Color(0, 139, 139));
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String file=fileChooser.getSelectedFile().getAbsolutePath();
						System.out.println(file);
						NewStudentPanel.pic_path_txt.setText(file);
						ChoseFile.this.dispose();
						
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setForeground(new Color(0, 139, 139));
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
