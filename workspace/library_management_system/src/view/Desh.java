
package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import com.alee.laf.WebLookAndFeel;

import de.javasoft.plaf.synthetica.SyntheticaPlainLookAndFeel;
import model.DateWiseReport;
import model.IssueTemp;
import model.NotReturn;
import model.SearchStudent;
import model.Student;
import model.Utility;
import view.allFrame.BackUpFrame;
import view.allFrame.ReStoreFrame;
import view.login2.log;
import view.message.MessageDailog;
import view.neww.IssueBookPanel2;
import view.neww.ReNewPanel;
import view.neww.ReturnBookPanel2;
import view.panel.ReturnAbleBookPanel;
import view.panel.BookPanel;
import view.panel.BooksCategoryPanel;
import view.panel.DailyReportPanel;
import view.panel.IssueBookPanel;
import view.panel.NewStudentPanel;
import view.panel.ReturnBookPanel;
import view.panel.StaticsPnael;
import view.panel.StockBookPanel;
import view.panel.StudentListPanel;
import view.splash.Splash;

import javax.imageio.ImageIO;
import javax.swing.JDesktopPane;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.SystemColor;

public class Desh extends JFrame {

	private JPanel contentPane;
	private JDesktopPane desktopPane;
	private int xx,xy;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(new SyntheticaPlainLookAndFeel());
					//UIManager.setLookAndFeel("com.seaglasslookandfeel.SeaGlassLookAndFeel");
					// UIManager.setLookAndFeel ( WebLookAndFeel.class.getCanonicalName () );
					Desh frame = new Desh();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Desh() {
		setForeground(new Color(128, 0, 0));
//		try {
//			UIManager.setLookAndFeel("com.seaglasslookandfeel.SeaGlassLookAndFeel");
//		} catch (ClassNotFoundException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		} catch (InstantiationException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		} catch (IllegalAccessException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		} catch (UnsupportedLookAndFeelException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		setBackground(new Color(240, 248, 255));
		setIconImage(Toolkit.getDefaultToolkit().getImage(Desh.class.getResource("/resource/li2.png")));
		setFont(new Font("Dialog", Font.PLAIN, 13));
		//setIconImage(Toolkit.getDefaultToolkit().getImage(HomePage.class.getResource("/resource/logo.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(250, 80, 1051, 774);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setForeground(Color.DARK_GRAY);
		menuBar.setBackground(new Color(112, 128, 144));
		setJMenuBar(menuBar);
		
		JMenu mnHome = new JMenu("Home");
		mnHome.setIcon(new ImageIcon(Desh.class.getResource("/resource/h1.png")));
		mnHome.setForeground(new Color(128, 0, 0));
		menuBar.add(mnHome);
		
		JMenu mnAccount = new JMenu("Account");
		mnHome.add(mnAccount);
		
		JMenuItem mntmLogOut = new JMenuItem("Log Out");
		mntmLogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				log log=new log();
				log.setVisible(true);
				Desh.this.dispose();
			}
		});
		mnAccount.add(mntmLogOut);
		
		JMenuItem mntmChangePassword = new JMenuItem("Change Password");
		mnAccount.add(mntmChangePassword);
		
		JMenuItem mntmBackup = new JMenuItem("Backup");
		mntmBackup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BackUpFrame frame=new BackUpFrame();
				frame.setVisible(true);
			}
		});
		mnHome.add(mntmBackup);
		
		JMenuItem mntmRestore = new JMenuItem("Restore");
		mntmRestore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ReStoreFrame fram=new ReStoreFrame();
				fram.setVisible(true);
			}
		});
		mnHome.add(mntmRestore);
		
		JMenu mnRegistration = new JMenu("Student");
		mnRegistration.setIcon(new ImageIcon(Desh.class.getResource("/resource/s2.png")));
		mnRegistration.setForeground(new Color(128, 0, 0));
		menuBar.add(mnRegistration);
		
		JMenuItem mntmNewSt = new JMenuItem("New Student");
		mntmNewSt.setIcon(new ImageIcon(Desh.class.getResource("/image/regStudent.png")));
		mntmNewSt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame newStudentInF=new JInternalFrame("New Student",true, true, false,true);
				NewStudentPanel newStudentPanel=new NewStudentPanel();
				newStudentInF.getContentPane().add(newStudentPanel);
				newStudentInF.pack();
				Student.load();
				desktopPane.add(newStudentInF);
				Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
				newStudentInF.setBounds((dimension.width-900)/2,(dimension.height-700)/2,700,600);
				newStudentInF.setVisible(true);
			}
		});
		mnRegistration.add(mntmNewSt);
		
		JMenuItem mntmStudentList = new JMenuItem("Student List");
		mntmStudentList.setIcon(new ImageIcon(Desh.class.getResource("/image/studentList.png")));
		mntmStudentList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame newStudentInF=new JInternalFrame("Student List", true, true, false,true);
				StudentListPanel panel=new StudentListPanel();
				newStudentInF.getContentPane().add(panel);
				newStudentInF.pack();
				desktopPane.add(newStudentInF);
				Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
				newStudentInF.setBounds((dimension.width-1000)/2,(dimension.height-700)/2,1000,600);
				newStudentInF.setVisible(true);
			}
		});
		mnRegistration.add(mntmStudentList);
		
		JMenu mnBook = new JMenu("Book");
		mnBook.setIcon(new ImageIcon(Desh.class.getResource("/resource/b.png")));
		mnBook.setForeground(new Color(128, 0, 0));
		menuBar.add(mnBook);
		
		JMenuItem mntmBookCategory = new JMenuItem("Book Category");
		mntmBookCategory.setIcon(new ImageIcon(Desh.class.getResource("/image/bookAdd.png")));
		mntmBookCategory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame catInF=new JInternalFrame("Book Category", true, true, false,true);
				BooksCategoryPanel booksCat=new BooksCategoryPanel();
				catInF.getContentPane().add(booksCat);
				catInF.pack();
				desktopPane.add(catInF);
				Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
				catInF.setBounds((dimension.width-900)/2,(dimension.height-700)/2,800,600);
				catInF.setVisible(true);
				
			}
		});
		mnBook.add(mntmBookCategory);
		
		JMenuItem mntmBook = new JMenuItem("Book");
		mntmBook.setIcon(new ImageIcon(Desh.class.getResource("/image/addbook.png")));
		mntmBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame bookInf=new JInternalFrame("Book", false, true, false,true);
				BookPanel bookPanel=new BookPanel();
				bookInf.getContentPane().add(bookPanel);
				bookInf.pack();
				desktopPane.add(bookInf);
				Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
				bookInf.setBounds((dimension.width-900)/2,(dimension.height-700)/2,900,600);
				bookInf.setVisible(true);
			}
		});
		mnBook.add(mntmBook);
		
		JMenu mnLibrary = new JMenu("Library");
		mnLibrary.setIcon(new ImageIcon(Desh.class.getResource("/resource/li3.png")));
		mnLibrary.setForeground(new Color(128, 0, 0));
		menuBar.add(mnLibrary);
		
		JMenuItem mntmIssueBook = new JMenuItem("Issue Book");
		mntmIssueBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame issueInF=new JInternalFrame("Issue Book", true, true, false,true);
				IssueBookPanel2 issueBookPanel=new IssueBookPanel2();
				issueInF.getContentPane().add(issueBookPanel);
				issueInF.pack();
				desktopPane.add(issueInF);
				Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
				issueInF.setBounds((dimension.width-900)/2,(dimension.height-700)/2,900,710);
				//curent date
				String date=Utility.getCurrentDate();
				issueBookPanel.date_txt.setText(date);
				
				IssueTemp.truncateTemp();
				IssueTemp.load();
				
				int sum=IssueTemp.getTotal();
				String total=Integer.toString(sum);
				issueBookPanel.tatal_txt.setText(total);
				
//				// hide component
//				issueBookPanel.name_txt.hide();
//				issueBookPanel.dpt_txt.hide();
//				issueBookPanel.semester_txt.hide();
//				issueBookPanel.shift_txt.hide();
//			//	issueBookPanel.book_txt.hide();
//				
//				issueBookPanel.name_label.hide();
//				issueBookPanel.department_label.hide();
//				issueBookPanel.semester_label.hide();
//				issueBookPanel.shift_label.hide();
//			//	issueBookPanel.book_label.hide();
//				issueBookPanel.btnIssue.hide();
				
				issueInF.setVisible(true);
				
			}
		});
		mnLibrary.add(mntmIssueBook);
		
		JMenuItem mntmReturnBook = new JMenuItem("Return Book");
		mntmReturnBook.setIcon(new ImageIcon(Desh.class.getResource("/image/returnBook.png")));
		mntmReturnBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JInternalFrame bookInf=new JInternalFrame("Return Book", true, true, false,true);
				ReturnBookPanel2 returnBook=new ReturnBookPanel2();
				bookInf.getContentPane().add(returnBook);
				bookInf.pack();
				desktopPane.add(bookInf);
				Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
				bookInf.setBounds((dimension.width-900)/2,(dimension.height-700)/2,900,700);
				// hide component
//				returnBook.name_txt.hide();
//				returnBook.dpt_txt.hide();
//				returnBook.semester_txt.hide();
//				returnBook.shift_txt.hide();
//				returnBook.book_txt.hide();
//				returnBook.i_date_txt.hide();
//				returnBook.return_date_txt.hide();
//				
//				returnBook.name_label.hide();
//				returnBook.dpt_label.hide();
//				returnBook.semester_label.hide();
//				returnBook.shift_label.hide();
//				returnBook.book_label.hide();
				returnBook.btn_return.hide();
//				returnBook.i_date_label.hide();
//				returnBook.r_date_label.hide();
				
				bookInf.setVisible(true);
			}
		});
		mnLibrary.add(mntmReturnBook);
		
		JMenuItem mntmReNewBook = new JMenuItem("Renew Book");
		mntmReNewBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JInternalFrame staticsInF=new JInternalFrame("Renew Bokk", true, true, false,true);
				ReNewPanel statics=new ReNewPanel();
				staticsInF.getContentPane().add(statics);
				staticsInF.pack();
				desktopPane.add(staticsInF);
				Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
				staticsInF.setBounds((dimension.width-900)/2,(dimension.height-700)/2,900,700);
				staticsInF.setVisible(true);
			}
		});
		mnLibrary.add(mntmReNewBook);
		
		JMenu mnHelp = new JMenu("Inventory");
		mnHelp.setIcon(new ImageIcon(Desh.class.getResource("/resource/r1.png")));
		mnHelp.setForeground(new Color(128, 0, 0));
		menuBar.add(mnHelp);
		
		JMenuItem mntmIssuereport = new JMenuItem("Statics");
		mntmIssuereport.setIcon(new ImageIcon(Desh.class.getResource("/image/status.png")));
		mntmIssuereport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame staticsInF=new JInternalFrame("Statics", true, true, false,true);
				StaticsPnael statics=new StaticsPnael();
				staticsInF.getContentPane().add(statics);
				staticsInF.pack();
				desktopPane.add(staticsInF);
				Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
				staticsInF.setBounds((dimension.width-900)/2,(dimension.height-700)/2,900,600);
				staticsInF.setVisible(true);
			}
		});
		
		JMenuItem mntmStockBook = new JMenuItem("Stock Book");
		mntmStockBook.setIcon(new ImageIcon(Desh.class.getResource("/image/stockbook.png")));
		mntmStockBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame staticsInF=new JInternalFrame("Stock Book", true, true, false,true);
				StockBookPanel stockBook=new StockBookPanel();
				staticsInF.getContentPane().add(stockBook);
				staticsInF.pack();
				desktopPane.add(staticsInF);
				Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
				staticsInF.setBounds((dimension.width-900)/2,(dimension.height-700)/2,800,600);
				staticsInF.setVisible(true);
			}
		});
		mnHelp.add(mntmStockBook);
		
		JMenuItem mntmRet = new JMenuItem("Repayable Book");
		mntmRet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame staticsInF=new JInternalFrame("Return Able Book", true, true, false,true);
				ReturnAbleBookPanel arrearBook=new ReturnAbleBookPanel();
				staticsInF.getContentPane().add(arrearBook);
				staticsInF.pack();
				desktopPane.add(staticsInF);
				NotReturn.load();
				Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
				staticsInF.setBounds((dimension.width-900)/2,(dimension.height-700)/2,900,600);
				staticsInF.setVisible(true);
			}
		});
		mnHelp.add(mntmRet);
		mnHelp.add(mntmIssuereport);
		
		JMenu mnReport = new JMenu("Report");
		mnHelp.add(mnReport);
		
		JMenuItem mntmDaily = new JMenuItem("Daily");
		mntmDaily.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JInternalFrame staticsInF=new JInternalFrame("Daily", true, true, false,true);
				DailyReportPanel stockBook=new DailyReportPanel();
				staticsInF.getContentPane().add(stockBook);
				DateWiseReport.load();
				DateWiseReport.load1();
				staticsInF.pack();
				desktopPane.add(staticsInF);
				Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
				staticsInF.setBounds((dimension.width-900)/2,(dimension.height-700)/2,900,600);
				staticsInF.setVisible(true);
			}
		});
		mnReport.add(mntmDaily);
		
		JMenuItem mntmMonthly = new JMenuItem("Monthly");
		mnReport.add(mntmMonthly);
		
		JMenuItem mntmYearly = new JMenuItem("Yearly");
		mnReport.add(mntmYearly);
		
		JMenu mnSettings = new JMenu("Settings");
		menuBar.add(mnSettings);
		
		JMenuItem mntmCamera = new JMenuItem("camera");
		mntmCamera.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Process process=Runtime.getRuntime().exec("E:/soft/MyCam/MyCam.exe");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		mnSettings.add(mntmCamera);
		
		JMenu mnHelp_1 = new JMenu("Help");
		mnHelp_1.setIcon(new ImageIcon(Desh.class.getResource("/resource/help1.png")));
		mnHelp_1.setForeground(new Color(128, 0, 0));
		menuBar.add(mnHelp_1);
		contentPane = new JPanel();
		contentPane.addMouseListener(new MouseAdapter() {
			@Override
			
			public void mousePressed(MouseEvent evt) {
				  setOpacity((float)0.8);
			        xx=evt.getX();
			        xy=evt.getY();
			}
			@Override
			public void mouseReleased(MouseEvent arg0) {
				 setOpacity((float)1.0);
			}
		});
		
		contentPane.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent evt) {
				  int x=evt.getXOnScreen();
			      int y=evt.getYOnScreen();
			      Desh.this.setLocation(x-xx, y-xy);
		
			}
		});
		contentPane.setForeground(new Color(128, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		desktopPane = new JDesktopPane() {
			
			
		    private Image image;
		    {
		        try {
		        	 image = ImageIO.read(Splash.class.getResource("/image/bglogo.jpg"));
		           // image = ImageIO.read(new File("resource/icon.jpg"));
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		desktopPane.setForeground(new Color(128, 0, 0));
		desktopPane.setBackground(new Color(47, 79, 79));
		contentPane.add(desktopPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		panel.setForeground(Color.WHITE);
		panel.setBackground(Color.BLACK);
		panel.setBounds(12, 13, 168, 626);
		desktopPane.add(panel);
		panel.setLayout(null);
		
		JButton btnNewButton = new JButton("New Student");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JInternalFrame newStudentInF=new JInternalFrame("New Student",true, true, false,true);
				NewStudentPanel newStudentPanel=new NewStudentPanel();
				newStudentInF.getContentPane().add(newStudentPanel);
				newStudentInF.pack();
				Student.load();
				desktopPane.add(newStudentInF);
				Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
				newStudentInF.setBounds((dimension.width-900)/2,(dimension.height-700)/2,700,600);
				newStudentInF.setVisible(true);
			}
		});
		btnNewButton.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBackground(Color.BLACK);
		btnNewButton.setBounds(0, 0, 168, 130);
		panel.add(btnNewButton);
		
		JButton btnIssue = new JButton("Issue Book");
		btnIssue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JInternalFrame issueInF=new JInternalFrame("Issue Book", true, true, false,true);
				IssueBookPanel2 issueBookPanel=new IssueBookPanel2();
				issueInF.getContentPane().add(issueBookPanel);
				issueInF.pack();
				desktopPane.add(issueInF);
				Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
				issueInF.setBounds((dimension.width-900)/2,(dimension.height-700)/2,900,710);
				//curent date
				String date=Utility.getCurrentDate();
				issueBookPanel.date_txt.setText(date);
				
				IssueTemp.truncateTemp();
				IssueTemp.load();
				
				int sum=IssueTemp.getTotal();
				String total=Integer.toString(sum);
				issueBookPanel.tatal_txt.setText(total);
				
//				// hide component
//				issueBookPanel.name_txt.hide();
//				issueBookPanel.dpt_txt.hide();
//				issueBookPanel.semester_txt.hide();
//				issueBookPanel.shift_txt.hide();
//			//	issueBookPanel.book_txt.hide();
//				
//				issueBookPanel.name_label.hide();
//				issueBookPanel.department_label.hide();
//				issueBookPanel.semester_label.hide();
//				issueBookPanel.shift_label.hide();
//			//	issueBookPanel.book_label.hide();
//				issueBookPanel.btnIssue.hide();
				
				issueInF.setVisible(true);
			}
		});
		btnIssue.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnIssue.setForeground(Color.WHITE);
		btnIssue.setBackground(Color.BLACK);
		btnIssue.setBounds(0, 128, 168, 130);
		panel.add(btnIssue);
		
		JButton btnReturnBook = new JButton("Return Book");
		btnReturnBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JInternalFrame bookInf=new JInternalFrame("Return Book", true, true, false,true);
				ReturnBookPanel2 returnBook=new ReturnBookPanel2();
				bookInf.getContentPane().add(returnBook);
				bookInf.pack();
				desktopPane.add(bookInf);
				Dimension dimension=getToolkit().getDefaultToolkit().getScreenSize();
				bookInf.setBounds((dimension.width-900)/2,(dimension.height-700)/2,900,700);
				// hide component
//				returnBook.name_txt.hide();
//				returnBook.dpt_txt.hide();
//				returnBook.semester_txt.hide();
//				returnBook.shift_txt.hide();
//				returnBook.book_txt.hide();
//				returnBook.i_date_txt.hide();
//				returnBook.return_date_txt.hide();
//				
//				returnBook.name_label.hide();
//				returnBook.dpt_label.hide();
//				returnBook.semester_label.hide();
//				returnBook.shift_label.hide();
//				returnBook.book_label.hide();
				returnBook.btn_return.hide();
//				returnBook.i_date_label.hide();
//				returnBook.r_date_label.hide();
				
				bookInf.setVisible(true);
			}
		});
		btnReturnBook.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnReturnBook.setForeground(Color.WHITE);
		btnReturnBook.setBackground(Color.BLACK);
		btnReturnBook.setBounds(0, 245, 168, 130);
		panel.add(btnReturnBook);
		
		JButton btnCamera = new JButton("Camera");
		btnCamera.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Process process=Runtime.getRuntime().exec("E:/soft/MyCam/MyCam.exe");
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		btnCamera.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnCamera.setForeground(Color.WHITE);
		btnCamera.setBackground(Color.BLACK);
		btnCamera.setBounds(0, 370, 168, 130);
		panel.add(btnCamera);
		
		JButton btnStockBook = new JButton("Stock Book");
		btnStockBook.setFont(new Font("Dialog", Font.PLAIN, 18));
		btnStockBook.setForeground(Color.WHITE);
		btnStockBook.setBackground(Color.BLACK);
		btnStockBook.setBounds(0, 497, 168, 130);
		panel.add(btnStockBook);
	}
}
