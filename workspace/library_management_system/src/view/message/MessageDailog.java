package view.message;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.SwingConstants;

public class MessageDailog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	public JLabel message_label;
	public JButton okButton;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			MessageDailog dialog = new MessageDailog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public MessageDailog() {
		getContentPane().setForeground(new Color(0, 139, 139));
		setForeground(new Color(0, 139, 139));
		setIconImage(Toolkit.getDefaultToolkit().getImage(MessageDailog.class.getResource("/resource/logo.png")));
		//setIconImage(Toolkit.getDefaultToolkit().getImage(MessageDailog.class.getResource("/resource/logo.png")));
		setBounds(100, 100, 529, 180);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setForeground(new Color(0, 139, 139));
		contentPanel.setBackground(Color.WHITE);
		contentPanel.setBorder(UIManager.getBorder("PopupMenu.border"));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			 message_label = new JLabel("");
			 message_label.setForeground(new Color(0, 139, 139));
			 message_label.setFont(new Font("Plantagenet Cherokee", Font.PLAIN, 15));
			message_label.setBounds(138, 8, 370, 97);
			contentPanel.add(message_label);
		}
		
		JLabel pic_label = new JLabel("");
		pic_label.setForeground(new Color(0, 139, 139));
		pic_label.setIcon(new ImageIcon(MessageDailog.class.getResource("/resource/images.jpg")));
		pic_label.setBounds(10, 8, 118, 101);
		contentPanel.add(pic_label);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setForeground(new Color(0, 139, 139));
			buttonPane.setBackground(Color.WHITE);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				 okButton = new JButton("OK");
				 okButton.setForeground(new Color(0, 139, 139));
				 okButton.setBackground(new Color(0, 139, 139));
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						MessageDailog.this.dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setForeground(new Color(0, 139, 139));
				cancelButton.setBackground(new Color(0, 139, 139));
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						MessageDailog.this.dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
