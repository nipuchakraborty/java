package controller;

import java.util.Date;

import model.NotReturn1;
import model.Utility;

public class RenewController {
	
	public RenewController(Date renew_d, String total, String paid, String due, int s_id) {
		
		String r_date=Utility.dateToString(renew_d);
		int total_tk=Integer.parseInt(total);
		int paid_tk=Integer.parseInt(paid);
		int due_tk=Integer.parseInt(due);
		
		NotReturn1 notReturn1=new NotReturn1();
		notReturn1.prepare(r_date,total_tk,paid_tk,due_tk);
		notReturn1.updateNotReturn1(s_id);
	}

}
