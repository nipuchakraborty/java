package controller;

import model.BookCategory;
import model.Utility;

public class BookCategoryEditContoller {
	public BookCategoryEditContoller(String cat, String status, int id) {
		
		int st=Utility.changeStatusIntoZeroOne(status);
		
		BookCategory category=new BookCategory();
		category.prepare(cat, st);
		category.updateCategory(id);
		category.load();
	}

}
