package controller;

import java.util.ArrayList;

import model.Return1;
import model.Return2;

public class Return2Controller {
	
	public Return2Controller(int s_id) {
		
		Return1 return1=new Return1();
		int return1_id=return1.get_Id_form_Return1();
		
		ArrayList<Integer>student_id=return1.getAll_s_id(s_id);
		ArrayList<Integer>book_id=return1.getAll_book_id(s_id);
		
		Return2 return2=new Return2();
		return2.store(return1_id,student_id,book_id);
	}
}
