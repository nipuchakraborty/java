package controller;

import model.Book;
import model.IssueTemp;

public class IssueTempController {

	public IssueTempController(int d_s_id, String book, int issue_prize) {
		
		int b_id=Book.getBookId(book);
		
		IssueTemp temp=new  IssueTemp();
		temp.store(d_s_id,b_id,issue_prize);
	}
	

}
