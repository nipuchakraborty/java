package controller;

import java.util.ArrayList;

import model.Issue1;
import model.Issue2;
import model.IssueTemp;
import model.Student;

public class Issue1Controller {
	
	public Issue1Controller(String i_date, String r_date, String id, String total, String paid, String due) {
		
		// for issue1 
	//	int s_id=Student.getStudentId(id);
		int s_id=Integer.parseInt(id);
		int total_tk=Integer.parseInt(total);
		int paid_tk=Integer.parseInt(paid);
		int due_tk=Integer.parseInt(due);
		
		Issue1 issue1=new Issue1();
		issue1.store(i_date,r_date,s_id,total_tk,paid_tk,due_tk);
		
		
		// for issue2
		int issue1_id=issue1.get_Id_form_Issue1();
		
		IssueTemp temp=new IssueTemp();
		ArrayList<Integer>student_id=temp.getAll_s_id();
		ArrayList<Integer>b_id=temp.getAll_book_id();
		ArrayList<Integer>issue_prize=temp.getAll_issue_prize();
		
		Issue2 issue2=new Issue2();
		issue2.store(issue1_id,student_id,b_id,issue_prize);
	}

}
