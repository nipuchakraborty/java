package controller;

import java.awt.print.Book;

import model.BookCategory;
import model.Utility;

public class BookController {
	
	public BookController(String book, String category, String writer, String quentity, String self, 
			String prize, String issue_prize, String status) {
		
		int cat_id=BookCategory.getCategoryId(category);
		
		int quen=Integer.parseInt(quentity);
		int selfno=Integer.parseInt(self);
		
		int p=Integer.parseInt(prize);
		int total=quen*p;
		int i_p=Integer.parseInt(issue_prize);
		
		
		int st=Utility.changeStatusIntoZeroOne(status);
		
		
		model.Book book2=new model.Book();
		book2.prepare(book,cat_id,writer,quen,selfno,p,total,i_p,st);
		book2.store();
		
	}

}
