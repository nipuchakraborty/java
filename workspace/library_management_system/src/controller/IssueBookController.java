package controller;

import java.util.Date;

import model.Book;
import model.Department;
import model.IssueBook;
import model.Semester;
import model.Shift;
import model.Student;
import model.Utility;

public class IssueBookController {
	
	public IssueBookController(String date, String id, String name, String dpt, String semester, 
			String shift, String book) {
		
		int s_id=Integer.parseInt(id);
		int dpt_id=Department.getDepartmentId(dpt);
		int semester_id=Semester.getSemestertId(semester);
		int shift_id=Shift.getShiftId(shift);
		int book_id=Book.getBookId(book);
		IssueBook issueBook=new IssueBook();
		issueBook.store(date,s_id,name,dpt_id,semester_id,shift_id,book_id);
		
	}

}
