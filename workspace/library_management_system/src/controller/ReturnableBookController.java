package controller;

import model.Book;
import model.ReturnAbleBook;
import model.Student;

public class ReturnableBookController {
	
	public ReturnableBookController(String date, String id, String name, String book) {
		
		int book_id=Book.getBookId(book);
		int s_id=Integer.parseInt(id);
		
		ReturnAbleBook ableBook=new ReturnAbleBook();
		ableBook.store(date,s_id,name,book_id);
		
		
	}
	

}
