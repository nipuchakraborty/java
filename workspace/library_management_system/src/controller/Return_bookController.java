package controller;

import java.util.Date;

import model.Book;
import model.Department;
import model.ReturnAbleBook;
import model.Semester;
import model.Shift;
import model.Utility;
import view.message.MessageDailog;


public class Return_bookController {
	
	public Return_bookController(String id, String date, String name, String dpt, String semester, String shift, String book) {
		
		int s_id=Integer.parseInt(id);
		int dpt_id=Department.getDepartmentId(dpt);
		int semester_id=Semester.getSemestertId(semester);
		int shift_id=Shift.getShiftId(shift);
		int book_id=Book.getBookId(book);
//		int book1_id=Book.getBookId(book1);
//		int book2_id=Book.getBookId(book2);
		
			
		model.ReturnBook b=new model.ReturnBook();
		b.store(s_id,date,name,dpt_id,semester_id,shift_id,book_id);
		
		
	}

}
