package controller;

import javax.swing.JTextField;

import model.BookCategory;
import model.Utility;

public class BookCategoryController {
	
	public BookCategoryController(String cat, String status) {
		
		int st=Utility.changeStatusIntoZeroOne(status);
		System.out.println(cat);
		System.out.println(st);
		// data send to controller model
		BookCategory category=new BookCategory();
		category.prepare(cat,st);
		category.store();
		
	}

}
