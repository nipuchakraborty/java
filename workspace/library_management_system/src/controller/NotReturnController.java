package controller;

import java.util.ArrayList;

import model.IssueTemp;
import model.NotReturn;
import model.NotReturn1;

public class NotReturnController {
	
	public NotReturnController() {
		
		NotReturn1 notReturn1=new NotReturn1();
		int not_return1_id=notReturn1.get_Id_form_NotReturn1();
		
		IssueTemp temp=new IssueTemp();
		ArrayList<Integer>student_id=temp.getAll_s_id();
		ArrayList<Integer>b_id=temp.getAll_book_id();
		
		NotReturn notReturn=new NotReturn();
		notReturn.store(not_return1_id,student_id,b_id);
	}

}
