package controller;

import model.Registration;
import model.Role;

public class RegistrationController {
	
	public RegistrationController(String user_name, String pass, String role) {
		
		int role_id=Role.getRoleId(role);
		
		Registration registration=new Registration();
		registration.store(user_name,pass,role_id);
		
	}

}
