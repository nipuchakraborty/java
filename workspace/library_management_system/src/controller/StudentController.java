package controller;

import model.Department;
import model.Semester;
import model.Shift;
import model.Student;
import model.Utility;

public class StudentController {
	
	public StudentController(String s_id, String name, String dpt, String semester, String shift, String sex, String num, String image) {
		
		// converting
		int student_id=Integer.parseInt(s_id);
		int dpt_id=Department.getDepartmentId(dpt);
		int semester_id=Semester.getSemestertId(semester);
		int shift_id=Shift.getShiftId(shift);
		int sex_int=Utility.sex_StringToInt(sex);
		
		Student student=new Student();
		student.store(student_id,name,dpt_id,semester_id,shift_id,sex_int,num,image);
	}

}
