package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import net.proteanit.sql.DbUtils;
import view.panel.NewStudentPanel;
import view.panel.StockBookPanel;
import view.panel.StudentListPanel;


public class SearchStudent {
	
	public static void search(int id) {
		Connection conn=Database.getconnection();
		String sql="SELECT * FROM `student` WHERE student_id=?";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, id);
			ResultSet rs=pst.executeQuery();
			
			
			//StudentSearchPanel.table.setModel(DbUtils.resultSetToTableModel(rs));
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// search data and show on table
	public static void loadSearch(String name) {
		Connection conn=Database.getconnection();
		String sql="SELECT a.`student_id`, a.`name`, b.name, c.semester, d.name, "
				+ "(CASE a.`sex` WHEN 0 THEN 'Male' ELSE 'Female' END)as sex ,"
				+ " a.`number` FROM `student` a, department b,semster c, shift d "
				+ "WHERE b.id=a.`department_id` AND c.id=a.`semester_id` and d.id=a.`shift_id` "
				+ "AND a.`name` LIKE '%"+name+"%'";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			StudentListPanel.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void loadSearchInt(int search) {
		Connection conn=Database.getconnection();
		String sql="SELECT a.`student_id`, a.`name`, b.name, c.semester, d.name, "
				+ "(CASE a.`sex` WHEN 0 THEN 'Male' ELSE 'Female' END)as sex ,"
				+ " a.`number` FROM `student` a, department b,semster c, shift d "
				+ "WHERE b.id=a.`department_id` AND c.id=a.`semester_id` and d.id=a.`shift_id` "
				+ "AND a.`id` LIKE '%"+search+"%'";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			StudentListPanel.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
