package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.proteanit.sql.DbUtils;
import view.panel.ReturnAbleBookPanel;
import view.panel.StaticsPnael;

public class ReturnAbleBook {
	
public void store(String date, int id, String name, int book_id){
		
		Connection conn=Database.getconnection();
		String sql="INSERT INTO `returnable_book`(`date`, `student_id`, `name`, `book_id`) VALUES (?,?,?,?)";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, date);
			pst.setInt(2,id );
			pst.setString(3, name);;
			pst.setInt(4, book_id);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void load() {
		Connection conn=Database.getconnection();
		String sql="SELECT a.`date` as Date , a.`student_id` as Student_ID, a.`name` as Name, b.`book_name` as Book "
				+ "FROM `returnable_book` a, book b WHERE a.`book_id`=b.id";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			//ReturnAbleBookPanel.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static int getStudentId_FromReturn(String student_name) {
		int s_id=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `s_id` FROM `returnable_book_final` WHERE name=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, student_name);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				s_id=rs.getInt("s_id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return s_id;
		
	}
	
	public static int get_return_able_book_id(int s_id) {
		int book_id=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `returnable_book_id` FROM `returnable_book_final` WHERE`s_id`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, s_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				book_id=rs.getInt("returnable_book_id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return book_id;
		
	}
	
	public void delete(int id) {
		
		Connection conn=Database.getconnection();
		String sql="DELETE FROM `returnable_book` WHERE `student_id`=?";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, id);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static int getrowInfo(int stuent_id) {
		int row=0;

		Connection conn=Database.getconnection();
		String sql="SELECT COUNT(`id`) as row_info FROM `returnable_book` WHERE `student_id`=?";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, stuent_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				row=rs.getInt("row_info");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return row;
	}
}
