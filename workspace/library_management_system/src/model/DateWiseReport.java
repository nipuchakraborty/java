package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.proteanit.sql.DbUtils;
import view.panel.DailyReportPanel;
import view.panel.StaticsPnael;

public class DateWiseReport {
	
	public static void load() {
		Connection conn=Database.getconnection();
		String sql="SELECT a.i_date as Date,b.`student_id` as Student_Id,d.name as Name,c.book_name as Book "
				+ "FROM issue1 a, `issue2` b,book c,student d WHERE a.id=b.`issue1_id` AND c.id=b.`book_id` AND b.`student_id`=d.student_id";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			DailyReportPanel.issue_table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void load1() {
		Connection conn=Database.getconnection();
		String sql="SELECT a.`date`as Date,b.`student_id` as Student_Id,d.name as Name,c.book_name as Book "
				+ "FROM return1 a,`return2` b, book c,student d WHERE a.id=b.`return1_id` AND b.book_id=c.id AND d.student_id=b.`student_id`";
		
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			DailyReportPanel.return_table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
