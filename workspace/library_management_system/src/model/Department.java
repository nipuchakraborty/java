package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Department {
	
	

	public static ArrayList<String> getDepartment() {
		
		ArrayList<String> status=new ArrayList<>();
		
		Connection conn=Database.getconnection();
		String sql="SELECT `name` FROM `department`";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			
			while (rs.next()) {
				status.add(rs.getString("name"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return status;
		
	}
	
	public static int getDepartmentId(String dpt) {
		int dpt_id=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT * FROM `department` WHERE name=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, dpt);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				dpt_id=rs.getInt("id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dpt_id;
		
	}
	// for getting department name by id..
	public static String getDepartment(int dpt_id) {
		
		String dpt_name ="";
		
		Connection conn=Database.getconnection();
		String sql="SELECT * FROM `department` WHERE id=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, dpt_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				dpt_name=rs.getString("name");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dpt_name;
		
	}
	

}
