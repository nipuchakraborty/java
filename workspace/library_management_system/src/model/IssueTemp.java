package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import net.proteanit.sql.DbUtils;
import view.neww.IssueBookPanel2;


public class IssueTemp {
	public static Connection conn=Database.getconnection();
	
	public void store(int s_id, int b_id, int issue_prize) {
		
		String sql="INSERT INTO `issue_book_temp`(`student_id`, `book_id`, `issue_prize`) VALUES (?,?,?)";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, s_id);
			pst.setInt(2, b_id);;
			pst.setInt(3, issue_prize);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public static void load() {
		String sql="SELECT a.book_name as Book,b.`issue_prize` as Issue_Prize FROM `issue_book_temp` b, book a WHERE a.id=b.`book_id`";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			IssueBookPanel2.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//TRUNCATE TABLE 
	public static void truncateTemp() {
		
		try {
			 Statement statement =conn.createStatement();
			 statement.executeUpdate("TRUNCATE TABLE `issue_book_temp`");

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public static void copyTemp() {
		
		String sql="INSERT INTO `issue2` SELECT * FROM issue_book_temp";
		
		try {
			PreparedStatement pstm = conn.prepareStatement(sql);
	        pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static ArrayList<Integer> getAll_s_id() {
		
		ArrayList<Integer> s_id=new ArrayList<>();
		
		Connection conn=Database.getconnection();
		String sql="SELECT `student_id` FROM `issue_book_temp`";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			
			while (rs.next()) {
				s_id.add(rs.getInt("student_id"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return s_id;
		
	}
	
	public static ArrayList<Integer> getAll_book_id() {
		
		ArrayList<Integer> b_id=new ArrayList<>();
		
		Connection conn=Database.getconnection();
		String sql="SELECT `book_id` FROM `issue_book_temp`";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			
			while (rs.next()) {
				b_id.add(rs.getInt("book_id"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return b_id;
		
	}
	
	public static ArrayList<Integer> getAll_issue_prize() {
		
		ArrayList<Integer> issue_prize=new ArrayList<>();
		
		Connection conn=Database.getconnection();
		String sql="SELECT `issue_prize` FROM `issue_book_temp`";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			
			while (rs.next()) {
				issue_prize.add(rs.getInt("issue_prize"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return issue_prize;
		
	}
	
	public static int getTotal() {
		int total=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT SUM(`issue_prize`) FROM `issue_book_temp`";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				total=rs.getInt("SUM(`issue_prize`)");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return total;
	
	}
	
	public static int getrowInfo() {
		int row=0;

		Connection conn=Database.getconnection();
		String sql="SELECT COUNT(`student_id`) as row_info FROM `issue_book_temp`";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				row=rs.getInt("row_info");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return row;
	}
}
