package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import net.proteanit.sql.DbUtils;

import view.panel.BookPanel;
import view.panel.StockBookPanel;


public class Book {
	
	private String book;
	private int cat;
	private String writer;
	private int quentity;
	private int self;
	private int prize;
	private int total;
	private int issue_prize;
	private int status;
	
	public void prepare(String book, int cat_id, String writer, int quen, int selfno, int prize, int total, 
		int issue_prize, int st){
		
		this.book=book;
		this.cat=cat_id;
		this.writer=writer;
		this.quentity=quen;
		this.self=selfno;
		this.prize=prize;
		this.total=total;
		this.issue_prize=issue_prize;
		this.status=st;
	}
	
	
	public void store() {
		
		Connection conn=Database.getconnection();
		String sql="INSERT INTO `book`(`book_name`, `category_id`, `writter`, `quentity`, `sefl_no`, `prize`, "
				+ "`total`, `issue_prize`, `status`) VALUES (?,?,?,?,?,?,?,?,?)";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, this.book);
			pst.setInt(2, this.cat);
			pst.setString(3, this.writer);
			pst.setInt(4, this.quentity);
			pst.setInt(5, this.self);
			pst.setInt(6, this.prize);
			pst.setInt(7, this.total);
			pst.setInt(8, this.issue_prize);
			pst.setInt(9, this.status);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	// show data on table
	public static void load() {
		Connection conn=Database.getconnection();
		String sql="SELECT a.`book_name` as Book, b.`category_name` as Category, a.`writter` as Writer,"
				+ " a.`quentity` as Quentity, a.`sefl_no` as Self_No, "
				+ "(CASE a.`status` WHEN 0 THEN 'Available' ELSE 'Not available' END)as Status "
				+ "FROM `book` a ,book_category b WHERE a.`category_id`=b.id AND a.id>1";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			BookPanel.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static ArrayList<String> giveCatIdTakeBookName(int Category_id){
		
		ArrayList<String> bookName=new ArrayList<>();
		Connection is=Database.getconnection();
		String command="SELECT * FROM `book` WHERE `category_id`=?";
		
		try{
			PreparedStatement ps=is.prepareStatement(command);
			ps.setInt(1, Category_id);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				bookName.add(rs.getString("book_name")); 
			}
			
		}
		catch(Exception e){
			
		}
		return bookName;
	}
	
	public static int getSelfNo(int book_id) {
		
		int self_no=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `sefl_no` FROM `book` WHERE id=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, book_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				self_no=rs.getInt("sefl_no");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return self_no;
		
	}
	public static int getIssuePrize(int book_id) {
		
		int issue_prize=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `issue_prize` FROM `book` WHERE id=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, book_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				issue_prize=rs.getInt("issue_prize");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return issue_prize;
		
	}
	
	public static int getBookId(String book_name) {
		int b_id=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `id` FROM `book` WHERE `book_name`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, book_name);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				b_id=rs.getInt("id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return b_id;
		
	}
	
	public static String getBook(int book_id) {
		String book_name="";
		
		Connection conn=Database.getconnection();
		String sql="SELECT `book_name` FROM `book` WHERE `id`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, book_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				book_name=rs.getString("book_name");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return book_name;
		
	}
	
	public void updateCategory(int id) {
		
		Connection conn=Database.getconnection();
		String sql="UPDATE `book` SET `book_name`=?,`category_id`=?,`writter`=?,`quentity`=?,`sefl_no`=?,`status`=? WHERE id=?";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, this.book);
			pst.setInt(2, this.cat);
			pst.setString(3, this.writer);
			pst.setInt(4, this.quentity);
			pst.setInt(5, this.self);
			pst.setInt(6, this.status);
			pst.setInt(7, id);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void deleteCategory(int id) {
		
		Connection conn=Database.getconnection();
		String sql="DELETE FROM `book` WHERE id=?";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, id);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
