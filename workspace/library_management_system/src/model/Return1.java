package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import net.proteanit.sql.DbUtils;
import view.panel.StaticsPnael;

public class Return1 {
	
	public static Connection conn=Database.getconnection();
	
	public void store(int s_id, String date) {
		
		String sql="INSERT INTO `return1`( `date`, `student_id`) VALUES (?,?)";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, date);
			pst.setInt(2, s_id);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public int get_Id_form_Return1() {
		int id=0;
		String sql="SELECT id FROM return1 ORDER BY `id` DESC LIMIT 1";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			while(rs.next()){
				id=rs.getInt("id");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return id;
		
	}
	
public static ArrayList<Integer> getAll_s_id(int id) {
		
		ArrayList<Integer> s_id=new ArrayList<>();
		
		Connection conn=Database.getconnection();
		String sql="SELECT `student_id` FROM `not_return` WHERE `student_id`="+id;
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			
			while (rs.next()) {
				s_id.add(rs.getInt("student_id"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return s_id;
		
	}
	
	public static ArrayList<Integer> getAll_book_id(int id) {
		
		ArrayList<Integer> b_id=new ArrayList<>();
		
		Connection conn=Database.getconnection();
		String sql="SELECT `book_id` FROM `not_return` WHERE `student_id`="+id;
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			
			while (rs.next()) {
				b_id.add(rs.getInt("book_id"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return b_id;
		
	}
	
	public static void load() {
		Connection conn=Database.getconnection();
		String sql="SELECT a.`date`as Date,b.`student_id` as Student_Id,d.name as Name,c.book_name as Book "
				+ "FROM return1 a,`return2` b, book c,student d WHERE a.id=b.`return1_id` AND b.book_id=c.id AND d.student_id=b.`student_id`";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			StaticsPnael.return_table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
