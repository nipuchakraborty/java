package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.proteanit.sql.DbUtils;
import view.neww.IssueBookPanel2;
import view.neww.ReturnBookPanel2;
import view.panel.StaticsPnael;

public class Issue1 {
	
	public static Connection conn=Database.getconnection();
	
	public void store(String i_date, String r_date, int s_id, int total_tk, int paid_tk, int due_tk) {
		
		String sql="INSERT INTO `issue1`(`i_date`, `r_date`, `student_id`, `total_prize`, `paid`, `due`) "
				+ "VALUES (?,?,?,?,?,?)";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, i_date);
			pst.setString(2, r_date);
			pst.setInt(3, s_id);
			pst.setInt(4, total_tk);
			pst.setInt(5, paid_tk);
			pst.setInt(6, due_tk);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public int get_Id_form_Issue1() {
		int id=0;
		String sql="SELECT id FROM `issue1` ORDER BY `id` DESC LIMIT 1";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			while(rs.next()){
				id=rs.getInt("id");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return id;
		
	}
	
	public static int getIssue1_id(int stuent_id) {
		
		int id=0;

		String sql="SELECT `id` FROM `issue1` WHERE `student_id`=?";
		
		try {
			
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, stuent_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				id=rs.getInt("id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return id;
		
	}
	
	public static int getDue(int stuent_id) {
		
		int due=0;

		String sql="SELECT `due` FROM `issue1` WHERE `student_id`=?";
		
		try {
			
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, stuent_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				due=rs.getInt("due");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return due;
		
	}
	

	public static void load() {
		Connection conn=Database.getconnection();
		String sql="SELECT a.i_date as Date,b.`student_id` as Student_Id,d.name as Name,c.book_name as Book "
				+ "FROM issue1 a, `issue2` b,book c,student d WHERE a.id=b.`issue1_id` AND c.id=b.`book_id` AND b.`student_id`=d.student_id";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			StaticsPnael.issue_table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
