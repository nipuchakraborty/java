package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Registration {
	
	
	public void store(String user_name, String pass, int role_id){
		
		Connection conn=Database.getconnection();
		String sql="INSERT INTO `ragistration`(`user_name`, `password`, `role_id`) VALUES (?,?,?)";
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			
			pst.setString(1, user_name);
			pst.setString(2, pass);
			pst.setInt(3, role_id);
			
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

	public static String getRegistration(String name,String password) {
		String pass = null;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `user_name`,`password` FROM `ragistration` WHERE `user_name`=? AND `password`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, name);
			pst.setString(2, password);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				pass=rs.getString("password");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pass;
		
		
	}
	public static String getPassword(String user) {
		String pass = null;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `password` FROM `ragistration` WHERE `user_name`=? ";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, user);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				pass=rs.getString("password");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pass;

	}
	
	public static String getUser(String password) {
		String user = null;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `user_name` FROM `ragistration` WHERE `password`=? ";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, password);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				user=rs.getString("user_name");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
		
	}
	public static int getRole(String password) {
		int role = 0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `role_id` FROM `ragistration` WHERE `password`=? ";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, password);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				role=rs.getInt("role_id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return role;
		
		
		
	}
}
