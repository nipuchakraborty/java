package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.proteanit.sql.DbUtils;
import view.panel.BooksCategoryPanel;
import view.panel.NewStudentPanel;
import view.panel.StudentListPanel;

public class Student {
	
	
	
	
	public void store(int student_id, String name, int dpt_id, int semester_id, int shift_id, int sex, String num, String image){
		
		Connection conn=Database.getconnection();
		String sql="INSERT INTO `student`(`student_id`, `name`, `department_id`, `semester_id`, `shift_id`, "
				+ "`sex`, `number`, `image`) VALUES (?,?,?,?,?,?,?,?)";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, student_id);
			pst.setString(2, name);
			pst.setInt(3, dpt_id);
			pst.setInt(4, semester_id);
			pst.setInt(5, shift_id);
			pst.setInt(6, sex);
			pst.setString(7, num);
			pst.setString(8, image);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//for showing data in table
	public static void load() {
		Connection conn=Database.getconnection();
		String sql="SELECT a.`student_id`,a.name,b.name,c.semester,d.name,"
				+ "(CASE a.`sex` WHEN 0 THEN 'Male' ELSE 'Female' END)as sex,"
				+ "a.number FROM `student` a,department b,semster c,shift d "
				+ "WHERE a.`department_id`=b.id AND a.`semester_id`=c.id AND a.`shift_id`=d.id";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			
			
			StudentListPanel.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static int getStudentId(String student_name) {
		
		int studntId=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `student_id` FROM `student` WHERE `name`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, student_name);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				studntId=rs.getInt("student_id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return studntId;
		
	}
	
	
	public static int getDepartmentId(int student_id) {
		
		int dpt_id=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `department_id` FROM `student` WHERE `student_id`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, student_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				dpt_id=rs.getInt("department_id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dpt_id;
		
	}
	
	public static int getSemesterId(int student_id) {
		
		int semester_id=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT * FROM `student` WHERE `student_id`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, student_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				semester_id=rs.getInt("semester_id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return semester_id;
		
	}
	
	public static int getShiftId(int student_id) {
		
		int shift_id=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT * FROM `student` WHERE `student_id`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, student_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				shift_id=rs.getInt("shift_id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return shift_id;
		
	}
	

	public static String getStudentName(int student_id) {
		
		String student_name ="";
		
		Connection conn=Database.getconnection();
		String sql="SELECT `name` FROM `student` WHERE student_id=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, student_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				student_name=rs.getString("name");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return student_name;
		
	}
	
	public static String getEmail(int student_id) {
		
		String email ="";
		
		Connection conn=Database.getconnection();
		String sql="SELECT `number` FROM `student` WHERE `student_id`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, student_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				email=rs.getString("number");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return email;
		
	}
	
	
	public static String getImage(int student_id) {
		
		String image ="";
		
		Connection conn=Database.getconnection();
		String sql="SELECT `image` FROM `student` WHERE `student_id`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, student_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				image=rs.getString("image");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return image;
		
	}
	public static int getId(int student_id) {
		
		int id=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `id` FROM `student` WHERE `student_id`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, student_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				id=rs.getInt("id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return id;
		
	}
	

}
