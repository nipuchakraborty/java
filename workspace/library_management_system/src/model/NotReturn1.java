package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class NotReturn1 {
	
	private String date;
	private int total;
	private int paid;
	private int due;
	
	public void prepare(String r_date, int total_tk, int paid_tk, int due_tk){
		
		this.date=r_date;
		this.total=total_tk;
		this.paid=paid_tk;
		this.due=due_tk;
	}
	
	public static Connection conn=Database.getconnection();
	public void store(String i_date, String r_date, int s_id, int total_tk, int paid_tk, int due_tk) {
		
		String sql="INSERT INTO `not_return1`(`i_date`, `r_date`, `student_id`, `total_prize`, `paid`, `due`) "
				+ "VALUES (?,?,?,?,?,?)";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, i_date);
			pst.setString(2, r_date);
			pst.setInt(3, s_id);
			pst.setInt(4, total_tk);
			pst.setInt(5, paid_tk);
			pst.setInt(6, due_tk);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public int get_Id_form_NotReturn1() {
		int id=0;
		String sql="SELECT id FROM `not_return1` ORDER BY `id` DESC LIMIT 1";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			while(rs.next()){
				id=rs.getInt("id");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return id;
		
	}
	public void delete(int id) {
		
		Connection conn=Database.getconnection();
		String sql="DELETE FROM `not_return1` WHERE `student_id`=?";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, id);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String getReturnDate(int stuent_id) {
		
		String date = "";

		String sql="SELECT `r_date` FROM `not_return1` WHERE `student_id`=?";
		
		try {
			
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, stuent_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				date=rs.getString("r_date");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return date;
		
	}
	
	public static int getTotal(int stuent_id) {
		
		int total=0;

		String sql="SELECT `total_prize` FROM `not_return1` WHERE `student_id`=?";
		
		try {
			
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, stuent_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				total=rs.getInt("total_prize");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return total;
		
	}
	
	public static int getNotReurn1_id(int stuent_id) {
		
		int id=0;

		String sql="SELECT `id` FROM `not_return1` WHERE `student_id`=?";
		
		try {
			
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, stuent_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				id=rs.getInt("id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return id;	
	}
	
	public static int getStudentId(int id) {
		
		int s_id=0;

		String sql="SELECT `student_id` FROM `not_return1` WHERE `id`=?";
		
		try {
			
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				s_id=rs.getInt("student_id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return s_id;
		
	}
	
	// renew
	public void updateNotReturn1(int id) {
		
		Connection conn=Database.getconnection();
		String sql="UPDATE `not_return1` SET `r_date`=?,`total_prize`=?,`paid`=?,`due`=? WHERE `student_id`=?";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, this.date);
			pst.setInt(2, this.total);
			pst.setInt(3, this.paid);
			pst.setInt(4, this.due);
			pst.setInt(5, id);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
