package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Shift {
	
	public static ArrayList<String> getShift() {
		
		ArrayList<String> status=new ArrayList<>();
		
		Connection conn=Database.getconnection();
		String sql="SELECT `name` FROM `shift`";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			
			while (rs.next()) {
				status.add(rs.getString("name"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return status;
		
	}
	
	
	public static int getShiftId(String shift) {
		int shift_id=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT * FROM `shift` WHERE `name`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, shift);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				shift_id=rs.getInt("id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return shift_id;
		
	}
	
	public static String getShift(int shift_id) {
		
		String shift ="";
		
		Connection conn=Database.getconnection();
		String sql="SELECT * FROM `shift` WHERE id=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, shift_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				shift=rs.getString("name");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return shift;
		
	}

}
