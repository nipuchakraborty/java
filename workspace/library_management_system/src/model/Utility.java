package model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Utility {
	
	
	public static int changeStatusIntoZeroOne(String status) {
		int st;
		if(status.equals("Available")){
			st=0;
		}
		else{
			st=1;
		}
		return st;
	}
	
	public static int sex_StringToInt(String sex) {
		int sex_int;
		if(sex.equals("Male")){
			sex_int=0;
		}
		else{
			sex_int=1;
		}
		return sex_int;
	}
	
	public static String dateToString(Date date){
		
		String mainDate=null;
		
		DateFormat cldate=new SimpleDateFormat("dd/MM/yy");
		mainDate=cldate.format(date);
		
		return mainDate;
	}
	
	public static String getCurrentDate() {
		
		Calendar cal=new GregorianCalendar();
		int month=cal.get(Calendar.MONTH);
		int mn=month+1;
		int year=cal.get(Calendar.YEAR);
		int day=cal.get(Calendar.DAY_OF_MONTH);
//		cal.add(Calendar.DAY_OF_MONTH, 7);
//		Date d=cal.getTime();
//		String date=dateToString(d);
		String date=day+"/"+mn+"/"+year;
		return date;
	}
	public static String getReturnDate() {
		
		Calendar cal=new GregorianCalendar();
		int month=cal.get(Calendar.MONTH);
		int mn=month+1;
		int year=cal.get(Calendar.YEAR);
		int day=cal.get(Calendar.DAY_OF_MONTH);
		cal.add(Calendar.DAY_OF_MONTH, 7);
		Date d=cal.getTime();
		String date=dateToString(d);
	//	String date=day+"/"+mn+"/"+year;
		return date;
	}


}
