package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.proteanit.sql.DbUtils;
import view.panel.ReturnAbleBookPanel;
import view.panel.StaticsPnael;
import view.panel.StockBookPanel;

public class StockBook {
	
	public static void load() {
		Connection conn=Database.getconnection();
		String sql="SELECT a.book_name as Book,b.category_name as Category,a.writter as Writter,c.`stock` as Stock"
				+ " FROM bookstocksecend c, book a, book_category b WHERE a.id=c.book_id AND b.id=a.category_id AND a.id>1";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			StockBookPanel.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// search data and show on table
	public static void loadSearch(String user) {
		Connection conn=Database.getconnection();
		String sql="SELECT b.book_name as Book,c.category_name as Category,b.writter as Writter,a.`stock` as Stock"
				+ " FROM `bookstocksecend` a,book b,book_category c"
				+ " WHERE a.`book_id`=b.id AND c.id=b.id and a.`book_id`>1 AND b.book_name LIKE %"+user+"%";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			StockBookPanel.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static int getStudentId_FromReturn(String student_name) {
		int s_id=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `Id` FROM `returnable_book_2nd` WHERE `Name`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, student_name);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				s_id=rs.getInt("Id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return s_id;
		
	}
}
