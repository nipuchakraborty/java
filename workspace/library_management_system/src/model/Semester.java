package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Semester {
	
	
		public static ArrayList<String> getSemester() {
			
			ArrayList<String> status=new ArrayList<>();
			
			Connection conn=Database.getconnection();
			String sql="SELECT `semester` FROM `semster`";
			
			try {
				PreparedStatement pst=conn.prepareStatement(sql);
				ResultSet rs=pst.executeQuery();
				
				while (rs.next()) {
					status.add(rs.getString("semester"));
					
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return status;
			
		}
		
		
		public static int getSemestertId(String semester) {
			int semeter_id=0;
			
			Connection conn=Database.getconnection();
			String sql="SELECT * FROM `semster` WHERE `semester`=?";
			
			try {
				
				PreparedStatement pst=conn.prepareStatement(sql);
				pst.setString(1, semester);
				ResultSet rs=pst.executeQuery();
				
				while(rs.next()){
					semeter_id=rs.getInt("id");
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return semeter_id;
			
		}
		
		public static String getSemester(int semester_id) {
			
			String semester ="";
			
			Connection conn=Database.getconnection();
			String sql="SELECT * FROM `semster` WHERE id=?";
			
			try {
				
				PreparedStatement pst=conn.prepareStatement(sql);
				pst.setInt(1, semester_id);
				ResultSet rs=pst.executeQuery();
				
				while(rs.next()){
					semester=rs.getString("semester");
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return semester;
			
		}
		

}
