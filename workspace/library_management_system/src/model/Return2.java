package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

public class Return2 {
	public static Connection conn=Database.getconnection();
	public void store(int return1_id, ArrayList<Integer> student_id, ArrayList<Integer> book_id) {
		
		String sql="INSERT INTO `return2`(`return1_id`, `student_id`, `book_id`) VALUES (?,?,?)";
		
		try {
			for(int i=0;i<student_id.size();i++){
				PreparedStatement pst=conn.prepareStatement(sql);
				pst.setInt(1, return1_id);
				pst.setInt(2, student_id.get(i));
				pst.setInt(3, book_id.get(i));
				pst.execute();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
