package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.proteanit.sql.DbUtils;
import view.panel.BookPanel;
import view.panel.StaticsPnael;

public class IssueBook {
	
	public void store(String main_date, int s_id, String name, int dpt_id, int semester_id, int shift_id, 
			int book_id){
		
		Connection conn=Database.getconnection();
		String sql="INSERT INTO `issue_book`"
				+ "(`issue_date`, `student_id`, `name`, `dpt_id`, `semester_id`, `shift_id`, `book_id`)"
				+ " VALUES (?,?,?,?,?,?,?)";
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, main_date);
			pst.setInt(2, s_id);
			pst.setString(3, name);;
			pst.setInt(4, dpt_id);
			pst.setInt(5, semester_id);
			pst.setInt(6, shift_id);
			pst.setInt(7, book_id);
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	public static void load() {
		Connection conn=Database.getconnection();
		String sql="SELECT a.`issue_date` as Date, a.`student_id` as Student_Id , a.`name` as Name ,b.book_name as Book "
				+ "FROM `issue_book` a, book b WHERE a.`book_id`=b.id";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			StaticsPnael.issue_table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static int getIssueBookId(int stuent_id) {
		
		int book_id=0;
		
		
		Connection conn=Database.getconnection();
		String sql="SELECT `book_id` FROM `issue_book` WHERE `student_id`=?";
		
		try {
			
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, stuent_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				book_id=rs.getInt("book_id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return book_id;
		
	}
	public static int getFristIssueBookId(int stuent_id) {
		
		int book_id=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `book1_id` FROM `issue_book` WHERE `student_id`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, stuent_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				book_id=rs.getInt("book1_id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return book_id;
		
	}
	public static int getSecendIssueBookId(int stuent_id) {
		
		int book_id=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `book2_id` FROM `issue_book` WHERE `student_id`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, stuent_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				book_id=rs.getInt("book2_id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return book_id;
		
	}
	
	public static int getIssueStudentId(String name) {
		
		int issue_s_id=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `student_id` FROM `issue_book` WHERE `name`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, name);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				issue_s_id=rs.getInt("student_id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return issue_s_id;
		
	}
	
	public static String getIssueDate(int student_id) {
		
		String date ="";
		
		Connection conn=Database.getconnection();
		String sql="SELECT `issue_date` FROM `issue_book` WHERE `student_id`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, student_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				date=rs.getString("issue_date");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return date;
		
	}

}
