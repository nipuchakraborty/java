package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import net.proteanit.sql.DbUtils;
import view.edit.BooksCategoryEidtDailog;
import view.panel.BooksCategoryPanel;

public class BookCategory {
	
	private String cat;
	private int status;
	
	public void prepare(String cat, int st) {
		
		this.cat=cat;
		this.status=st;
		
	}
	
	//for inserting data
	public void store() {
			
			Connection conn=Database.getconnection();
			String sql="INSERT INTO `book_category`(`category_name`, `status`) VALUES (?,?)";
			
			try {
				PreparedStatement pst=conn.prepareStatement(sql);
				pst.setString(1, this.cat);
				pst.setInt(2, this.status);
				pst.execute();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
	}
	//for showing data in table
	public static void load() {
		Connection conn=Database.getconnection();
		String sql="SELECT `category_name`, (CASE `status` WHEN 0 THEN 'Available' ELSE 'Not Available' END) as status "
				+ "FROM `book_category` WHERE id>1";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			
			
			BooksCategoryPanel.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateCategory(int id) {
		
		Connection conn=Database.getconnection();
		String sql="UPDATE `book_category` SET `category_name`=?,`status`=? WHERE id=?";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, this.cat);
			pst.setInt(2, this.status);
			pst.setInt(3, id);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void deleteCategory(int id) {
		
		Connection conn=Database.getconnection();
		String sql="DELETE FROM `book_category` WHERE id=?";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, id);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static ArrayList<String> getCategoryName() {
		
		ArrayList<String> catName=new ArrayList<>();
		
		Connection conn=Database.getconnection();
		String sql="SELECT `category_name` FROM `book_category`";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			
			while (rs.next()) {
				catName.add(rs.getString("category_name"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return catName;
		
	}
	
	public static int getCategoryId(String cat_Name) {
		int categoryId=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `id` FROM `book_category` where category_name=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, cat_Name);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				categoryId=rs.getInt("id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return categoryId;
		
	}
	
	
	public static String getCategoryName(int cat_id) {
		String categoryName = null;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `category_name` FROM `book_category` where id=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, cat_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				categoryName=rs.getString("name");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return categoryName;
		
	}

}
