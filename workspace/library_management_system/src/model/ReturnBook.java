package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.proteanit.sql.DbUtils;
import view.panel.BookPanel;
import view.panel.StaticsPnael;

public class ReturnBook {
	
public void store(int s_id, String date, String name, int dpt_id, int semester_id, int shift_id, int book_id){
		
		Connection conn=Database.getconnection();
		String sql="INSERT INTO `return_book`"
				+ "(`return_date`, `student_id`, `name`, `dpt_id`, `semester_id`, `shift_id`, `book_id`)"
				+ " VALUES (?,?,?,?,?,?,?)";
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, date);
			pst.setInt(2, s_id);
			pst.setString(3, name);;
			pst.setInt(4, dpt_id);
			pst.setInt(5, semester_id);
			pst.setInt(6, shift_id);
			pst.setInt(7, book_id);
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}


	public static void load() {
		Connection conn=Database.getconnection();
		String sql="SELECT a.`return_date` as Date, a.`student_id` as Student_Id , a.`name` as Name ,"
				+ "b.book_name as Book FROM return_book a, book b WHERE a.`book_id`=b.id";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			StaticsPnael.return_table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
