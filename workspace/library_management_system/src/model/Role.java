package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Role {
	
	public static ArrayList<String> getRole() {
		
		ArrayList<String> status=new ArrayList<>();
		
		Connection conn=Database.getconnection();
		String sql="SELECT * FROM `role`";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			
			while (rs.next()) {
				status.add(rs.getString("name"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return status;
		
	}
	

	public static int getRoleId(String role) {
		int role_id=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT * FROM `role` WHERE `name`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, role);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				role_id=rs.getInt("id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return role_id;
		
	}

}
