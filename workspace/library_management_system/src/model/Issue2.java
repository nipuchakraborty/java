package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Issue2 {
	
	public static Connection conn=Database.getconnection();
	
	public static void store(int issue1_id, ArrayList<Integer> student_id, ArrayList<Integer> b_id, ArrayList<Integer> issue_prize) {
		
		String sql="INSERT INTO `issue2`(`issue1_id`, `student_id`, `book_id`, `issue_prize`) VALUES (?,?,?,?)";
		
		try {
			for(int i=0;i<student_id.size();i++){
				PreparedStatement pst=conn.prepareStatement(sql);
				pst.setInt(1, issue1_id);
				pst.setInt(2, student_id.get(i));
				pst.setInt(3, b_id.get(i));
				pst.setInt(4, issue_prize.get(i));
				pst.execute();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public static int getSumOfIssuePrize(int stuent_id) {
		
		int sum=0;

		String sql="SELECT SUM(`issue_prize`) FROM `issue2` WHERE `issue1_id`=? GROUP by issue1_id";
		
		try {
			
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, stuent_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				sum=rs.getInt("issue_prize");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return sum;
		
	}
}
