package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import net.proteanit.sql.DbUtils;
import view.neww.ReNewPanel;
import view.neww.ReturnBookPanel2;
import view.panel.ReturnAbleBookPanel;

public class NotReturn {
	
public static Connection conn=Database.getconnection();
	
	public static void store(int not_return1_id, ArrayList<Integer> student_id, ArrayList<Integer> b_id) {
		
		String sql="INSERT INTO `not_return`(`not_return1_id`, `student_id`, `book_id`) VALUES (?,?,?)";
		
		try {
			for(int i=0;i<student_id.size();i++){
				PreparedStatement pst=conn.prepareStatement(sql);
				pst.setInt(1, not_return1_id);
				pst.setInt(2, student_id.get(i));
				pst.setInt(3, b_id.get(i));
				pst.execute();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void delete(int id) {
		
		Connection conn=Database.getconnection();
		String sql="DELETE FROM `not_return` WHERE `student_id`=?";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, id);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// ReturnBookPanel2
	public static void loadForReturnById(int id) {
		String sql="SELECT a.`student_id` AS Student_id, b.book_name as Book "
				+ "FROM `not_return` a,book b WHERE a.book_id=b.id AND a.`student_id`="+id;
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			ReturnBookPanel2.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//for renew
	public static void loadForReturnById_renew(int id) {
		String sql="SELECT a.`student_id` AS Student_Id, b.book_name as Book "
				+ "FROM `not_return` a,book b WHERE a.book_id=b.id AND a.`student_id`="+id;
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			ReNewPanel.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// for ReturnAbleBookPanel 
	public static void load() {
		Connection conn=Database.getconnection();
		String sql="SELECT a.`student_id` as Student_Id,b.name as Name, c.book_name as Book FROM `not_return` a,student b,book c WHERE a.`student_id`=b.student_id AND a.`book_id`=c.id";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			ReturnAbleBookPanel.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static int getrowInfo(int stuent_id) {
		int row=0;

		Connection conn=Database.getconnection();
		String sql="SELECT COUNT(`not_return1_id`) as row_info FROM `not_return` WHERE `student_id`=?";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, stuent_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				row=rs.getInt("row_info");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return row;
	}
	
	public static int getNotReturn_Student_id(int id) {
		
		int s_id=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT a.`student_id` FROM `not_return1` a ,not_return b WHERE b.student_id=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				s_id=rs.getInt("a.`student_id`");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return s_id;
		
	}
	// total of not return issue_prize 
	public static int getTotal(int stuent_id) {
		
		int sum=0;

		String sql="SELECT SUM(a.issue_prize) FROM `not_return` b, book a "
				+ "WHERE a.id=b.book_id and b.`student_id`=1 GROUP by b.`student_id`";
		
		try {
			
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, stuent_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				sum=rs.getInt("sum(issue_prize)");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return sum;
		
	}
}
