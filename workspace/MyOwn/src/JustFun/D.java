package JustFun;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class D {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					D window = new D();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public D() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 541, 418);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblMonthIncome = new JLabel("Month Income");
		lblMonthIncome.setBounds(58, 32, 81, 16);
		frame.getContentPane().add(lblMonthIncome);
		
		textField = new JTextField();
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Database database=new Database();
				String t = database.Store();
				String s=database.Store();
				textField.setText(s);
				
			}
		});
		textField.setBounds(173, 29, 116, 22);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblExpense = new JLabel("Expense");
		lblExpense.setBounds(58, 104, 56, 16);
		frame.getContentPane().add(lblExpense);
		
		textField_1 = new JTextField();
		textField_1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				String salary=textField.getText();
				int s=Integer.parseInt(salary);
				String expense=textField_1.getText();
				int e=Integer.parseInt(expense);
				int sum=s-e;
				String save=Integer.toString(sum);
				textField_2.setText(save);
			}
		});
		textField_1.setBounds(173, 101, 116, 22);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblSafe = new JLabel("Safe");
		lblSafe.setBounds(58, 202, 56, 16);
		frame.getContentPane().add(lblSafe);
		
		textField_2 = new JTextField();
		textField_2.setBounds(173, 199, 116, 22);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
	}
}
