package JustFun;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.Dialog.ModalExclusionType;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;

public class E extends JFrame {
	static Point mouseDownScreenCoords;
    static Point mouseDownCompCoords;

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					E frame = new E();
					frame.setUndecorated(true);
frame.addMouseListener(new MouseListener() {
						
						@Override
						public void mouseReleased(MouseEvent e) {
							mouseDownScreenCoords = null;
		                    mouseDownCompCoords = null;
						}
						
						@Override
						public void mousePressed(MouseEvent e) {
							 mouseDownScreenCoords = e.getLocationOnScreen();
			                    mouseDownCompCoords = e.getPoint();
							
						}
						
						@Override
						public void mouseExited(MouseEvent e) {
						
							
						}
						
						@Override
						public void mouseEntered(MouseEvent e) {
							
							
						}
						
						@Override
						public void mouseClicked(MouseEvent e) {
							
							
						}
					});
					
					frame.addMouseMotionListener(new MouseMotionListener() {
						
						@Override
						public void mouseMoved(MouseEvent e) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void mouseDragged(MouseEvent e) {
							Point currCoords = e.getLocationOnScreen();
		                    frame.setLocation(mouseDownScreenCoords.x + (currCoords.x - mouseDownScreenCoords.x) - mouseDownCompCoords.x,
		                                  mouseDownScreenCoords.y + (currCoords.y - mouseDownScreenCoords.y) - mouseDownCompCoords.y);
						}
					});
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public E() {
setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		
		
		setModalExclusionType(ModalExclusionType.TOOLKIT_EXCLUDE);
	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setSize(800,450);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblExpenseList = new JLabel("Expense List");
		lblExpenseList.setBounds(51, 71, 108, 16);
		contentPane.add(lblExpenseList);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Fasion", "Study", "House", "Friends", "Snakes", "Help"}));
		comboBox.setBounds(199, 68, 153, 22);
		contentPane.add(comboBox);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 135, 714, 230);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JLabel lblExit = new JLabel("Exit");
		lblExit.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});
		lblExit.setBounds(753, 0, 29, 22);
		contentPane.add(lblExit);
	}
	 public static class FrameDragListener extends MouseAdapter {

	        private final JFrame frame;
	        private Point mouseDownCompCoords = null;

	        public FrameDragListener(JFrame frame) {
	            this.frame = frame;
	        }

	        public void mouseReleased(MouseEvent e) {
	            mouseDownCompCoords = null;
	        }

	        public void mousePressed(MouseEvent e) {
	            mouseDownCompCoords = e.getPoint();
	        }

	        public void mouseDragged(MouseEvent e) {
	            Point currCoords = e.getLocationOnScreen();
	            frame.setLocation(currCoords.x - mouseDownCompCoords.x, currCoords.y - mouseDownCompCoords.y);
	        }
	 }
}
