package calculator1;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*;
 
public class Calulator2 extends JFrame implements ActionListener{
    private JButton[] nums;
    private JButton eq, mult, div, clr, clrEnt, sub, add, dot, back;
    private JTextField textfield;
    private String[] numString = {"0", "1", "2", "3", "4", "5", "6", "7",
        "8", "9"};
    private StringBuilder strBuild;
    private double result;
    private String operator;
 
    Calulator2() {
        super("Calculator");
 
        result = 0;
        operator = "=";
 
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        strBuild = new StringBuilder();
 
        OpButtonHandler handle = new OpButtonHandler();
 
        textfield = new JTextField("0");
        textfield.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
        AbstractDocument doc = (AbstractDocument) textfield.getDocument();
        //doc.setDocumentFilter(new LengthRestrictedDocument(5));
 
        strBuild = new StringBuilder(textfield.getText());
 
        JPanel pane = new JPanel(new GridLayout(8, 1, 0, 5));
        pane.add(textfield);
 
        JPanel paneSecond = new JPanel(new GridLayout(1, 3, 5, 5));
        JPanel paneThird = new JPanel(new GridLayout(1, 3, 5, 5));
        JPanel paneFourth = new JPanel(new GridLayout(1, 3, 5, 5));
        JPanel paneFifth = new JPanel(new GridLayout(1, 3, 5, 5));
        JPanel paneSixth = new JPanel(new GridLayout(1, 3, 5, 5));
        JPanel paneSeventh = new JPanel(new GridLayout(1, 3, 5, 5));
 
        nums = new JButton[11];
        mult = createOpButton("x", handle);
        div = createOpButton("/", handle);
        add = createOpButton("+", handle);
        sub = createOpButton("-", handle);
        eq = createOpButton("=", handle);
        clr = createOpButton("C", handle);
        clrEnt = createOpButton("CE", handle);
        back = createOpButton("BACKSPACE", handle);
        dot = createOpButton(".", handle);
 
        for (int i = 0; i < numString.length; i++) {
            nums[i] = new JButton(numString[i]);
            nums[i].setActionCommand(numString[i]);
            nums[i].addActionListener(this);
        }
 
        addPanel(paneSecond, 1, 4);
        addPanel(paneThird, 4, 7);
        addPanel(paneFourth, 7, 10);
        addButtons(paneFifth, add, nums[0], sub);
        addButtons(paneSixth, mult, div, dot);
        addButtons(paneSeventh, clr, clrEnt, eq);
        pane.add(paneSecond);
        pane.add(paneThird);
        pane.add(paneFourth);
        pane.add(paneFifth);
        pane.add(paneSixth);
        pane.add(paneSeventh);
        pane.add(back);
        add(pane);
        pack();
    }
 
    void addPanel(JComponent pane, int start, int condition) {
        for (int i = start; i < condition; i++) {
            pane.add(nums[i]);
        }
    }
 
    void addButtons(JComponent pane, JButton btn1, JButton btn2, JButton btn3) {
        pane.add(btn1);
        pane.add(btn2);
        pane.add(btn3);
    }
 
    JButton createOpButton(String op, ActionListener listener) {
        JButton btn = new JButton(op);
        btn.setText(op);
        if (op.equals(".")) {
            btn.addActionListener(listener);
        }
        else {
            btn.setActionCommand(op);
        }
 
        btn.addActionListener(new OpButtonHandler());
        btn.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 17));
        return btn;
    }
 
    private class OpButtonHandler implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            strBuild.delete(0, strBuild.length());
            String comm = e.getActionCommand();
            if (comm.equals("CE")) {
                strBuild.delete(0, strBuild.length());
                textfield.setText("0");
            }
            else if (comm.equals("C")) {
                strBuild.delete(0, strBuild.length());
                textfield.setText("0");
                result = 0;
            }
            else if (comm.equals("BACKSPACE")) {
                if (textfield.getText().length() > 0) {
                    strBuild.deleteCharAt(strBuild.length());
                    setTextField(strBuild);
                }
            }
            calc(Double.parseDouble(textfield.getText()));
            operator = comm;
        }
    }
 
 
    public void actionPerformed(ActionEvent e) {
        if (getTextField().equals("0")) {
            strBuild.delete(0, strBuild.length());
        }
        String number = e.getActionCommand();
        if (strBuild.length() < 15) {
            strBuild.append(number);
            setTextField(strBuild);
        }
    }
 
    public String getTextField() {
        return textfield.getText();
    }
 
    public void setTextField(StringBuilder strBuilder) {
        String str = strBuilder.toString();
        textfield.setText(str);
    }
 
    public void calc(double d) {
        if (operator.equals("+")) result += d;
              else if (operator.equals("-")) result -= d;
              else if (operator.equals("x")) result *= d;
              else if (operator.equals("/")) result /= d;
              else if (operator.equals("=")) result = d;
        textfield.setText("" + result);
    }
 
    public static void main(String[] args) {
        new Calulator2();
    }
}