package calculator1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.ImageIcon;
import javax.swing.border.MatteBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import net.miginfocom.swing.MigLayout;

public class Cal extends JInternalFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel label;
	private JDesktopPane desktopPane;
	int w = 750;
	int h = 400;
	private JInternalFrame jInternalFrame;
	private JDesktopPane desktopen;
	JPanel header, center, footer;
	Border border = new EtchedBorder();
	Font font = new Font("courier new", Font.PLAIN, 16);
	private JTextField disply_TxtF;
	
	//function variable 
	 double firstNum;
	    double secondNum;
	    double total;
	    double plusminus;

	    int plusClick;
	    int minusClick;
	    int multiplyClick;
	    int devideClick;
	    int decimalClick;
	    int calcOperation = 0;
	    int currentCalc;
	    boolean error;
	    String oper="";
	boolean isDot=false;
	String tempNum,decimalUsed;
	double acc1 = 0, acc2 = 0, acc3 = 10;
	///////////////////////////////
	
	
	
	public boolean  setClear=true;
	double number;
	char op;
	//MyDigitButton my=new MyDigitButton();
	

	public Cal() {
		
		getContentPane().setBackground(new Color(128, 0, 0));
		getContentPane().setLayout(null);
		
		disply_TxtF = new JTextField();
		disply_TxtF.setBounds(7, 7, 292, 35);
		disply_TxtF.setFont(new Font("Sitka Small", Font.PLAIN, 12));
		disply_TxtF.setBackground(Color.WHITE);
		getContentPane().add(disply_TxtF);
		disply_TxtF.setColumns(10);
		
		
		//backSapcework
		
		
		JButton bBS = new JButton("BackSpace");
		bBS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 String s = e.getActionCommand().trim();
				if(s.equals("BackSpace"))
					
						if(!error)  {
					
						    if(disply_TxtF.getText().length() < 2) {
					
						        tempNum = "";
					
						        disply_TxtF.setText("0");
					
						    } else  {
					
						        if((disply_TxtF.getText().charAt(disply_TxtF.getText().length() - 1) == '.' &&
					
						        disply_TxtF.getText().charAt(disply_TxtF.getText().length() - 2) == '0') ||
					
						        (disply_TxtF.getText().length() == 2 && disply_TxtF.getText().charAt(0) == '-'))    {
					
						            tempNum = "";
				
						            disply_TxtF.setText("0");
					
						            isDot = false;
				
						        }   else    {
					
						            if(disply_TxtF.getText().charAt(disply_TxtF.getText().length() - 1) == '.')
					
						            isDot = false;
					
						            tempNum = disply_TxtF.getText().substring(0, disply_TxtF.getText().length() - 1);
					
						           disply_TxtF.setText(tempNum);
					
						        }
				
						    }
					
						    acc1 = Double.parseDouble(disply_TxtF.getText()); // here, we assign the value
					
						 
					
						}
			
			}
		});
		bBS.setBounds(7, 54, 90, 28);
		getContentPane().add(bBS);
		
		JButton bClr = new JButton("C");
		bClr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				disply_TxtF.setText("");
			}
		});
		bClr.setBounds(110, 54, 45, 28);
		getContentPane().add(bClr);
		
		JButton bCE = new JButton("CE");
		bCE.setBounds(168, 54, 45, 28);
		getContentPane().add(bCE);
		
		JButton btnMC = new JButton("MC");
		btnMC.setBounds(7, 94, 46, 28);
		getContentPane().add(btnMC);
		
		JButton btnMr = new JButton("MR");
		btnMr.setBounds(7, 123, 46, 28);
		getContentPane().add(btnMr);
		
		JButton btnMS = new JButton("MS");
		btnMS.setBounds(7, 159, 46, 28);
		getContentPane().add(btnMS);
		
		JButton btnMpluse = new JButton("M+");
		btnMpluse.setBounds(7, 188, 46, 28);
		getContentPane().add(btnMpluse);
		
		JButton b7 = new JButton("7");
		b7.setBounds(65, 94, 37, 28);
		b7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 // display.setText(display.getText()+jButton1.getText());
				  disply_TxtF.setText(disply_TxtF.getText()+b7.getText());
			}
		});
		getContentPane().add(b7);
		
		JButton b4 = new JButton("4");
		b4.setBounds(65, 123, 37, 28);
		b4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				disply_TxtF.setText(disply_TxtF.getText()+b4.getText());
			}
		});
		getContentPane().add(b4);
		
		JButton b1 = new JButton("1");
		b1.setBounds(65, 159, 37, 28);
		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				disply_TxtF.setText(disply_TxtF.getText()+b1.getText());
			}
		});
		getContentPane().add(b1);
		
		JButton b0 = new JButton("0");
		b0.setBounds(65, 188, 37, 28);
		b0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				disply_TxtF.setText(disply_TxtF.getText()+b0.getText());
			}
		});
		getContentPane().add(b0);
		
		JButton b8 = new JButton("8");
		b8.setBounds(110, 94, 45, 28);
		b8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				disply_TxtF.setText(disply_TxtF.getText()+b8.getText());
			}
		});
		getContentPane().add(b8);
		
		JButton b5 = new JButton("5");
		b5.setBounds(110, 123, 45, 28);
		b5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				disply_TxtF.setText(disply_TxtF.getText()+b5.getText());
			}
		});
		getContentPane().add(b5);
		
		JButton b2 = new JButton("2");
		b2.setBounds(110, 159, 45, 28);
		b2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				disply_TxtF.setText(disply_TxtF.getText()+b2.getText());
			}
		});
		getContentPane().add(b2);
		
		JButton pluse_minus = new JButton("+/_");
		pluse_minus.setBounds(110, 188, 45, 28);
		pluse_minus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 plusminus=(Double.parseDouble(String.valueOf(disply_TxtF.getText())));
				    plusminus=plusminus*(-1);
				    disply_TxtF.setText(String.valueOf(plusminus));
			}
		});
		getContentPane().add(pluse_minus);
		
		JButton b9 = new JButton("9");
		b9.setBounds(159, 94, 35, 28);
		b9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				disply_TxtF.setText(disply_TxtF.getText()+b9.getText());
			}
		});
		getContentPane().add(b9);
		
		JButton b6 = new JButton("6");
		b6.setBounds(159, 123, 35, 28);
		b6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				disply_TxtF.setText(disply_TxtF.getText()+b6.getText());
			}
		});
		getContentPane().add(b6);
		
		JButton b3 = new JButton("3");
		b3.setBounds(159, 159, 35, 28);
		b3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				disply_TxtF.setText(disply_TxtF.getText()+b3.getText());
			}
		});
		getContentPane().add(b3);
		
		JButton bDot = new JButton(".");
		bDot.setBounds(159, 188, 35, 28);
		getContentPane().add(bDot);
		
		JButton divate = new JButton("/");
		divate.setBounds(202, 94, 39, 28);
		getContentPane().add(divate);
		
		
		//pluse boutton
		JButton pluse = new JButton("+");
		pluse.setActionCommand("+");
		pluse.setBounds(202, 123, 39, 28);
		getContentPane().add(pluse);
		
		JButton button_14 = new JButton("%");
		button_14.setBounds(202, 159, 39, 28);
		getContentPane().add(button_14);
		
		JButton bEquals = new JButton("=");
		bEquals.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!disply_TxtF.getText().isEmpty()) {
					int number =Integer.parseInt(disply_TxtF.getText());
					if (calcOperation==1) {
						int calculate=currentCalc+number;
						disply_TxtF.setText(Integer.toString(calculate));
						
					}
					else if (calcOperation==2) {
						int calculate=currentCalc-number;
						disply_TxtF.setText(Integer.toString(calculate));
						
					}
					
				}
			
                }
		});
		bEquals.setBounds(202, 188, 39, 28);
		
		
		getContentPane().add(bEquals);
		
		JButton button_16 = new JButton("*");
		button_16.setActionCommand("*");
		button_16.setBounds(249, 94, 50, 28);
		getContentPane().add(button_16);
		//Minus Boutton
		
		JButton subButton = new JButton("-");
		subButton.setActionCommand("-");
		subButton.setActionCommand("-");
		subButton.setBounds(249, 123, 50, 28);
		getContentPane().add(subButton);
		
		////////////////////////////
		JButton btnx = new JButton("1/X");
		btnx.setBounds(249, 159, 50, 28);
		getContentPane().add(btnx);
		
		JButton bSqrt = new JButton("Sqrt");
		bSqrt.setBounds(249, 188, 50, 28);
		getContentPane().add(bSqrt);
		setLocation(new Point(350, 100));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		setDoubleBuffered(true);
		
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		//Dimention Screen
		Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();
		//header Design
		header=new JPanel();
		header.setBorder(new EtchedBorder(EtchedBorder.LOWERED,null,null));
		header.setBackground(Color.BLUE);
		header.setPreferredSize(new Dimension(screen.width, 30));
		//center Design
		center=new JPanel();
		center.setPreferredSize(new Dimension(screen.width+50,50));
		//foter design
		footer=new JPanel();
		//footer.setBackground(Color.blue);
		footer.setBackground(new Color(155, 200, 200));
		footer.setPreferredSize(new Dimension(screen.width, 50));

		setResizable(true);
		
		setSize(327, 271);
		show();
	}
	public  static void  Main(String []args){
		try {
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		Cal addFood=new Cal();
		addFood.setVisible(true);
		addFood.setDefaultCloseOperation(HIDE_ON_CLOSE);
	}
	 private class OperatorAction implements ActionListener
	    {
	        private int operator;
	        
	        public OperatorAction(int operation)
	        {
	            operator = operation;
	        }
	        
	        public void actionPerformed(ActionEvent event)
	        {
	            currentCalc = Integer.parseInt(disply_TxtF.getText()); 
	            calcOperation = operator;
	        }
	    }
	 public void addNumber(int _num)

	    {

	        if (isDot)

	        {

	            acc1 = acc1 + _num/acc3;

	            acc3 = acc3 * 10;

	        }

	        else

	        {

	            acc1 = acc1 * 10 + _num;

	        }

	        disply_TxtF.setText(Double.toString(acc1));

	    }

	 

	    public void performAction(String currentOper)

	    {

	        if (oper.equals("+"))

	        {

	            acc2 = acc2 + acc1;

	            acc1 = 0;

	        }

	        else if (oper.equals("-"))

	        {

	            acc2 = acc2 - acc1;

	            acc1 = 0;

	        }

	        else if (oper.equals("*"))

	        {

	            acc2 = acc2 * acc1;

	            acc1 = 0;

	        }
	        else if (oper.equals("/"))

	        {

	            acc2 = acc2 / acc1;

	            acc1 = 0;

	            disply_TxtF.setText(""+acc2);

	        }
	        /*else if (oper.equals("="))

	        {

	            acc2 = acc1;

	            acc1 = 0;

	        }*/

	        else

	        {

	            acc2 = acc1;

	            acc1 = 0;

	        }

	 

	        oper = currentOper;

	        disply_TxtF.setText(Double.toString(acc2));

	        isDot = false;

	        acc3 = 10;

	    }
	    public void clearMemory()

	    {

	    acc2 = 0;

	    acc3 = 0;

	    acc1 = 0;

	    oper = "";

	    disply_TxtF.setText("0");

	    //t.setTotal("0");

	    }

	 

	    public void CcMemory()
    {

	        disply_TxtF.setText("0");

	        //acc2 = 0;

	        acc1 = 0;

	    }

	 

	    public void X3memory()

	    {

	    //  acc1 = Math.pow(acc1, 6);

	        //oper = currentOper1;

	    //  t.setText(Double.toString(acc1));

	    //  isDot = false;

	    //  acc3 = 10;

	    }

	
}
