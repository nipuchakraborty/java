package oparetor;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Button;

public class Mobile_oparetor extends JFrame {

	private JPanel contentPane;
	private JTextField num;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mobile_oparetor frame = new Mobile_oparetor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Mobile_oparetor() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMobileNumber = new JLabel("Mobile Number :");
		lblMobileNumber.setBounds(34, 62, 96, 14);
		contentPane.add(lblMobileNumber);
		
		num = new JTextField();
		num.setBounds(119, 59, 148, 20);
		contentPane.add(num);
		num.setColumns(10);
		
		JLabel lblMobileOparetorSelection = new JLabel("Mobile Oparetor Selection ");
		lblMobileOparetorSelection.setBounds(119, 22, 148, 29);
		contentPane.add(lblMobileOparetorSelection);
		
		JButton Find = new JButton("Find");
		Find.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String number=num.getText();
				int number_for_calculate=Integer.parseInt(number);
				int []remainder=new int[11];
				if(number.length()==11){
					int i=0;
					
					while(number_for_calculate!=0){
						
						remainder[i]=(number_for_calculate%10);
						number_for_calculate=number_for_calculate/10;

						i++;
					}
					
					
					if(remainder[8]==8){
						M_oparator_output mp=new M_oparator_output("Robi");
						mp.setVisible(true);
						Mobile_oparetor.this.setVisible(false);
						
						
					}
					else if(remainder[8]==7){
						M_oparator_output mp=new M_oparator_output("Grameenphone");
						mp.setVisible(true);
						Mobile_oparetor.this.setVisible(false);
					}
					else if(remainder[8]==9){
						M_oparator_output mp=new M_oparator_output("Banglalink");
						mp.setVisible(true);
						Mobile_oparetor.this.setVisible(false);
					}
					else if(remainder[8]==6){
						M_oparator_output mp=new M_oparator_output("Aritel");
						mp.setVisible(true);
						Mobile_oparetor.this.setVisible(false);
					}
					else if(remainder[8]==5){
						M_oparator_output mp=new M_oparator_output("Taletalk");
						mp.setVisible(true);
						Mobile_oparetor.this.setVisible(false);
					}
					else {
						JOptionPane.showMessageDialog(null, "Your input is wrong...");
						Mobile_oparetor.this.setVisible(false);
					}
					
					
				}
				else{
					JOptionPane.showMessageDialog(null, "please enter a valid number");
				}
				
				
			}
		});
		Find.setBounds(133, 154, 89, 23);
		contentPane.add(Find);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 68, 21);
		contentPane.add(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmHome = new JMenuItem("Home");
		mnFile.add(mntmHome);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Mobile_oparetor.this.setVisible(false);
			}
		});
		mnFile.add(mntmExit);
	}
}
