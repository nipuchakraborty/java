package oparetor;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class M_oparator_output extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	
	/**
	 * Create the frame.
	 */
	public M_oparator_output(String text) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblThisOparatorIs = new JLabel("This Oparator is : ");
		lblThisOparatorIs.setBounds(22, 89, 108, 14);
		contentPane.add(lblThisOparatorIs);
		
		JTextArea op_name = new JTextArea();
		op_name.setBounds(140, 84, 122, 22);
		contentPane.add(op_name);
		op_name.setText(text);
		
		JLabel logo = new JLabel("");
		logo.setBounds(93, 143, 232, 107);
		contentPane.add(logo);
		
		
		
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 97, 21);
		contentPane.add(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmHome = new JMenuItem("Home");
		mntmHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Mobile_oparetor mo=new Mobile_oparetor();
				mo.setVisible(true);
				M_oparator_output.this.setVisible(false);
			}
		});
		mnFile.add(mntmHome);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				M_oparator_output.this.setVisible(false);
			}
		});
		mnFile.add(mntmExit);
	}
}
