public class PersonUI extends QWidget {

   private QLineEdit idField = new QLineEdit();
   //... mNameField, lNameField, emailField, phoneField

   private QPushButton createButton = new QPushButton("New...");
   //... updateButton, deleteButton, firstButton, prevButton, 
   //... nextButton,lastButton
   private PersonBean bean = new PersonBean();
   public PersonUI() {
      QVBoxLayout vlayout = new QVBoxLayout();
      vlayout.addLayout(initFields());
      vlayout.addLayout(initButtons());
      setFieldData(bean.moveFirst());
      setLayout(vlayout);
   }

   private QHBoxLayout initButtons() {
      QHBoxLayout hlayout = new QHBoxLayout();
      hlayout.addWidget(createButton);
      createButton.clicked.connect(this, "createAction()");
      hlayout.addWidget(updateButton);
      //...
      lastButton.clicked.connect(this, "moveLastAction()");
      return hlayout;
   }

   private QFormLayout initFields() {
      QFormLayout form = new QFormLayout();
      form.addRow("ID", idField);
      idField.setReadOnly(true);
      form.addRow("First Name", fNameField);
      //...
      form.addRow("Phone", phoneField);
      return form;
   }

   private Person getFieldData() {
      Person p = new Person();
      //...
      return p;
   }

   private void setFieldData(Person p) {
      idField.setText(String.valueOf(p.getPersonId()));
      //...
   }

   private boolean isEmptyFieldData() {
      //...
   }

   private void createAction() {
      Person p = getFieldData();
      switch (createButton.text()) {
      case "Save":
         if (isEmptyFieldData()) {
            QMessageBox.critical(this, "Invalid data",
            "Cannot create an empty record");
            return;
         }
         if (bean.create(p) != null)
            QMessageBox.information(this, "Create Successful",
            "New person created successfully.");
            createButton.setText("New...");
            break;
      case "New...":
         p.setPersonId(new Random().nextInt(Integer.MAX_VALUE) + 1);
         p.setFirstName("");
         //...
         setFieldData(p);
         createButton.setText("Save");
         break;
      }
   }

   private void updateAction() {
      Person p = getFieldData();
      if (isEmptyFieldData()) {
         QMessageBox.critical(this, "Invalid data",
         "Cannot update an empty record");
         return;
      }
      if (bean.update(p) != null)
         QMessageBox.information(this,
         "Update Successful","Person with ID:"
         + String.valueOf(p.getPersonId()
         + " is updated successfully"));
   }

   private void deleteAction() {
      Person p = getFieldData();
      if (isEmptyFieldData()) {
         QMessageBox.critical(this, "Invalid data",
         "Cannot delete an empty record");
         return;
      }
      p = bean.getCurrent();
      bean.delete();
      QMessageBox.information(this, "Delete Successful",
      "Person with ID:"+ String.valueOf(p.getPersonId()
      + " is deleted successfully"));
   }

   private void moveFirstAction() {
      setFieldData(bean.moveFirst());
   }

   private void movePreviousAction() {
      setFieldData(bean.movePrevious());
   }

   private void moveNextAction() {
      setFieldData(bean.moveNext());
   }

   private void moveLastAction() {
      setFieldData(bean.moveLast());
   }
}

Listing 8: QtJambi framework as a JDBC front-end

public class AppMain {
   public static void main(String[] args) {
      QApplication.initialize(args);
      QWidget w=new PersonUI();
      w.show();
      QApplication.exec();
   }
}
