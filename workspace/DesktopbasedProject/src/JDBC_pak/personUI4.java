public class PersonUI extends Composite {
   private Shell shell=this.getShell();

   private Text idField;
   //... mNameField, lNameField, emailField, phoneField

   private Button createButton;
   //... updateButton, deleteButton, firstButton,
   //... prevButton, nextButton, lastButton

   private PersonBean bean = new PersonBean();

   public PersonUI(Composite parent, int style) {
      super(parent, style);
      setLayout(new FillLayout(SWT.VERTICAL));
      initFields(this, SWT.NONE);
      initButtons(this, SWT.NONE);
      setFieldData(bean.moveFirst());

   }

   private Button createButton(Composite c, String caption, int style) {
      Button b = new Button(c, style);
      b.setText(caption);
      return b;
   }

   private void initButtons(Composite parent, int style) {
      Composite c = new Composite(parent, style);
      c.setLayout(new RowLayout(SWT.HORIZONTAL));
      createButton = createButton(c, "New...", SWT.PUSH);
      createButton.addSelectionListener(new ButtonHandler());
      //...
   }

   private void initFields(Composite parent, int style) {
      Composite c = new Composite(parent, style);
      GridLayout grid = new GridLayout(2, false);
      c.setLayout(grid);
      new Label(c, SWT.NONE).setText("ID");
      idField = new Text(c, SWT.BORDER);
      idField.setEditable(false);
      new Label(c, SWT.NONE).setText("First Name");
      fNameField = new Text(c, SWT.BORDER);
      //...
   }

   private Person getFieldData() {
      Person p = new Person();
      //...
      return p;
   }

   private void setFieldData(Person p) {
      //...
   }

   private boolean isEmptyFieldData() {
      //...
   }

   private class ButtonHandler extends SelectionAdapter {

      public void widgetSelected(SelectionEvent e) {
         Person p = getFieldData();
         if (e.getSource().equals(createButton)
         && createButton.getText().equals("Save")) {
            if (isEmptyFieldData()) {
               new MessageBox(shell, SWT.OK)
               .setText("Cannot create an empty record");
               return;
            }
         if (bean.create(p) != null)
            new MessageBox(shell, SWT.OK)
            .setText("New person created successfully.");
            createButton.setText("New...");
         } else if (e.getSource().equals(createButton)
            && createButton.getText().equals("New...")) {
            p.setPersonId(new Random()
            .nextInt(Integer.MAX_VALUE) + 1);
            //...
            setFieldData(p);
            createButton.setText("Save");
         } else if (e.getSource().equals(updateButton)) {
               if (isEmptyFieldData()) {
               new MessageBox(shell, SWT.OK)
               .setText("Cannot update an empty record");
               return;
            }
            if (bean.update(p) != null)
               new MessageBox(shell, SWT.OK)
               .setText("Person with ID:"
               + String.valueOf(p.getPersonId()
               + " is updated successfully"));

         } else if (e.getSource().equals(deleteButton)) {
            if (isEmptyFieldData()) {
               new MessageBox(shell, SWT.OK)
               .setText("Cannot delete an empty record");
               return;
            }
            p = bean.getCurrent();
            bean.delete();
            new MessageBox(shell, SWT.OK)
            .setText("Person with ID:"
            + String.valueOf(p.getPersonId()
            + " is deleted successfully"));

         } else if (e.getSource().equals(firstButton)) {
            setFieldData(bean.moveFirst());
         } else if (e.getSource().equals(prevButton)) {
            setFieldData(bean.movePrevious());
         } else if (e.getSource().equals(nextButton)) {
            setFieldData(bean.moveNext());
         } else if (e.getSource().equals(lastButton)) {
            setFieldData(bean.moveLast());
         }
      }
   }
}