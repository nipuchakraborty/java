package JDBC_pak;

public class AppMain {
   public static void main(String[] args) {
      Display display = new Display();
      Shell shell = new Shell(display);
      shell.setLayout(new RowLayout(SWT.VERTICAL));
      shell.setSize(600, 280);
      new PersonUI(shell, SWT.BORDER);
      shell.pack();
      shell.open();
      while (!shell.isDisposed()) {
         if (!display.readAndDispatch())
            display.sleep();
      }
      display.dispose();
   }
}