import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class Combo extends JFrame {

	private JPanel contentPane;
	private JTextField set_txt;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Combo frame = new Combo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Combo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JComboBox Combo = new JComboBox();
		Combo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				
				int selected=Combo.getSelectedIndex();
				switch(selected){
					case 0: break;
					case 1:set_txt.setText("Vala foa");break;
					case 2:set_txt.setText("Mota_muti vala");break;
					case 3:set_txt.setText("ekkana ekkana vala");break;
					case 4:set_txt.setText("temon vala no ");break;
					case 5:set_txt.setText("beyaddop");break;
				}
			}
		});
		Combo.setModel(new DefaultComboBoxModel(new String[] {"select", "arif", "nipu", "alamin", "frdin", "smrity"}));
		Combo.setBounds(139, 68, 91, 20);
		contentPane.add(Combo);
		
		set_txt = new JTextField();
		set_txt.setBounds(144, 127, 141, 20);
		contentPane.add(set_txt);
		set_txt.setColumns(10);
	}
}
