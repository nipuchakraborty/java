package viwe;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import com.toedter.calendar.JDateChooser;


public class Design extends JFrame {

	private JPanel contentPane;
	private JTextField name_txt;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Design frame = new Design();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Design() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 487, 552);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmFile = new JMenuItem("File");
		mnFile.add(mntmFile);
		
		JMenuItem mntmOpen = new JMenuItem("Open");
		mnFile.add(mntmOpen);
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mnFile.add(mntmSave);
		
		JMenuItem mntmSaveAs = new JMenuItem("Save as");
		mnFile.add(mntmSaveAs);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mnFile.add(mntmExit);
		
		JMenu mnAbout = new JMenu("About");
		menuBar.add(mnAbout);
		
		JMenuItem mntmVersion = new JMenuItem("Version");
		mnAbout.add(mntmVersion);
		
		JMenuItem mntmUpdate = new JMenuItem("Update");
		mnAbout.add(mntmUpdate);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHobbyInformation = new JLabel("Hobby Information");
		lblHobbyInformation.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblHobbyInformation.setBounds(140, 32, 164, 22);
		contentPane.add(lblHobbyInformation);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblName.setBounds(12, 134, 76, 22);
		contentPane.add(lblName);
		
		JLabel lblDate = new JLabel("Date");
		lblDate.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDate.setBounds(12, 205, 76, 22);
		contentPane.add(lblDate);
		
		JLabel lblHobby = new JLabel("Hobby");
		lblHobby.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblHobby.setBounds(12, 277, 76, 22);
		contentPane.add(lblHobby);
		
		JCheckBox criket = new JCheckBox("Criket");
		criket.setBounds(37, 308, 113, 25);
		contentPane.add(criket);
		
		JCheckBox basketbal = new JCheckBox("Basketbal");
		basketbal.setBounds(238, 308, 113, 25);
		contentPane.add(basketbal);
		
		JCheckBox gamming = new JCheckBox("Gamming ");
		gamming.setBounds(37, 350, 113, 25);
		contentPane.add(gamming);
		
		JCheckBox footbal = new JCheckBox("Footbal");
		footbal.setBounds(238, 350, 113, 25);
		contentPane.add(footbal);
		
		name_txt = new JTextField();
		name_txt.setBounds(100, 136, 204, 22);
		contentPane.add(name_txt);
		name_txt.setColumns(10);
		
		JCheckBox badmilton = new JCheckBox("Badmilton");
		badmilton.setBounds(37, 396, 113, 25);
		contentPane.add(badmilton);
		
		JCheckBox photography = new JCheckBox("Photography");
		photography.setBounds(238, 396, 113, 25);
		contentPane.add(photography);
		JDateChooser date_txt = new JDateChooser();
		date_txt.setBounds(83, 205, 255, 28);
		contentPane.add(date_txt);
		
		JButton save = new JButton("Save");
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
ArrayList <String>hobby_ara=new ArrayList<String>();
				
				if(criket.isSelected()){
					hobby_ara.add(criket.getText());
				}
				if(basketbal.isSelected()){
					hobby_ara.add(basketbal.getText());
				}
				if(gamming.isSelected()){
					hobby_ara.add(gamming.getText());
				}
				if(badmilton.isSelected()){
					hobby_ara.add(badmilton.getText());
					System.out.println(hobby_ara);
				}
				if(photography.isSelected()){
					hobby_ara.add(photography.getText());
				}
				
				/*
				inputter n=new inputter();
				inputter.input(name, date);
				
				inputter.hoby(hobby_ara);
				
				
			}*/
			}
		});
		save.setBounds(274, 441, 90, 28);
		contentPane.add(save);
		
		
	}
}
