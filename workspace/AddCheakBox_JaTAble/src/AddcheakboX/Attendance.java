package AddcheakboX;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

/**
 *
 * @author Admin
 */
public class Attendance{

    Connection con = Connectdatabase.getconnection();
    public Vector getEmployee()throws Exception
    {

        Vector<Vector<String>> employeeVector = new Vector<Vector<String>>();

        PreparedStatement pre = con.prepareStatement("SELECT `Id`, `Name` FROM `student`");
        ResultSet rs = pre.executeQuery();
        while(rs.next())
        {

            Vector<String> employee = new Vector<String>();
            employee.add(rs.getString(1)); //Empid
            employee.add(rs.getString(2));//name
            employeeVector.add(employee);

        }        
        if(con!=null)
        con.close();
        rs.close();
        pre.close();

        return employeeVector;
    }
}