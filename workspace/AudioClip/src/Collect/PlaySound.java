private void playSound(File f) {
    Runnable r = new Runnable() {
        private File f;

        public void run() {
            playSoundInternal(this.f);
        }

        public Runnable setFile(File f) {
            this.f = f;
            return this;
        }
    }.setFile(f);

    new Thread(r).start();
}

private void playSoundInternal(File f) {
    try {
        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(f);
        try {
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            try {
                clip.start();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                clip.drain();
            } finally {
                clip.close();
            }
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        } finally {
            audioInputStream.close();
        }
    } catch (UnsupportedAudioFileException e) {
        e.printStackTrace();
    } catch (FileNotFoundException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
}