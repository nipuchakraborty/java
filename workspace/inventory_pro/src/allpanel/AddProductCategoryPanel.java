package allpanel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;

import controller.AddProductCategory;
import model.ProductCategory;

import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AddProductCategoryPanel extends JPanel {
	private JTextField textField;
	public static JTable table;

	/**
	 * Create the panel.
	 */
	public AddProductCategoryPanel() {
		setLayout(new MigLayout("", "[grow][][][][][][grow]", "[][][][][grow]"));
		
		JLabel lblCategoryName = new JLabel("Category Name");
		add(lblCategoryName, "cell 0 0");
		
		textField = new JTextField();
		add(textField, "cell 6 0,growx");
		textField.setColumns(10);
		
		JLabel lblStatus = new JLabel("Status");
		add(lblStatus, "cell 0 2");
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Select", "Available", "Not-Available"}));
		add(comboBox, "cell 6 2,growx");
		
		JButton btnSave = new JButton("save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name=textField.getText();
				String status=comboBox.getSelectedItem().toString();
				AddProductCategory addProductCategory=new AddProductCategory(name, status);
				ProductCategory.load();
			}
		});
		add(btnSave, "cell 6 3");
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, "cell 0 4 7 1,grow");
		
		table = new JTable();
		ProductCategory.load();
		scrollPane.setViewportView(table);

	}

}
