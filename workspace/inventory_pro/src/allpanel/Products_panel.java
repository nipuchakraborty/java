package allpanel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;

import java.util.ArrayList;

import javax.net.ssl.SSLEngineResult.Status;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import controller.AddProducts_Controller;
import controller.GetProduct;
import model.ProductCategory;
import model.Products;
import model.Unit;
import javax.swing.JScrollBar;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Products_panel extends JPanel {
	private JTextField productName_textField;
	private JTable table;

	/**
	 * Create the panel.
	 */
	public Products_panel() {
		setBackground(SystemColor.inactiveCaption);
		setLayout(new MigLayout("", "[grow][][][][][][][grow]", "[][][][][][][][][][grow]"));
		
		JLabel lblNewLabel = new JLabel("Product name");
		add(lblNewLabel, "cell 0 0");
		
		productName_textField = new JTextField();
		add(productName_textField, "cell 7 0,growx");
		productName_textField.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Unit");
		add(lblNewLabel_1, "cell 0 2");
		
		Unit unit=new Unit();
		ArrayList<String>allunit=new ArrayList<String>();
		allunit=unit.getUnit();
		
		JComboBox Unit_combox = new JComboBox();
		Unit_combox.setModel(new DefaultComboBoxModel<>(allunit.toArray()));
		
		add(Unit_combox, "cell 7 2,growx");
		
		
		JLabel lblNewLabel_2 = new JLabel("Status");
		add(lblNewLabel_2, "cell 0 4");
		
		
		JComboBox statusCombox = new JComboBox();
		statusCombox.setModel(new DefaultComboBoxModel(new String[] {"SelectOne", "Avilable", "Not avilable"}));
		
		
		add(statusCombox, "cell 7 4,growx");
		
		JLabel lblNewLabel_3 = new JLabel("Select Product Category");
		add(lblNewLabel_3, "cell 0 6");
		
		ArrayList<String>setcat=new ArrayList<>();
		ProductCategory ProductCategory=new ProductCategory();
		setcat=ProductCategory.getallCat();
		JComboBox selectCat_combox = new JComboBox();
		selectCat_combox.setModel(new DefaultComboBoxModel<>(setcat.toArray()));
		add(selectCat_combox, "cell 7 6,growx");
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String Name=productName_textField.getText();
				String status=statusCombox.getSelectedItem().toString();
				String Unit=Unit_combox.getSelectedItem().toString();
				String category=selectCat_combox.getSelectedItem().toString();
				new AddProducts_Controller(Name, category, Unit, status);
	  		  
			}
		});
		add(btnSave, "cell 7 7");
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, "cell 0 9 8 1,grow");
		
		table = new JTable();
		scrollPane.setViewportView(table);

	}

}
