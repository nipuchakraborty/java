package allpanel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;

import controller.AddUnitcontroller;
import model.Unit;

import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class AddUnitPanel extends JPanel {
	private JTextField textField;
	public static JTable table;

	/**
	 * Create the panel.
	 */
	public AddUnitPanel() {
		setBackground(Color.GREEN);
		setLayout(new MigLayout("", "[grow][][][][][][grow]", "[][][][][grow]"));
		
		JLabel lblUnitName = new JLabel("Unit Name");
		add(lblUnitName, "cell 0 0");
		
		textField = new JTextField();
		add(textField, "cell 6 0,growx");
		textField.setColumns(10);
		
		JLabel lblStatus = new JLabel("Status");
		add(lblStatus, "cell 0 2");
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"select", "Available", "UnAvailable"}));
		add(comboBox, "cell 6 2,growx");
		
		JButton btnSave = new JButton("Save");
		btnSave.setBackground(Color.BLUE);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String unitname=textField.getText();
				String status=comboBox.getSelectedItem().toString();
				new AddUnitcontroller(unitname,status);
				
				Unit.load();
			}
		});
		add(btnSave, "cell 6 3");
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, "cell 0 4 7 1,grow");
		
		table = new JTable();
		Unit.load();
		scrollPane.setViewportView(table);

	}

}
