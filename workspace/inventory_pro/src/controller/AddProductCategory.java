package controller;

import model.ProductCategory;

public class AddProductCategory {

	public  AddProductCategory(String cat_name,String status){
		int st;
		
		if(status.equals("Available")){
			 st=0;
		}
		else {
			st=1;
		}
		ProductCategory productCategory=new ProductCategory();
		productCategory.prepareToinsert(cat_name,st);
		productCategory.store();
		
	}
	
}
