package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import allpanel.AddProductCategoryPanel;
import allpanel.AddUnitPanel;
import allpanel.Create_bill;
import allpanel.Products_panel;
import controller.AddProductCategory;

import javax.swing.JDesktopPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class HomeScreenFrame extends JFrame {
	private JDesktopPane desktopPane ;

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
					//UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
					/*UIManager.setLookAndFeel(
				            UIManager.getCrossPlatformLookAndFeelClassName());*/
					HomeScreenFrame frame = new HomeScreenFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public HomeScreenFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 992, 649);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnHome = new JMenu("Home");
		menuBar.add(mnHome);
		
		JMenuItem mntmAddProductCategory = new JMenuItem("Add Product Category");
		mntmAddProductCategory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame internalFrame=new JInternalFrame("ProductCategory",true,true,true,true);
				AddProductCategoryPanel addProductCategoryPanel=new AddProductCategoryPanel();
				internalFrame.getContentPane().add(addProductCategoryPanel);
				internalFrame.pack();
				
				desktopPane.add(internalFrame);
				internalFrame.setVisible(true);
				
				
			}
		});
		mnHome.add(mntmAddProductCategory);
		
		JMenuItem mntmAddProducts = new JMenuItem("Add Products");
		mntmAddProducts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame interFrame=new JInternalFrame("Add Products", true, true, true,true);
				Products_panel products_panel=new Products_panel();
				interFrame.getContentPane().add(products_panel);
				interFrame.pack();
				desktopPane.add(interFrame);
				interFrame.setVisible(true);
			}
		});
		mnHome.add(mntmAddProducts);
		
		JMenuItem mntmAddUnit = new JMenuItem("Add Unit");
		mntmAddUnit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame iFrame=new JInternalFrame("Add Unit", true, true, true);
				AddUnitPanel unitpanel=new AddUnitPanel();
				iFrame.getContentPane().add(unitpanel);
				iFrame.pack();
				desktopPane.add(iFrame);
				iFrame.setVisible(true);
			}
		});
		mnHome.add(mntmAddUnit);
		
		JMenu mnPurchase = new JMenu("purchase");
		menuBar.add(mnPurchase);
		
		JMenuItem mntmPuchaes = new JMenuItem("Puchaes");
		mntmPuchaes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame iFrame=new JInternalFrame("creaeParchese", true, true, true);
				
				Create_bill create_bill=new Create_bill();
				iFrame.getContentPane().add(create_bill);
				iFrame.pack();
				desktopPane.add(iFrame);
				iFrame.setVisible(true);
			}
		});
		mnPurchase.add(mntmPuchaes);
		
		JMenu mnSales = new JMenu("Sales");
		menuBar.add(mnSales);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		desktopPane = new JDesktopPane();
		desktopPane.setBackground(Color.CYAN);
		contentPane.add(desktopPane, BorderLayout.CENTER);
	}
}
