package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import allpanel.AddProductCategoryPanel;
import net.proteanit.sql.DbUtils;

public class ProductCategory {
public String catName;
public int status;

	
	public void prepareToinsert(String cat_name,int status){
		this.catName=cat_name;
		this.status=status;
		
		
	}
	public void store(){
		Connection conn=DbConnect.getconnection();
		String query="INSERT INTO `product_category`(`name`, `status`) VALUES (?,?)";
		try {
			PreparedStatement pst=conn.prepareStatement(query);
			pst.setString(1, this.catName);
			pst.setInt(2, this.status);
			pst.execute();
			
		} catch (Exception e) {
			
		}
		
	}
	public static void load(){
		Connection conn=DbConnect.getconnection();
		String query="SELECT name,(CASE status WHEN 0 THEN 'Available' ELSE 'Not_available' END)as status FROM `product_category`";
		try {
			PreparedStatement pst=conn.prepareStatement(query);
			ResultSet rSet=pst.executeQuery();
			AddProductCategoryPanel.table.setModel(DbUtils.resultSetToTableModel(rSet));
			
			
		} catch (Exception e) {
			
		}
	}
	public ArrayList<String> getallCat(){
		ArrayList<String>allcat=new ArrayList<>();
		Connection conn=DbConnect.getconnection();
		String quary="SELECT * FROM `product_category`";
		try {
			PreparedStatement pst=conn.prepareStatement(quary);
			ResultSet rSet=pst.executeQuery();
			while(rSet.next()){
				allcat.add(rSet.getString("Name"));
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return allcat;
		}
	
	
	public ArrayList<String> getCatId(String CatId){
		ArrayList<String>catId=new ArrayList<>();
		Connection conn=DbConnect.getconnection();
		String quary="SELECT Id FROM `product_category` WHERE Nmae=?";
		try {
			PreparedStatement preparedStatement=conn.prepareStatement(quary);
			ResultSet resultSet=preparedStatement.executeQuery();
			while(resultSet.next()){
				catId.add(resultSet.getString("Name"));
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return catId;}
	
	
	
}
