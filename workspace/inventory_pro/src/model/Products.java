package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import allpanel.AddProductCategoryPanel;
import net.proteanit.sql.DbUtils;

public class Products {
	public String Name;
	public int status;
	public int Unit;
	public int Category;
	public void prepareToinsert(String Name,int status,int unit,int category){
		this.Name=Name;
		this.status=status;
		this.Unit=unit;
		this.Category=category;
		
		
	}
	public void store(){
		Connection connection=DbConnect.getconnection();
		String query="INSERT INTO `products`(`Name`, `Status`, `unit_Id`, `category_id`) VALUES (?,?,?,?)";
		try {
			PreparedStatement preparedStatement=connection.prepareStatement(query);
			preparedStatement.setString(1, this.Name);
			preparedStatement.setInt(2, this.status);
			preparedStatement.execute();
			
		} catch (Exception e) {
			
		}
		
	}
	public static void load(){
		Connection conn=DbConnect.getconnection();
		String query="SELECT name,(CASE status WHEN 0 THEN 'Available' ELSE 'Not_available' END)as status FROM `product_category`";
		try {
			PreparedStatement pst=conn.prepareStatement(query);
			ResultSet rSet=pst.executeQuery();
			AddProductCategoryPanel.table.setModel(DbUtils.resultSetToTableModel(rSet));
			
			
		} catch (Exception e) {
			
		}
	}

}
