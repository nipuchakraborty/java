package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import allpanel.AddProductCategoryPanel;
import allpanel.AddUnitPanel;
import net.proteanit.sql.DbUtils;

public class Unit {
	
	private String name;
	private  int status;
	
	
	public void prepare(String name,int status){
		
		
		this.name=name;
		this.status=status;
		
	}
	
	
	
	
	
	public void store(){
		Connection conn=DbConnect.getconnection();
		String query="INSERT INTO `Unit`(`Unit_Name`, `Status`) VALUES (?,?)";
		try {
			PreparedStatement pst=conn.prepareStatement(query);
			pst.setString(1, this.name);
			pst.setInt(2, this.status);
			pst.execute();
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "insert failed");
			
		}
		
		
	}
	
	public static void load(){
		Connection conn=DbConnect.getconnection();
		String query="SELECT Unit_Name,(CASE Status WHEN 0 THEN 'Available' ELSE 'Not_available' END)as status FROM `unit`";
		try {
			PreparedStatement pst=conn.prepareStatement(query);
			ResultSet rSet=pst.executeQuery();
			AddUnitPanel.table.setModel(DbUtils.resultSetToTableModel(rSet));
			
			
		} catch (Exception e) {
			
		}
		
		
	}
	public ArrayList<String> getUnit(){
		ArrayList<String>allunit=new ArrayList<String>();
		
		Connection conn=DbConnect.getconnection();
		String query="select * from Unit";
		try {
			PreparedStatement preparedStatement=conn.prepareStatement(query);
			ResultSet resultSet=preparedStatement.executeQuery();
			while(resultSet.next()){
				allunit.add(resultSet.getString("Unit_Name"));
			}
			
			
		} catch (Exception e) {
			
		}
		System.out.println(allunit);
		return allunit;
		
	}
	public int getUnit_id(String unitname){
		int cat_id=0;
		Connection conn=DbConnect.getconnection();
		String quary="Select id From `unit` WHERE Name";
		try {
			PreparedStatement pst=conn.prepareStatement(quary);
			pst.setString(1, unitname);
			ResultSet resultSet=pst.executeQuery();
			while(resultSet.next()){
			cat_id=resultSet.getInt("Id");
			}
		} catch (Exception e) {
			
		}
		return cat_id;
	}

}
