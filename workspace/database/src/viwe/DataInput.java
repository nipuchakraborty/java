package viwe;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.PasswordView;

import model.Profile_manage;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.JDatePanelImpl;
import javax.swing.JFormattedTextField.AbstractFormatter;

public class DataInput extends JFrame {

	private JPanel contentPane;
	private JTextField name_txt;
	private JTextField password_txt;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DataInput frame = new DataInput();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DataInput() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 606, 525);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(31, 69, 56, 16);
		contentPane.add(lblName);
		
		name_txt = new JTextField();
		name_txt.setBounds(144, 66, 116, 22);
		contentPane.add(name_txt);
		name_txt.setColumns(10);
		
		JButton btnInsertData = new JButton("Insert Data");
		btnInsertData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name=name_txt.getText();
				String password=password_txt.getText();
				Profile_manage profile_manage=new Profile_manage();
				profile_manage.store(name, password);
			
		
				
			}
		});
		btnInsertData.setBounds(373, 419, 97, 25);
		contentPane.add(btnInsertData);
		
		password_txt = new JTextField();
		password_txt.setBounds(144, 116, 116, 22);
		contentPane.add(password_txt);
		password_txt.setColumns(10);
		
		JLabel lblPassword = new JLabel("password");
		lblPassword.setBounds(31, 119, 56, 16);
		contentPane.add(lblPassword);
		
		textField = new JTextField();
		textField.setBounds(144, 170, 116, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel label = new JLabel("New label");
		label.setBounds(31, 176, 56, 16);
		contentPane.add(label);
	}
}
