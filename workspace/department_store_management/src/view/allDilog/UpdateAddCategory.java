package view.allDilog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.UpdateAddCategoryController;
import controller.UpdateAddUnitController;
import model.Status;
import model.Unit;
import model.Utility;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class UpdateAddCategory extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField cat_txt;
	private JComboBox comboBox_status;

	/**
	 * Create the dialog.
	 * @param secendValue 
	 * @param fristValue 
	 * @param id 
	 */
	public UpdateAddCategory(String fristValue, String secendValue, int id) {
		
		setBounds(100, 100, 338, 294);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new MigLayout("", "[][][][grow]", "[][][]"));
		{
			JLabel lblUnitName = new JLabel("Unit Name");
			contentPanel.add(lblUnitName, "cell 0 0");
		}
		{
			cat_txt = new JTextField();
			contentPanel.add(cat_txt, "cell 3 0,growx");
			cat_txt.setText(fristValue);
			cat_txt.setColumns(10);
		}
		{
			JLabel lblStatus = new JLabel("Status");
			contentPanel.add(lblStatus, "cell 0 2");
		}
		{
			comboBox_status = new JComboBox();
			comboBox_status.setSelectedItem(secendValue);
			ArrayList<String> status=Status.getStatus();
			comboBox_status.setModel(new DefaultComboBoxModel<>(status.toArray()));
			contentPanel.add(comboBox_status, "cell 3 2,growx");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Update");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String cat=cat_txt.getText();
						String status=comboBox_status.getSelectedItem().toString();
						
						new UpdateAddCategoryController(cat,status,id);
						
					}
				});
				okButton.setActionCommand("Update");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Delete");
				cancelButton.setActionCommand("Delete");
				buttonPane.add(cancelButton);
			}
		}
	}

}
