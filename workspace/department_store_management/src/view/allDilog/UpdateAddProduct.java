package view.allDilog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.UpdateAddProductController;
import model.Product;
import model.ProductCategory;
import model.Status;
import model.Unit;
import model.Utility;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class UpdateAddProduct extends JDialog {

	private final JPanel dailog = new JPanel();
	public static JTextField pro_txt;
	public static JComboBox comboBox_category;
	public static JComboBox comboBox_unit;
	public static JComboBox comboBox_status;


	/**
	 * Create the dialog.
	 * @param forthValue 
	 * @param thirdValue 
	 * @param secendValue 
	 * @param fristValue 
	 * @param id 
	 */
	public UpdateAddProduct(String fristValue, String secendValue, String thirdValue, String forthValue, int id) {
		setBounds(100, 100, 407, 328);
		getContentPane().setLayout(new BorderLayout());
		dailog.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(dailog, BorderLayout.CENTER);
		dailog.setLayout(new MigLayout("", "[][][][][grow]", "[][][][][][][]"));
		{
			JLabel lblProdcutName = new JLabel("Prodcut Name");
			dailog.add(lblProdcutName, "cell 0 0");
		}
		{
			pro_txt = new JTextField();
			dailog.add(pro_txt, "cell 4 0,growx");
			pro_txt.setText(fristValue);
			pro_txt.setColumns(10);
		}
		{
			JLabel label = new JLabel("Category");
			dailog.add(label, "cell 0 2");
		}
		{
			comboBox_category = new JComboBox();
			comboBox_category.setSelectedItem(secendValue);
			ArrayList<String> category=ProductCategory.getCategoryName();
			comboBox_category.setModel(new DefaultComboBoxModel<>(category.toArray()));
			dailog.add(comboBox_category, "cell 4 2,growx");
		}
		{
			JLabel lblUnit = new JLabel("Unit");
			dailog.add(lblUnit, "cell 0 4");
		}
		{
			comboBox_unit = new JComboBox();
			comboBox_unit.setSelectedItem(thirdValue);
			ArrayList<String> unit=Unit.getUnitName();
			comboBox_unit.setModel(new DefaultComboBoxModel<>(unit.toArray()));
			dailog.add(comboBox_unit, "cell 4 4,growx");
		}
		{
			JLabel lblStatus = new JLabel("Status");
			dailog.add(lblStatus, "cell 0 6");
		}
		{
			comboBox_status = new JComboBox();
			comboBox_status.setSelectedItem(forthValue);
			ArrayList<String> status=Status.getStatus();
			comboBox_status.setModel(new DefaultComboBoxModel<>(status.toArray()));
			dailog.add(comboBox_status, "cell 4 6,growx");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Update");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String product=pro_txt.getText();
						String category=comboBox_category.getSelectedItem().toString();
						String unit=comboBox_unit.getSelectedItem().toString();
						String status=comboBox_status.getSelectedItem().toString();
						
						new UpdateAddProductController(product,category,unit,status,id);
						UpdateAddProduct.this.setVisible(false);
					}
				});
				okButton.setActionCommand("Update");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Delete");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String product=pro_txt.getText();
						String category=comboBox_category.getSelectedItem().toString();
						String unit=comboBox_unit.getSelectedItem().toString();
						String status=comboBox_status.getSelectedItem().toString();
						
					}
				});
				cancelButton.setActionCommand("Delete");
				buttonPane.add(cancelButton);
			}
		}
	}

}
