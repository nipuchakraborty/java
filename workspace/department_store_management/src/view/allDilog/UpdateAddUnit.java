package view.allDilog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.UpdateAddUnitController;
import model.Status;
import model.Unit;
import model.Utility;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class UpdateAddUnit extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField unit_txt;
	private JComboBox comboBox_status;

	/**
	 * Create the dialog.
	 * @param secendValue 
	 * @param fristValue 
	 * @param id 
	 */
	public UpdateAddUnit(String fristValue, String secendValue, int id) {
		
		setBounds(100, 100, 338, 294);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new MigLayout("", "[][][][grow]", "[][][]"));
		{
			JLabel lblUnitName = new JLabel("Unit Name");
			contentPanel.add(lblUnitName, "cell 0 0");
		}
		{
			unit_txt = new JTextField();
			contentPanel.add(unit_txt, "cell 3 0,growx");
			unit_txt.setText(fristValue);
			unit_txt.setColumns(10);
		}
		{
			JLabel lblStatus = new JLabel("Status");
			contentPanel.add(lblStatus, "cell 0 2");
		}
		{
			comboBox_status = new JComboBox();
			comboBox_status.setSelectedItem(secendValue);
			ArrayList<String>status=Status.getStatus();
			comboBox_status.setModel(new DefaultComboBoxModel<>(status.toArray()));
			contentPanel.add(comboBox_status, "cell 3 2,growx");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Update");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String unit=unit_txt.getText();
						String status=comboBox_status.getSelectedItem().toString();
						
						new UpdateAddUnitController(unit,status,id);
						
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Delete");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
