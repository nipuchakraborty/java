package view.allDilog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.crypto.Data;

import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import org.jdesktop.swingx.JXDatePicker;

import controller.UpdatePurchaseTempController;
import model.Product;
import model.ProductCategory;
import model.Utility;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UpdateCreatePurchaseTemp extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField quentity_txt;
	private JTextField vendor_txt;
	private JTextField buy_txt;
	private JTextField sale_txt;
	private JComboBox comboBox_product;
	private JComboBox comboBox_cat;
	private JXDatePicker datePicker ;

	/**
	 * Create the dialog.
	 * @param sixthValue 
	 * @param fivthValue 
	 * @param forthValue 
	 * @param thirdValue 
	 * @param secendValue 
	 * @param fristValue 
	 */
	public UpdateCreatePurchaseTemp(Object fristValue, String secendValue, String thirdValue, String forthValue, String fivthValue, String sixthValue) {
		setBounds(100, 100, 450, 463);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new MigLayout("", "[][][][][][grow]", "[][][][][][][][][][][][][]"));
		{
			JLabel lblDate = new JLabel("Date");
			contentPanel.add(lblDate, "cell 0 0");
		}
		{
			 datePicker = new JXDatePicker();
			contentPanel.add(datePicker, "cell 5 0");
			

		}
		{
			JLabel lblCategory = new JLabel("Category");
			contentPanel.add(lblCategory, "cell 0 2");
		}
		{
			JComboBox comboBox_cat = new JComboBox();
			contentPanel.add(comboBox_cat, "cell 5 2,growx");
			comboBox_cat.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent arg0) {
					String category=comboBox_cat.getSelectedItem().toString();
					int category_id=ProductCategory.getCategoryId(category);
					ArrayList<String> takeProdcutName_toCatId=new ArrayList<>();
					takeProdcutName_toCatId.addAll(Product.giveCategoryIdTakeProductName(category_id));
					//System.out.println(product);
					comboBox_cat.setModel(new DefaultComboBoxModel<>(takeProdcutName_toCatId.toArray()));
				}
			});
			ArrayList<String> category=ProductCategory.getCategoryName();
			comboBox_cat.setModel(new DefaultComboBoxModel<>(category.toArray()));
			
			comboBox_cat.setSelectedItem(fristValue);
			
		}
		{
			JLabel lblProduct = new JLabel("Product");
			contentPanel.add(lblProduct, "cell 0 4");
		}
		{
			comboBox_product = new JComboBox();
			contentPanel.add(comboBox_product, "cell 5 4,growx");
			comboBox_product.setSelectedItem(secendValue);
		}
		{
			JLabel lblQuentity = new JLabel("Quentity");
			contentPanel.add(lblQuentity, "cell 0 6");
		}
		{
			quentity_txt = new JTextField();
			contentPanel.add(quentity_txt, "cell 5 6,growx");
			quentity_txt.setColumns(10);
			quentity_txt.setText(thirdValue);
		}
		{
			JLabel lblVe = new JLabel("Vendor name");
			contentPanel.add(lblVe, "cell 0 8");
		}
		{
			vendor_txt = new JTextField();
			contentPanel.add(vendor_txt, "cell 5 8,growx");
			vendor_txt.setColumns(10);
			vendor_txt.setText(forthValue);
		}
		{
			JLabel lblBuyPrice = new JLabel("Buy price");
			contentPanel.add(lblBuyPrice, "cell 0 10");
		}
		{
			buy_txt = new JTextField();
			contentPanel.add(buy_txt, "cell 5 10,growx");
			buy_txt.setColumns(10);
			buy_txt.setText(fivthValue);
		}
		{
			JLabel lblSalePrice = new JLabel("Sale price");
			contentPanel.add(lblSalePrice, "cell 0 12");
		}
		{
			sale_txt = new JTextField();
			contentPanel.add(sale_txt, "cell 5 12,growx");
			sale_txt.setColumns(10);
			sale_txt.setText(sixthValue);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						DateFormat date=new SimpleDateFormat("yyyy/MM/dd");
						Date jx_date=datePicker.getDate();
						String main_date=date.format(jx_date);
				
						String vendorName=vendor_txt.getText();
						String productName=comboBox_product.getSelectedItem().toString();
						String category=comboBox_cat.getSelectedItem().toString();
						String quentity=quentity_txt.getText();
						String buyPrize=buy_txt.getText();
						String sellPrize=sale_txt.getText();
						new UpdatePurchaseTempController(vendorName,productName,category,quentity,buyPrize,sellPrize);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
