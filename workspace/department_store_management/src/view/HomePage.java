package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel;
import view.allpanel.AddProductCategoryPanel;
import view.allpanel.AddProductPanel;
import view.allpanel.AddUnitPanel;
import view.allpanel.CreatePurchasePanel;
import view.allpanel.SalesEntryPenel;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import java.awt.SystemColor;
import java.awt.Color;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class HomePage extends JFrame {

	private JPanel contentPane;
	private JDesktopPane desktopPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
//					UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
//					UIManager.setLookAndFeel(
//				            UIManager.getCrossPlatformLookAndFeelClassName());
					HomePage frame = new HomePage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public HomePage() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1133, 560);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnHome = new JMenu("Home");
		menuBar.add(mnHome);
		
		JMenu mnProducts = new JMenu("Products");
		menuBar.add(mnProducts);
		
		JMenuItem mntmAddUnit = new JMenuItem("Add Unit");
		mntmAddUnit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame unit=new JInternalFrame("Add Unit", true,true,true,true);
				AddUnitPanel addUnit=new AddUnitPanel();
				unit.getContentPane().add(addUnit);
				unit.pack();
				desktopPane.add(unit);
				unit.setVisible(true);
			}
		});
		mnProducts.add(mntmAddUnit);
		
		JMenuItem mntmAddProductCategory = new JMenuItem("Add Product Category");
		mntmAddProductCategory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JInternalFrame category=new JInternalFrame("Add Product Category", true,true,true,true);
				AddProductCategoryPanel addProductCategory=new AddProductCategoryPanel();
				category.getContentPane().add(addProductCategory);
				category.pack();
				desktopPane.add(category);
				category.setVisible(true);
			}
		});
		mnProducts.add(mntmAddProductCategory);
		
		JMenuItem mntmAddProduct = new JMenuItem("Add Product");
		mntmAddProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JInternalFrame product=new JInternalFrame("Add Product", true,true,true,true);
				AddProductPanel addProduct=new AddProductPanel();
				product.getContentPane().add(addProduct);
				product.pack();
				desktopPane.add(product);
				product.setVisible(true);
			}
		});
		mnProducts.add(mntmAddProduct);
		
		JMenu mnInventoy = new JMenu("Inventoy");
		menuBar.add(mnInventoy);
		
		JMenu mnDailyactivities = new JMenu("DailyActivities");
		menuBar.add(mnDailyactivities);
		
		JMenuItem mntmSalesEntry = new JMenuItem("Sales Entry");
		mntmSalesEntry.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JInternalFrame sale=new JInternalFrame("Sales Entry", true,true,true,true);
				SalesEntryPenel salesEntry=new SalesEntryPenel();
				sale.getContentPane().add(salesEntry);
				sale.pack();
				desktopPane.add(sale);
				sale.setVisible(true);
			}
		});
		mnDailyactivities.add(mntmSalesEntry);
		
		JMenuItem mntmPurchase = new JMenuItem("Create Purchase");
		mntmPurchase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JInternalFrame pruchase=new JInternalFrame("Create Purchase", true,true,true,true);
				CreatePurchasePanel createPurchasePanel=new CreatePurchasePanel();
				pruchase.getContentPane().add(createPurchasePanel);
				pruchase.pack();
				desktopPane.add(pruchase);
				pruchase.setVisible(true);
			}
		});
		mnDailyactivities.add(mntmPurchase);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.activeCaption);
		contentPane.setForeground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		desktopPane = new JDesktopPane();
		contentPane.add(desktopPane, BorderLayout.CENTER);
	}
}
