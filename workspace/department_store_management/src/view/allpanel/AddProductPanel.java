package view.allpanel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import view.allDilog.UpdateAddProduct;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.AddProduct;
import model.Product;
import model.ProductCategory;
import model.Status;
import model.Unit;
import model.Utility;

import javax.swing.JComboBox;

import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AddProductPanel extends JPanel {
	private JTextField productName_txt;
	public static JTable table;

	/**
	 * Create the panel.
	 */
	public AddProductPanel() {
		setLayout(new MigLayout("", "[grow][][][][][][grow]", "[][][][][][grow]"));
		
		JLabel lblProductName = new JLabel("Product name");
		add(lblProductName, "cell 0 0");
		
		productName_txt = new JTextField();
		add(productName_txt, "cell 6 0,growx");
		productName_txt.setColumns(10);
		
		JLabel lblCategory = new JLabel("Category");
		add(lblCategory, "cell 0 1");
		
		JComboBox category_comboBox = new JComboBox();
		add(category_comboBox, "cell 6 1,growx");
		ArrayList<String> category=ProductCategory.getCategoryName();
		category_comboBox.setModel(new DefaultComboBoxModel<>(category.toArray()));
		
		JLabel lblUnit = new JLabel("Unit");
		add(lblUnit, "cell 0 2");
		
		JComboBox unit_comboBox = new JComboBox();
		add(unit_comboBox, "cell 6 2,growx");
		ArrayList<String> unit=Unit.getUnitName();
		unit_comboBox.setModel(new DefaultComboBoxModel<>(unit.toArray()));
		
		JLabel lblStatus = new JLabel("Status");
		add(lblStatus, "cell 0 3");
		
		JComboBox status_comboBox = new JComboBox();
		ArrayList<String> status=Status.getStatus();
		status_comboBox.setModel(new DefaultComboBoxModel<>(status.toArray()));
		add(status_comboBox, "cell 6 3,growx");

		JButton btnNewButton = new JButton("save");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String productName=productName_txt.getText();
				String catName=category_comboBox.getSelectedItem().toString();
				String unitName=unit_comboBox.getSelectedItem().toString();
				String status=status_comboBox.getSelectedItem().toString();
				
				new AddProduct(productName,catName,unitName,status);
				Product.load();
			}
		});

		add(btnNewButton, "cell 6 4");
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, "cell 0 5 7 1,grow");
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row=table.getSelectedRow();
				String fristValue=table.getModel().getValueAt(row, 0).toString();
				String secendValue=table.getModel().getValueAt(row, 1).toString();
				String thirdValue=table.getModel().getValueAt(row, 2).toString();
				String forthValue=table.getModel().getValueAt(row, 3).toString();
				int id=Product.getProductId(fristValue);
				
				UpdateAddProduct edit=new UpdateAddProduct(fristValue,secendValue,thirdValue,forthValue,id);
				edit.setVisible(true);
			}
		});
		Product.load();
		scrollPane.setViewportView(table);

	}

}
