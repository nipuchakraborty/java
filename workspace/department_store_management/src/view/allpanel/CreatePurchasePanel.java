package view.allpanel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import view.allDilog.UpdateAddCategory;
import view.allDilog.UpdateCreatePurchaseTemp;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import org.jdesktop.swingx.JXDatePicker;

import controller.TempPurchaseController;
import model.Product;
import model.ProductCategory;
import model.TempPurchase;

import javax.swing.JComboBox;

import java.util.ArrayList;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CreatePurchasePanel extends JPanel {
	private JTextField total_ammount_txt;
	private JTextField paid_txt;
	private JTextField due_txt;
	public static JTable table;
	private JTextField textField_3;
	private JTextField vendor_txt;
	private JTextField buy_txt;
	private JTextField sale_txt;
	private JComboBox comboBox_category;
	private JComboBox comboBox_product; 
	private JTextField quentity_txt;

	/**
	 * Create the panel.
	 */
	public CreatePurchasePanel() {
		setLayout(new MigLayout("", "[][grow][][][][][][][][grow][][][grow][][][][][grow][][][grow][grow][][][][][][grow]", "[][][][][][][][][][][][][][][][grow]"));
		
		JLabel lblMrnNo = new JLabel("M.R.N no");
		add(lblMrnNo, "cell 0 0,alignx trailing");
		
		textField_3 = new JTextField();
		add(textField_3, "cell 1 0,growx");
		textField_3.setColumns(10);
		
		JLabel lblDate = new JLabel("Date");
		add(lblDate, "cell 6 0");
		
		JXDatePicker datePicker = new JXDatePicker();
		add(datePicker, "cell 9 0");
		
		JLabel lblVendorName = new JLabel("Vendor name");
		add(lblVendorName, "cell 15 0");
		
		vendor_txt = new JTextField();
		add(vendor_txt, "cell 17 0 3 1,growx");
		vendor_txt.setColumns(10);
		
		JLabel lblCategory = new JLabel("Category");
		add(lblCategory, "cell 0 1,alignx trailing");
		
		comboBox_category = new JComboBox();
		comboBox_category.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				String category=comboBox_category.getSelectedItem().toString();
				int category_id=ProductCategory.getCategoryId(category);
				ArrayList<String> takeProdcutName_toCatId=new ArrayList<>();
				takeProdcutName_toCatId.addAll(Product.giveCategoryIdTakeProductName(category_id));
				//System.out.println(product);
				comboBox_product.setModel(new DefaultComboBoxModel<>(takeProdcutName_toCatId.toArray()));
			}
		});
	
		ArrayList<String> category=ProductCategory.getCategoryName();
		comboBox_category.setModel(new DefaultComboBoxModel<>(category.toArray()));
		add(comboBox_category, "cell 1 1 5 1,growx");
		
		JLabel lblProduct = new JLabel("Product");
		add(lblProduct, "cell 6 1");
		
		comboBox_product = new JComboBox();
		add(comboBox_product, "cell 9 1,growx");
		
		JLabel lblBuyPrize = new JLabel("Buy prize");
		add(lblBuyPrize, "cell 15 1");
		
		buy_txt = new JTextField();
		add(buy_txt, "cell 17 1,growx");
		buy_txt.setColumns(10);
		
		JButton btnEnter = new JButton("Enter");
		btnEnter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				/*DateFormat date=new SimpleDateFormat("yyyy/MM/dd");
				Date jx_date=datePicker.getDate();
				String main_date=date.format(jx_date);*/
				Date clDate=datePicker.getDate();
				String vendorName=vendor_txt.getText();
				String productName=comboBox_product.getSelectedItem().toString();
				String category=comboBox_category.getSelectedItem().toString();
				String quentity=quentity_txt.getText();
				String buyPrize=buy_txt.getText();
				String sellPrize=sale_txt.getText();
				
				new TempPurchaseController(clDate,vendorName,category,productName,quentity,buyPrize,sellPrize);
				TempPurchase.load();
				
			}
		});
		add(btnEnter, "cell 23 1");
		
		JLabel lblQuentity = new JLabel("Quentity");
		add(lblQuentity, "cell 6 2");
		
		quentity_txt = new JTextField();
		add(quentity_txt, "cell 9 2,growx");
		quentity_txt.setColumns(10);
		
		JLabel lblSellPrize = new JLabel("Sell prize");
		add(lblSellPrize, "cell 15 2");
		
		sale_txt = new JTextField();
		add(sale_txt, "cell 17 2,growx");
		sale_txt.setColumns(10);
		
		JPanel panel = new JPanel();
		add(panel, "cell 18 4 10 12,grow");
		panel.setLayout(new MigLayout("", "[][][][][grow]", "[][][][]"));
		
		JLabel lblTotalAmmount = new JLabel("Total Ammount");
		panel.add(lblTotalAmmount, "cell 0 0");
		
		total_ammount_txt = new JTextField();
		panel.add(total_ammount_txt, "cell 4 0,growx");
		total_ammount_txt.setColumns(10);
		
		JLabel lblPaid = new JLabel("Paid");
		panel.add(lblPaid, "cell 0 1,alignx right");
		
		paid_txt = new JTextField();
		panel.add(paid_txt, "cell 4 1,growx");
		paid_txt.setColumns(10);
		
		JLabel lblD = new JLabel("Due");
		panel.add(lblD, "cell 0 2,alignx right");
		
		due_txt = new JTextField();
		panel.add(due_txt, "cell 4 2,growx");
		due_txt.setColumns(10);
		
		JCheckBox chckbxBlanced = new JCheckBox("Blanced");
		panel.add(chckbxBlanced, "cell 0 3");
		
		JPanel panel_1 = new JPanel();
		add(panel_1, "cell 0 4 18 12,grow");
		panel_1.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane, "cell 0 0,grow");
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				int row=table.getSelectedRow();
				
				String fristValue=table.getModel().getValueAt(row, 0).toString();
				String secendValue=table.getModel().getValueAt(row, 1).toString();
				String thirdValue=table.getModel().getValueAt(row, 2).toString();
				String forthValue=table.getModel().getValueAt(row, 3).toString();
				String fivthValue=table.getModel().getValueAt(row, 4).toString();
				String sixthValue=table.getModel().getValueAt(row, 5).toString();
//				String seventhValue=table.getModel().getValueAt(row, 6).toString();
				
				UpdateCreatePurchaseTemp createPurchaseTemp=new UpdateCreatePurchaseTemp(fristValue,secendValue,thirdValue,forthValue,fivthValue,sixthValue);
				createPurchaseTemp.setVisible(true);
			}
		});
		TempPurchase.load();
		scrollPane.setViewportView(table);

	}

}
