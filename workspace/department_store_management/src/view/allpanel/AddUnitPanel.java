package view.allpanel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import view.allDilog.UpdateAddUnit;

import javax.swing.JLabel;
import javax.swing.JTextField;

import controller.AddUnit;
import model.Status;
import model.Unit;
import model.Utility;

import javax.swing.JComboBox;

import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AddUnitPanel extends JPanel {
	private JTextField unitName_txt;
	public static JTable table;

	/**
	 * Create the panel.
	 */
	public AddUnitPanel() {
		setLayout(new MigLayout("", "[grow][][][][][grow]", "[][][][grow]"));
		
		JLabel lblStatus = new JLabel("Unit name");
		add(lblStatus, "cell 0 0");
		
		unitName_txt = new JTextField();
		add(unitName_txt, "cell 5 0,growx");
		unitName_txt.setColumns(10);
		
		JLabel lblStatus_1 = new JLabel("Status");
		add(lblStatus_1, "cell 0 1");
		
		JComboBox comboBox_status = new JComboBox();
		ArrayList<String>status=Status.getStatus();
		comboBox_status.setModel(new DefaultComboBoxModel<>(status.toArray()));
		add(comboBox_status, "cell 5 1,growx");
		
		JButton btnSave = new JButton("save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String unitName=unitName_txt.getText();
				String status=comboBox_status.getSelectedItem().toString();
				new AddUnit(unitName,status);
				Unit.load();
			}
		});
		add(btnSave, "cell 5 2");
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, "cell 0 3 6 1,grow");
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row=table.getSelectedRow();
				String fristValue=table.getModel().getValueAt(row, 0).toString();
				String secendValue=table.getModel().getValueAt(row, 1).toString();
				
				int id=Unit.getUnitId(fristValue);
				
				UpdateAddUnit unit=new UpdateAddUnit(fristValue,secendValue,id);
				unit.setVisible(true);
			}
		});
		Unit.load();
		scrollPane.setViewportView(table);

	}

}
