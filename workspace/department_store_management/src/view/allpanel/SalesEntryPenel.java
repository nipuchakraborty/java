package view.allpanel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import org.jdesktop.swingx.JXDatePicker;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class SalesEntryPenel extends JPanel {
	private JTable table;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField castomer_txt;
	private JTextField quentity_txt;
	private JComboBox comboBox_category;
	private JComboBox comboBox_product;


	/**
	 * Create the panel.
	 */
	public SalesEntryPenel() {
		setLayout(new MigLayout("", "[][][grow][][][][][][grow][][grow][grow][][][][][][][][][][][][][][][][grow]", "[][][][][][][][][][][][][][][][grow]"));
		
		JLabel lblCategory = new JLabel("Category");
		add(lblCategory, "cell 0 0");
		
		comboBox_category = new JComboBox();
		add(comboBox_category, "cell 2 0,growx");
		
		JLabel lblDate = new JLabel("Date");
		add(lblDate, "cell 6 0");
		
		JXDatePicker datePicker = new JXDatePicker();
		add(datePicker, "cell 8 0");
		
		JLabel lblCustomerName = new JLabel("Customer Name");
		add(lblCustomerName, "cell 13 0");
		
		castomer_txt = new JTextField();
		add(castomer_txt, "cell 20 0,growx");
		castomer_txt.setColumns(10);
		
		JLabel lblProduct = new JLabel("Product");
		add(lblProduct, "cell 0 2");
		
		comboBox_product = new JComboBox();
		add(comboBox_product, "cell 2 2 3 1,growx");
		
		JLabel lblNewLabel = new JLabel("Quantity");
		add(lblNewLabel, "cell 6 2");
		
		quentity_txt = new JTextField();
		add(quentity_txt, "cell 8 2,growx");
		quentity_txt.setColumns(10);
		
		JButton btnSave = new JButton("Enter");
		add(btnSave, "cell 13 2");
		
		JButton btnNew = new JButton("New");
		add(btnNew, "cell 20 2");
		
		JPanel panel = new JPanel();
		add(panel, "cell 13 6 15 10,grow");
		panel.setLayout(new MigLayout("", "[][][grow]", "[][][][][]"));
		
		JLabel lblTotalAmmount = new JLabel("Total Ammount");
		panel.add(lblTotalAmmount, "cell 0 1");
		
		textField = new JTextField();
		panel.add(textField, "cell 2 1,growx");
		textField.setColumns(10);
		
		JLabel lblDeo = new JLabel("Paid");
		panel.add(lblDeo, "cell 0 2");
		
		textField_1 = new JTextField();
		panel.add(textField_1, "cell 2 2,growx");
		textField_1.setColumns(10);
		
		JLabel lblDeu = new JLabel("Due");
		panel.add(lblDeu, "cell 0 3");
		
		textField_2 = new JTextField();
		panel.add(textField_2, "cell 2 3,growx");
		textField_2.setColumns(10);
		
		JCheckBox chckbxBlance = new JCheckBox("Blanced");
		panel.add(chckbxBlance, "cell 0 4");
		
		JPanel panel_1 = new JPanel();
		add(panel_1, "cell 0 6 11 10,grow");
		panel_1.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane, "cell 0 0,grow");
		
		table = new JTable();
		scrollPane.setViewportView(table);

	}

}
