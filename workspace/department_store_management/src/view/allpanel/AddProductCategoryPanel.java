package view.allpanel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import view.allDilog.UpdateAddCategory;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.AddProductCategory;
import model.ProductCategory;
import model.Status;
import model.Utility;

import javax.swing.JComboBox;

import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AddProductCategoryPanel extends JPanel {
	private JTextField catName_txt;
	public static JTable table;
	private JComboBox status_comboBox;

	/**
	 * Create the panel.
	 */
	public AddProductCategoryPanel() {
		setLayout(new MigLayout("", "[grow][][][][][][grow]", "[][][][grow]"));
		
		JLabel lblCategoryName = new JLabel("Category name");
		add(lblCategoryName, "cell 0 0");
		
		catName_txt = new JTextField();
		add(catName_txt, "cell 6 0,growx");
		catName_txt.setColumns(10);
		
		JLabel lblStatus = new JLabel("Status");
		add(lblStatus, "cell 0 1");
		
		status_comboBox = new JComboBox();
		ArrayList<String> status=Status.getStatus();
		status_comboBox.setModel(new DefaultComboBoxModel<>(status.toArray()));
		add(status_comboBox, "cell 6 1,growx");
		
		
		JButton btnNewButton = new JButton("save");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String categoryName=catName_txt.getText();
				String status=status_comboBox.getSelectedItem().toString();
				new AddProductCategory(categoryName,status);
				ProductCategory.load();
			}
		});
	
		add(btnNewButton, "cell 6 2");
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, "cell 0 3 7 1,grow");
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row=table.getSelectedRow();
				String fristValue=table.getModel().getValueAt(row, 0).toString();
				String secendValue=table.getModel().getValueAt(row, 1).toString();
				
				int id=ProductCategory.getCategoryId(fristValue);
				
				UpdateAddCategory addCategory=new UpdateAddCategory(fristValue, secendValue, id);
				addCategory.setVisible(true);
			}
		});
		ProductCategory.load();
		scrollPane.setViewportView(table);

	}

}
