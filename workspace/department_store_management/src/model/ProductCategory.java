package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import net.proteanit.sql.DbUtils;
import view.allpanel.AddProductCategoryPanel;
import view.allpanel.AddUnitPanel;

public class ProductCategory {
	
	private String name;
	private int status;
	
	public void prepareToInsert(String name,int status){
		
		this.name=name;
		this.status=status;
		
	}
	
	public void store() {
		
		Connection conn=Database.getconnection();
		String sql="INSERT INTO `product_category`(`name`, `status`) VALUES (?,?)";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, this.name);
			pst.setInt(2, this.status);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public static void load() {
		Connection conn=Database.getconnection();
		String sql="SELECT name,(CASE status WHEN 0 THEN 'Available' ELSE 'Not available' END)as status FROM `product_category`";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			AddProductCategoryPanel.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void updateCategory(int id) {
		
		Connection conn=Database.getconnection();
		String sql="UPDATE `product_category` SET `name`=?,`status`=? WHERE id=?";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, this.name);
			pst.setInt(2, this.status);
			pst.setInt(3, id);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public static ArrayList<String> getCategoryName() {
		
		ArrayList<String> catName=new ArrayList<>();
		
		Connection conn=Database.getconnection();
		String sql="SELECT `name` FROM `product_category`";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			
			while (rs.next()) {
				catName.add(rs.getString("name"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return catName;
		
	}
	
	public static int getCategoryId(String cat_Name) {
		int categoryId=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `id` FROM `product_category` where name=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, cat_Name);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				categoryId=rs.getInt("id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return categoryId;
		
	}
	public static String getCategoryName(int cat_id) {
		String categoryName = null;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `name` FROM `product_category` where id=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(1, cat_id);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				categoryName=rs.getString("name");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return categoryName;
		
	}


}
