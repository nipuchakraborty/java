package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import net.proteanit.sql.DbUtils;
import view.allpanel.AddUnitPanel;



public class Unit {
	
	private String name;
	private int status;
	
	public void prepareToInsert(String name,int status){
		
		this.name=name;
		this.status=status;
		
	}
	
	public void store() {
		
		Connection conn=Database.getconnection();
		String sql="INSERT INTO `product_unit`(`name`, `status`) VALUES (?,?)";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, this.name);
			pst.setInt(2, this.status);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public void updateUnit(int id) {
		
		Connection conn=Database.getconnection();
		String sql="UPDATE `product_unit` SET `name`=?,`status`=? WHERE id=?";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, this.name);
			pst.setInt(2, this.status);
			pst.setInt(3, id);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public static void load() {
		Connection conn=Database.getconnection();
		String sql="SELECT name,(CASE status WHEN 0 THEN 'Available' ELSE 'Not available' END)as status FROM `product_unit`";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			AddUnitPanel.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static ArrayList<String> getUnitName() {
		
		ArrayList<String> unitName=new ArrayList<>();
		
		Connection conn=Database.getconnection();
		String sql="SELECT `name` FROM `product_unit`";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			
			while (rs.next()) {
				unitName.add(rs.getString("name"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return unitName;
		
	}
	public static int getUnitId(String unitName) {
		int unitId=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `id` FROM `product_unit` where name=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, unitName);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				unitId=rs.getInt("id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return unitId;
		
	}

}
