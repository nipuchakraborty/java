package model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.jdesktop.swingx.JXDatePicker;

public class Utility {
	// string to int
	public static int stringToInteger(String giveString){
		
		int takeInt=Integer.parseInt(giveString);
		
		return takeInt;
		
	}

	// status
	/*public static ArrayList<String> getStatus(){
		
		ArrayList<String> status=new ArrayList<>();
		
		status.add("select one");
		status.add("Available");
		status.add("Not available");
		
		return status;
		
	}*/
	
	//status turn into 0 or 1 in database
	public static int changeStatusIntoZeroOne(String status) {
		int st;
		if(status.equals("Enable")){
			st=0;
		}
		else{
			st=1;
		}
		return st;
	}
	
	public static String dateToString(JXDatePicker datePicker){
		
		String mainDate=null;
		
		DateFormat date=new SimpleDateFormat("yyyy/MM/dd");
		Date jx_date=datePicker.getDate();
		mainDate=date.format(jx_date);
		
		return mainDate;
	}

}