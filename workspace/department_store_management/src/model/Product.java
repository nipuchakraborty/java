package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import net.proteanit.sql.DbUtils;
import view.allpanel.AddProductCategoryPanel;
import view.allpanel.AddProductPanel;

public class Product {
	
	private String productName;
	private int categoy;
	private int unit;
	private int status;
	
	public void prepareToInsert(String productName,int category,int unit,int status){
		
		this.productName=productName;
		this.categoy=category;
		this.unit=unit;
		this.status=status;
		
	}

	public void updateProduct(int id) {
		
		Connection conn=Database.getconnection();
		String sql="UPDATE `product` SET `name`=?,`category_id`=?,`unit_id`=?,`status`=? WHERE id=?";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, this.productName);
			pst.setInt(2, this.categoy);
			pst.setInt(3, this.unit);
			pst.setInt(4, this.status);
			pst.setInt(5, id);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public void deleteProduct(int id) {
		
		Connection conn=Database.getconnection();
		String sql="DELETE FROM `product` WHERE id=?";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setInt(5, id);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void store() {
		
		Connection conn=Database.getconnection();
		String sql="INSERT INTO `product`(`name`, `category_id`, `unit_id`, `status`) VALUES (?,?,?,?)";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, this.productName);
			pst.setInt(2, this.categoy);
			pst.setInt(3, this.unit);
			pst.setInt(4, this.status);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public static void load() {
		Connection conn=Database.getconnection();
		String sql="SELECT a.`name` as Product,b.name as Category,c.name as Unit,(CASE a.status WHEN 0 THEN 'Available' ELSE 'Not available' END)as status FROM product a,product_category b,product_unit c WHERE a.`category_id`=b.id AND a.`unit_id`=c.id";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			AddProductPanel.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static ArrayList<String> giveCategoryIdTakeProductName(int Category_id){
		
		ArrayList<String> catIdToProductName=new ArrayList<>();
		Connection is=Database.getconnection();
		String command="SELECT * FROM `product` WHERE category_id=?";
		
		try{
			PreparedStatement ps=is.prepareStatement(command);
			ps.setInt(1, Category_id);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				catIdToProductName.add(rs.getString("name")); 
			}
			
		}
		catch(Exception e){
			
		}
		return catIdToProductName;
	}
	
	public static int getProductId(String productName) {
		int productId=0;
		
		Connection conn=Database.getconnection();
		String sql="SELECT `id` FROM `product` WHERE `name`=?";
		
		try {
			
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, productName);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				productId=rs.getInt("id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return productId;
		
	}

}
