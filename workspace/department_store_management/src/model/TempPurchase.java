package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.proteanit.sql.DbUtils;
import view.allpanel.AddProductPanel;
import view.allpanel.CreatePurchasePanel;

public class TempPurchase {
	
	private String date;
	private String  vendor;
	private int categoryId;
	private int productId;
	private int quentity;
	private int buyPrice;
	private int salePrice;
	
	public void prepareTOInsert(String date, String vendorName, int cat_id, int product_Id, int pruchase_quentity, int buy, int sale){
		
		this.date=date;
		this.vendor=vendorName;
		this.categoryId=cat_id;
		this.productId=product_Id;
		this.quentity=pruchase_quentity;
		this.buyPrice=buy;
		this.salePrice=sale;
	}
	
	public void store() {
		
		Connection conn=Database.getconnection();
		String sql="INSERT INTO `temp_purchase`(`date`, `vendorName`, `product_id`, `category_id`, `quentity`, `buy_price`, `sale_price`) VALUES (?,?,?,?,?,?,?)";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			pst.setString(1, this.date);
			pst.setString(2, this.vendor);
			pst.setInt(3, this.categoryId);
			pst.setInt(4, this.productId);
			pst.setInt(5, this.quentity);
			pst.setInt(6, this.buyPrice);
			pst.setInt(7, this.salePrice);
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public static void load() {
		Connection conn=Database.getconnection();
		String sql="SELECT a.date ,a.`vendorName`,b.name as productName,c.name as category,a.`quentity`,a.`buy_price` as buyPrice,a.`sale_price` as salePrice FROM temp_purchase a, product b,product_category c WHERE a.product_id=b.id AND a.`category_id`=c.id";
		
		try {
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			CreatePurchasePanel.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
