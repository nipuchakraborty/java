package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import model.Database;

public class Status {
	
public static ArrayList<String> getStatus(){
		
		ArrayList<String> status=new ArrayList<>();
		Connection conn=Database.getconnection();
		String sql="SELECT * FROM `status`";
		
		try{
			PreparedStatement ps=conn.prepareStatement(sql);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				status.add(rs.getString("status_name")); 
			}
			
		}
		catch(Exception e){
			
			e.printStackTrace();
			
		}
		return status;
	}
	
	

}
