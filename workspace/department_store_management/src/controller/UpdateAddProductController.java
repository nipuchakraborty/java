package controller;

import model.Product;
import model.ProductCategory;
import model.Unit;
import model.Utility;

public class UpdateAddProductController {
	
	public UpdateAddProductController(String product, String category, String unit, String status, int id) {
		
		int cat_id=ProductCategory.getCategoryId(category);
		int unit_id=Unit.getUnitId(unit);
		int st=Utility.changeStatusIntoZeroOne(status);
		
		Product pro=new Product();
		pro.prepareToInsert(product, cat_id, unit_id, st);
		pro.updateProduct(id);
		pro.load();
		
	}

}
