package controller;

import model.ProductCategory;
import model.Utility;

public class UpdateAddCategoryController {
	
	public UpdateAddCategoryController(String cat, String status, int id) {
		
		int st=Utility.changeStatusIntoZeroOne(status);
		
		ProductCategory category=new ProductCategory();
		
		category.prepareToInsert(cat, st);
		category.updateCategory(id);
		category.load();
		
		
	}

}
