package controller;

import model.Unit;
import model.Utility;

public class AddUnit {
	
	public AddUnit(String unitName, String status) {
		
		int unitStatus=Utility.changeStatusIntoZeroOne(status);
		
		Unit unit=new Unit();
		unit.prepareToInsert(unitName, unitStatus);
		unit.store();
		
		
	}

}
