package controller;

import model.Unit;
import model.Utility;

public class UpdateAddUnitController {
	
	public UpdateAddUnitController(String unit, String status, int id) {
		
		int st=Utility.changeStatusIntoZeroOne(status);
		
		Unit u=new Unit();
		u.prepareToInsert(unit, st);
		u.updateUnit(id);
		u.load();
		
	}

}
