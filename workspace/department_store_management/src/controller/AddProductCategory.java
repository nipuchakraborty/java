package controller;

import model.ProductCategory;
import model.Utility;

public class AddProductCategory {
	
	public AddProductCategory(String categoryName, String Catstatus) {
		
		int status=Utility.changeStatusIntoZeroOne(Catstatus);
		
		ProductCategory category=new ProductCategory();
		category.prepareToInsert(categoryName, status);
		category.store();
		
	}

}
