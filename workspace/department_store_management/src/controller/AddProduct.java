package controller;

import model.Product;
import model.ProductCategory;
import model.Unit;
import model.Utility;

public class AddProduct {
	
	public AddProduct(String productName, String catName, String unitName, String status) {
		
		int category=ProductCategory.getCategoryId(catName);
		int unit=Unit.getUnitId(unitName);
		int st=Utility.changeStatusIntoZeroOne(status);
	
		Product product=new Product();
		product.prepareToInsert(productName, category, unit, st);
		product.store();
		
	}

}
