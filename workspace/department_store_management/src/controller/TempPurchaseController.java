package controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.Product;
import model.ProductCategory;
import model.Utility;

public class TempPurchaseController {
	
	public TempPurchaseController(Date clDate, String vendorName,  String category, String productName,String quentity, String buyPrize, String sellPrize) {
		
		DateFormat date=new SimpleDateFormat("yyyy/MM/dd");
		String main_date=date.format(clDate);
		
		int cat_id=ProductCategory.getCategoryId(category);
		int product_Id=Product.getProductId(productName);
		int pruchase_quentity=Utility.stringToInteger(quentity);
		int buy=Utility.stringToInteger(buyPrize);
		int sale=Utility.stringToInteger(sellPrize);
		
		model.TempPurchase purchase=new model.TempPurchase();
		purchase.prepareTOInsert(main_date,vendorName,cat_id,product_Id,pruchase_quentity,buy,sale);
		purchase.store();
	}

}
