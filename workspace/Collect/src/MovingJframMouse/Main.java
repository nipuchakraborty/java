package MovingJframMouse;

    import java.awt.Point;
    import java.awt.event.MouseEvent;
    import java.awt.event.MouseListener;
    import java.awt.event.MouseMotionListener;
    import javax.swing.JFrame;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
    public class Main
    {
        static Point mouseDownScreenCoords;
        static Point mouseDownCompCoords;
        public static void main(String[] args)
        {
            final JFrame f = new JFrame("Hello");
            mouseDownScreenCoords = null;
            mouseDownCompCoords = null;
            f.setUndecorated(true);
            f.setVisible(true);
            f.setBounds(0, 0, 500, 500);
            f.getContentPane().setLayout(null);
            
            
            f.addMouseListener(new MouseListener(){
                public void mouseReleased(MouseEvent e) {
                	
                    mouseDownScreenCoords = null;
                    mouseDownCompCoords = null;
                }
                public void mousePressed(MouseEvent e) {
                    mouseDownScreenCoords = e.getLocationOnScreen();
                    mouseDownCompCoords = e.getPoint();
                }
                public void mouseExited(MouseEvent e) {
                }
                public void mouseEntered(MouseEvent e) {
                }
                public void mouseClicked(MouseEvent e) {
                	
                }
            });
            f.addMouseMotionListener(new MouseMotionListener(){
                public void mouseMoved(MouseEvent e) {
                }
                public void mouseDragged(MouseEvent e) {
                    Point currCoords = e.getLocationOnScreen();
                    f.setLocation(mouseDownScreenCoords.x + (currCoords.x - mouseDownScreenCoords.x) - mouseDownCompCoords.x,
                                  mouseDownScreenCoords.y + (currCoords.y - mouseDownScreenCoords.y) - mouseDownCompCoords.y);
                }
            });
        }
    }