package MovingJframMouse;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import MovingJframMouse.UndecoratedExample.BorderPanel;
import MovingJframMouse.UndecoratedExample.MainPanel;
import MovingJframMouse.UndecoratedExample.OutsidePanel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JTextField;

public class MoveMF extends JFrame {
	public MoveMF() {
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		
		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(10);
	}

	private JPanel contentPane;
	private JFrame frame;
	private JTextField textField;
	class MainPanel extends JPanel {

		public MainPanel() {
			setBackground(Color.gray);
		}

		@Override
		public Dimension getPreferredSize() {
			return new Dimension(400, 400);
		}
	}

	class BorderPanel extends JPanel {

		private JLabel label;
		int pX, pY;

		public BorderPanel() {
			label = new JLabel(" X ");
			label.setOpaque(true);
			label.setBackground(Color.RED);
			label.setForeground(Color.WHITE);

			setBackground(Color.black);
			setLayout(new MigLayout());

			add(label);

			label.addMouseListener(new MouseAdapter() {
				public void mouseReleased(MouseEvent e) {
					System.exit(0);
				}
			});
			addMouseListener(new MouseAdapter() {
				public void mousePressed(MouseEvent me) {
					// Get x,y and store them
					pX = me.getX();
					pY = me.getY();

				}

				public void mouseDragged(MouseEvent me) {

					frame.setLocation(frame.getLocation().x + me.getX() - pX, frame.getLocation().y + me.getY() - pY);
				}
			});

			addMouseMotionListener(new MouseMotionAdapter() {
				public void mouseDragged(MouseEvent me) {

					frame.setLocation(frame.getLocation().x + me.getX() - pX, frame.getLocation().y + me.getY() - pY);
				}
			});
		}
	}

	class OutsidePanel extends JPanel {

		public OutsidePanel() {
			setLayout(new BorderLayout());
			add(new MainPanel(), BorderLayout.CENTER);
			add(new BorderPanel(), BorderLayout.PAGE_START);
			setBorder(new LineBorder(Color.BLACK, 5));
		}
	}

	private void createAnsShowGui() {
		ComponetResizer cr = new ComponetResizer();
		cr.setMinimumSize(new Dimension(300, 300));
		cr.setMaximumSize(new Dimension(800, 600));
		cr.setSize(new Dimension(10, 10));
		frame.setUndecorated(true);
		frame.getContentPane().add(new OutsidePanel());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			public void run() {
				new UndecoratedExample().createAnsShowGui();
			}
		});
	}

}
