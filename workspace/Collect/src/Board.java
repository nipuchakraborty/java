import javax.swing.*;
import java.awt.*;

/**
* Created by evengultvedt on 14.02.14.
*/

import javax.swing.*;
import java.awt.*;
//The board class, which the drawing is on
class Board extends JPanel {
    //The image of the player
    private Image imgPlayer;

public Board() {
    setPreferredSize(new Dimension(400, 400));
    setBackground(new Color(255, 255, 255));
    setLayout(null);
    //getting the player.gif file
    ImageIcon player = new ImageIcon("G:/New folder/photos/pic/New folder (3)/F&M/138.GIF");
    //and put in the imgPlayer variable
    imgPlayer = player.getImage();
}
public void paintComponent(Graphics graphics) {
    Graphics2D graphics2D = (Graphics2D) graphics;
    //this doesn't work
    graphics2D.drawImage(imgPlayer, 10, 10, 100, 100, null);
    //this works
    graphics2D.drawString("Test drawing", 120, 120);
}
}

//The JFrame to put the panel on
class AnimatePlayer extends JFrame{

    public AnimatePlayer() {
        Board board = new Board();
        add(board);
        setTitle("PlayerTestAnimation");
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(400, 400);
        setVisible(true);

    }

public static void main(String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
            new AnimatePlayer();
        }
    });
}
}