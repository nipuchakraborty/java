import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class LebelTextPos {

  public static void main(String args[]) {
    JFrame frame = new JFrame("Label Text Pos");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    Container content = frame.getContentPane();
    content.setLayout(new GridLayout(2, 2));

    Border border = LineBorder.createGrayLineBorder();
    Icon warnIcon = new ImageIcon("Warn.gif");

    JLabel label1 = new JLabel(new ImageIcon("C:\\Users\\Nk\\Downloads\\15-128.png"));
    label1.setText("Left-Bottom");
    label1.setHorizontalTextPosition(JLabel.LEFT);
    label1.setVerticalTextPosition(JLabel.BOTTOM);
    label1.setBorder(border);
    content.add(label1);

    JLabel label2 = new JLabel(new ImageIcon("C:\\Users\\Nk\\Downloads\\15-128.png"));
    label2.setText("Right-TOP");
    label2.setHorizontalTextPosition(JLabel.RIGHT);
    label2.setVerticalTextPosition(JLabel.TOP);
    label2.setBorder(border);
    content.add(label2);

    JLabel label3 = new JLabel(new ImageIcon("C:\\Users\\Nk\\Downloads\\15-128.png"));
    label3.setText("Center-Center");
    label3.setHorizontalTextPosition(JLabel.CENTER);
    label3.setVerticalTextPosition(JLabel.CENTER);
    label3.setBorder(border);
    content.add(label3);

    JLabel label4 = new JLabel(new ImageIcon("C:\\Users\\Nk\\Downloads\\15-128.png"));
    label4.setText("Center-Bottom");
    label4.setHorizontalTextPosition(JLabel.CENTER);
    label4.setVerticalTextPosition(JLabel.BOTTOM);
    label4.setBorder(border);
    content.add(label4);

    frame.setSize(553, 411);
    frame.setVisible(true);
  }
}