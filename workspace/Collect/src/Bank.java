import java.util.*;
	 
	class Bank {
	 
	    String Name;
	    String address;
	    int AccountName;
	    int balance;
	    int dep;
	 
	    public Bank(String Name, String address, int AccountName, int balance) {
	 
	        this.address = address;
	        this.balance = balance;
	        this.AccountName = AccountName;
	        this.Name = Name;
	    }
	 
	    /** Returns string with this format:
	     * Name of the customer.:Cust Name, Address of the customer.:Address, A/c no..:ac no, Balance in A/c..:balance
	     */
	    @Override
	    public String toString() {
	        //using StringBuilder is a better idea over concatinating by + operator
	        StringBuilder sb = new StringBuilder();
	        sb.append("Name of the customer.:");
	        sb.append(this.Name);
	        sb.append(", ");
	        sb.append("Address of the customer.:");
	        sb.append(this.address);
	        sb.append(", ");
	        sb.append("A/c no..:");
	        sb.append(this.AccountName);
	        sb.append(", ");
	        sb.append("Balance in A/c..:");
	        sb.append(this.balance);
       StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("Name of the customer.:");
			stringBuilder.append(this.Name);
			stringBuilder.append(", ");
			stringBuilder.append("Address of the customer.:");
			stringBuilder.append(this.address);
			stringBuilder.append(", ");
			stringBuilder.append("A/c no..:");
			stringBuilder.append(this.AccountName);
			stringBuilder.append(", ");
			stringBuilder.append("Balance in A/c..:");
			stringBuilder.append(this.balance);
		return stringBuilder.toString();
	    }

	 

	 
	    public static void main(String[] args) {
	 
	        List<Bank> l = new LinkedList<Bank>();
	 
	 
	        Bank b1 = new Bank("Nasir", "Nawabpur,Dhaka", 123, 1000);
	        Bank b2 = new Bank("Ashraf", "Kakrile,Dhaka", 124, 1500);
	        Bank b3 = new Bank("Enamul", "Banani, Dhaka", 125, 1600);
	        Bank b4 = new Bank("Masuk", "Samastipur,B.Baria", 126, 1700);
	        Bank b5 = new Bank("Zia", "Lalkhanbazar,Chittagong", 127, 1800);
	        l.add(b1);
	        l.add(b2);
	        l.add(b3);
	        l.add(b4);
	        l.add(b5);
	        Iterator<Bank> i = l.iterator();
	        while (i.hasNext()) {
	            System.out.println(i.next());
	        }
	    }
	}

