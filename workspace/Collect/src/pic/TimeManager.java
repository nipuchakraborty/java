package pic;


import javax.jws.WebParam.Mode;

public class TimeManager<Display, ModeManager, Alarm> {
private static final int SNOOZE_DURATION_IN_MINUTES = 2;
private Time currentTime;
private Time alarmTime;
private Time snoozeTime;
private Display display;
private ModeManager modeMgr;
private Alarm       alarm;

public TimeManager() {
    currentTime = new Time(12,0);
    alarmTime = new Time(12,0);
    snoozeTime = new Time(12,0);
}

public void setDisplay(Display newDisplay) {
    display = newDisplay;
    showCurrentTime();
}

public void setModeManager(ModeManager newModeMgr) {
    modeMgr = newModeMgr;
}

public void setAlarm(Alarm newAlarm) {
    alarm = newAlarm;
}

    public void incrementCurrentMinute() {
    currentTime.addOneMinute();
    Mode mode = modeMgr.getMode();
    if (mode == mode.ALARM_ON || mode == mode.ALARM_OFF || mode == mode.SET_TIME) {
        updateDisplay();
    }
    if (mode == mode.ALARM_ON) {
        if (currentTime.equals(alarmTime) || currentTime.equals(snoozeTime)) {
        soundAlarmIfNecessary();
        }
    }
}

public void incrementCurrentHour() {
    currentTime.incrementHour();
    Mode mode = modeMgr.getMode();
    if (mode == mode.ALARM_ON || mode == mode.ALARM_OFF || mode == mode.SET_TIME) {
        updateDisplay();
    }
    if (mode == mode.ALARM_ON) {
        if (currentTime.equals(alarmTime) || currentTime.equals(snoozeTime)) {
        soundAlarmIfNecessary();
        }
    }
}

public void incrementAlarmMinute() {
    alarmTime.incrementMinute();
    Mode mode = modeMgr.getMode();
    if (mode == mode.SET_ALARM) {
        updateDisplay();
    }
    if (mode == mode.ALARM_ON) {
        if (currentTime.equals(alarmTime)) {
            soundAlarmIfNecessary();
        }
    }
}

public void incrementAlarmHour() {
    alarmTime.incrementHour();
    Mode mode = modeMgr.getMode();
    if (mode == mode.SET_ALARM) {
        updateDisplay();
    }
    if (mode == mode.ALARM_ON) {
        if (currentTime.equals(alarmTime)) {
            soundAlarmIfNecessary();
        }
    }
}

public void snooze() {
    while (alarm.isOn()) {
        alarm.off();
        int hour = currentTime.getHour();
        int minute = currentTime.getMinute();
        minute += SNOOZE_DURATION_IN_MINUTES;
            if (minute > 59) {
                minute -= 60;
                ++hour;
            }
        snoozeTime = new Time(hour, minute);
        }
}

public void showCurrentTime() {
    display.showHour(currentTime.getHour());
    display.showMinute(currentTime.getMinute());
}

public void showAlarmTime() {
    display.showHour(alarmTime.getHour());
    display.showMinute(alarmTime.getMinute());
}

private void updateDisplay() {
    Mode mode = modeMgr.getMode();
    if (mode == mode.SET_TIME || mode == mode.ALARM_ON || mode == mode.ALARM_OFF) {
        showCurrentTime();
    }
    else {
        showAlarmTime();
    }
}

private void soundAlarmIfNecessary() {
    Mode mode = modeMgr.getMode();
    if (mode == mode.ALARM_ON && currentTime.equals(alarmTime) || currentTime.equals(snoozeTime)) {
        alarm.on();
    }
}
}

