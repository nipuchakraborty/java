package pic;

public class Time {
private int hour;
private int minute;

public Time(int initHour, int initMinute) {
    if (1 <= hour && hour <= 12) {
        hour = initHour;
    }
    else if (hour == 0) {
        hour = 12;
    }
    if (0 <= minute && minute <= 59 ) {
        minute = initMinute;
    }
}

public void addOneMinute() {
    minute++;
    if (minute == 59) {
        incrementHour();
        minute = 0;
    }
}

public void incrementHour() {
    hour++;
    if (hour > 12) {
        hour = 1;
    }
}

public void incrementMinute() {
    minute++; 
    if (minute == 59) {
        minute = 0;
    }
}

public int getHour() {
    if (hour >= 1 && hour <= 12) {
    }
    return hour;
}

public int getMinute() {
    if (minute >=0 && minute <= 59) {
    }
    return minute;
}

public boolean equals(Time time) {
    if (hour == hour && minute == minute) {     
        return true;
    }
    else {
        return false;
    }
}
}
