import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
 
public class ToastDemo extends JFrame {
 
    JButton btn;
 
    public ToastDemo() {
        super("Toast Demo");
        btn = new JButton("Ok");
        setSize(400, 300);
        setLayout(new FlowLayout());
        getContentPane().add(btn);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
 
        btn.addActionListener(new ActionListener() {
 
            @Override
            public void actionPerformed(ActionEvent e) {
                final JDialog dialog = new AndroidLikeToast(ToastDemo.this, true, "Hello NK welcome you have successfull to login!");
                Timer timer = new Timer(AndroidLikeToast.LENGTH_LONG, new ActionListener() {
 
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        dialog.setVisible(false);
                        dialog.dispose();
                    }
                });
                timer.setRepeats(false);
                timer.start();
 
                dialog.setVisible(true); // if modal, application will pause here
            }
        });
    }
 
    public static void main(String[] args) {
        ToastDemo e = new ToastDemo();
        //e.pack();
        e.setVisible(true);
    }
}