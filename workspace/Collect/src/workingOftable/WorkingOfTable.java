package workingOftable;

import javax.swing.event.*;
import javax.swing.table.DefaultTableModel;
 
public class WorkingOfTable extends javax.swing.JFrame {
 
    DefaultTableModel model;
 
    /** Creates new form DynRowCheckSum */
    public WorkingOfTable() {
        initComponents();
        model = new DefaultTableModel() {
 
            Class[] types = new Class[]{
                java.lang.Integer.class, java.lang.String.class, java.lang.Double.class, java.lang.Boolean.class
            };
 
            @Override
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
        };
        jTable1.setModel(model);
        model.addColumn("Id");
        model.addColumn("Name");
        model.addColumn("Salary");
        model.addColumn("Check");
 
        model.addTableModelListener(new TableModelListener() {
 
            @Override
            public void tableChanged(TableModelEvent e) {
                int numberOfRaw = model.getRowCount();
                double total = 0;
                for (int i = 0; i < numberOfRaw; i++) {
                    boolean b = Boolean.parseBoolean(model.getValueAt(i, 3).toString());
                   //if checkbox is checked
                    if (b) {
                        double value = Double.valueOf(model.getValueAt(i, 2).toString());
                        total += value;
                    }
                }
                lblTotal.setText(Double.toString(total));
            }
        });
    }
 
    private void initComponents() {
        //Ntebeans Generated Code goes here
    }
 
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {                                         
 
        model.addRow(new Object[]{jTextField1.getText(), jTextField2.getText(), Double.valueOf(jTextField3.getText()), true});
 
    }                                        
 
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {  
        // remove all rows from table
        if (jTable1.getRowCount() > 0) {
            for (int i = jTable1.getRowCount() - 1; i > -1; i--) {
                model.removeRow(i);
            }
        }
    }                                        
 
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
 
            public void run() {
                new WorkingOfTable().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JLabel lblTotal;
    // End of variables declaration
}
