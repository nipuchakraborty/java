import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.SwingConstants;
import net.miginfocom.swing.MigLayout;
import java.awt.Font;
import java.awt.Dialog.ModalExclusionType;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Dimension;
import java.awt.SystemColor;

public class Image extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Image frame = new Image();
					frame.setVisible(true);
					frame.setUndecorated(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	public Image() {
		setModalExclusionType(ModalExclusionType.TOOLKIT_EXCLUDE);
	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 846, 464);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.activeCaptionBorder);
		contentPane.setForeground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		 Border border = BorderFactory.createLineBorder(Color.black);
		contentPane.setLayout(new MigLayout("", "[127px][127px][127px][127px][127px][127px]", "[150px]"));
		
		JLabel label4 = new JLabel(new ImageIcon("C:\\Users\\Nk\\Downloads\\food-grey.png"));
		label4.setFont(new Font("Segoe UI Historic", Font.BOLD, 18));
	    label4.setText("Order");
	    label4.setHorizontalTextPosition(JLabel.CENTER);
	    label4.setVerticalTextPosition(JLabel.BOTTOM);
	    label4.setBorder(border);
	    label4.setPreferredSize(new Dimension(150, 100));
	    contentPane.add(label4, "cell 0 0,alignx left,aligny top");
	    label4.addMouseListener(new MouseAdapter() {
	    	@Override
	    	public void mouseClicked(MouseEvent e){
	    		label4.setBackground(Color.BLUE);
	    	}
		});
	   
	    
	    JLabel lblSpcialDish = new JLabel(new ImageIcon("C:\\Users\\Nk\\Downloads\\34-128.png"));
	    lblSpcialDish.setFont(new Font("Segoe UI Historic", Font.BOLD, 18));
	    lblSpcialDish.setVerticalTextPosition(SwingConstants.BOTTOM);
	    lblSpcialDish.setText("Spcial Dish");
	    lblSpcialDish.setHorizontalTextPosition(SwingConstants.CENTER);
	    lblSpcialDish.setBorder(border);
	    contentPane.add(lblSpcialDish, "cell 1 0,grow");
	    
	    JLabel label_1 = new JLabel(new ImageIcon("C:\\Users\\Nk\\Downloads\\1495296173_Discover.png"));
	    label_1.setFont(new Font("Segoe UI Historic", Font.BOLD, 18));
	    label_1.setVerticalTextPosition(SwingConstants.BOTTOM);
	    label_1.setText("Center-Bottom");
	    label_1.setHorizontalTextPosition(SwingConstants.CENTER);
	    label_1.setBorder(border);
	    contentPane.add(label_1, "cell 2 0,grow");
	    
	    JLabel label_2 = new JLabel(new ImageIcon("C:\\Users\\Nk\\Downloads\\Dinner-128.png"));
	    label_2.setFont(new Font("Segoe UI Historic", Font.BOLD, 18));
	    label_2.setVerticalTextPosition(SwingConstants.BOTTOM);
	    label_2.setText("Center-Bottom");
	    label_2.setHorizontalTextPosition(SwingConstants.CENTER);
	    label_2.setBorder(border);
	    contentPane.add(label_2, "cell 3 0,grow");
	    
	    JLabel label_3 = new JLabel(new ImageIcon("C:\\Users\\Nk\\Downloads\\martini_dinner_lunch_restaurant_vegetables_drink-128.png"));
	    label_3.setFont(new Font("Segoe UI Historic", Font.BOLD, 18));
	    label_3.setVerticalTextPosition(SwingConstants.BOTTOM);
	    label_3.setText("Center-Bottom");
	    label_3.setHorizontalTextPosition(SwingConstants.CENTER);
	    label_3.setBorder(border);
	    contentPane.add(label_3, "cell 4 0,grow");
	    
	    JLabel label_4 = new JLabel(new ImageIcon("C:\\Users\\Nk\\Downloads\\1495296109_09_Calculator.png"));
	    label_4.setFont(new Font("Segoe UI Semilight", Font.BOLD, 18));
	    label4.setFont(new Font("Segoe UI Historic", Font.BOLD, 18));
	    label_4.setVerticalTextPosition(SwingConstants.BOTTOM);
	    label_4.setText("Center-Bottom");
	    label_4.setHorizontalTextPosition(SwingConstants.CENTER);
	    label_4.setBorder(border);
	    contentPane.add(label_4, "cell 5 0,grow");
	    
	    
	}

}
