import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PattenCompailer {

  static public final String poem = "PC Bird Cow Slow Doe Hole Joe\n"
      + "Shy Shift Sleep Do Down Doing.\n" + "Yes No How Whose\n";

  public static void main(String[] args) {
    Matcher m = Pattern.compile("(?m)(\\S+)\\s+((\\S+)\\s+(\\S+))$").matcher(poem);
    while (m.find()) {
      for (int j = 0; j <= m.groupCount(); j++)
        System.out.print("[" + m.group(j) + "]");
      System.out.println();
    }
  }
}