

public void getAllRecord() {
      String fn = txtName.getText();
        _add = txtAdd.getText();
        _coun = cmbCountry.getSelectedItem().toString();
        if (rdMale.isSelected()) {
            _gender = "Male";
        } else {
            _gender = "Female";
        }
 
        //get value from Checkbox   
        for (int i = 0; i < pnlDegree.getComponentCount(); i++) {
            JCheckBox checkBox = (JCheckBox) pnlDegree.getComponent(i);
            if (checkBox.isSelected()) {
                _degree += checkBox.getText();
                _degree += gap;
            }
        }
 
    }


private void writeAll() {
	 
    BufferedWriter buf;
    try {
        buf = new BufferedWriter(new FileWriter(file, true));
        buf.write(_fn + "," + _add + "," + _coun + "," + _degree + "," + _gender);
        buf.newLine();
        buf.close();
        JOptionPane.showMessageDialog(this, "Data Saved");

    } catch (Exception e) {
    }
}


private void displayAll() {
    FileInputStream fobj = null;
    try {
        String str4 = "";
        File f = new File("d://info.txt");
        fobj = new FileInputStream(f);
        int len = (int) f.length();
        for (int j = 0; j < len; j++) {
            char str5 = 0;
            try {
                str5 = (char) fobj.read();
                //System.out.println(str5);
                if (str5 == ',') {
                    str5 = '\t';
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            str4 = str4 + str5;
        }
        txtShow.setText(str4);
        fobj.close();
    } catch (IOException ex) {
        ex.printStackTrace();
    }
}


private void displayinTable() {
	 
    InputStream is;

    try {
        File f = new File("d://info.txt");
        is = new FileInputStream(f);
        Scanner scan = new Scanner(is);
        String[] array;
        while (scan.hasNextLine()) {
            String line = scan.nextLine();
            if (line.indexOf(",") > -1) {
                array = line.split(",");
            } else {
                array = line.split("\n");
            }
            Object[] data = new Object[array.length];
            for (int i = 0; i < array.length; i++) {
                data[i] = array[i];
            }

            model.addRow(data);
        }


    } catch (Exception e) {
    }

}





public void clear() {
    //Clear all text field
    txtName.setText("");
    txtAdd.setText("");
    //set combo box to select first value
    cmbCountry.setSelectedIndex(0);
    //clearing variavle degree so that it can take new value each time
    _degree = "";
    //deselecting all radio
    gender.clearSelection();
    //deselecting all check box
    for (int i = 0; i < pnlDegree.getComponentCount(); i++) {
        JCheckBox checkBox = (JCheckBox) pnlDegree.getComponent(i);
        if (checkBox.isSelected()) {
            checkBox.setSelected(false);
        }
    }
    //System.out.println(tblStudent.getRowCount());
    // remove all row from table
    if (tblStudent.getRowCount() > 0) {
        for (int i = tblStudent.getRowCount() - 1; i > -1; i--) {
            model.removeRow(i);
        }
    }
    txtShow.setText("");
}