package backgroundImage;
import java.awt.*;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.event.*;
import javax.swing.*;
 
public class BackGroundImage extends JFrame {
 
    int x, y;
    JDesktopPane desk;
    private Image img;
    JPopupMenu deskMenu;
    JMenuItem refreshMI, mit2, mit3;
 
public BackGroundImage() throws HeadlessException {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        x = dim.width;
        y = dim.height;
        setSize(x, y);
        setTitle("Background image demo");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        desk = new JDesktopPane() {
 
            @Override
            public void paintComponent(Graphics g) {
                bakImage(g);
            }
        };
        add(desk);
        // creating the popup menu items
        refreshMI = new JMenuItem("Refresh");
        mit2 = new JMenuItem("New");
        mit3 = new JMenuItem("Send to..");
        refreshMI.setBackground(new Color(205, 210, 195));
        //creating the popup menu which will display when user clicks the right mouse button
        deskMenu = new JPopupMenu();
        deskMenu.add(refreshMI);
        deskMenu.addSeparator();
        deskMenu.add(mit2);
        deskMenu.addSeparator();
        deskMenu.add(mit3);
 
        addMouseListener(new MouseAdapter() {
 
            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    deskMenu.show(BackGroundImage.this, e.getX(), e.getY());
                }
            }
        });
  }
 
    public void bakImage(Graphics g) {
        try {
            String s = "C:/Users/NK CHAKRABORTY/Desktop/image/holi.jpg";//you need to place the image in images folder.
            Toolkit kit = Toolkit.getDefaultToolkit();
            img = kit.getImage(getClass().getResource(s));
            img = img.getScaledInstance(1500, -1, Image.SCALE_FAST);
            MediaTracker t = new MediaTracker(this);
            t.addImage(img, 0);
            while (true) {
                try {
                    t.waitForAll();
                    break;
                } catch (Exception ee) {
                }
            }
            g.drawImage(img, 0, 0, x, y, null);
 
        } catch (Exception re) {
            System.out.println("nor found");
        }
    }
 
    public static void main(String[] args) {
        new BackGroundImage().setVisible(true);
    }
}