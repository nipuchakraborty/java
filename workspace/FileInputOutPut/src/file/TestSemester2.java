package file;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
  
import javax.swing.*;
  
public class TestSemester2 extends JFrame {
  
public TestSemester2() {
 super("Arabic Stemmer..");
 setSize(350, 470);
 setDefaultCloseOperation(EXIT_ON_CLOSE);
 setResizable(false);
 Container c = getContentPane();
 c.setLayout(new FlowLayout());
  
 JButton openButton = new JButton("Open");
 JButton saveButton = new JButton("Save");
 JButton dirButton = new JButton("Pick Dir");
 final JTextArea ta=new JTextArea(10, 25);
 JTextArea ta2=new JTextArea("Stemmed File will be written here", 10, 25);
 final JLabel statusbar = 
              new JLabel("Output of your selection will go here");
  
 // Create a file chooser that opens up as an Open dialog
 openButton.addActionListener(new ActionListener() {
    
     public void actionPerformed(ActionEvent ae) {
     
     JFileChooser chooser = new JFileChooser();
     chooser.setMultiSelectionEnabled(true);
     int option = chooser.showOpenDialog(TestSemester2.this);
     if (option == JFileChooser.APPROVE_OPTION) {
       File[] sf = chooser.getSelectedFiles();
       String filelist = "nothing";
       if (sf.length > 0) filelist = sf[0].getName();
       for (int i = 1; i < sf.length; i++) {
         filelist += ", " + sf[i].getName();
       }
       statusbar.setText("You chose " + filelist);
        
        // Read in data file to JTextArea
        try{
        String strLine;
        File selectedFile = chooser.getSelectedFile();
        FileInputStream in = new FileInputStream(selectedFile);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        while ((strLine = br.readLine()) != null) {
            ta.append(strLine + "\n");
        }
        }catch(Exception e){
           System.out.println("Ouch, I fell over! " + e);
        }
        
     }
     else {
       statusbar.setText("You canceled.");
     }
   }
 });
  
 // Create a file chooser that opens up as a Save dialog
 saveButton.addActionListener(new ActionListener() {
   public void actionPerformed(ActionEvent ae) {
     JFileChooser chooser = new JFileChooser();
     int option = chooser.showSaveDialog(TestSemester2.this);
     if (option == JFileChooser.APPROVE_OPTION) {
       statusbar.setText("You saved " + ((chooser.getSelectedFile()!=null)?
                         chooser.getSelectedFile().getName():"nothing"));
     }
     else {
       statusbar.setText("You canceled.");
     }
   }
 });
  
 // Create a file chooser that allows you to pick a directory
 // rather than a file
 dirButton.addActionListener(new ActionListener() {
   public void actionPerformed(ActionEvent ae) {
     JFileChooser chooser = new JFileChooser();
     chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
     int option = chooser.showOpenDialog(TestSemester2.this);
     if (option == JFileChooser.APPROVE_OPTION) {
       statusbar.setText("You opened " + ((chooser.getSelectedFile()!=null)?
                         chooser.getSelectedFile().getName():"nothing"));
        
     }
     else {
       statusbar.setText("You canceled.");
     }
   }
 });
  
 c.add(openButton);
 c.add(saveButton);
 c.add(dirButton);
 c.add(statusbar);
 c.add(ta);
 c.add(ta2);
}
  
public static void main(String args[]) {
 TestSemester2 sfc = new TestSemester2();
 sfc.setVisible(true);
}
}