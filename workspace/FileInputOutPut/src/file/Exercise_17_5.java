package file;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import java.io.*;

public class Exercise_17_5 extends Application{
    @Override 
    public void start(Stage primaryStage) throws FileNotFoundException, IOException{
        BorderPane pane = new BorderPane();

        HBox hbox = new HBox(10);
        hbox.setAlignment(Pos.CENTER);
        Button btWrite = new Button("Write");
        Button btRead = new Button("Read");
        hbox.getChildren().addAll(btWrite, btRead);
        hbox.setPadding(new Insets(5,5,5,5));
        pane.setBottom(hbox);

        TextArea taDisplay = new TextArea();
        taDisplay.setWrapText(true);
        pane.setCenter(taDisplay);


        primaryStage.setTitle("Exercise 17_05");
        Scene scene = new Scene(pane, 350,250);
        primaryStage.setScene(scene);
        primaryStage.show();

        btWrite.setOnAction(e -> writeData());
        btRead.setOnAction(e -> readData());

        private int[] array = {1,2,3,4,5};
        private double[] arrayDouble = {5.5};
        private Date[] dateArray = {new Date()};

        public void writeData() throws IOException{
            try(ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("Exercise15_05.dat"));
                    ){
                output.writeObject(array);
                output.writeObject(arrayDouble);
                output.writeObject(dateArray);
            }
        }

        public void readData() throws IOException{
            try(ObjectInputStream input = new ObjectInputStream(new FileInputStream("Exercise15_05.dat"));
                    ){
                int[] newInt = (int[])(input.readObject());
                double[] newDouble = (double[])(input.readObject());
                Date[] newDate = (Date[])(input.readObject());

                String existingText = taDisplay.getText();

                for(int i =0; i < newInt.length; i ++){
                    taDisplay.setText = (existingText +  " " + newInt[i]);
                }
                taDisplay.setText(existingText + "\n" + newDouble[0] + "\n" + newDate[0]);

            }
        }
    }

    public static void main(String[] args){
        Application.launch(args);
    }
}