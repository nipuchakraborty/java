package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Manage;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Toolkit;

public class Contact_design extends JFrame {

	private JPanel contentPane;
	private JTextField name_txt;
	private JTextField mobile_num_txt;
	private JTextField email_txt;
	private JTextField faceBook_txt;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Contact_design frame = new Contact_design();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Contact_design() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("F:\\my moment picture\\Untitled-2.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 518, 500);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmOpen = new JMenuItem("Open");
		mnFile.add(mntmOpen);
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mnFile.add(mntmSave);
		
		JMenuItem mntmSaveAs = new JMenuItem("Save as");
		mnFile.add(mntmSaveAs);
		
		JMenuItem mntmClose = new JMenuItem("Close");
		mnFile.add(mntmClose);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(173, 255, 47));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblContactInformation = new JLabel("Contact Information");
		lblContactInformation.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblContactInformation.setBounds(111, 26, 202, 16);
		contentPane.add(lblContactInformation);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblName.setBounds(12, 108, 72, 16);
		contentPane.add(lblName);
		
		JLabel lblMobilenumber = new JLabel("Mobile_Number");
		lblMobilenumber.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblMobilenumber.setBounds(12, 161, 151, 22);
		contentPane.add(lblMobilenumber);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblEmail.setBounds(12, 224, 81, 22);
		contentPane.add(lblEmail);
		
		JLabel lblFacebook = new JLabel("Facebook");
		lblFacebook.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblFacebook.setBounds(12, 290, 117, 22);
		contentPane.add(lblFacebook);
		
		name_txt = new JTextField();
		name_txt.setBackground(new Color(255, 218, 185));
		name_txt.setBounds(179, 107, 241, 22);
		contentPane.add(name_txt);
		name_txt.setColumns(10);
		
		mobile_num_txt = new JTextField();
		mobile_num_txt.setBackground(new Color(255, 218, 185));
		mobile_num_txt.setBounds(179, 163, 241, 22);
		contentPane.add(mobile_num_txt);
		mobile_num_txt.setColumns(10);
		
		email_txt = new JTextField();
		email_txt.setBackground(new Color(255, 218, 185));
		email_txt.setBounds(179, 226, 241, 22);
		contentPane.add(email_txt);
		email_txt.setColumns(10);
		
		faceBook_txt = new JTextField();
		faceBook_txt.setBackground(new Color(255, 218, 185));
		faceBook_txt.setBounds(179, 292, 241, 22);
		contentPane.add(faceBook_txt);
		faceBook_txt.setColumns(10);
		
		JButton save_btn = new JButton("Save");
		save_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String Name=name_txt.getText();
				String Mobile_number=mobile_num_txt.getText();
				String Email_id=email_txt.getText();
				String Facebook_id=faceBook_txt.getText();
				Manage manage=new Manage();
				manage.storeContact(Name, Mobile_number, Email_id, Facebook_id);
				
			}
		});
		save_btn.setBounds(352, 386, 97, 25);
		contentPane.add(save_btn);
	}
}
