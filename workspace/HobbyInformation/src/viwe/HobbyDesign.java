package viwe;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.LayoutManager;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import com.toedter.calendar.JDateChooser;

public class HobbyDesign extends JFrame {

	private JPanel layOut;
	private JTextField Name;
	private JCheckBox Ho1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HobbyDesign frame = new HobbyDesign();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public HobbyDesign() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 510, 510);
		layOut = new JPanel();
		layOut.setBackground(new Color(72, 61, 139));
		layOut.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(layOut);
		layOut.setLayout(null);
		
		JLabel lblHobbyInformation = new JLabel("Hobby Information");
		lblHobbyInformation.setFont(new Font("Nirmala UI", Font.BOLD | Font.ITALIC, 18));
		lblHobbyInformation.setBackground(new Color(0, 0, 139));
		lblHobbyInformation.setBounds(140, 29, 196, 34);
		layOut.add(lblHobbyInformation);
		
		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Nirmala UI", Font.BOLD | Font.ITALIC, 18));
		lblName.setBackground(new Color(255, 215, 0));
		lblName.setBounds(24, 109, 91, 34);
		layOut.add(lblName);
		
		Ho1 = new JCheckBox("Football");
		Ho1.setBackground(new Color(255, 255, 0));
		Ho1.setBounds(112, 176, 113, 25);
		layOut.add(Ho1);
		
		JCheckBox ho3 = new JCheckBox("Programming");
		ho3.setBackground(new Color(255, 255, 0));
		ho3.setBounds(112, 219, 113, 25);
		layOut.add(ho3);
		
		JCheckBox ho2 = new JCheckBox("Pc gamming");
		ho2.setBackground(new Color(255, 255, 0));
		ho2.setBounds(255, 176, 113, 25);
		layOut.add(ho2);
		
		JCheckBox ho4 = new JCheckBox("Desinger");
		ho4.setBackground(new Color(255, 255, 0));
		ho4.setBounds(255, 219, 113, 25);
		layOut.add(ho4);
		
		JButton save_btn = new JButton("Save");
		save_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ArrayList<String>hob=new ArrayList<>();
				
				if(Ho1.isSelected()){
				 hob.add(Ho1.getText());
					
				}
				if(ho2.isSelected()){
					hob.add(ho2.getText());
				}
				if(ho3.isSelected()){
					 hob.add(ho4.getText());
				}
			}
		});
		save_btn.setBackground(new Color(0, 191, 255));
		save_btn.setBounds(325, 318, 97, 25);
		layOut.add(save_btn);
		
		Name = new JTextField();
		Name.setBackground(new Color(154, 205, 50));
		Name.setBounds(112, 118, 218, 22);
		layOut.add(Name);
		Name.setColumns(10);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(126, 296, 142, 25);
		layOut.add(dateChooser);
		}
}
