package file_read;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

//FileReader inputFile=new FileReader("C:/Users/Nk chakraborty/Desktop/Input.txt");

public class FileRead {

	public static void main(String[] args) {
		File inputfile = new File("C:/Users/Nk chakraborty/Desktop/Input.txt");
		File outputFile = new File("C:/Users/Nk chakraborty/Desktop/Output.txt");
		try {

			FileReader fileReader = new FileReader(inputfile);

			try {
				FileWriter fileWriter = new FileWriter(outputFile);
				Scanner scanner = new Scanner(System.in);
				while (scanner.hasNextLine()) {
					String tempString = scanner.nextLine();
					// System.out.println(tempString);
					fileWriter.write(tempString + "\n");
				}
				fileReader.close();
				fileWriter.close();
				scanner.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {

			e.printStackTrace();
			System.out.println("file not found!");
		}

	}

}
