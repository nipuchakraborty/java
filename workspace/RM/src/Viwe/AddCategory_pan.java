package Viwe;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;

public class AddCategory_pan extends JPanel {
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Create the panel.
	 */
	public AddCategory_pan() {
		setLayout(null);
		
		JLabel lblProductCategory = new JLabel("Product Category");
		lblProductCategory.setBounds(87, 160, 98, 16);
		add(lblProductCategory);
		
		textField = new JTextField();
		textField.setBounds(213, 81, 202, 22);
		add(textField);
		textField.setColumns(10);
		
		JLabel lblAddProductName = new JLabel("Add Product name");
		lblAddProductName.setBounds(87, 84, 105, 16);
		add(lblAddProductName);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(213, 157, 202, 22);
		add(comboBox);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.setBounds(394, 376, 97, 25);
		add(btnNewButton);
		
		JLabel lblAmountOfProduct = new JLabel("Amount of Product");
		lblAmountOfProduct.setBounds(87, 255, 106, 16);
		add(lblAmountOfProduct);
		
		textField_1 = new JTextField();
		textField_1.setBounds(213, 252, 202, 22);
		add(textField_1);
		textField_1.setColumns(10);

	}
}
