

import java.awt.List;
import java.util.ArrayList;

public class ArrayList_to_string {

	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		list.add("one");
		list.add("two");
		list.add("three");

		String listString = "";

		for (String s : list)
		{
		    listString += s + "\t";
		}

		System.out.println(listString);
		
	/*	ArrayList<String> list = new ArrayList<String>();
		list.add("one");
		list.add("two");
		list.add("three");

		StringBuilder sb = new StringBuilder();
		for (String s : list)
		{
		    sb.append(s);
		    sb.append("\t");
		}

		System.out.println(sb.toString());
*/
		/*ArrayList<String> list = new ArrayList<String>();
		list.add("Item 1");
		list.add("Item 2");
		String joined = TextUtils.join(", ", list);*/
		
	}

}
