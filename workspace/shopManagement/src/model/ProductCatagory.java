package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import controller.AddProductCatagory;
import net.proteanit.sql.DbUtils;

import view.allpanel.AddProductCatagoryPanel;

public class ProductCatagory {
	
	public String name;
	public int status;
	public void prepareToInsert(String catagoryName,int status) {
		
		this.name=catagoryName;
		this.status=status;
		
	}
	
	public void store() {
		
		Connection conn=DatabaseConnector.getconnection();
		
		String sql="INSERT INTO `product_catagory`(`name`, `status`) VALUES (?,?)";
		
		try {
			
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setString(1, name);
			ps.setInt(2, status);
			ps.execute();
			
		} catch (Exception e) {
			
		}
		
	}

	public static void load() {
		Connection conn=DatabaseConnector.getconnection();
		
		String sql="select * from product_catagory";
		
		try {
			
			PreparedStatement ps=conn.prepareStatement(sql);
			ResultSet rs=ps.executeQuery();
			AddProductCatagoryPanel.table.setModel(DbUtils.resultSetToTableModel(rs));
			
		} catch (Exception e) {
			
		}
		
	}
}
