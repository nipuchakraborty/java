package model;

import java.util.ArrayList;

public class StatusComboBox {

	public static ArrayList<String> getStatus(){
		
		ArrayList<String> status=new ArrayList<>();
		
		status.add("select one");
		status.add("available");
		status.add("not available");
		
		return status;
		
	}
}
