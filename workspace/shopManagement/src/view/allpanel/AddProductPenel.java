package view.allpanel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;

public class AddProductPenel extends JPanel {
	private JTextField textField;

	/**
	 * Create the panel.
	 */
	public AddProductPenel() {
		setLayout(new MigLayout("", "[][][][][][][grow]", "[][]"));
		
		JLabel label = new JLabel("");
		add(label, "flowx,cell 0 0");
		
		JLabel lblProductName = new JLabel("Product name");
		add(lblProductName, "cell 0 0");
		
		textField = new JTextField();
		add(textField, "cell 6 0,growx");
		textField.setColumns(10);
		
		JLabel lblUnit = new JLabel("unit");
		add(lblUnit, "cell 0 1");
		
		JComboBox comboBox = new JComboBox();
		add(comboBox, "cell 6 1,growx");

	}

}
