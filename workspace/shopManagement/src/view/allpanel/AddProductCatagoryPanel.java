package view.allpanel;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.AddProductCatagory;
import model.ProductCatagory;
import model.StatusComboBox;

import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class AddProductCatagoryPanel extends JPanel {
	private JTextField name_txt;
	public static JTable table;

	public AddProductCatagoryPanel() {
		setLayout(new MigLayout("", "[grow][][][][][][grow]", "[][][][grow]"));
		
		JLabel lblC = new JLabel("Catagory name");
		add(lblC, "cell 1 0");
		
		name_txt = new JTextField();
		add(name_txt, "cell 6 0,growx");
		name_txt.setColumns(10);
		
		JLabel label = new JLabel("");
		add(label, "flowx,cell 1 1");
		
		JLabel lblStatus = new JLabel("Status");
		add(lblStatus, "cell 1 1");
		
		JComboBox status_comboBox = new JComboBox();
		add(status_comboBox, "cell 6 1,growx");
		
		JButton btnSave = new JButton("save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String catagoryName=name_txt.getText();
				String status=status_comboBox.getSelectedItem().toString();
				
				if(status.equals("select one")){
					JOptionPane.showMessageDialog(null, "Can't work...");
				}
				else{
					new AddProductCatagory(catagoryName,status);
					ProductCatagory productCatagory=new ProductCatagory();
//					productCatagory.store();
					productCatagory.load();
				
				}
			}
		});
	
		add(btnSave, "cell 6 2");
		ArrayList<String> status=StatusComboBox.getStatus();
		status_comboBox.setModel(new DefaultComboBoxModel<>(status.toArray()));
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, "cell 0 3 7 1,grow");
		
		table = new JTable();
		ProductCatagory.load();
		scrollPane.setViewportView(table);

	}

}
