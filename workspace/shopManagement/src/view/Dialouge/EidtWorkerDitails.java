package view.Dialouge;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;

import javax.swing.JLabel;
import java.awt.Font;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;

public class EidtWorkerDitails extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			EidtWorkerDitails dialog = new EidtWorkerDitails();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public EidtWorkerDitails() {
		setTitle("Edit");
		getContentPane().setBackground(new Color(169, 169, 169));
		setBounds(100, 100, 816, 405);
		getContentPane().setLayout(null);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(0, 323, 798, 35);
			buttonPane.setBackground(new Color(128, 0, 0));
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane);
			buttonPane.add(contentPanel);
			contentPanel.setBackground(new Color(0, 0, 139));
			contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
			contentPanel.setLayout(null);
			
			JLabel label = new JLabel("Worker Id");
			label.setForeground(Color.WHITE);
			label.setFont(new Font("Tahoma", Font.ITALIC, 18));
			label.setBounds(12, 13, 119, 22);
			contentPanel.add(label);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		
		JLabel label = new JLabel("Worker Id");
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Tahoma", Font.ITALIC, 18));
		label.setBounds(28, 13, 119, 22);
		getContentPane().add(label);
		
		JLabel label_1 = new JLabel("Name");
		label_1.setForeground(Color.WHITE);
		label_1.setFont(new Font("Tahoma", Font.ITALIC, 18));
		label_1.setBounds(28, 59, 119, 22);
		getContentPane().add(label_1);
		
		textField = new JTextField();
		textField.setText("00");
		textField.setColumns(10);
		textField.setBounds(174, 13, 66, 28);
		getContentPane().add(textField);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(174, 58, 128, 28);
		getContentPane().add(textField_1);
		
		JLabel label_2 = new JLabel("Date");
		label_2.setForeground(Color.WHITE);
		label_2.setFont(new Font("Tahoma", Font.ITALIC, 18));
		label_2.setBounds(358, 13, 119, 22);
		getContentPane().add(label_2);
		JDateChooser datePicker=new JDateChooser();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	    java.util.Date date=new java.util.Date();
	    datePicker.setBounds(417, 13, 138, 22);
		getContentPane().add(datePicker);
		datePicker.setDate(date);
		
		JLabel label_3 = new JLabel("Post");
		label_3.setForeground(Color.WHITE);
		label_3.setFont(new Font("Tahoma", Font.ITALIC, 18));
		label_3.setBounds(358, 62, 119, 22);
		getContentPane().add(label_3);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(414, 59, 141, 22);
		getContentPane().add(comboBox);
		
		JRadioButton radioButton = new JRadioButton("Female");
		radioButton.setForeground(Color.WHITE);
		radioButton.setBounds(593, 15, 73, 23);
		getContentPane().add(radioButton);
		
		JRadioButton radioButton_1 = new JRadioButton("Male");
		radioButton_1.setForeground(Color.WHITE);
		radioButton_1.setBounds(593, 58, 73, 23);
		getContentPane().add(radioButton_1);
		
		JCheckBox checkBox = new JCheckBox("Absent");
		checkBox.setForeground(Color.WHITE);
		checkBox.setBounds(677, 15, 70, 23);
		getContentPane().add(checkBox);
		
		JCheckBox checkBox_1 = new JCheckBox("Present");
		checkBox_1.setForeground(Color.WHITE);
		checkBox_1.setBounds(677, 58, 70, 23);
		getContentPane().add(checkBox_1);
	}
}
