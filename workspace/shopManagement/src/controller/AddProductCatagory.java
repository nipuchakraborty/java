package controller;

import model.ProductCatagory;

public class AddProductCatagory {
	
	public AddProductCatagory(String catagoryName,String status){
		int st;
		if(status.equals("available")){
			st=0;
		}
		else{
			st=1;
		}
		ProductCatagory productCatagory=new ProductCatagory();
		productCatagory.prepareToInsert(catagoryName, st);
		productCatagory.store();
		
	}

}
