/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gui.nk;

import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import net.miginfocom.swing.MigLayout;
import javax.swing.JInternalFrame;
import javax.swing.JSeparator;
import javax.swing.JLayeredPane;
import java.awt.BorderLayout;
import javax.swing.JTree;
import javax.swing.JList;
import javax.swing.border.MatteBorder;
import javax.swing.JTabbedPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;


public class Home extends javax.swing.JFrame {

   
    public Home() {
    	getContentPane().setBackground(new Color(255, 255, 255));
        initComponents();
        
    }
    static boolean maximized= true;
    static  boolean  minimaze=true;

   
   
    static Point mouseDownScreenCoords;
	static Point mouseDownCompCoords;
	
    private void initComponents() {
    	mouseDownScreenCoords = null;
        mouseDownCompCoords = null;
        jPanel1 = new javax.swing.JPanel();
        MaxL = new javax.swing.JLabel();
        cancelL = new javax.swing.JLabel();
        cancelL1 = new javax.swing.JLabel();
        setFont(new Font("Sylfaen", Font.BOLD | Font.ITALIC, 18));
		setLocation(new Point(200, 50));
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocationByPlatform(true);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));

        MaxL.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Cancel_48px.png"));
        MaxL.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MaxLMouseClicked(evt);
            }
        });

        cancelL.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Maximize_Window_48px.png")); // NOI18N
        cancelL.setText("jLabel1");
        cancelL.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancelLMouseClicked(evt);
            }
        });

        cancelL1.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Minus_48px.png")); // NOI18N
        cancelL1.setText("jLabel1");
        cancelL1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancelL1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1Layout.setHorizontalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.TRAILING)
        		.addGroup(jPanel1Layout.createSequentialGroup()
        			.addContainerGap(753, Short.MAX_VALUE)
        			.addComponent(cancelL1, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(cancelL, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
        			.addGap(2)
        			.addComponent(MaxL, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(jPanel1Layout.createSequentialGroup()
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
        				.addComponent(MaxL, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
        				.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        					.addComponent(cancelL, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
        					.addComponent(cancelL1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
        			.addContainerGap(70, Short.MAX_VALUE))
        );
        jPanel1.setLayout(jPanel1Layout);
        
        JPanel panel = new JPanel();
        panel.setBackground(new Color(255, 255, 255));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        layout.setHorizontalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 1200, Short.MAX_VALUE)
        		.addGroup(layout.createSequentialGroup()
        			.addGap(6)
        			.addComponent(panel, GroupLayout.DEFAULT_SIZE, 720, Short.MAX_VALUE)
        			.addGap(6))
        );
        layout.setVerticalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(panel, GroupLayout.DEFAULT_SIZE, 574, Short.MAX_VALUE))
        );
        panel.setLayout(new MigLayout("", "[1182px,grow]", "[88px][grow]"));
        
        panel_1 = new JPanel();
        panel_1.setBackground(new Color(255, 255, 255));
        panel_1.setBorder(new MatteBorder(0, 0, 2, 0, (Color) new Color(220, 20, 60)));
        panel.add(panel_1, "cell 0 0,grow");
        panel_1.setLayout(null);
        
        btnNewButton = new JButton("New button");
        btnNewButton.setBounds(6, 6, 253, 81);
        panel_1.add(btnNewButton);
        
        button = new JButton("Facebook");
        button.setBounds(271, 11, 253, 76);
        panel_1.add(button);
        
        button_1 = new JButton("Facebook");
        button_1.setBounds(543, 6, 267, 76);
        panel_1.add(button_1);
        
        button_2 = new JButton("Facebook");
        button_2.setBounds(842, 6, 332, 76);
        panel_1.add(button_2);
        
        tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        panel.add(tabbedPane, "cell 0 1,grow");
        
        panel_2 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_2, null);
        panel_2.setLayout(null);
        
        panel_3 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_3, null);
        
        JPanel panel_4 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_4, null);
        
        JPanel panel_5 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_5, null);
        
        JPanel panel_6 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_6, null);
        
        JPanel panel_7 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_7, null);
        
        JPanel panel_8 = new JPanel();
        panel_8.setFont(new Font("SansSerif", Font.PLAIN, 27));
        tabbedPane.addTab("New tab", null, panel_8, null);
        
        JPanel panel_9 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_9, null);
        
        JPanel panel_10 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_10, null);
        
        JPanel panel_11 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_11, null);
        
        JPanel panel_12 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_12, null);
        
        JPanel panel_13 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_13, null);
        
        JPanel panel_14 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_14, null);
        
        JPanel panel_15 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_15, null);
        
        JPanel panel_16 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_16, null);
        
        JPanel panel_17 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_17, null);
        
        JPanel panel_18 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_18, null);
        
        JPanel panel_19 = new JPanel();
        tabbedPane.addTab("New tab", null, panel_19, null);
        getContentPane().setLayout(layout);
        addMouseListener(new MouseListener(){
            public void mouseReleased(MouseEvent e) {
            	
                mouseDownScreenCoords = null;
                mouseDownCompCoords = null;
            }
            public void mousePressed(MouseEvent e) {
                mouseDownScreenCoords = e.getLocationOnScreen();
                mouseDownCompCoords = e.getPoint();
            }
            public void mouseExited(MouseEvent e) {
            }
            public void mouseEntered(MouseEvent e) {
            }
            public void mouseClicked(MouseEvent e) {
            	
            }
        });
        addMouseMotionListener(new MouseMotionListener(){
            public void mouseMoved(MouseEvent e) {
            }
            public void mouseDragged(MouseEvent e) {
                Point currCoords = e.getLocationOnScreen();
                setLocation(mouseDownScreenCoords.x + (currCoords.x - mouseDownScreenCoords.x) - mouseDownCompCoords.x,
                              mouseDownScreenCoords.y + (currCoords.y - mouseDownScreenCoords.y) - mouseDownCompCoords.y);
            }
        });

        pack();
    }
    
    

    private void MaxLMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MaxLMouseClicked
       Home .this.dispose();
    }//GEN-LAST:event_MaxLMouseClicked

    private void cancelLMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancelLMouseClicked
        if(maximized){
            Home.this.setExtendedState(JFrame.MAXIMIZED_BOTH);
            GraphicsEnvironment eve=GraphicsEnvironment.getLocalGraphicsEnvironment();
            Home.this.setMaximizedBounds(eve.getMaximumWindowBounds());
            
            maximized=false;
        }
        else{
            setExtendedState(JFrame.NORMAL);
        maximized=true;
        }
    }//GEN-LAST:event_cancelLMouseClicked

    private void cancelL1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancelL1MouseClicked
        setExtendedState(ICONIFIED);
    }
    public static void main(String args[]) {
       
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Home().setVisible(true);
            }
        });
        
      
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel MaxL;
    private javax.swing.JLabel cancelL;
    private javax.swing.JLabel cancelL1;
    private javax.swing.JPanel jPanel1;
    private JPanel panel_1;
    private JButton btnNewButton;
    private JButton button;
    private JButton button_1;
    private JButton button_2;
    private JTabbedPane tabbedPane;
    private JPanel panel_2;
    private JPanel panel_3;
}
