/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gui.nk;

import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;


public class WorkerInfo extends javax.swing.JFrame {

   
    public WorkerInfo() {
    	getContentPane().setBackground(new Color(255, 228, 225));
        initComponents();
        
    }
    static boolean maximized= true;
    static  boolean  minimaze=true;

   
   
    static Point mouseDownScreenCoords;
	static Point mouseDownCompCoords;
	
    private void initComponents() {
    	mouseDownScreenCoords = null;
        mouseDownCompCoords = null;
        jPanel1 = new javax.swing.JPanel();
        MaxL = new javax.swing.JLabel();
        cancelL = new javax.swing.JLabel();
        cancelL1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocationByPlatform(true);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));

        MaxL.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Cancel_48px.png"));
        MaxL.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MaxLMouseClicked(evt);
            }
        });

        cancelL.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Maximize_Window_48px.png")); // NOI18N
        cancelL.setText("jLabel1");
        cancelL.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancelLMouseClicked(evt);
            }
        });

        cancelL1.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Minus_48px.png")); // NOI18N
        cancelL1.setText("jLabel1");
        cancelL1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancelL1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1Layout.setHorizontalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.TRAILING)
        		.addGroup(jPanel1Layout.createSequentialGroup()
        			.addContainerGap(753, Short.MAX_VALUE)
        			.addComponent(cancelL1, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(cancelL, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
        			.addGap(2)
        			.addComponent(MaxL, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(jPanel1Layout.createSequentialGroup()
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
        				.addComponent(MaxL, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
        				.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        					.addComponent(cancelL, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
        					.addComponent(cancelL1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
        			.addContainerGap(70, Short.MAX_VALUE))
        );
        jPanel1.setLayout(jPanel1Layout);
        
        JLabel label = new JLabel("");
        label.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Home_100px.png"));
        
        JLabel lblHome = new JLabel("Home");
        lblHome.setFont(new Font("Sitka Text", Font.BOLD, 20));
        
        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Report_Card_100px_1.png"));
        
        JLabel lblReports = new JLabel("Reports");
        lblReports.setFont(new Font("Sitka Text", Font.BOLD, 20));
        
        JLabel lblNewLabel_1 = new JLabel("");
        lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_User_Groups_100px.png"));
        
        JLabel lblWorkersInfo = new JLabel("Workers Info.");
        lblWorkersInfo.setFont(new Font("Sitka Text", Font.BOLD, 20));
        
        JLabel lblNewLabel_2 = new JLabel("");
        lblNewLabel_2.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Calculator_100px.png"));
        
        JLabel lblCalculator = new JLabel("CalCulator");
        lblCalculator.setFont(new Font("Sitka Text", Font.BOLD, 20));
        
        JLabel lblNewLabel_3 = new JLabel("");
        lblNewLabel_3.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Add_List_96px.png"));
        
        JLabel lblAdd = new JLabel("Add ");
        lblAdd.setFont(new Font("Sitka Text", Font.BOLD, 20));
        
        JLabel lblNewLabel_4 = new JLabel("");
        lblNewLabel_4.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Settings_100px.png"));
        
        JLabel lblSetting = new JLabel("Setting");
        lblSetting.setFont(new Font("Sitka Text", Font.BOLD, 20));
        
        JLabel lblNewLabel_5 = new JLabel("");
        lblNewLabel_5.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_About_100px.png"));
        
        JLabel lblAbout = new JLabel("About");
        lblAbout.setFont(new Font("Sitka Text", Font.BOLD, 20));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        layout.setHorizontalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addGroup(layout.createParallelGroup(Alignment.LEADING)
        				.addGroup(layout.createSequentialGroup()
        					.addContainerGap()
        					.addComponent(label, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE))
        				.addGroup(layout.createSequentialGroup()
        					.addGap(36)
        					.addComponent(lblHome)))
        			.addGap(28)
        			.addGroup(layout.createParallelGroup(Alignment.LEADING, false)
        				.addGroup(layout.createSequentialGroup()
        					.addGap(10)
        					.addComponent(lblReports, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        				.addComponent(lblNewLabel))
        			.addGroup(layout.createParallelGroup(Alignment.LEADING)
        				.addGroup(layout.createSequentialGroup()
        					.addGap(43)
        					.addComponent(lblNewLabel_1))
        				.addGroup(layout.createSequentialGroup()
        					.addGap(35)
        					.addComponent(lblWorkersInfo)))
        			.addGap(31)
        			.addGroup(layout.createParallelGroup(Alignment.TRAILING)
        				.addGroup(Alignment.LEADING, layout.createSequentialGroup()
        					.addComponent(lblNewLabel_2)
        					.addGap(64)
        					.addComponent(lblNewLabel_3))
        				.addGroup(Alignment.LEADING, layout.createSequentialGroup()
        					.addComponent(lblCalculator, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
        					.addGap(79)
        					.addComponent(lblAdd, GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)))
        			.addGap(42)
        			.addGroup(layout.createParallelGroup(Alignment.TRAILING)
        				.addComponent(lblNewLabel_4, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
        				.addComponent(lblSetting, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
        			.addGap(80)
        			.addGroup(layout.createParallelGroup(Alignment.LEADING)
        				.addGroup(layout.createSequentialGroup()
        					.addGap(10)
        					.addComponent(lblAbout, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
        				.addComponent(lblNewLabel_5, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)))
        		.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 1323, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(Alignment.TRAILING, layout.createSequentialGroup()
        			.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED, 385, Short.MAX_VALUE)
        			.addGroup(layout.createParallelGroup(Alignment.LEADING)
        				.addGroup(Alignment.TRAILING, layout.createSequentialGroup()
        					.addGroup(layout.createParallelGroup(Alignment.TRAILING)
        						.addComponent(label, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
        						.addComponent(lblNewLabel)
        						.addComponent(lblNewLabel_1)
        						.addGroup(layout.createParallelGroup(Alignment.LEADING)
        							.addComponent(lblNewLabel_2)
        							.addComponent(lblNewLabel_3)))
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addGroup(layout.createParallelGroup(Alignment.LEADING)
        						.addComponent(lblHome)
        						.addComponent(lblReports, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
        						.addComponent(lblWorkersInfo, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
        						.addGroup(layout.createParallelGroup(Alignment.BASELINE)
        							.addComponent(lblCalculator, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
        							.addComponent(lblAdd, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)))
        					.addGap(26))
        				.addGroup(Alignment.TRAILING, layout.createSequentialGroup()
        					.addGroup(layout.createParallelGroup(Alignment.LEADING)
        						.addGroup(layout.createSequentialGroup()
        							.addComponent(lblNewLabel_5, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
        							.addPreferredGap(ComponentPlacement.RELATED)
        							.addComponent(lblAbout, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
        						.addGroup(layout.createSequentialGroup()
        							.addComponent(lblNewLabel_4)
        							.addGap(10)
        							.addComponent(lblSetting, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)))
        					.addGap(22))))
        );
        getContentPane().setLayout(layout);
        addMouseListener(new MouseListener(){
            public void mouseReleased(MouseEvent e) {
            	
                mouseDownScreenCoords = null;
                mouseDownCompCoords = null;
            }
            public void mousePressed(MouseEvent e) {
                mouseDownScreenCoords = e.getLocationOnScreen();
                mouseDownCompCoords = e.getPoint();
            }
            public void mouseExited(MouseEvent e) {
            }
            public void mouseEntered(MouseEvent e) {
            }
            public void mouseClicked(MouseEvent e) {
            	
            }
        });
        addMouseMotionListener(new MouseMotionListener(){
            public void mouseMoved(MouseEvent e) {
            }
            public void mouseDragged(MouseEvent e) {
                Point currCoords = e.getLocationOnScreen();
                setLocation(mouseDownScreenCoords.x + (currCoords.x - mouseDownScreenCoords.x) - mouseDownCompCoords.x,
                              mouseDownScreenCoords.y + (currCoords.y - mouseDownScreenCoords.y) - mouseDownCompCoords.y);
            }
        });

        pack();
    }
    
    

    private void MaxLMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MaxLMouseClicked
        System.exit(0);
    }//GEN-LAST:event_MaxLMouseClicked

    private void cancelLMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancelLMouseClicked
        if(maximized){
            WorkerInfo.this.setExtendedState(JFrame.MAXIMIZED_BOTH);
            GraphicsEnvironment eve=GraphicsEnvironment.getLocalGraphicsEnvironment();
            WorkerInfo.this.setMaximizedBounds(eve.getMaximumWindowBounds());
            
            maximized=false;
        }
        else{
            setExtendedState(JFrame.NORMAL);
        maximized=true;
        }
    }//GEN-LAST:event_cancelLMouseClicked

    private void cancelL1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancelL1MouseClicked
        setExtendedState(ICONIFIED);
    }
    public static void main(String args[]) {
       
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(WorkerInfo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(WorkerInfo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(WorkerInfo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(WorkerInfo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new WorkerInfo().setVisible(true);
            }
        });
        
      
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel MaxL;
    private javax.swing.JLabel cancelL;
    private javax.swing.JLabel cancelL1;
    private javax.swing.JPanel jPanel1;
}
