package findDay;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import com.toedter.calendar.JDayChooser;
import org.jdesktop.swingx.JXDatePicker;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.awt.event.ActionEvent;

public class Day extends JFrame {
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Day frame = new Day();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Day() {
		getContentPane().setBackground(new Color(0, 0, 51));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 352, 300);
		getContentPane().setLayout(null);
		
		JLabel lblDate = new JLabel("Date");
		lblDate.setForeground(Color.WHITE);
		lblDate.setBounds(48, 141, 70, 24);
		getContentPane().add(lblDate);
		
		JXDatePicker datePicker = new JXDatePicker();
		datePicker.setBounds(145, 142, 114, 22);
		getContentPane().add(datePicker);
		
		JButton btnShow = new JButton("show");
		btnShow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Date date=datePicker.getDate();
				DateFormat cldate=new SimpleDateFormat("yyyy/MM/dd");
				String mainDate=cldate.format(date);
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat formatter = new SimpleDateFormat("EEE");
				String text = formatter.format(cal.getTime());
				
				textField.setText(text+" day."+mainDate);
				
			}
		});
		btnShow.setBackground(Color.BLACK);
		btnShow.setForeground(Color.WHITE);
		btnShow.setBounds(145, 175, 89, 23);
		getContentPane().add(btnShow);
		
		textField = new JTextField();
		textField.setBounds(48, 24, 211, 92);
		getContentPane().add(textField);
		textField.setColumns(10);
	}
}
