package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Model.manage;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Jurnal extends JFrame {

	private JPanel contentPane;
	private JTextField account_name_txt;
	private JTextField debit;
	private JTextField cardit;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Jurnal frame = new Jurnal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Jurnal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 410);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAcountInformation = new JLabel("Acount Information");
		lblAcountInformation.setBounds(129, 35, 122, 16);
		contentPane.add(lblAcountInformation);
		
		JLabel lblAccountName = new JLabel("Account name");
		lblAccountName.setBounds(25, 95, 122, 16);
		contentPane.add(lblAccountName);
		
		account_name_txt = new JTextField();
		account_name_txt.setBounds(144, 92, 187, 22);
		contentPane.add(account_name_txt);
		account_name_txt.setColumns(10);
		
		JLabel lblDebit = new JLabel("Debit");
		lblDebit.setBounds(28, 168, 80, 16);
		contentPane.add(lblDebit);
		
		debit = new JTextField();
		debit.setText("0");
		debit.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				cardit.setEnabled(false);
			}
		});
		debit.setColumns(10);
		debit.setBounds(144, 165, 187, 22);
		contentPane.add(debit);
		
		JLabel lblCradit = new JLabel("Cradit");
		lblCradit.setBounds(28, 246, 80, 16);
		contentPane.add(lblCradit);
		
		cardit = new JTextField();
		cardit.setText("0");
		cardit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				debit.setEnabled(false);
			}
		});
		cardit.setColumns(10);
		cardit.setBounds(144, 243, 187, 22);
		contentPane.add(cardit);
		
		JButton save = new JButton("Save");
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String ac_name=account_name_txt.getText();
				String debit_text=debit.getText();
				String cradit_text=cardit.getText();
				manage managedata=new manage();
			
				managedata.inpting_method(ac_name, debit_text, cradit_text);
				
				
			}
		});
		save.setBounds(305, 305, 97, 25);
		contentPane.add(save);
	}
}
