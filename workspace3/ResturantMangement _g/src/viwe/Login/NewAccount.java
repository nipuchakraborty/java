package viwe.Login;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;

import java.awt.Font;
import java.awt.Image;

import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.border.EtchedBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel;

import java.awt.Window.Type;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.Dialog.ModalExclusionType;
import javax.swing.border.LineBorder;

public class NewAccount extends JFrame {

	private JPanel contentPane;
	private JTextField un;
	private JTextField fn;
	private JPasswordField p;
	private JPasswordField cp;
	private JButton image_btn;
	public JComboBox cB ;
	public JLabel picL;
	public JLabel lblUserName;
	public JLabel lblFullName;

	public JLabel lblPostion ;
	public JLabel lblPassword;
	private JButton btnSave;
	public String path ;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					NewAccount frame = new NewAccount();
					//UIManager.setLookAndFeel(new SyntheticaAluOxideLookAndFeel());
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public NewAccount() {
		setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		setType(Type.UTILITY);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 703, 442);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 139));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		lblUserName = new JLabel("User Name");
		lblUserName.setForeground(new Color(255, 255, 255));
		lblUserName.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblUserName.setBounds(24, 65, 97, 24);
		contentPane.add(lblUserName);
		
		un = new JTextField();
		un.setFont(new Font("SansSerif", Font.PLAIN, 18));
		un.setBounds(217, 68, 165, 31);
		contentPane.add(un);
		un.setColumns(10);
		
		lblFullName = new JLabel("Full Name");
		lblFullName.setForeground(new Color(255, 255, 255));
		lblFullName.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblFullName.setBounds(24, 29, 88, 24);
		contentPane.add(lblFullName);
		
		fn = new JTextField();
		fn.setFont(new Font("SansSerif", Font.PLAIN, 18));
		fn.setBounds(220, 27, 162, 27);
		contentPane.add(fn);
		fn.setColumns(10);
		
		lblPassword = new JLabel("Password");
		lblPassword.setForeground(new Color(255, 255, 255));
		lblPassword.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblPassword.setBounds(24, 111, 97, 24);
		contentPane.add(lblPassword);
		
		p = new JPasswordField();
		p.setBounds(217, 111, 165, 28);
		contentPane.add(p);
		
		JLabel lblConfirmPassword = new JLabel("Confirm Password");
		lblConfirmPassword.setForeground(new Color(255, 255, 255));
		lblConfirmPassword.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblConfirmPassword.setBounds(24, 162, 161, 24);
		contentPane.add(lblConfirmPassword);
		
		cp = new JPasswordField();
		
		cp.setBackground(Color.WHITE);
		cp.setBounds(217, 162, 165, 28);
		contentPane.add(cp);
		
		JLabel lblPostion = new JLabel("Postion");
		lblPostion.setForeground(new Color(255, 255, 255));
		lblPostion.setFont(new Font("SansSerif", Font.BOLD, 18));
		lblPostion.setBounds(24, 237, 66, 24);
		contentPane.add(lblPostion);
		
		cB = new JComboBox();
		cB.setModel(new DefaultComboBoxModel(new String[] {"Select One", "Manager", "Accountent", "Worker"}));
		cB.setBounds(217, 238, 165, 26);
		contentPane.add(cB);
		
		picL = new JLabel("picture");
		picL.setRequestFocusEnabled(false);
		picL.setForeground(new Color(255, 255, 255));
		picL.setHorizontalAlignment(JLabel.CENTER);
		picL.setBackground(new Color(255, 255, 255));
		picL.setBounds(500, 29, 144, 143);
		contentPane.add(picL);
		
		image_btn = new JButton("Browse");
		image_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser jf=new JFileChooser();
				jf.setCurrentDirectory(new File(System.getProperty("user.home")));
				FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images", "jpg", "gif", "png");
				jf.addChoosableFileFilter(filter);
				int result = jf.showSaveDialog(null);
				if (result == JFileChooser.APPROVE_OPTION) {
					File selectedFile =jf.getSelectedFile();
					path = selectedFile.getAbsolutePath();
					picL.setIcon(ResizeImage(path));
				}
				else if (result == JFileChooser.CANCEL_OPTION) {
					System.out.println("No File Select");
				}
				
			}
		});
		image_btn.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		image_btn.setFocusable(false);
		image_btn.setFocusTraversalPolicyProvider(true);
		image_btn.setBounds(529, 184, 90, 28);
		contentPane.add(image_btn);
		
		btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String fName=fn.getText();
				System.out.println(fName);
				String userName=un.getText();
				String pass=p.getText();
				String con_pas= cp.getText();
				String position=cB.getSelectedItem().toString();
				String pic=path;
				
				if(!pass.equals(con_pas)){
					cp.setBorder(new LineBorder(Color.RED, 1, true));
					cp.setText("password not match");
					cp.setForeground(Color.RED);
				}
				else{
					 pass=p.getText();
					con_pas= cp.getText();
				}
				
				
				if (fn.getText().equals(null)) {
					if (un.getText().equals(null)) {
						if (p.equals(null)) {
							if(cp.equals(null)){
								if (image_btn.getText().equals(null)) {
									if(cB.equals(null)){}
									JOptionPane.showMessageDialog(null, "Sorry cheak information");
								}
								
							}
						}
						
						
					}
					
				}
				new LoginAccountController( userName, userName, pass, con_pas,position ,pic);
			}
		});
		btnSave.setBounds(529, 329, 90, 28);
		contentPane.add(btnSave);
	}
	public ImageIcon ResizeImage(String ImagePath) {
		ImageIcon MyImage = new ImageIcon(ImagePath);
		Image img = MyImage.getImage();
		Image newImg = img.getScaledInstance(picL.getWidth(),picL.getHeight(), Image.SCALE_SMOOTH);
		ImageIcon image = new ImageIcon(newImg);
		return image;
	}
}
