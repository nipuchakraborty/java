package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import net.proteanit.sql.DbUtils;
import viwe.panel.AddFood;
import viwe.panel.Order;
//import viwe.panel.Order;

public  class TakeOrder {
	public String foodname;
	public int price;
	public int quntity;
	public String date;
	
	public void Store(String food,int price,int quntity,String date){
		this.foodname =food;
		this.price=price;
		this.quntity=quntity;
		this.date=date;
	}
	
	
	//////brakfasttable qurry
	
public static void breakFastDataLoad(){
	
	Connection connection=Database.getconnection();
	String qury="SELECT  `Name`, `Price`, `Category`, `Status` FROM `add_food_breakfast`";
	
	
	try {
		PreparedStatement pStatement=connection.prepareStatement(qury);
		ResultSet rSet=pStatement.executeQuery();
		
		
		AddFood.BreakfastTab.setModel(DbUtils.resultSetToTableModel(rSet));
		
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}


////view data 
public static void breakFastDataLoadView(){
	
	Connection connection=Database.getconnection();
	String qury="SELECT  `Name`, `Price`, `Category`, `Status` FROM `breakfast_view`";
	
	
	try {
		PreparedStatement pStatement=connection.prepareStatement(qury);
		ResultSet rSet=pStatement.executeQuery();
		
		
		AddFood.BreakfastTab.setModel(DbUtils.resultSetToTableModel(rSet));
		
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}
////////////////////////////////////////////////////////////////////////////////////







public static void DnnerDataLoad(){
	
	Connection connection=Database.getconnection();
	String qury="SELECT `Food Name`, `Price`, `Category`, `Status` FROM `addfooddinner` ";
	try {
		PreparedStatement pStatement=connection.prepareStatement(qury);
		ResultSet rSet=pStatement.executeQuery();
		AddFood.dinner_tab.setModel(DbUtils.resultSetToTableModel(rSet));
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
	
}


public static void DnnerDataLoad_view(){
	
	Connection connection=Database.getconnection();
	String qury="SELECT  `Food Name`, `Price`, `Category`, `Status` FROM `dinner_view` ";
	try {
		PreparedStatement pStatement=connection.prepareStatement(qury);
		ResultSet rSet=pStatement.executeQuery();
		AddFood.dinner_tab.setModel(DbUtils.resultSetToTableModel(rSet));
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
	
}





public static void EveningsnakcsDataLoad(){
	
	Connection connection=Database.getconnection();
	String qury="SELECT  `foodname`, `Price`, `Category`, `Status` FROM `addfoodeveningsnacks` ";
	try {
		PreparedStatement pStatement=connection.prepareStatement(qury);
		ResultSet rSet=pStatement.executeQuery();

		AddFood.even_Tab.setModel(DbUtils.resultSetToTableModel(rSet));
		
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}
public static void EveningsnakcsDataLoad_View(){
	
	Connection connection=Database.getconnection();
	String qury="SELECT  `foodname`, `Price`, `Category`, `Status` FROM `evening` ";
	try {
		PreparedStatement pStatement=connection.prepareStatement(qury);
		ResultSet rSet=pStatement.executeQuery();

		AddFood.even_Tab.setModel(DbUtils.resultSetToTableModel(rSet));
		
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}






public static void lunchDataLoad(){
	
	Connection connection=Database.getconnection();
	String qury="SELECT  `FoodName`, `Price`, `Category`, `Status` FROM `addfoodlunch`";
	try {
		PreparedStatement pStatement=connection.prepareStatement(qury);
		ResultSet rSet=pStatement.executeQuery();
		AddFood.lunch_tab.setModel(DbUtils.resultSetToTableModel(rSet));
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}
public static void lunchDataLoad_View(){
	
	Connection connection=Database.getconnection();
	String qury="SELECT `FoodName`, `Price`, `Category`, `Status` FROM `addfoodlunch`";
	try {
		PreparedStatement pStatement=connection.prepareStatement(qury);
		ResultSet rSet=pStatement.executeQuery();
		AddFood.lunch_tab.setModel(DbUtils.resultSetToTableModel(rSet));
		pStatement.execute();
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}
public static void trancate(){
	Connection conn=Database.getconnection();
	String q1="TRUNCATE addfoodeveningsnacks;";

 
 
 
 try {
	PreparedStatement pset=conn.prepareStatement(q1);
	
	ResultSet rSet=pset.executeQuery();
	
} catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}}
 public static void trancate2(){
		Connection conn=Database.getconnection();
		
		String q2 ="TRUNCATE addfooddinner;";

	 
	 
	 
	 try {
		
		PreparedStatement pset2=conn.prepareStatement(q2);
		
		
		boolean resultSet=pset2.execute();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
	
}
 public static void trancate3(){
		Connection conn=Database.getconnection();
	

	 String q3=" TRUNCATE addfoodlunch;";


	 
	 
	 
	 try {
		
		PreparedStatement pset3=conn.prepareStatement(q3);
		;
		
		ResultSet resultSet=pset3.executeQuery();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}
 
 
 
 
 ///Oreder List
 
 
 public static void breakFastOrder(){
		
		Connection connection=Database.getconnection();
		String qury="SELECT  `Name`, `Price`, `Category`, `Status` FROM `breakfast_view`";
		
		
		try {
			PreparedStatement pStatement=connection.prepareStatement(qury);
			ResultSet rSet=pStatement.executeQuery();
			
			
			Order.breakfast_table.setModel(DbUtils.resultSetToTableModel(rSet));
			
			pStatement.execute();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}
 public static void DnnerOrder(){
		
		Connection connection=Database.getconnection();
		String qury="SELECT  `Food Name`, `Price`, `Category`, `Status` FROM `dinner_view` ";
		try {
			PreparedStatement pStatement=connection.prepareStatement(qury);
			ResultSet rSet=pStatement.executeQuery();
			Order.dinner_table.setModel(DbUtils.resultSetToTableModel(rSet));
			pStatement.execute();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
	}
 public static void EveningsnakcsOrder(){
		
		Connection connection=Database.getconnection();
		String qury="SELECT  `foodname`, `Price`, `Category`, `Status` FROM `evening` ";
		try {
			PreparedStatement pStatement=connection.prepareStatement(qury);
			ResultSet rSet=pStatement.executeQuery();

			Order.snacks_table.setModel(DbUtils.resultSetToTableModel(rSet));
			
			pStatement.execute();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}
 public static void lunchOrder(){
		
		Connection connection=Database.getconnection();
		String qury="SELECT `FoodName`, `Price`, `Category`, `Status` FROM `addfoodlunch`";
		try {
			PreparedStatement pStatement=connection.prepareStatement(qury);
			ResultSet rSet=pStatement.executeQuery();
			Order.luch_table.setModel(DbUtils.resultSetToTableModel(rSet));
			pStatement.execute();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}
 
 }
