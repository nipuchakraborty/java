package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.experimental.theories.Theories;

import net.proteanit.sql.DbUtils;
import viwe.panel.AddWorker;

public class AddWorkerModel {
	public static String workerId ;
	public static String Name;
	public static String Post;
	public static String gender;
	public  void prepareAddWorker(String workerId ,String Name,String Post,String gender){
		this.workerId=workerId;
		this.Name=Name;
		this.Post=Post;
		this.gender=gender;
		
	}
public void insertWorker(){
	Connection con=Database.getconnection();
	String q="INSERT INTO `addworker`(`workerId`, `Name`, `Post`, `Gender`) VALUES (?,?,?,?)";
	try {
		PreparedStatement statement=con.prepareStatement(q);
		statement.setString(1, this.workerId);
		statement.setString(2, this.Name);
		statement.setString(3, this.Post);
		statement.setString(4, this.gender);
		statement.execute();
		
	} catch (SQLException e) {
		
		e.printStackTrace();
	}
	
}
public static void load(){
	Connection con=Database.getconnection();
	String q="SELECT * FROM `addworker`";
	try {
		PreparedStatement statement=con.prepareStatement(q);
		ResultSet set=statement.executeQuery();
		AddWorker.WorkerDitails_Tab.setModel(DbUtils.resultSetToTableModel(set));
		statement.execute();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}

public static void Search( String Id){
	Connection con=Database.getconnection();
	String q="SELECT * FROM `addworker` WHERE  workerId=?";
	try {
		PreparedStatement statement=con.prepareStatement(q);
		statement.setString(1, Id);
		ResultSet set=statement.executeQuery();
		AddWorker.WorkerDitails_Tab.setModel(DbUtils.resultSetToTableModel(set));
		statement.execute();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
public static void RemoveWorker( String Id){
	Connection con=Database.getconnection();
	String q="DELETE FROM `addworker` WHERE `workerId`=?";
	try {
		PreparedStatement statement=con.prepareStatement(q);
		statement.setString(1, Id);
		
		
		statement.executeUpdate();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}

}
