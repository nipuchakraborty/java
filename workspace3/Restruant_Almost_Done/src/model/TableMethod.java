package model;

import javax.net.ssl.SSLEngineResult.Status;

public class TableMethod {

   
    private String name;
    private int price;
    private String category;
   
    private String status;
    private byte[] Image;
    private int catId;
    
    public TableMethod(){}
    
    public TableMethod( String Name, int price, String category, String status ,byte[] image, int CatID){
    
       
        this.name = Name;
        this.price = price;
        this.category = category;
        this.status=status;
        this.Image = image;
        this.catId = CatID;
       
    }
    
    
    
    public String getName(){
        return name;
    }
    
    public void setName(String Name){
        this.name = Name;
    }
    
    
    
    public int getPrice(){
        return price;
    }
    
    public void setPrice(int Price){
        this.price = Price;
    }
    public String category(){
        return category;
    }
    
    public void setcategory(String cat){
        this.category =cat;
    }
    public String status(){
        return status;
    }
    
    public void setStatus(String Status){
        this.status =Status;
    }
    
    public byte[] getMyImage(){
        return Image;
    }
    public int getCatID(){
        return catId;
    }
    
    public void setCatID(int CatID){
        this.catId = CatID;
    }
}