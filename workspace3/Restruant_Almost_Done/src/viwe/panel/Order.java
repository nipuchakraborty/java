package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PrinterJob;
import java.nio.channels.SelectableChannel;
import java.sql.Date;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import net.miginfocom.swing.MigLayout;
import viwe.Dialouge.ConfirmOrrde;

import javax.swing.JTabbedPane;
import java.awt.SystemColor;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.JScrollPane;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JCheckBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import model.MyQuery;
import model.TableMethod;
import model.TakeOrder;
import model.TempOrder;
import model.TheModel;

import org.jdesktop.swingx.JXDatePicker;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.border.LineBorder;
import com.toedter.calendar.JDateChooser;

import controller.TempOderCtrl;

import javax.swing.border.TitledBorder;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Order extends JInternalFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel label;
	private JDesktopPane desktopPane;

	int w = 750;
	int h = 400;
	private JInternalFrame jInternalFrame;
	private JDesktopPane desktopen;
	JPanel header, center, footer;
	Border border = new EtchedBorder();
	Font font = new Font("courier new", Font.PLAIN, 16);
	public static JTable dinner_table;
	public static JTable breakfast_table;
	public static JTable luch_table;
	public static JTable snacks_table;
	public JScrollPane luch_scrol;

	// Table variable
	public static String name;
	public static String price;
	public static String category;
	public static String status;
	public static String pic;
	public static JComboBox table_noCB;
	public static JCheckBox Ch_TAb;
	public static JTextField customername;
	public static JXDatePicker datePicker;
	public static JTable table;
	public static JLabel lblTableL;
	public static JLabel takaL;

	public Order() {
		setFrameIcon(new ImageIcon(Order.class.getResource("/pictureResource/images/orders.png")));
		
		setLocation(new Point(150, 100));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);

		setVisible(true);
		setDoubleBuffered(true);
		setMaximizable(true);

		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		setTitle("Oder Food");
		// Dimention Screen
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		// header Design
		header = new JPanel();
		header.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		header.setBackground(Color.BLUE);
		header.setPreferredSize(new Dimension(screen.width, 30));
		// center Design
		center = new JPanel();
		center.setPreferredSize(new Dimension(screen.width + 50, 50));
		// foter design
		footer = new JPanel();
		// footer.setBackground(Color.blue);
		footer.setBackground(new Color(155, 200, 200));
		footer.setPreferredSize(new Dimension(screen.width, 50));

		setResizable(true);

		setSize(1316, 607);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new MigLayout("", "[102px][5px][5px][2px][][][110px][4px][141px][11px][202px][4px][61px][][13px][137px,grow][43px,grow][424px,grow]", "[65px,grow][9px][31px][3px][389px,grow][][19px][87px]"));

		JDateChooser dateChooser = new JDateChooser();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		java.util.Date date = new java.util.Date();
		dateChooser.setDate(date);
		dateChooser.getCalendarButton().setBackground(new Color(128, 0, 0));
		dateChooser.getCalendarButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});

		JLabel lblDate = new JLabel("Date");
		lblDate.setForeground(Color.BLACK);
		lblDate.setFont(new Font("SansSerif", Font.BOLD | Font.ITALIC, 19));
		panel.add(lblDate, "cell 13 0");
		panel.add(dateChooser, "cell 15 0,growx");

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		panel.add(tabbedPane, "cell 0 4 16 1,grow");

		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane_1.setBackground(new Color(0, 0, 128));
		tabbedPane.setBackground(Color.RED);
		tabbedPane_1.setFont(new Font("Sylfaen", Font.BOLD, 18));
		tabbedPane.addTab("Break Fast", null, tabbedPane_1, null);

		// Get Item For for order;

		JTabbedPane lunch_tab = new JTabbedPane(JTabbedPane.TOP);
		lunch_tab.setFont(new Font("Sylfaen", Font.BOLD, 18));

		tabbedPane.addTab("Luch", null, lunch_tab, null);

		JPanel luch_pan = new JPanel();
		lunch_tab.addTab("Menu", null, luch_pan, null);
		luch_pan.setLayout(new BoxLayout(luch_pan, BoxLayout.X_AXIS));

		luch_scrol = new JScrollPane();
		luch_pan.add(luch_scrol);

		JTabbedPane evening_snacks_tab = new JTabbedPane(JTabbedPane.TOP);
		evening_snacks_tab.setFont(new Font("Sylfaen", Font.BOLD, 18));
		tabbedPane.addTab("Evening Snackes", null, evening_snacks_tab, null);

		JPanel snacks_pan = new JPanel();
		evening_snacks_tab.addTab("Menu", null, snacks_pan, null);
		snacks_pan.setLayout(new BoxLayout(snacks_pan, BoxLayout.X_AXIS));

		JTabbedPane DinnerPan = new JTabbedPane(JTabbedPane.TOP);
		DinnerPan.setFont(new Font("Sylfaen", Font.BOLD, 18));
		tabbedPane.addTab("Dinner", null, DinnerPan, null);

		JPanel panel_4 = new JPanel();
		DinnerPan.addTab("Menu", null, panel_4, null);
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.X_AXIS));

		JScrollPane scrollPane = new JScrollPane();
		panel_4.add(scrollPane);

		

		JLabel lblTableNo = new JLabel("Table No:");
		lblTableNo.setFont(new Font("Tahoma", Font.ITALIC, 18));
		lblTableNo.setForeground(Color.BLACK);
		lblTableNo.setBackground(Color.WHITE);
		panel.add(lblTableNo, "cell 0 0");

		JLabel cusL = new JLabel("Customer Name:");
		cusL.setForeground(Color.BLACK);
		cusL.setFont(new Font("Tahoma", Font.ITALIC, 18));
		cusL.setBackground(Color.WHITE);
		panel.add(cusL, "cell 8 0");

		table_noCB = new JComboBox();
		table_noCB.setEditable(true);
		table_noCB.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				String t = table_noCB.getSelectedItem().toString();
				lblTableL.setText(t);
			}
		});

		table_noCB.setModel(new DefaultComboBoxModel(new String[] { "Select Table", "Table No1", "Table No 2",
				"Table No 3", "Table No 4", "Table No 5", "Table No 6", "Table No 7", "Table No 8" }));
		panel.add(table_noCB, "cell 2 0 5 1");
		show();

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Bill", TitledBorder.CENTER, TitledBorder.TOP, null, null));
		panel.add(panel_2, "cell 17 0 1 8,grow");
		panel_2.setLayout(new MigLayout("", "[][][][][][][][][grow][][][][][][]", "[grow][][][][][][][grow][][][][][][][][][][]"));

		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(0, 0, 0));
		panel_3.setForeground(new Color(255, 255, 255));
		panel_2.add(panel_3, "cell 0 0 15 2,grow");
		panel_3.setLayout(new MigLayout("", "[][][][][][][][][]", "[][]"));

		lblTableL = new JLabel("Table No:");
		lblTableL.setForeground(new Color(255, 255, 255));
		lblTableL.setBackground(new Color(255, 255, 255));
		panel_3.add(lblTableL, "cell 0 0");

		JLabel lblName = new JLabel("Name");
		// String name= customername.getText();
		lblName.setText("CustomerName" + name);
		lblName.setForeground(new Color(255, 255, 255));
		panel_3.add(lblName, "cell 3 0");

		JLabel dateL = new JLabel("Date");
		String d = dateChooser.getDate().toString();
		dateL.setText("Date :" + d);
		dateL.setForeground(Color.WHITE);
		dateL.setBackground(Color.BLACK);
		panel_3.add(dateL, "cell 6 0");

		JScrollPane scrollPane_1 = new JScrollPane();
		panel_2.add(scrollPane_1, "cell 0 2 15 15,grow");

		// Table of Ricpet
		table = new JTable();
		TempOrder.getDataFromAddFood();
		scrollPane_1.setViewportView(table);

		////////////////////////////////

		JLabel lblTotalAmount = new JLabel("Total Amount");
		lblTotalAmount.setIcon(new ImageIcon(Order.class.getResource("/pictureResource/images/Quarterly Sales.png")));
		panel_2.add(lblTotalAmount, "cell 1 17");

		takaL = new JLabel("000000");
		panel_2.add(takaL, "cell 3 17");

		JLabel lblTaka = new JLabel("Taka");
		panel_2.add(lblTaka, "cell 5 17");

		JButton btnPrint = new JButton("Print");
		btnPrint.setIcon(new ImageIcon(Order.class.getResource("/pictureResource/images/print.gif")));
		int heading1 = 800;
		int footer1 = 900;
		btnPrint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					MessageFormat heading = new MessageFormat("Biil");
					MessageFormat center = new MessageFormat("Printing.....");
					MessageFormat footer = new MessageFormat("this is footer");
					table.print(JTable.PrintMode.FIT_WIDTH);

					table.print();
				} catch (Exception E) {
					E.printStackTrace();
				}
			}

		});
		
		JButton btnSave = new JButton("Save");
		btnSave.setBorderPainted(false);
		btnSave.setRolloverEnabled(false);
		btnSave.setRequestFocusEnabled(false);
		btnSave.setFocusable(false);
		btnSave.setFocusPainted(false);
		btnSave.setIcon(new ImageIcon(Order.class.getResource("/images/save_all.png")));
		panel_2.add(btnSave, "cell 13 17");
		panel_2.add(btnPrint, "cell 14 17");

		// AllTable
		// BreakFast Table
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(128, 0, 0));
		tabbedPane_1.addTab("Menu", null, panel_1, null);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));

		JScrollPane breakfast_panel = new JScrollPane();
		panel_1.add(breakfast_panel);
		breakfast_table = new JTable();
		breakfast_table.setColumnSelectionAllowed(true);
		breakfast_table.setFont(new Font("SansSerif", Font.PLAIN, 18));
		breakfast_table.setForeground(Color.BLACK);
		breakfast_table.setBackground(new Color(255, 255, 255));

		TakeOrder.breakFastOrder();
		TempOrder.getDataFromAddFood();
		breakfast_table.setRowSelectionAllowed(true);
		breakfast_table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		customername = new JTextField();
		customername.setText("");
		customername.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				String cu = customername.getText();
				lblName.setText("Customer Name: " + cu);
			}
		});

		panel.add(customername, "cell 10 0,growx");
		customername.setColumns(10);

		breakfast_panel.setViewportView(breakfast_table);
		breakfast_table.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row = breakfast_table.getSelectedRow();

				String name = breakfast_table.getModel().getValueAt(row, 0).toString();

				String price = breakfast_table.getModel().getValueAt(row, 1).toString();

				String category = breakfast_table.getModel().getValueAt(row, 2).toString();

				new ConfirmOrrde().setVisible(true);
				ConfirmOrrde.foodnameTax.setText(name);
				ConfirmOrrde.periceTax.setText(price);
				ConfirmOrrde.CategoryTax.setText(category);
				/*
				 * String TableNo = table_noCB.getSelectedItem().toString();
				 * String qun = quantity.getText(); String customer =
				 * customername.getText(); String totalPrice=toatal.getText();
				 * String date=dateChooser.getDate().toString(); String
				 * discoun=discount.getText();
				 */
				// new TempOderCtrl(name ,price,category);

			}
		});

		// lunch Table
		luch_table = new JTable();
		luch_table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row = luch_table.getSelectedRow();

				String name = luch_table.getModel().getValueAt(row, 0).toString();

				String price = luch_table.getModel().getValueAt(row, 1).toString();

				String category = luch_table.getModel().getValueAt(row, 2).toString();

				new ConfirmOrrde().setVisible(true);
				ConfirmOrrde.foodnameTax.setText(name);
				ConfirmOrrde.periceTax.setText(price);
				ConfirmOrrde.CategoryTax.setText(category);	
			}
		});
		luch_table.setBackground(new Color(255, 255, 255));
		luch_table.setFont(new Font("SansSerif", Font.PLAIN, 18));
		luch_table.setForeground(new Color(51, 51, 51));
		TakeOrder.lunchOrder();
		luch_scrol.setViewportView(luch_table);

		// Dinner Table
		dinner_table = new JTable();
		dinner_table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row = dinner_table.getSelectedRow();

				String name = dinner_table.getModel().getValueAt(row, 0).toString();

				String price = dinner_table.getModel().getValueAt(row, 1).toString();

				String category = dinner_table.getModel().getValueAt(row, 2).toString();

				new ConfirmOrrde().setVisible(true);
				ConfirmOrrde.foodnameTax.setText(name);
				ConfirmOrrde.periceTax.setText(price);
				ConfirmOrrde.CategoryTax.setText(category);		
			}
		});
		dinner_table.setBackground(new Color(255, 255, 255));
		dinner_table.setFont(new Font("SansSerif", Font.PLAIN, 18));
		dinner_table.setForeground(new Color(0, 0, 0));

		TakeOrder.DnnerOrder();

		scrollPane.setViewportView(dinner_table);

		// Evening Table

		JScrollPane snack_scrol = new JScrollPane();
		snacks_pan.add(snack_scrol);

		snacks_table = new JTable();
		snacks_table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row = snacks_table.getSelectedRow();

				String name = snacks_table.getModel().getValueAt(row, 0).toString();

				String price = snacks_table.getModel().getValueAt(row, 1).toString();

				String category = snacks_table.getModel().getValueAt(row, 2).toString();

				new ConfirmOrrde().setVisible(true);
				ConfirmOrrde.foodnameTax.setText(name);
				ConfirmOrrde.periceTax.setText(price);
				ConfirmOrrde.CategoryTax.setText(category);		
			}
		});
		snacks_table.setFont(new Font("SansSerif", Font.PLAIN, 18));
		snacks_table.setForeground(new Color(0, 0, 0));
		snacks_table.setBackground(new Color(255, 255, 255));

		TakeOrder.EveningsnakcsOrder();
		snack_scrol.setViewportView(snacks_table);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBorder(new LineBorder(new Color(204, 204, 204), 10));
		panel.add(panel_6, "cell 16 0 1 8,grow");

	}

	public static void Main(String[] args) {
		try {

		} catch (Exception e) {
			// TODO: handle exception
		}
		Order addFood = new Order();
		addFood.setVisible(true);
		addFood.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}
