package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.ImageIcon;
import javax.swing.border.MatteBorder;

public class Help extends JInternalFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel label;
	private JDesktopPane desktopPane;


	//variable declaration;
	int w = 750;
	int h = 400;
	private JInternalFrame jInternalFrame;
	private JDesktopPane desktopen;
	JPanel header, center, footer;
	Border border = new EtchedBorder();
	Font font = new Font("courier new", Font.PLAIN, 16);
	

	public Help() {
		
	setTitle("Help");
		setRootPaneCheckingEnabled(false);
		setFrameIcon(new ImageIcon(Help.class.getResource("/pictureResource/images/Help.gif")));
		getContentPane().setBackground(new Color(128, 0, 0));
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 455, 779, -442);
		getContentPane().add(scrollPane);
		
		JTextPane txtpnEclipseIdeFor = new JTextPane();
		txtpnEclipseIdeFor.setText("Search Online\r\nhttp://www.nipuchakraborty.blog.com\r\n\r\nResturant Management Application Power By Java Language\r\n\r\nVersion: 1.1.0.0\r\nBuild id: 27263486\r\nYou Can Manage Everything of your Resturant By This Application . \r\n2017 -2018 Alright Reserved(c) CopyRight 2017\r\nDeveloped by \r\nNk chakraborty\r\n0182-7263486\r\nnipuchakraborty86@hotmail.com");
		txtpnEclipseIdeFor.setBounds(10, 158, 779, 295);
		
		getContentPane().add(txtpnEclipseIdeFor);
		txtpnEclipseIdeFor.setEditable(false);
		
		JPanel panel = new JPanel();
		panel.setBounds(-40, -50, 929, 575);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Learn For Earn Knowledge");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Yu Gothic Light", Font.BOLD, 30));
		lblNewLabel_1.setForeground(new Color(51, 0, 153));
		lblNewLabel_1.setBounds(180, 66, 550, 49);
		panel.add(lblNewLabel_1);
		setLocation(new Point(350, 100));
		setClosable(true);
		
		setVisible(true);
		
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		//Dimention Screen
		Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();
		//header Design
		header=new JPanel();
		header.setBorder(new EtchedBorder(EtchedBorder.LOWERED,null,null));
		header.setBackground(Color.BLUE);
		header.setPreferredSize(new Dimension(screen.width, 30));
		//center Design
		center=new JPanel();
		center.setPreferredSize(new Dimension(screen.width+50,50));
		//foter design
		footer=new JPanel();
		//footer.setBackground(Color.blue);
		footer.setBackground(new Color(155, 200, 200));
		footer.setPreferredSize(new Dimension(screen.width, 50));
		//setUndecorated(true);
	
		setSize(815, 494);
		show();
	}
	public  static void  Main(String []args){
		try {
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		Help addFood=new Help();
		addFood.setVisible(true);
		addFood.setDefaultCloseOperation(HIDE_ON_CLOSE);
	}
}
