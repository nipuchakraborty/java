package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;



import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.ImageIcon;
import javax.swing.border.MatteBorder;
import javax.swing.JPopupMenu;
import java.awt.Component;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Sit_Plane extends JInternalFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel label;
	private JDesktopPane desktopPane;

	//variable declaration;
	int w = 750;
	int h = 400;
	private JInternalFrame jInternalFrame;
	private JDesktopPane desktopen;
	JPanel header, center, footer;
	Border border = new EtchedBorder();
	Font font = new Font("courier new", Font.PLAIN, 16);
	public static JLabel t1;
	public static JLabel t2 ;
	public static JLabel t3 ;
	public static JLabel t4 ;
	public static JLabel t5;
	public static JLabel t6;
	public static JLabel t7;
	public static JLabel t8;
	public Sit_Plane() {
		setRootPaneCheckingEnabled(false);
		getContentPane().setBackground(new Color(128, 0, 0));
		getContentPane().setLayout(null);
		
		
		JLabel lblNewLabel = new JLabel("Manager");
		lblNewLabel.setBackground(Color.GREEN);
		lblNewLabel.setForeground(Color.GREEN);
		lblNewLabel.setFont(new Font("SansSerif", Font.BOLD, 20));
		lblNewLabel.setBounds(341, 41, 82, 26);
		getContentPane().add(lblNewLabel);
		
	t1 = new JLabel("Table No 1");
		t1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				t1.setText("BOOKING");
				t1.setFont(new Font("SansSerif", Font.PLAIN, 20));
				t1.setForeground(Color.WHITE);
			}
		});
		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(getContentPane(), popupMenu);
		
		JMenuItem mntmTableNo = new JMenuItem("Table No1");
		mntmTableNo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				t1.setText("Table No 1");
				t1.setFont(new Font("SansSerif", Font.PLAIN, 20));
				t1.setForeground(Color.WHITE);
			}
		});
		popupMenu.add(mntmTableNo);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Table No 2");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				t2.setText("Table No 2");
				t2.setFont(new Font("SansSerif", Font.PLAIN, 20));
				t2.setForeground(Color.WHITE);
			}
		});
		popupMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Table No 3");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				t3.setText("Table No 3");
				t3.setFont(new Font("SansSerif", Font.PLAIN, 20));
				t3.setForeground(Color.WHITE);
			}
		});
		popupMenu.add(mntmNewMenuItem_1);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Table No 4");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				t4.setText("Table No 4");
				t4.setFont(new Font("SansSerif", Font.PLAIN, 20));
				t4.setForeground(Color.WHITE);	
			}
		});
		popupMenu.add(mntmNewMenuItem_2);
		
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Table No 5");
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				t5.setText("Table No 5");
				t5.setFont(new Font("SansSerif", Font.PLAIN, 20));
				t5.setForeground(Color.WHITE);
			}
		});
		popupMenu.add(mntmNewMenuItem_3);
		
		JMenuItem mntmNewMenuItem_4 = new JMenuItem("Table No 6");
		popupMenu.add(mntmNewMenuItem_4);
		
		JMenuItem mntmNewMenuItem_5 = new JMenuItem("Table No 7");
		popupMenu.add(mntmNewMenuItem_5);
		
		JMenuItem mntmTableNo_1 = new JMenuItem("Table No 8");
		popupMenu.add(mntmTableNo_1);
		
			
				
		
		t1.setForeground(Color.WHITE);
		t1.setHorizontalTextPosition(JLabel.CENTER);
		t1.setVerticalTextPosition(JLabel.BOTTOM);
		t1.setIcon(new ImageIcon(Sit_Plane.class.getResource("/pictureResource/chair1600_60_60.jpg")));
		t1.setFont(new Font("SansSerif", Font.PLAIN, 20));
		t1.setBounds(28, 116, 96, 90);
		getContentPane().add(t1);
		
		JLabel t2 = new JLabel("Table No 2");
		t2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				t2.setText("Booking");
				t2.setFont(new Font("SansSerif", Font.PLAIN, 20));
				t2.setForeground(Color.WHITE);
			}
		});
		t2.setForeground(Color.WHITE);
		t2.setHorizontalTextPosition(JLabel.CENTER);
		t2.setVerticalTextPosition(JLabel.BOTTOM);
		t2.setIcon(new ImageIcon(Sit_Plane.class.getResource("/pictureResource/chair1600_60_60.jpg")));
		t2.setFont(new Font("SansSerif", Font.PLAIN, 20));
		t2.setBounds(337, 116, 96, 90);
		getContentPane().add(t2);
		
	 t3 = new JLabel("Table No 3");
		t3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				t3.setText("BOOKIN");
				t3.setFont(new Font("SansSerif", Font.PLAIN, 20));
				t3.setForeground(Color.WHITE);
			}
		});
		t3.setForeground(Color.WHITE);
		t3.setHorizontalTextPosition(JLabel.CENTER);
		t3.setVerticalTextPosition(JLabel.BOTTOM);
		t3.setIcon(new ImageIcon(Sit_Plane.class.getResource("/pictureResource/chair1600_60_60.jpg")));
		t3.setFont(new Font("SansSerif", Font.PLAIN, 20));
		t3.setBounds(666, 116, 96, 90);
		getContentPane().add(t3);
		
	 t4 = new JLabel("Table No 4");
		t4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				t4.setText("BOOKIN");
				t4.setFont(new Font("SansSerif", Font.PLAIN, 20));
				t4.setForeground(Color.WHITE);
			}
		});
		t4.setForeground(Color.WHITE);
		t4.setHorizontalTextPosition(JLabel.CENTER);
		t4.setVerticalTextPosition(JLabel.BOTTOM);
		t4.setIcon(new ImageIcon(Sit_Plane.class.getResource("/pictureResource/chair1600_60_60.jpg")));
		t4.setFont(new Font("SansSerif", Font.PLAIN, 20));
		t4.setBounds(28, 280, 96, 90);
		getContentPane().add(t4);
		
	t5 = new JLabel("Table No 5");
		t5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				t5.setText("BOOKIN");
				t5.setFont(new Font("SansSerif", Font.PLAIN, 20));
				t5.setForeground(Color.WHITE);
			}
		});
		t5.setForeground(Color.WHITE);
		t5.setHorizontalTextPosition(JLabel.CENTER);
		t5.setVerticalTextPosition(JLabel.BOTTOM);
		t5.setIcon(new ImageIcon(Sit_Plane.class.getResource("/pictureResource/chair1600_60_60.jpg")));
		t5.setFont(new Font("SansSerif", Font.PLAIN, 20));
		t5.setBounds(341, 280, 96, 90);
		getContentPane().add(t5);
		
		JLabel t6 = new JLabel("Table No 6");
		t6.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				t6.setText("Table No 6");
				t6.setFont(new Font("SansSerif", Font.PLAIN, 20));
				t6.setForeground(Color.WHITE);
			}
		});
		t6.setForeground(Color.WHITE);
		t6.setHorizontalTextPosition(JLabel.CENTER);
		t6.setVerticalTextPosition(JLabel.BOTTOM);
		t6.setIcon(new ImageIcon(Sit_Plane.class.getResource("/pictureResource/chair1600_60_60.jpg")));
		t6.setFont(new Font("SansSerif", Font.PLAIN, 20));
		t6.setBounds(666, 280, 96, 90);
		getContentPane().add(t6);
		
		JLabel t7 = new JLabel("Table No 7");
		t7.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				t7.setText("Table No 7");
				t7.setFont(new Font("SansSerif", Font.PLAIN, 20));
				t7.setForeground(Color.WHITE);
			}
		});
		t7.setForeground(Color.WHITE);
		t7.setHorizontalTextPosition(JLabel.CENTER);
		t7.setVerticalTextPosition(JLabel.BOTTOM);
		t7.setIcon(new ImageIcon(Sit_Plane.class.getResource("/pictureResource/chair1600_60_60.jpg")));
		t7.setFont(new Font("SansSerif", Font.PLAIN, 20));
		t7.setBounds(28, 415, 96, 90);
		getContentPane().add(t7);
		
		t8 = new JLabel("Table No 8");
		t8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				t8.setText("Table No 8");
				t8.setFont(new Font("SansSerif", Font.PLAIN, 20));
				t8.setForeground(Color.WHITE);
				
			}
		});
		t8.setForeground(Color.WHITE);
		t8.setHorizontalTextPosition(JLabel.CENTER);
		t8.setVerticalTextPosition(JLabel.BOTTOM);
		t8.setIcon(new ImageIcon(Sit_Plane.class.getResource("/pictureResource/chair1600_60_60.jpg")));
		t8.setFont(new Font("SansSerif", Font.PLAIN, 20));
		t8.setBounds(337, 425, 96, 90);
		getContentPane().add(t8);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.BLACK);
		panel.setBounds(182, 101, 82, 423);
		getContentPane().add(panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.RED);
		panel_1.setBounds(0, 76, 803, 26);
		getContentPane().add(panel_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(274, 6, 17, 70);
		getContentPane().add(panel_2);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(463, 6, 17, 70);
		getContentPane().add(panel_3);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(Color.BLACK);
		panel_4.setBounds(524, 101, 82, 423);
		getContentPane().add(panel_4);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.BLACK);
		panel_5.setBounds(0, 517, 803, 31);
		getContentPane().add(panel_5);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBackground(Color.RED);
		panel_6.setBounds(666, 462, 137, 43);
		getContentPane().add(panel_6);
		
		JLabel lblComeIn = new JLabel("Come In");
		panel_6.add(lblComeIn);
		lblComeIn.setForeground(Color.WHITE);
		lblComeIn.setFont(new Font("SansSerif", Font.BOLD, 20));
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(Sit_Plane.class.getResource("/pictureResource/User_icon-up.png")));
		lblNewLabel_1.setBounds(351, 6, 64, 64);
		getContentPane().add(lblNewLabel_1);
		
		JPanel panel_7 = new JPanel();
		panel_7.setBackground(Color.RED);
		panel_7.setBounds(0, 0, 131, 98);
		getContentPane().add(panel_7);
		
		JLabel lblWashRoom = new JLabel("Wash Room");
		lblWashRoom.setForeground(Color.WHITE);
		lblWashRoom.setFont(new Font("SansSerif", Font.BOLD, 20));
		panel_7.add(lblWashRoom);
		
		JPanel panel_8 = new JPanel();
		panel_8.setBackground(Color.RED);
		panel_8.setBounds(672, 0, 131, 98);
		getContentPane().add(panel_8);
		
		JLabel lblKitchen = new JLabel("Kitchen");
		lblKitchen.setForeground(Color.WHITE);
		lblKitchen.setFont(new Font("SansSerif", Font.BOLD, 20));
		panel_8.add(lblKitchen);
		setLocation(new Point(350, 100));
		setInheritsPopupMenu(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		setDoubleBuffered(true);
		
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		//Dimention Screen
		Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();
		//header Design
		header=new JPanel();
		header.setBorder(new EtchedBorder(EtchedBorder.LOWERED,null,null));
		//header.setBackground(Color.BLUE);
		header.setPreferredSize(new Dimension(screen.width, 30));
		//center Design
		center=new JPanel();
		center.setPreferredSize(new Dimension(screen.width+50,50));
		//foter design
		footer=new JPanel();
		//footer.setBackground(Color.blue);
		//footer.setBackground(new Color(155, 200, 200));
		//footer.setPreferredSize(new Dimension(screen.width, 50));
		
	
		setSize(815, 579);
		show();
	}
	public  static void  Main(String []args){
		try {
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		Sit_Plane addFood=new Sit_Plane();
		addFood.setVisible(true);
		addFood.setDefaultCloseOperation(HIDE_ON_CLOSE);
	}
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
		
	}
}
