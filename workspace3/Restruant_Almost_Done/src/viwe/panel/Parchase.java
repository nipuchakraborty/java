package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import controller.ParchaseCtrl;
import model.ParchaseModel;

import javax.swing.SwingConstants;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import net.miginfocom.swing.MigLayout;
import viwe.Dash;
import javax.swing.border.TitledBorder;
import com.toedter.calendar.JDateChooser;

public  class Parchase extends JInternalFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel label;
	private JDesktopPane desktopPane;

	//main mehtod 
	
	//variable declaration;
	int w = 750;
	int h = 400;
	private JInternalFrame jInternalFrame;
	private JDesktopPane desktopen;
	JPanel header, center, footer;
	Border border = new EtchedBorder();
	Font font = new Font("courier new", Font.PLAIN, 16);
	private JTextField billTxt;
	private JTextField vendorName;
	private JTextField productName;
	private JTextField quntity_txt;
	private JTextField price_txt;
	private JTextField total_txt;
	public static JTable table;
	

	public Parchase() {
		setBackground(new Color(0, 100, 0));
		setTitle("Parchase");
		setLocation(new Point(350, 100));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);
		
		setVisible(true);
		setDoubleBuffered(true);
		setMaximizable(true);
		
		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		getContentPane().setBackground(new Color(47, 79, 79));
		//Dimention Screen
		Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();
		//header Design
		header=new JPanel();
		header.setBorder(new EtchedBorder(EtchedBorder.LOWERED,null,null));
		header.setBackground(Color.BLUE);
		header.setPreferredSize(new Dimension(screen.width, 30));
		//center Design
		center=new JPanel();
		center.setPreferredSize(new Dimension(screen.width+50,50));
		//foter design
		footer=new JPanel();
		
		footer.setBackground(new Color(155, 200, 200));
		footer.setPreferredSize(new Dimension(screen.width, 50));
		
		
		setResizable(false);
		
		setSize(1076, 597);
		getContentPane().setLayout(null);
		
		
		
		JLabel lblBillNo = new JLabel("Bill No:");
		lblBillNo.setBackground(Color.LIGHT_GRAY);
		lblBillNo.setBounds(7, 13, 64, 22);
		lblBillNo.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblBillNo.setForeground(new Color(240, 255, 240));
		getContentPane().add(lblBillNo);
		
		JLabel lblVendorName = new JLabel("Vendor Name:");
		lblVendorName.setBounds(0, 86, 113, 22);
		lblVendorName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblVendorName.setForeground(new Color(245, 255, 250));
		getContentPane().add(lblVendorName);
		
		billTxt = new JTextField();
		billTxt.setBounds(83, 10, 64, 33);
		getContentPane().add(billTxt);
		billTxt.setColumns(10);
		
		vendorName = new JTextField();
		vendorName.setBounds(151, 83, 229, 33);
		getContentPane().add(vendorName);
		vendorName.setColumns(10);
		
		JLabel lblProductName = new JLabel("Product Name");
		lblProductName.setBounds(2, 152, 111, 22);
		lblProductName.setForeground(new Color(255, 255, 255));
		lblProductName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		getContentPane().add(lblProductName);
		
		JLabel lblProductCategory = new JLabel("Product Category");
		lblProductCategory.setBounds(0, 211, 136, 22);
		lblProductCategory.setForeground(new Color(255, 255, 255));
		lblProductCategory.setFont(new Font("Tahoma", Font.PLAIN, 18));
		getContentPane().add(lblProductCategory);
		
		productName = new JTextField();
		productName.setBounds(151, 149, 229, 33);
		getContentPane().add(productName);
		productName.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(151, 208, 229, 33);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Select", "Spaici", "Vegitable", "Stationary", "Fish", "Meat", "Fruit", "Others"}));
		getContentPane().add(comboBox);
		
		JLabel lblAmount = new JLabel("Amount");
		lblAmount.setBounds(7, 400, 62, 22);
		lblAmount.setForeground(new Color(255, 255, 255));
		lblAmount.setFont(new Font("Tahoma", Font.PLAIN, 18));
		getContentPane().add(lblAmount);
		
		quntity_txt = new JTextField();
		quntity_txt.setBounds(81, 397, 52, 33);
		getContentPane().add(quntity_txt);
		quntity_txt.setColumns(10);
		
		price_txt = new JTextField();
		price_txt.setBounds(226, 397, 83, 33);
		price_txt.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				/*String qun=quntity_txt.getText();
				int q=Integer.parseInt(qun);
			
				String price=price_txt.getText();
				int pri=Integer.parseInt(price);
				int total=q*pri;
				String to=Integer.toString(total);
				total_txt.setText(to);*/
				
			}
		});
		
		JLabel lblPrice = new JLabel("Price:");
		lblPrice.setBounds(171, 400, 43, 22);
		lblPrice.setBackground(new Color(255, 255, 255));
		lblPrice.setForeground(new Color(255, 255, 255));
		lblPrice.setFont(new Font("Tahoma", Font.PLAIN, 18));
		getContentPane().add(lblPrice);
		getContentPane().add(price_txt);
		price_txt.setColumns(10);
		
		JLabel lblTotal = new JLabel("Total:");
		lblTotal.setBounds(7, 470, 46, 22);
		lblTotal.setForeground(new Color(255, 255, 255));
		lblTotal.setFont(new Font("Tahoma", Font.PLAIN, 18));
		getContentPane().add(lblTotal);
		
		total_txt = new JTextField();
		total_txt.setBounds(81, 467, 133, 33);
		getContentPane().add(total_txt);
		total_txt.setColumns(10);
		
		JPanel panel = new JPanel();
		panel.setBounds(439, 7, 609, 509);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 9, 597, 494);
		scrollPane.setBorder(new TitledBorder(null, "Parchase Table", TitledBorder.CENTER, TitledBorder.TOP, null, null));
		panel.add(scrollPane);
		
		table = new JTable();
		ParchaseModel.load();
		table.setBackground(new Color(0, 0, 0));
		table.setForeground(new Color(0, 255, 0));
		scrollPane.setViewportView(table);
		ParchaseModel.load();
		JScrollPane scrollPane1 = new JScrollPane();
		panel.add(scrollPane1, "cell 0 0,alignx center,aligny center");
		
		
		
		
		JButton btnSave = new JButton("Save");
		btnSave.setBounds(997, 518, 61, 25);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String bill=billTxt.getText();
				String ven=vendorName.getText();
				String pro=productName.getText();
				String cat=comboBox.getSelectedItem().toString();
				
				String qun =quntity_txt.getText();
				String pric=price_txt.getText();
				String totalPric=total_txt.getText();
				new ParchaseCtrl( bill, ven,  pro, cat, qun,  pric, totalPric);
				ParchaseModel.load();
			}
		});
		getContentPane().add(btnSave);
		
		JLabel lblDate = new JLabel("Date:");
		lblDate.setForeground(new Color(240, 255, 240));
		lblDate.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblDate.setBackground(Color.LIGHT_GRAY);
		lblDate.setBounds(150, 13, 50, 22);
		getContentPane().add(lblDate);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(215, 13, 153, 28);
		getContentPane().add(dateChooser);
		show();
		desktopPane = new JDesktopPane() {
		    private Image image;
		    {
		        //image = ImageIO.read(new File("E:/dark wood texture plank floor wooded panel free stock photo.jpg"));
				image=(Toolkit.getDefaultToolkit().getImage(Parchase.class.getResource("/pictureResource/rsB.jpg")));
		    }

		    @Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		    }
		};
		desktopPane.setFocusable(false);
		desktopPane.setFocusTraversalKeysEnabled(false);
		desktopPane.setFocusCycleRoot(false);
		desktopPane.setBorder(null);
		
		desktopPane.setBackground(Color.GRAY);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		desktopPane.setLayout(new GridLayout(1, 0, 0, 0));
	
	
	}
	
	public  static void  Main(String []args){
		try {
			
			
		} catch (Exception e) {
			
		}
		Parchase addFood=new Parchase();
		addFood.setVisible(true);
		addFood.setDefaultCloseOperation(HIDE_ON_CLOSE);
	}
}
