package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.nio.channels.SelectableChannel;
import java.security.PublicKey;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.ImageIcon;
import javax.swing.border.MatteBorder;
import org.jdesktop.swingx.JXDatePicker;

import com.toedter.calendar.JDateChooser;

import controller.EditWorkerCtrl;
import controller.WorkerDitails_ctrl;
import model.Database;
import model.WorkerDitailsModel;
import net.proteanit.sql.DbUtils;
import viwe.TextPrompt;
import viwe.Dialouge.EditWorkerDitails;

import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import net.miginfocom.swing.MigLayout;

public class WorkerDetails extends JInternalFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel label;
	private JDesktopPane desktopPane;
	private JCheckBox present_ch;

	// variable declaration;
	int w = 750;
	int h = 400;
	private JInternalFrame jInternalFrame;
	private JDesktopPane desktopen;
	public static JPanel header, center, footer;
	public static Border border = new EtchedBorder();
	Font font = new Font("courier new", Font.PLAIN, 16);
	public static JTextField id_txt;
	public static  JTextField WorkerName_txt;
	public static JTable presentList;
	public static JRadioButton fmaleR;
	public static JRadioButton MaleR;
	public static JTextField search_txt;
	public static String za;
	public static JCheckBox absent_ch;
	public static String date ;
	public static  JTextField posiotn;
	public WorkerDetails() {
		setFrameIcon(new ImageIcon(WorkerDetails.class.getResource("/pictureResource/images/Expenses.png")));
		setRequestFocusEnabled(true);
		setTitle("Worker Ditails");
		getContentPane().setBackground(new Color(128, 0, 0));
		getContentPane().setLayout(null);
		JPanel panel = new JPanel();
		panel.setBounds(10, 187, 848, 389);
		getContentPane().add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane);

		presentList = new JTable();
		presentList.setForeground(new Color(0, 0, 0));
		presentList.setBackground(new Color(255, 255, 255));

		// WorkerDitailsModel.search(za);
		WorkerDitailsModel.load();
		scrollPane.setViewportView(presentList);
		
		
		
		present_ch = new JCheckBox("Present");
		present_ch.setBackground(new Color(0, 0, 128));
		present_ch.setForeground(new Color(255, 255, 255));
		present_ch.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (absent_ch.isSelected()) {
					present_ch.setSelected(false);

				}
			}
		});
		 absent_ch = new JCheckBox("Absent");
		absent_ch.setBackground(new Color(0, 0, 128));
		absent_ch.setForeground(new Color(255, 255, 255));
		absent_ch.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (present_ch.isSelected()) {
					absent_ch.setSelected(false);

				}
			}
		});
		absent_ch.setBounds(609, 110, 70, 23);
		getContentPane().add(absent_ch);

		
		present_ch.setBounds(609, 138, 70, 23);
		getContentPane().add(present_ch);
		fmaleR = new JRadioButton("Female");
		fmaleR.setBackground(new Color(0, 0, 128));
		fmaleR.setForeground(new Color(255, 255, 255));
		fmaleR.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (fmaleR.isSelected()) {
					MaleR.setSelected(false);
				}
			}
		});

		fmaleR.setBounds(525, 110, 73, 23);
		getContentPane().add(fmaleR);
		MaleR = new JRadioButton("Male");
		MaleR.setBackground(new Color(0, 0, 128));
		MaleR.setForeground(new Color(255, 255, 255));
		MaleR.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (MaleR.isSelected()) {
					fmaleR.setSelected(false);

				}

			}
		});
		MaleR.setBounds(525, 138, 73, 23);
		getContentPane().add(MaleR);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.BLACK);
		panel_1.setForeground(new Color(128, 0, 128));
		panel_1.setBorder(new LineBorder(new Color(128, 0, 128), 4, true));
		panel_1.setBounds(512, 42, 177, 131);
		getContentPane().add(panel_1);
		panel_1.setLayout(new MigLayout("", "[109px][]", "[20px]"));
		
				JLabel lblSelectOption = new JLabel("Select Option");
				lblSelectOption.setForeground(new Color(0, 255, 0));
				lblSelectOption.setFont(new Font("Tahoma", Font.BOLD, 16));
				panel_1.add(lblSelectOption, "cell 0 0 2 1,alignx center");

		JPanel post2 = new JPanel();
		post2.setBorder(new LineBorder(new Color(128, 0, 128), 4, true));
		post2.setBackground(Color.BLACK);
		post2.setBounds(10, 42, 496, 131);
		getContentPane().add(post2);
		post2.setLayout(null);

		JLabel lblPost = new JLabel("Post");
		lblPost.setForeground(Color.WHITE);
		lblPost.setFont(new Font("Tahoma", Font.ITALIC, 18));
		lblPost.setBounds(273, 92, 119, 22);
		post2.add(lblPost);

		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 88, 119, 22);
		post2.add(lblName);
		lblName.setForeground(Color.WHITE);
		lblName.setFont(new Font("Tahoma", Font.ITALIC, 18));

		WorkerName_txt = new JTextField();
		WorkerName_txt.setBounds(124, 88, 128, 28);
		post2.add(WorkerName_txt);
		WorkerName_txt.setColumns(10);

		JLabel lblWorkerId = new JLabel("Worker Id");
		lblWorkerId.setBounds(10, 43, 119, 22);
		post2.add(lblWorkerId);
		lblWorkerId.setForeground(Color.WHITE);
		lblWorkerId.setFont(new Font("Tahoma", Font.ITALIC, 18));

		id_txt = new JTextField();
		id_txt.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				String id=id_txt.getText();
				WorkerDitailsModel.NameOfWorker(id);
				
				//WorkerDitailsModel.loadWorker(id);

			}
		});
		TextPrompt textPrompt=new TextPrompt("Enter Worker Id", id_txt);
		id_txt.setBounds(124, 43, 128, 28);
		post2.add(id_txt);
		id_txt.setColumns(10);

		JLabel lblDate = new JLabel("Date");
		lblDate.setBounds(273, 43, 119, 22);
		post2.add(lblDate);
		lblDate.setForeground(Color.WHITE);
		lblDate.setFont(new Font("Tahoma", Font.ITALIC, 18));

		JDateChooser datePicker = new JDateChooser();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		java.util.Date date2 = new Date(System.currentTimeMillis());
		datePicker.setBounds(345, 46, 138, 22);
		datePicker.setDate(date2);
		post2.add(datePicker);
		
		posiotn = new JTextField();
		posiotn.setBounds(345, 91, 138, 28);
		post2.add(posiotn);
		posiotn.setColumns(10);
		

		search_txt = new JTextField();
		TextPrompt textPrompt2 = new TextPrompt("Enter Id", search_txt);
		textPrompt.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				za = search_txt.getText();
				
				WorkerDitailsModel.search(za);
			}
		});

		search_txt.setBounds(768, 42, 90, 28);
		getContentPane().add(search_txt);
		search_txt.setColumns(10);

		JLabel searchL = new JLabel("Search");
		searchL.setForeground(Color.WHITE);
		searchL.setFont(new Font("SansSerif", Font.ITALIC, 12));
		searchL.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String getd = search_txt.getText();
				WorkerDitailsModel.search(getd);
			

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				searchL.setForeground(Color.GREEN);
				searchL.setFont(new Font("SansSerif", Font.BOLD, 12));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				searchL.setForeground(Color.WHITE);
				searchL.setFont(new Font("SansSerif", Font.ITALIC, 12));
			}
		});
		
		JLabel lblNewLabel = new JLabel("Worker Information");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				lblNewLabel.setFont(new Font("SansSerif", Font.PLAIN, 32));
				lblNewLabel.setForeground(Color.GREEN);	
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				lblNewLabel.setFont(new Font("SansSerif", Font.PLAIN, 30));
				lblNewLabel.setForeground(new Color(240, 248, 255));
			}
		});
		lblNewLabel.setFont(new Font("SansSerif", Font.PLAIN, 30));
		lblNewLabel.setForeground(new Color(240, 248, 255));
		lblNewLabel.setBounds(231, 0, 393, 34);
		getContentPane().add(lblNewLabel);
		searchL.setIcon(new ImageIcon(WorkerDetails.class.getResource("/pictureResource/b_search.png")));
		searchL.setBounds(694, 48, 73, 16);
		getContentPane().add(searchL);

		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String id = id_txt.getText();
				
				String name = WorkerName_txt.getText();
				
				String poS=posiotn.getText();

				String gender = null;
				String  date = datePicker.getDate().toString();

				
				if (MaleR.isSelected()) {
					gender = MaleR.getText();

				} else if (fmaleR.isSelected()) {
					gender = fmaleR.getText();

				} else {
					JOptionPane.showMessageDialog(null, "Please Select Gender");
				}
				String present = null;
				if (present_ch.isSelected()) {
					present = present_ch.getText();
				} else if (absent_ch.isSelected()) {
					present = absent_ch.getText();

				} else {
					JOptionPane.showMessageDialog(null, "Select Absent Or Present!");
				}
				
				new WorkerDitails_ctrl(id,name, gender, present,poS ,date );
				WorkerDitailsModel.load();
			}
		});
		btnSave.setBounds(759, 582, 90, 28);
		getContentPane().add(btnSave);

		JButton edit = new JButton("Edit");
		edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int row = presentList.getSelectedRow();

				String id = presentList.getModel().getValueAt(row, 0).toString();
				String Name = presentList.getModel().getValueAt(row, 1).toString();
				String Gender = presentList.getModel().getValueAt(row, 2).toString();
				String present = presentList.getModel().getValueAt(row, 3).toString();
				// String pos=presentList.getModel().getValueAt(row,
				// 4).toString();
				// String date=presentList.getModel().getValueAt(row,
				// 5).toString();
				EditWorkerDitails editWorkerDitails = new EditWorkerDitails();
				editWorkerDitails.setVisible(true);
				editWorkerDitails.workerIdTxt.setText(id);
				editWorkerDitails.name.setText(Name);
				editWorkerDitails.genderT.setText(Gender);
				editWorkerDitails.preT.setText(present);
				// editWorkerDitails.setTitle(pos);
				// editWorkerDitails.da2.setText(date);

				// new EditWorkerCtrl(id,Name,Gender,present,pos,date);
			}
		});
		edit.setBounds(642, 582, 90, 28);
		getContentPane().add(edit);

		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(WorkerDetails.class.getResource("/pictureResource/Foood.jpeg")));
		lblNewLabel_1.setBounds(-111, -469, 1047, 1096);
		getContentPane().add(lblNewLabel_1);
		setLocation(new Point(350, 100));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);

		setVisible(true);
		setDoubleBuffered(true);

		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		// Dimention Screen
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		// header Design
		header = new JPanel();
		header.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		header.setBackground(Color.BLUE);
		header.setPreferredSize(new Dimension(screen.width, 30));
		// center Design
		center = new JPanel();
		center.setPreferredSize(new Dimension(screen.width + 50, 50));
		// foter design
		footer = new JPanel();
		// footer.setBackground(Color.blue);
		footer.setBackground(new Color(155, 200, 200));
		footer.setPreferredSize(new Dimension(screen.width, 50));

		setSize(877, 658);
		show();
	}

	public static void Main(String[] args) {
		try {

		} catch (Exception e) {
			// TODO: handle exception
		}
		WorkerDetails addFood = new WorkerDetails();
		addFood.setVisible(true);
		addFood.setDefaultCloseOperation(HIDE_ON_CLOSE);
	}
}
