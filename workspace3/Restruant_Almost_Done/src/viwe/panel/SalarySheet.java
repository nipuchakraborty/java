package viwe.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.nio.channels.SelectableChannel;
import java.security.PublicKey;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.ImageIcon;
import javax.swing.border.MatteBorder;
import org.jdesktop.swingx.JXDatePicker;

import com.toedter.calendar.JDateChooser;

import controller.EditWorkerCtrl;
import controller.WorkerDitails_ctrl;
import model.Database;
import model.SalarySheetModel;
import model.WorkerDitailsModel;
import net.proteanit.sql.DbUtils;
import viwe.TextPrompt;
import viwe.Dialouge.EditWorkerDitails;

import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import net.miginfocom.swing.MigLayout;

public class SalarySheet extends JInternalFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel label;
	private JDesktopPane desktopPane;
	private JCheckBox present_ch;

	// variable declaration;
	int w = 750;
	int h = 400;
	private JInternalFrame jInternalFrame;
	private JDesktopPane desktopen;
	public static JPanel header, center, footer;
	public static Border border = new EtchedBorder();
	Font font = new Font("courier new", Font.PLAIN, 16);
	public static JTextField id_txt;
	public static  JTextField WorkerName_txt;
	public static JTable SalarySheetTab;
	public static JRadioButton fmaleR;
	public static JRadioButton MaleR;
	public static JTextField search_txt;
	public static String za;
	public static JCheckBox absent_ch;
	public static String date ;
	public static  JTextField posiotn;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	public SalarySheet() {
		setFrameIcon(new ImageIcon(SalarySheet.class.getResource("/pictureResource/images/Expenses.png")));
		setRequestFocusEnabled(true);
		setTitle("Salary Sheet");
		getContentPane().setBackground(new Color(128, 0, 0));
		getContentPane().setLayout(null);
		JPanel panel = new JPanel();
		panel.setBounds(10, 187, 848, 389);
		getContentPane().add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane);

		SalarySheetTab = new JTable();
		SalarySheetTab.setForeground(new Color(0, 0, 0));
		SalarySheetTab.setBackground(new Color(255, 255, 255));

		// WorkerDitailsModel.search(za);
		SalarySheetModel.load();
		scrollPane.setViewportView(SalarySheetTab);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.BLACK);
		panel_1.setForeground(new Color(128, 0, 128));
		panel_1.setBorder(new LineBorder(new Color(128, 0, 128), 4, true));
		panel_1.setBounds(411, 42, 177, 131);
		getContentPane().add(panel_1);
		panel_1.setLayout(new MigLayout("", "[][109px][]", "[20px][][][]"));
		
		JLabel lblNewLabel_3 = new JLabel("Gender");
		lblNewLabel_3.setForeground(Color.WHITE);
		panel_1.add(lblNewLabel_3, "cell 0 1");
		
		JLabel lblPayment = new JLabel("Payment");
		lblPayment.setForeground(Color.WHITE);
		panel_1.add(lblPayment, "cell 1 1");
		fmaleR = new JRadioButton("Female");
		panel_1.add(fmaleR, "cell 0 2");
		fmaleR.setBackground(Color.BLACK);
		fmaleR.setForeground(new Color(255, 255, 255));
		absent_ch = new JCheckBox("Due");
		panel_1.add(absent_ch, "cell 1 2");
		absent_ch.setBackground(Color.BLACK);
		absent_ch.setForeground(new Color(255, 255, 255));
		absent_ch.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (present_ch.isSelected()) {
					absent_ch.setSelected(false);

				}
			}
		});
		MaleR = new JRadioButton("Male");
		panel_1.add(MaleR, "cell 0 3");
		MaleR.setBackground(Color.BLACK);
		MaleR.setForeground(new Color(255, 255, 255));
		
		
		
		present_ch = new JCheckBox("Payment");
		panel_1.add(present_ch, "cell 1 3");
		present_ch.setBackground(Color.BLACK);
		present_ch.setForeground(new Color(255, 255, 255));
		present_ch.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (absent_ch.isSelected()) {
					present_ch.setSelected(false);

				}
			}
		});
		MaleR.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (MaleR.isSelected()) {
					fmaleR.setSelected(false);

				}

			}
		});
		fmaleR.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (fmaleR.isSelected()) {
					MaleR.setSelected(false);
				}
			}
		});

		JPanel post2 = new JPanel();
		post2.setBorder(new LineBorder(new Color(128, 0, 128), 4, true));
		post2.setBackground(Color.BLACK);
		post2.setBounds(10, 42, 393, 131);
		getContentPane().add(post2);
		post2.setLayout(new MigLayout("", "[24.00px][][][60px][138px]", "[28px][][31px][]"));
								
										id_txt = new JTextField();
										TextPrompt textPrompt=new TextPrompt("Enter Worker Id", id_txt);
										id_txt.addKeyListener(new KeyAdapter() {
											@Override
											public void keyReleased(KeyEvent arg0) {
												String id=id_txt.getText();
												WorkerDitailsModel.NameOfWorker(id);
												
												//WorkerDitailsModel.loadWorker(id);

											}
										});
										
												JLabel lblWorkerId = new JLabel("Worker Id");
												post2.add(lblWorkerId, "cell 1 1,alignx left");
												lblWorkerId.setForeground(Color.WHITE);
												lblWorkerId.setFont(new Font("Tahoma", Font.ITALIC, 18));
										
										post2.add(id_txt, "cell 2 1");
										id_txt.setColumns(10);
						
								JLabel lblDate = new JLabel("Date");
								post2.add(lblDate, "cell 3 1,growx,aligny top");
								lblDate.setForeground(Color.WHITE);
								lblDate.setFont(new Font("Tahoma", Font.ITALIC, 18));
				
						JDateChooser datePicker = new JDateChooser();
						//datePicker.setDate(date2);
						post2.add(datePicker, "cell 4 1,growx,aligny center");
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		java.util.Date date2 = new Date(System.currentTimeMillis());
						
								JLabel lblName = new JLabel("Name");
								post2.add(lblName, "flowx,cell 1 3,alignx left,aligny top");
								lblName.setForeground(Color.WHITE);
								lblName.setFont(new Font("Tahoma", Font.ITALIC, 18));
				
						WorkerName_txt = new JTextField();
						post2.add(WorkerName_txt, "cell 2 3");
						WorkerName_txt.setColumns(10);
		
				JLabel lblPost = new JLabel("Post");
				lblPost.setForeground(Color.WHITE);
				lblPost.setFont(new Font("Tahoma", Font.ITALIC, 18));
				post2.add(lblPost, "cell 3 3,growx,aligny center");
		
		posiotn = new JTextField();
		post2.add(posiotn, "cell 4 3,grow");
		posiotn.setColumns(10);
		

		search_txt = new JTextField();
		TextPrompt textPrompt2 = new TextPrompt("Enter Id", search_txt);
		textPrompt.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				za = search_txt.getText();
				
				SalarySheetModel.search(za);
			}
		});
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.BLACK);
		panel_2.setForeground(Color.BLACK);
		panel_2.setBounds(600, 42, 237, 111);
		getContentPane().add(panel_2);
		panel_2.setLayout(new MigLayout("", "[74.00][]", "[][][]"));
		
		JLabel lblNewLabel_2 = new JLabel("Salary Amount");
		lblNewLabel_2.setForeground(Color.WHITE);
		panel_2.add(lblNewLabel_2, "cell 0 0");
		
		textField_1 = new JTextField();
		panel_2.add(textField_1, "cell 2 0");
		textField_1.setColumns(10);
		
		JLabel lblPaidSalary = new JLabel("Paid Salary");
		lblPaidSalary.setForeground(Color.WHITE);
		panel_2.add(lblPaidSalary, "cell 0 1");
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		panel_2.add(textField_2, "cell 2 1,growx");
		
		JLabel lblDue = new JLabel("Due");
		lblDue.setForeground(Color.WHITE);
		panel_2.add(lblDue, "cell 0 2");
		
		textField_3 = new JTextField();
		panel_2.add(textField_3, "cell 2 2,growx");
		textField_3.setColumns(10);

		search_txt.setBounds(759, 10, 90, 28);
		getContentPane().add(search_txt);
		search_txt.setColumns(10);

		JLabel searchL = new JLabel("Search");
		searchL.setForeground(Color.WHITE);
		searchL.setFont(new Font("SansSerif", Font.ITALIC, 12));
		searchL.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String getd = search_txt.getText();
				SalarySheetModel.search(getd);
			

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				searchL.setForeground(Color.GREEN);
				searchL.setFont(new Font("SansSerif", Font.BOLD, 12));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				searchL.setForeground(Color.WHITE);
				searchL.setFont(new Font("SansSerif", Font.ITALIC, 12));
			}
		});
		
		JLabel lblNewLabel = new JLabel("SalarySheet");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				lblNewLabel.setFont(new Font("SansSerif", Font.PLAIN, 32));
				lblNewLabel.setForeground(Color.GREEN);	
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				lblNewLabel.setFont(new Font("SansSerif", Font.PLAIN, 30));
				lblNewLabel.setForeground(new Color(240, 248, 255));
			}
		});
		lblNewLabel.setFont(new Font("SansSerif", Font.PLAIN, 30));
		lblNewLabel.setForeground(new Color(240, 248, 255));
		lblNewLabel.setBounds(231, 0, 393, 34);
		getContentPane().add(lblNewLabel);
		searchL.setIcon(new ImageIcon(SalarySheet.class.getResource("/pictureResource/b_search.png")));
		searchL.setBounds(691, 16, 73, 16);
		getContentPane().add(searchL);

		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String id = id_txt.getText();
				
				String name = WorkerName_txt.getText();
				
				String poS=posiotn.getText();

				String gender = null;
				String  date = datePicker.getDate().toString();

				
				if (MaleR.isSelected()) {
					gender = MaleR.getText();

				} else if (fmaleR.isSelected()) {
					gender = fmaleR.getText();

				} else {
					JOptionPane.showMessageDialog(null, "Please Select Gender");
				}
				String present = null;
				if (present_ch.isSelected()) {
					present = present_ch.getText();
				} else if (absent_ch.isSelected()) {
					present = absent_ch.getText();

				} else {
					JOptionPane.showMessageDialog(null, "Select Absent Or Present!");
				}
				
				new WorkerDitails_ctrl(id,name, gender, present,poS ,date );
				WorkerDitailsModel.load();
			}
		});
		btnSave.setBounds(759, 582, 90, 28);
		getContentPane().add(btnSave);

		JButton edit = new JButton("Edit");
		edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int row = SalarySheetTab.getSelectedRow();

				String id = SalarySheetTab.getModel().getValueAt(row, 0).toString();
				String Name = SalarySheetTab.getModel().getValueAt(row, 1).toString();
				String Gender = SalarySheetTab.getModel().getValueAt(row, 2).toString();
				String present = SalarySheetTab.getModel().getValueAt(row, 3).toString();
				// String pos=presentList.getModel().getValueAt(row,
				// 4).toString();
				// String date=presentList.getModel().getValueAt(row,
				// 5).toString();
				EditWorkerDitails editWorkerDitails = new EditWorkerDitails();
				editWorkerDitails.setVisible(true);
				editWorkerDitails.workerIdTxt.setText(id);
				editWorkerDitails.name.setText(Name);
				editWorkerDitails.genderT.setText(Gender);
				editWorkerDitails.preT.setText(present);
				// editWorkerDitails.setTitle(pos);
				// editWorkerDitails.da2.setText(date);

				// new EditWorkerCtrl(id,Name,Gender,present,pos,date);
			}
		});
		edit.setBounds(642, 582, 90, 28);
		getContentPane().add(edit);

		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(SalarySheet.class.getResource("/pictureResource/Foood.jpeg")));
		lblNewLabel_1.setBounds(-111, -469, 1047, 1096);
		getContentPane().add(lblNewLabel_1);
		setLocation(new Point(350, 100));
		setInheritsPopupMenu(true);
		setIgnoreRepaint(true);
		setIconifiable(true);
		setClosable(true);

		setVisible(true);
		setDoubleBuffered(true);

		setFocusTraversalPolicyProvider(true);
		setPreferredSize(new Dimension(0, 0));
		// Dimention Screen
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		// header Design
		header = new JPanel();
		header.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		header.setBackground(Color.BLUE);
		header.setPreferredSize(new Dimension(screen.width, 30));
		// center Design
		center = new JPanel();
		center.setPreferredSize(new Dimension(screen.width + 50, 50));
		// foter design
		footer = new JPanel();
		// footer.setBackground(Color.blue);
		footer.setBackground(new Color(155, 200, 200));
		footer.setPreferredSize(new Dimension(screen.width, 50));

		setSize(877, 658);
		show();
	}

	public static void Main(String[] args) {
		try {

		} catch (Exception e) {
			// TODO: handle exception
		}
		SalarySheet addFood = new SalarySheet();
		addFood.setVisible(true);
		addFood.setDefaultCloseOperation(HIDE_ON_CLOSE);
	}
}
