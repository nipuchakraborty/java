package viwe.sequrity;

    import java.awt.Point;
    import java.awt.event.MouseEvent;
    import java.awt.event.MouseListener;
    import java.awt.event.MouseMotionListener;
    import javax.swing.JFrame;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.border.MatteBorder;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
    public class Main
    {
        static Point mouseDownScreenCoords;
        static Point mouseDownCompCoords;
        public static void main(String[] args)
        {
            final JFrame f = new JFrame("Hello");
            f.getContentPane().setBackground(new Color(0, 0, 0));
            mouseDownScreenCoords = null;
            mouseDownCompCoords = null;
            f.setUndecorated(true);
            f.setVisible(true);
            f.setBounds(500, 100, 750, 600);
            f.getContentPane().setLayout(null);
            
            JPanel panel = new JPanel();
            panel.setBackground(new Color(102, 102, 102));
            panel.setBounds(-18, 0, 370, 626);
            f.getContentPane().add(panel);
            
            JPanel panel_1 = new JPanel();
            panel_1.setBackground(new Color(0, 0, 0));
            panel_1.setBounds(350, 0, 400, 600);
            f.getContentPane().add(panel_1);
            panel_1.setLayout(null);
            
            JButton btnNewButton = new JButton("");
            btnNewButton.setIcon(new ImageIcon("C:\\Users\\Nk Chakraborty\\Desktop\\Icon\\icons8_Delete_20px.png"));
            btnNewButton.setBackground(new Color(0, 255, 0));
            btnNewButton.setBounds(364, 0, 36, 25);
            panel_1.add(btnNewButton);
            
            
            f.addMouseListener(new MouseListener(){
                public void mouseReleased(MouseEvent e) {
                	
                    mouseDownScreenCoords = null;
                    mouseDownCompCoords = null;
                }
                public void mousePressed(MouseEvent e) {
                    mouseDownScreenCoords = e.getLocationOnScreen();
                    mouseDownCompCoords = e.getPoint();
                }
                public void mouseExited(MouseEvent e) {
                }
                public void mouseEntered(MouseEvent e) {
                }
                public void mouseClicked(MouseEvent e) {
                	
                }
            });
            f.addMouseMotionListener(new MouseMotionListener(){
                public void mouseMoved(MouseEvent e) {
                }
                public void mouseDragged(MouseEvent e) {
                    Point currCoords = e.getLocationOnScreen();
                    f.setLocation(mouseDownScreenCoords.x + (currCoords.x - mouseDownScreenCoords.x) - mouseDownCompCoords.x,
                                  mouseDownScreenCoords.y + (currCoords.y - mouseDownScreenCoords.y) - mouseDownCompCoords.y);
                }
            });
        }
    }