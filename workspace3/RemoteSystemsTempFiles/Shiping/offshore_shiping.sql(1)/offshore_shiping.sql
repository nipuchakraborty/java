-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 10, 2013 at 12:39 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `offshore_shiping`
--

-- --------------------------------------------------------

--
-- Table structure for table `agent`
--

CREATE TABLE IF NOT EXISTS `agent` (
  `name` varchar(500) NOT NULL,
  `bill_entry_no` varchar(100) NOT NULL,
  `bill_entry_data` varchar(100) NOT NULL,
  `delivered` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agent`
--


-- --------------------------------------------------------

--
-- Table structure for table `basic_info`
--

CREATE TABLE IF NOT EXISTS `basic_info` (
  `serial` int(50) NOT NULL AUTO_INCREMENT,
  `ship_name` varchar(500) NOT NULL,
  `voy_no` varchar(100) NOT NULL,
  `bd_foreign` varchar(200) NOT NULL,
  `motor_steamer` varchar(500) NOT NULL,
  `name_bd_foreign` varchar(500) NOT NULL,
  PRIMARY KEY (`serial`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `basic_info`
--

INSERT INTO `basic_info` (`serial`, `ship_name`, `voy_no`, `bd_foreign`, `motor_steamer`, `name_bd_foreign`) VALUES
(1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `bl`
--

CREATE TABLE IF NOT EXISTS `bl` (
  `mlo_code` varchar(100) NOT NULL,
  `line_no` varchar(100) NOT NULL,
  `bl_no` varchar(100) NOT NULL,
  `number` int(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `marks_no` varchar(100) NOT NULL,
  `goods_description` varchar(500) NOT NULL,
  `entry_data` date NOT NULL,
  `net_weight` float NOT NULL,
  `gross_weight` float NOT NULL,
  PRIMARY KEY (`mlo_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bl`
--

INSERT INTO `bl` (`mlo_code`, `line_no`, `bl_no`, `number`, `description`, `marks_no`, `goods_description`, `entry_data`, `net_weight`, `gross_weight`) VALUES
('1', '2', '12', 2, '12', '12', '2003', '2013-01-09', 121, 121);

-- --------------------------------------------------------

--
-- Table structure for table `container_details`
--

CREATE TABLE IF NOT EXISTS `container_details` (
  `off_dock` varchar(30) NOT NULL,
  `con_number` varchar(30) NOT NULL,
  `ser_num` varchar(30) NOT NULL,
  `size` float NOT NULL,
  `type` varchar(100) NOT NULL,
  `height` float NOT NULL,
  `weight` float NOT NULL,
  `status` varchar(30) NOT NULL,
  `imco` varchar(30) NOT NULL,
  `un` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `container_details`
--

INSERT INTO `container_details` (`off_dock`, `con_number`, `ser_num`, `size`, `type`, `height`, `weight`, `status`, `imco`, `un`) VALUES
('qw', 'wq', 'qwq', 2.9, 'as', 5.9, 2.8, 'qw', 'qw', 'qw'),
('q', 'w', 'w', 2, 'qw', 34, 4, 'sa', 'a', 's'),
('s', 'a', 'a', 1, 's', 2, 2, 's', 's', 's');

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE IF NOT EXISTS `expense` (
  `bl_id` varchar(30) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cont_id` varchar(30) NOT NULL,
  `head` varchar(30) NOT NULL,
  `purpose` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `amount` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`bl_id`, `id`, `cont_id`, `head`, `purpose`, `date`, `amount`) VALUES
('1', 1, '1', 'Depot', '', '2013-01-09', 1),
('2', 2, '2', 'Depot', 'Transport', '2013-01-09', 1),
('1', 3, '2', 'Depot', 'Transport', '2013-01-09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE IF NOT EXISTS `income` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bl_id` varchar(30) NOT NULL,
  `cont_id` varchar(30) NOT NULL,
  `head` varchar(30) NOT NULL,
  `purpose` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `amount` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `income`
--


-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `USERNAME` varchar(30) NOT NULL,
  `PASSWORD` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`USERNAME`, `PASSWORD`) VALUES
('otsl', 'otsl');
