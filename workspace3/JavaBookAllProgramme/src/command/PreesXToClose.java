package command;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class PreesXToClose {

	private JFrame frmPressXTo;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PreesXToClose window = new PreesXToClose();
					window.frmPressXTo.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PreesXToClose() {
		initialize();
	}

	
	private void initialize() {
		frmPressXTo = new JFrame();
		frmPressXTo.setTitle("Press X To Close");
		frmPressXTo.setBounds(100, 100, 450, 300);
		frmPressXTo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	

}
