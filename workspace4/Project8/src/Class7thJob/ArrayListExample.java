package Class7thJob;

import java.util.ArrayList;

public class ArrayListExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<String> obj = new ArrayList<>();
		obj.add( "Ajeet" );
		obj.add( "Harry" );
		obj.add( "Chaitanya" );
		obj.add( "Steve" );
		obj.add( "Anuj" );
		System.out.println("Current Elements");
		System.out.println(obj);
		obj.add("Helo mr Nipu");
		obj.add("How are you?");
		obj.add("I thinks you are Fine by the grace of god..	");

		System.out.println("That is after Adding element ");
		System.out.println(obj);
		System.out.println("After Removing");
		obj.remove( "Chaitanya" );
		obj.remove( "Harry" );
		System.out.println(obj);
	}

}
