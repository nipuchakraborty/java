package JTCH;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JtCh extends JFrame {

	private JPanel contentPane;

	public String Text="";
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JtCh frame = new JtCh();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JtCh() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 588, 587);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JCheckBox ch1 = new JCheckBox("New check box");
		ch1.setBounds(21, 27, 113, 25);
		panel.add(ch1);
		
		JCheckBox ch2 = new JCheckBox("New check box");
		ch2.setBounds(174, 27, 113, 25);
		panel.add(ch2);
		
		JCheckBox ch3 = new JCheckBox("New check box");
		ch3.setBounds(351, 27, 113, 25);
		panel.add(ch3);
		
		JCheckBox ch4 = new JCheckBox("New check box");
		ch4.setBounds(21, 103, 113, 25);
		panel.add(ch4);
		
		JCheckBox ch5 = new JCheckBox("New check box");
		ch5.setBounds(174, 103, 113, 25);
		panel.add(ch5);
		
		JCheckBox ch6 = new JCheckBox("New check box");
		ch6.setBounds(351, 114, 113, 25);
		panel.add(ch6);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(21, 243, 527, 260);
		panel.add(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JTextArea textArea = new JTextArea();
		panel_1.add(textArea, BorderLayout.CENTER);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if (ch1.isSelected()) {
					String tString=ch1.getText();
					textArea.setText(Text+=tString);
					
				}
				if (ch2.isSelected()) {
					String gString=ch2.getText();
					textArea.setText(Text+="\n"+gString);
					
				}
			}
		});
		btnNewButton.setBounds(451, 505, 97, 25);
		panel.add(btnNewButton);
	}
}
